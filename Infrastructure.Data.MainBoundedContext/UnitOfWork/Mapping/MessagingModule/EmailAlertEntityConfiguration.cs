﻿using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.MessagingModule
{
    class EmailAlertEntityConfiguration : EntityTypeConfiguration<EmailAlert>
    {
        public EmailAlertEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(t => t.SequentialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(x => x.MailMessage.Subject).HasMaxLength(50);

            Property(x => x.MailMessage.CC).HasMaxLength(256);

            Property(x => x.MailMessage.To).HasMaxLength(256);

            Property(x => x.MailMessage.From).HasMaxLength(50);

            Property(x => x.MailMessage.AttachmentFilePath).HasMaxLength(256);

            Property(x => x.CreatedBy).HasMaxLength(75);

            Property(x => x.CreatedBy).HasMaxLength(10000);

            Property(x => x.ModifiedBy).HasMaxLength(75);

            Property(t => t.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_EmailAlert_CreatedDate")));

            ToTable($"{DefaultSettings.Instance.TablePrefix}EmailAlerts");
        }
    }
}
