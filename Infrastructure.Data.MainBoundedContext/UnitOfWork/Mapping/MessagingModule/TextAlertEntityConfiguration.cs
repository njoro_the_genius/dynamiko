﻿using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Domain.MainBoundedContext.MessagingModule.Aggregates.TextAlertAgg;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.MessagingModule
{
    public class TextAlertEntityConfiguration : EntityTypeConfiguration<TextAlert>
    {
        TextAlertEntityConfiguration()
        {
            HasKey(a => a.Id);

            Property(t => t.SequentialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(a => a.TextMessage.Recipient).IsRequired().HasMaxLength(20);

            Property(a => a.TextMessage.Body).HasMaxLength(256);

            Property(a => a.TextMessage.Reference).HasMaxLength(50);

            Property(x => x.CreatedBy).HasMaxLength(75);

            Property(x => x.ModifiedBy).HasMaxLength(75);

            Property(a => a.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_TextAlert_CreatedDate")));

            ToTable($"{DefaultSettings.Instance.TablePrefix}TextAlerts");
        }
    }
}
