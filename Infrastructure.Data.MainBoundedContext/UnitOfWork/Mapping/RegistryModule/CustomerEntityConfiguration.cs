﻿using Domain.MainBoundedContext.RegistryModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.RegistryModule
{
    class CustomerEntityConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(t => t.SequentialId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(x => x.CreatedBy).HasMaxLength(75);

            Property(x => x.ModifiedBy).HasMaxLength(75);

            Property(x => x.Name).HasMaxLength(256);

            Property(x => x.Address.MobileLine).HasMaxLength(15);

            Property(x => x.Address.Email).HasMaxLength(50);

            Property(x => x.Address.Postal).HasMaxLength(25);

            Property(x => x.Address.PostalCode).HasMaxLength(15);

            Property(x => x.Address.Physical).HasMaxLength(256);

            Property(x => x.Address.LandLine).HasMaxLength(15);

            Property(x => x.Address.Street).HasMaxLength(256);

            Property(x => x.Address.City).HasMaxLength(256);

            ToTable($"{DefaultSettings.Instance.TablePrefix}Customers");
        }
    }
}
