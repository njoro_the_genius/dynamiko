﻿using Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.RegistryModule
{
    public class EmployeeEntityConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeEntityConfiguration()
        {
            HasKey(x => x.Id);

            Property(t => t.SequentialId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(a => a.UserId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_UserIdCustomerId", 1) { IsUnique = true, Order = 1 }));

            Property(a => a.CustomerId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_UserIdCustomerId", 2) { IsUnique = true, Order = 2 }));

            Property(x => x.CreatedBy).HasMaxLength(200);

            Property(x => x.ModifiedBy).HasMaxLength(200);


            Property(x => x.UserName).HasMaxLength(20);
            Property(x => x.FirstName).HasMaxLength(200); ;

            Property(x => x.OtherNames).HasMaxLength(200); ;

            Property(x => x.Email).HasMaxLength(50);

            Property(x => x.PhoneNumber).HasMaxLength(20);

            Property(x => x.DepartmentId);

            Property(x => x.BankAccount).HasMaxLength(15);

            Property(x => x.KRAPin).HasMaxLength(20);

            Property(x => x.NHIFNumber).HasMaxLength(20);

            Property(x => x.NSSFNumber).HasMaxLength(20);

            Property(x => x.GrossSalary);

            Property(x => x.DeductNSSFContribution);

            Property(x => x.DeductNHIFContribution);

            Property(x => x.BankCode).HasMaxLength(10); ;

            Property(x => x.BranchCode).HasMaxLength(10);

            Property(x => x.AccountName).HasMaxLength(200);

            Property(x => x.JobNumber).HasMaxLength(15);

            Property(x => x.DateOfEmployment);

            Property(x => x.JobTitle).HasMaxLength(50) ;

            ToTable($"{DefaultSettings.Instance.TablePrefix}Employees");
        }
    }
}
