﻿using Domain.MainBoundedContext.TransactionModule.Aggregates.OrderAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.TransactionModule
{
    class OrderLineItemEntityConfiguration : EntityTypeConfiguration<OrderLineItem>
    {
        public OrderLineItemEntityConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.SequentialId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(c => c.CreatedBy).HasMaxLength(256);

            Property(x => x.ModifiedBy).HasMaxLength(256);

            Property(t => t.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_Order_CreatedDate")));

            ToTable($"{DefaultSettings.Instance.TablePrefix}OrderLineItems");
        }
    }
}
