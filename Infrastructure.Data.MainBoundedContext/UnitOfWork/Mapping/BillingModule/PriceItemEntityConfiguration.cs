﻿using Domain.MainBoundedContext.BillingModule.Aggregates.PriceAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.BillingModule
{
    class PriceItemEntityConfiguration : EntityTypeConfiguration<PriceItem>
    {
        public PriceItemEntityConfiguration()
        {
            HasKey(c => c.Id);

            Property(c => c.SequentialId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(c => c.CreatedBy).HasMaxLength(256);

            Property(x => x.ModifiedBy).HasMaxLength(256);

            Property(t => t.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_PriceItem_CreatedDate")));

            ToTable($"{DefaultSettings.Instance.TablePrefix}PriceItems");
        }
    }
}
