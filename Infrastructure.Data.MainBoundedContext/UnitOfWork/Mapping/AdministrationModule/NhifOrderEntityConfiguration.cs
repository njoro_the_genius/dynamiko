﻿using Domain.MainBoundedContext.AdministrationModule.Aggregates.NHIFAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.AdministrationModule
{
    class NhifOrderEntityConfiguration: EntityTypeConfiguration<NhifOrder>
    {
        public NhifOrderEntityConfiguration()
        {
            HasKey(c => c.Id);
            Property(c => c.SequentialId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));
            Property(c => c.CreatedBy).HasMaxLength(256);
            Property(x => x.ModifiedBy).HasMaxLength(256);
            Property(t => t.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_NhifOrder_CreatedDate")));
            ToTable($"{DefaultSettings.Instance.TablePrefix}NhifOrders");
        }
    }
}
