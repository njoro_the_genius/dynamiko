﻿using Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.AdministrationModule
{
    class ModuleNavigationItemEntityConfiguration : EntityTypeConfiguration<ModuleNavigationItem>
    {
        public ModuleNavigationItemEntityConfiguration()
        {
            HasKey(a => a.Id);

            Property(t => t.SequentialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(a => a.ActionName).HasMaxLength(100).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("IX_ModuleNavigationItem_ActionName")));

            Property(a => a.ModifiedBy).HasMaxLength(75);

            Property(a => a.ControllerName).HasMaxLength(100).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("IX_ModuleNavigationItem_ControllerName")));

            Property(a => a.CreatedBy).HasMaxLength(75);

            Property(a => a.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName,
                new IndexAnnotation(new IndexAttribute("IX_ModuleNavigationItem_CreatedDate")));

            ToTable($"{DefaultSettings.Instance.TablePrefix}ModuleNavigationItems");
        }
    }
}
