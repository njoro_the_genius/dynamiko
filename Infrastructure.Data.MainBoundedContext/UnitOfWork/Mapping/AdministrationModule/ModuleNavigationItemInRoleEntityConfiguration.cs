﻿using Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemInRoleAgg;
using Infrastructure.Crosscutting.Framework.Utils;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.AdministrationModule
{
    public class ModuleNavigationItemInRoleEntityConfiguration : EntityTypeConfiguration<ModuleNavigationItemInRole>
    {
        public ModuleNavigationItemInRoleEntityConfiguration()
        {
            HasKey(a => a.Id);

            Property(t => t.SequentialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(a => a.CreatedBy).HasMaxLength(75);

            Property(a => a.ModifiedBy).HasMaxLength(75);

            Property(a => a.RoleId).HasMaxLength(50).IsRequired().HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ModuleNavigationItemInRole_RoleName")));

            Property(a => a.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_ModuleNavigationItemInRole_CreatedDate")));

            ToTable($"{DefaultSettings.Instance.TablePrefix}ModuleNavigationItemInRoles");
        }
    }
}
