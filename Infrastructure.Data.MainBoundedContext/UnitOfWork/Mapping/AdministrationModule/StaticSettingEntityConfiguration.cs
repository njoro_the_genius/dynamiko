﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.SettingAgg;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Infrastructure.Data.MainBoundedContext.UnitOfWork.Mapping.AdministrationModule
{
    class StaticSettingEntityConfiguration : EntityTypeConfiguration<StaticSetting>
    {
        public StaticSettingEntityConfiguration()
        {
            HasKey(a => a.Id);

            Property(a => a.ClusteredId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute() { IsClustered = true, IsUnique = true }));

            Property(a => a.Key).HasMaxLength(256);

            Property(a => a.Value).HasMaxLength(100);

            Property(a => a.CreatedBy).HasMaxLength(75).IsRequired();

            Property(a => a.CreatedDate).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_StaticSetting_CreatedDate"))).IsRequired();

            ToTable($"{DefaultSettings.Instance.TablePrefix}StaticSettings");
        }
    }
}
