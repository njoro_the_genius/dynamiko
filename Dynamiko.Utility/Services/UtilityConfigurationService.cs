﻿using Dynamiko.Utility.Configuration;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamiko.Utility.Services
{
    public class UtilityConfigurationService : IUtilityConfigurationService
    {
        public ServiceHeader GetServiceHeader()
        {
            ServiceHeader serviceHeader = null;

            var apiAppConfigSection = (UtilityApplicationConfigSection)ConfigurationManager.GetSection("utilityApplicationConfiguration");

            foreach (var settingsItem in apiAppConfigSection.UtilityApplicationSettingsItems)
            {
                var utilityApiSettingsElement = (UtilityApplicationSettingsElement)settingsItem;

                serviceHeader = new ServiceHeader { ApplicationDomainName = utilityApiSettingsElement.UniqueId };

                break;
            }

            return serviceHeader;
        }
    }
}
