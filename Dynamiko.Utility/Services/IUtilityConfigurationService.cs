﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamiko.Utility.Services
{
    public interface IUtilityConfigurationService
    {
        ServiceHeader GetServiceHeader();
    }
}
