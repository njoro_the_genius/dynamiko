﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamiko.Utility.Configuration
{
    public class UtilityApplicationSettingsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new UtilityApplicationSettingsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UtilityApplicationSettingsElement)(element)).UniqueId;
        }

        public UtilityApplicationSettingsElement this[int index] => (UtilityApplicationSettingsElement)BaseGet(index);

        [ConfigurationProperty("name", IsRequired = false)]
        public string Name => (string)base["name"];
    }
}
