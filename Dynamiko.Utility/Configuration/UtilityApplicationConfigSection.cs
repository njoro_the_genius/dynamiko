﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamiko.Utility.Configuration
{
    public class UtilityApplicationConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("utilityApplicationSettings")]
        public UtilityApplicationSettingsCollection UtilityApplicationSettingsItems => ((UtilityApplicationSettingsCollection)(base["utilityApplicationSettings"]));
    }
}
