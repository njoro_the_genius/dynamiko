﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamiko.Utility.Configuration
{
    public class UtilityApplicationSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("uniqueId", IsKey = true, IsRequired = true)]
        public string UniqueId
        {
            get => ((string)(base["uniqueId"]));
            set => base["uniqueId"] = value;
        }
    }
}
