﻿using Application.MainBoundedContext.DTO.AdministrationModule;
using Dynamiko.Utility.Services;
using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Logging;
using MvcApplication.Attributes;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Dynamiko.Utility
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigureFactories();

            ILogger logger = new SerilogLogger();

            IChannelService channelService = new ChannelService(logger);

            IUtilityConfigurationService utilityConfigurationService = new UtilityConfigurationService();

            var serviceHeader = utilityConfigurationService.GetServiceHeader();

            try
            {
                async void worker(string[] input)
                {
                    var result = default(bool);

                    result = await channelService.ConfigureAspNetIdentityDatabaseAsync(serviceHeader);

                    Console.WriteLine("ConfigureAspNetIdentityDatabaseAsync>{0}", result);

                    result = await channelService.ConfigureApplicationDatabaseAsync(serviceHeader);

                    Console.WriteLine("ConfigureApplicationDatabaseAsync>{0}", result);

                  

                    var roles = await channelService.GetRolesAsync(serviceHeader);

                    result = await channelService.RemoveAllRightsAsync(roles.ToArray(), serviceHeader);

                    Console.WriteLine("RemoveAccessRightsAsync>{0}", result);

                    var navigationItems = GetAvailableControllersAndActions();

                    result = await channelService.AddModuleNavigationItemAsync(navigationItems, serviceHeader);

                    Console.WriteLine("AddModuleNavigationItemAsync>{0}", result);

                    result = await channelService.SeedSystemAccessRightsAsync(serviceHeader);

                    Console.WriteLine("SeedSystemAccessRightsAsync>{0}", result);

                    Console.WriteLine("DONE!");

                    Console.WriteLine("Press any key to continue...");

                    Console.ReadKey();

                    Environment.Exit(0);

                }

                worker(args);

                Thread.Sleep(Timeout.Infinite);
            }

            catch (Exception ex)
            {
                Console.WriteLine("Exception!");

                LoggerFactory.CreateLog().LogError("Urion.Utility...", ex);
            }
        }

        private static void ConfigureFactories()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;

                UriBuilder uri = new UriBuilder(codeBase);

                string path = Uri.UnescapeDataString(uri.Path);

                return Path.GetDirectoryName(path);
            }
        }
        public static List<ModuleNavigationItemDTO> GetAvailableControllersAndActions()
        {
            Assembly assembly = Assembly.LoadFrom($@"{AssemblyDirectory}\MvcApplication.dll");

            var result = new List<ModuleNavigationItemDTO>();

            if (assembly.FullName.StartsWith("MvcApplication", StringComparison.OrdinalIgnoreCase))
            {
                //var test = assembly.GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type));
                var enumTypes = assembly.GetTypes()
                    .Where(type => typeof(Controller).IsAssignableFrom(type))
                    .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any() && m.IsPublic && !m.IsDefined(typeof(NonActionAttribute)))
                    .Select(x => new
                    {
                        Controller = x.DeclaringType.Name,
                        Action = x.Name,
                        ReturnType = x.ReturnType.Name,
                        Attributes = string.Join(",", x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", "")))
                    })
                    .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();

                foreach (var type in enumTypes)
                {
                    var attributes = type.Attributes.Split(',').ToList();

                    if (!attributes.Contains(nameof(IgnoreAction)))
                    {
                        result.Add(new ModuleNavigationItemDTO
                        {
                            ControllerName = type.Controller,
                            ActionName = type.Action,
                        });
                    }
                }
            }

            return result.DistinctBy(p => new { p.ControllerName, p.ActionName }).ToList();
        }
    }
}
