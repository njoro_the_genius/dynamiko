﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.MessagingModule
{
    public class EmailAlertDTO
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        [Display(Name = "CustomerId")]
        public Guid CustomerId { get; set; }

        [DataMember]
        [Required]
        public string MailMessageFrom { get; set; }

        [DataMember]
        public string MailMessageTo { get; set; }

        [DataMember]
        public string MailMessageCC { get; set; }

        [DataMember]
        public string MailMessageSubject { get; set; }

        [DataMember]
        public string MailMessageBody { get; set; }

        [DataMember]
        public string MaskedMailMessageBody { get; set; }

        [DataMember]
        public bool MailMessageIsBodyHtml { get; set; }

        [DataMember]
        public byte MailMessageDLRStatus { get; set; }

        [DataMember]
        public string MailMessageDLRStatusDescription => EnumHelper.GetDescription((DLRStatus)MailMessageDLRStatus);

        [DataMember]
        public byte MailMessageOrigin { get; set; }

        [DataMember]
        public string MailMessageOriginDescription => EnumHelper.GetDescription((MessageOrigin)MailMessageOrigin);

        [DataMember]
        public byte MailMessagePriority { get; set; }

        [DataMember]
        public string MailMessagePriorityDescription => EnumHelper.GetDescription((QueuePriority)MailMessagePriority);

        [DataMember]
        public byte MailMessageSendRetry { get; set; }

        [DataMember]
        public bool MailMessageSecurityCritical { get; set; }

        [DataMember]
        public bool MailMessageHasAttachment { get; set; }

        [DataMember]
        public string MailMessageAttachmentFilePath { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string Commands { get; set; }
    }
}
