﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MvcApplication.Configuration
{
    public class WebApplicationSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("uniqueId", IsKey = true, IsRequired = true)]
        public string UniqueId
        {
            get => ((string)(base["uniqueId"]));
            set => base["uniqueId"] = value;
        }

        [ConfigurationProperty("emailTemplatesPath", IsKey = false, IsRequired = true)]
        public string EmailTemplatesPath
        {
            get => ((string)(base["emailTemplatesPath"]));
            set => base["emailTemplatesPath"] = value;
        }
    }
}