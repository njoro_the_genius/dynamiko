﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MvcApplication.Configuration
{
    public class WebApplicationSettingsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new WebApplicationSettingsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((WebApplicationSettingsElement)(element)).UniqueId;
        }

        public WebApplicationSettingsElement this[int index] => (WebApplicationSettingsElement)BaseGet(index);

        [ConfigurationProperty("name", IsRequired = false)]
        public string Name => (string)base["name"];
    }
}