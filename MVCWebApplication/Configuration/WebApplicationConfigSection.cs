﻿using System.Configuration;

namespace MvcApplication.Configuration
{
    public class WebApplicationConfigSection: ConfigurationSection
    {
        [ConfigurationProperty("webApplicationSettings")]
        public WebApplicationSettingsCollection WebApplicationSettingsItems => ((WebApplicationSettingsCollection)(base["webApplicationSettings"]));
    }
}