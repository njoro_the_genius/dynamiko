using Application.MainBoundedContext.Services;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Logging;
using Microsoft.Owin.Security;
using MvcApplication.EventHandler;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Web;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace MvcApplication
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<ILogger, SerilogLogger>(new ContainerControlledLifetimeManager());

            container.RegisterType<IChannelService, ChannelService>(new ContainerControlledLifetimeManager());


            container.RegisterType<ApplicationDbContext>(new InjectionConstructor("AuthStore"));

            container.RegisterType<IAuthenticationManager>(new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));

            container.RegisterType<ApplicationUserStore>(new TransientLifetimeManager(), new InjectionConstructor(typeof(ApplicationDbContext)));

            container.RegisterType<ApplicationUserManager>(new TransientLifetimeManager(), new InjectionConstructor(typeof(ApplicationUserStore)));

            container.RegisterType<ApplicationRoleStore>(new TransientLifetimeManager(), new InjectionConstructor(typeof(ApplicationDbContext)));

            container.RegisterType<ApplicationRoleManager>(new TransientLifetimeManager(), new InjectionConstructor(typeof(ApplicationRoleStore)));

            container.RegisterType<IWebConfigurationService, WebConfigurationService>();

            container.RegisterType<ISmtpService, SmtpService>();

            container.RegisterType<IEmailHandler, EmailHandler>();

            container.RegisterType<IPayRollHandler, PayRollHandler>();
        }
    }
}