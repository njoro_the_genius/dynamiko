﻿using System.Threading.Tasks;

namespace MvcApplication.EventHandler
{
    public interface IEmailHandler
    {
        Task Handle();
    }
}
