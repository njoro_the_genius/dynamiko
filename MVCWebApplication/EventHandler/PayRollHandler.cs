﻿using System;
using System.Web.Mvc;
using System.Configuration;
using System.Threading.Tasks;

using MvcApplication.Services;
using EmailAlertDispatcher.Configuration;
using Presentation.Infrastructure.Services;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.EventHandler.Configuration;
using Infrastructure.Crosscutting.Framework.Logging;

namespace MvcApplication.EventHandler
{
    public class PayRollHandler : IPayRollHandler
    {
        private readonly ILogger _logger;
        private readonly ServiceHeader _serviceHeader;
        private readonly IChannelService _channelService;
        private readonly IWebConfigurationService _webConfigurationService;
        private readonly EmailAlertDispatcherConfigurationSection _emailAlertDispatcherConfigSection;

        public PayRollHandler()
        {
            _webConfigurationService = DependencyResolver.Current.GetService<IWebConfigurationService>();

            _serviceHeader = _webConfigurationService.GetServiceHeader();

            _emailAlertDispatcherConfigSection = new EmailAlertDispatcherConfigurationSection();

            _logger = DependencyResolver.Current.GetService<ILogger>();

            _channelService = DependencyResolver.Current.GetService<IChannelService>();


        }

        public async Task Handle()
        {
            try
            {
                var gatewayDispatcherConfigurationSection = (PayRollConfigurationSection)ConfigurationManager.GetSection("payRollConfiguration");

                if (gatewayDispatcherConfigurationSection == null) return;

                foreach (var settingsItem in gatewayDispatcherConfigurationSection.GatewayDispatcherSettingsCollection)
                {
                    var settingElement = (PayRollSettingsElement)settingsItem;

                    if (settingElement != null && settingElement.Enabled == 1)
                    {
                        var serviceHeader = new ServiceHeader { ApplicationDomainName = settingElement.UniqueId };

                        var unProcessedPayRoll = await _channelService.FindUnProcessedPayRollFilterInPageAsync(settingElement.QueuePageIndex, settingElement.QueuePageSize, null, null, true, serviceHeader);

                        if (unProcessedPayRoll != null)
                        {
                            foreach (var payRoll in unProcessedPayRoll?.PageCollection)
                            {
                                await _channelService.ProcessPayRoll(payRoll.Id, serviceHeader);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {

                _logger.LogError("Urion.PayRoll.QuartzJobs.QueueingJob.Execute", exception.Message);
            }
        }
    }
}