﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcApplication.EventHandler.Configuration
{
    public class PayRollConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("payRollSettings")]
        public PayRollSettingsCollection GatewayDispatcherSettingsCollection => (PayRollSettingsCollection)base["payRollSettings"];
    }
}
