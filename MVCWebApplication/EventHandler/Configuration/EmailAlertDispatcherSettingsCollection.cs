﻿using System.Configuration;

namespace EmailAlertDispatcher.Configuration
{
    public class EmailAlertDispatcherSettingsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new EmailAlertDispatcherSettingsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((EmailAlertDispatcherSettingsElement)(element)).UniqueId;
        }

        public EmailAlertDispatcherSettingsElement this[int index] => (EmailAlertDispatcherSettingsElement)BaseGet(index);

        [ConfigurationProperty("name", IsKey = false)]
        public string Name => (string)(base["name"]);

        [ConfigurationProperty("queuePath", IsRequired = true)]
        public string QueuePath => (string)base["queuePath"];

        [ConfigurationProperty("queueingJobCronExpression", IsRequired = true)]
        public string QueueingJobCronExpression => (string)base["queueingJobCronExpression"];

        [ConfigurationProperty("queueReceivers", IsRequired = true)]
        public int QueueReceivers => (int)base["queueReceivers"];
    }
}
