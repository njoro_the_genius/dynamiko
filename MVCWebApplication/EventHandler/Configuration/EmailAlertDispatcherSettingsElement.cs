﻿using System.Configuration;

namespace EmailAlertDispatcher.Configuration
{
    public class EmailAlertDispatcherSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("uniqueId", IsKey = true, IsRequired = true)]
        public string UniqueId
        {
            get { return (string)(base["uniqueId"]); }
            set { base["uniqueId"] = value; }
        }

        [ConfigurationProperty("queuePageIndex", DefaultValue = "0", IsKey = false, IsRequired = true)]
        public int QueuePageIndex
        {
            get { return (int)(base["queuePageIndex"]); }
            set { base["queuePageIndex"] = value; }
        }

        [ConfigurationProperty("queuePageSize", DefaultValue = "100", IsKey = false, IsRequired = true)]
        public int QueuePageSize
        {
            get { return (int)(base["queuePageSize"]); }
            set { base["queuePageSize"] = value; }
        }

        [ConfigurationProperty("mailMessageFrom", IsKey = false, IsRequired = true)]
        public string MailMessageFrom
        {
            get { return (string)(base["mailMessageFrom"]); }
            set { base["mailMessageFrom"] = value; }
        }

        [ConfigurationProperty("smtpHost", IsKey = false, IsRequired = true)]
        public string SmtpHost
        {
            get { return (string)(base["smtpHost"]); }
            set { base["SmtpHost"] = value; }
        }

        [ConfigurationProperty("smtpPort", IsKey = false, IsRequired = true)]
        public int SmtpPort
        {
            get { return (int)(base["smtpPort"]); }
            set { base["smtpPort"] = value; }
        }
        [ConfigurationProperty("smtpEnableSsl", IsKey = false, IsRequired = true)]
        public int SmtpEnableSsl
        {
            get { return (int)(base["smtpEnableSsl"]); }
            set { base["smtpEnableSsl"] = value; }
        }
        [ConfigurationProperty("smtpUsername", IsKey = false, IsRequired = true)]
        public string SmtpUsername
        {
            get { return (string)(base["smtpUsername"]); }
            set { base["smtpUsername"] = value; }
        }

        [ConfigurationProperty("smtpPassword", IsKey = false, IsRequired = true)]
        public string SmtpPassword
        {
            get { return (string)(base["smtpPassword"]); }
            set { base["smtpPassword"] = value; }
        }
        [ConfigurationProperty("timeToBeReceived", IsKey = false, IsRequired = true)]
        public int TimeToBeReceived
        {
            get { return (int)(base["timeToBeReceived"]); }
            set { base["timeToBeReceived"] = value; }
        }
        [ConfigurationProperty("enabled", IsKey = false, IsRequired = true)]
        public int Enabled
        {
            get { return (int)(base["enabled"]); }
            set { base["enabled"] = value; }
        }
    }
}
