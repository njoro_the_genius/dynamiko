﻿using Application.MainBoundedContext.Services;
using EmailAlertDispatcher.Configuration;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.EventHandler
{
    public class EmailHandler : IEmailHandler
    {
        private readonly IChannelService _channelService;
        private readonly ServiceHeader _serviceHeader;
        private readonly IWebConfigurationService _webConfigurationService;
        private readonly EmailAlertDispatcherConfigurationSection _emailAlertDispatcherConfigSection;
        private readonly ISmtpService _smtpService;

        public EmailHandler()
        {
            _emailAlertDispatcherConfigSection = new EmailAlertDispatcherConfigurationSection();

            _channelService = DependencyResolver.Current.GetService<IChannelService>();

            _webConfigurationService = DependencyResolver.Current.GetService<IWebConfigurationService>();

            _serviceHeader = _webConfigurationService.GetServiceHeader();

            _smtpService = DependencyResolver.Current.GetService<ISmtpService>();
        }

        public async Task Handle()
        {
            try
            {

                var emailDispatcherConfigSection = (EmailAlertDispatcherConfigurationSection)ConfigurationManager.GetSection("emailAlertDispatcherConfiguration");

                if (emailDispatcherConfigSection != null)
                {
                    foreach (var settingsItem in emailDispatcherConfigSection.EmailAlertDispatcherSettingItems)
                    {
                        var emailDispatcherSettingsElement = (EmailAlertDispatcherSettingsElement)settingsItem;

                        if (emailDispatcherSettingsElement != null && emailDispatcherSettingsElement.Enabled == 1)
                        {

                            #region Queable Email Message

                            var emailMessageWithStatusPending = await _channelService.FindEmailAlertsWithDLRStatusAsync(null, (int)DLRStatus.Pending, emailDispatcherSettingsElement.QueuePageIndex, emailDispatcherSettingsElement.QueuePageSize, _serviceHeader);

                            if (emailMessageWithStatusPending != null && emailMessageWithStatusPending.PageCollection.Any())
                            {
                                foreach (var emailMessageDTO in emailMessageWithStatusPending.PageCollection)
                                {
                                    if (emailMessageDTO.MailMessageDLRStatus != (byte)DLRStatus.Delivered)
                                    {
                                        var fromAddress = new MailAddress(emailDispatcherSettingsElement.MailMessageFrom, "");

                                        var toAddress = new MailAddress(emailMessageDTO.MailMessageTo, "");

                                        string fromPassword = emailDispatcherSettingsElement.SmtpPassword;

                                        string subject = emailMessageDTO.MailMessageSubject;

                                        string body = emailMessageDTO.MailMessageBody;

                                        using (var message = new MailMessage(fromAddress, toAddress)
                                        {
                                            Subject = subject,

                                            Body = body,

                                            IsBodyHtml = emailMessageDTO.MailMessageIsBodyHtml

                                        })
                                        {
                                            if (emailMessageDTO.Id != null && emailMessageDTO.Id != Guid.Empty)
                                            {
                                                _smtpService.SendEmail(emailDispatcherSettingsElement.SmtpHost, emailDispatcherSettingsElement.SmtpPort, emailDispatcherSettingsElement.SmtpEnableSsl == 1, emailDispatcherSettingsElement.SmtpUsername, emailDispatcherSettingsElement.SmtpPassword, message);

                                                emailMessageDTO.MailMessageDLRStatus = (byte)DLRStatus.Delivered;

                                                await _channelService.UpdateEmailAlertAsync(emailMessageDTO, _serviceHeader);
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}