﻿using Hangfire;
using System;

namespace MvcApplication.EventHandler
{
    public class HangfireJobScheduler
    {
        public static void ScheduleRecurringJobs()
        {
            //Uses NCrontab Format:sec(0-59),min(0-59),hour(0-23),day of month(1-31), month(1-12), day of week(0-6, 0 = sunday)
            //REF https://codeburst.io/schedule-background-jobs-using-hangfire-in-net-core-2d98eb64b196

            RecurringJob.RemoveIfExists(nameof(EmailHandler));
            RecurringJob.AddOrUpdate<EmailHandler>(nameof(EmailHandler), job => job.Handle(), "*/10 * * * * *", timeZone: TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time"));

            RecurringJob.RemoveIfExists(nameof(PayRollHandler));
            RecurringJob.AddOrUpdate<PayRollHandler>(nameof(PayRollHandler), job => job.Handle(), "*/59 * * * * *", timeZone: TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time"));
        }
    }
}