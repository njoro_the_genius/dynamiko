﻿using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Helpers
{
    public static class IdentityExtensions
    {
        public static string GetUserFullName(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("FullName");
            // Test for null to avoid issues during local testing
            return (claim != null) ? claim.Value : string.Empty;
        }

        public static bool IsUserInRole(this IIdentity identity, string[] roles)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var userRole = GetUserRoleName(identity);

                return userRole.In(roles);
            }

            return false;
        }

        public static bool IsInternalRole(this IIdentity identity)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && identity.GetUserId() != null)
            {
                UserManager<ApplicationUser> userManager = DependencyResolver.Current.GetService<ApplicationUserManager>();

                ApplicationRoleManager _roleManager = DependencyResolver.Current.GetService<ApplicationRoleManager>();

                var user = userManager.FindById(identity.GetUserId());

                if (user != null)
                {
                    var roles = userManager.GetRoles(user.Id);

                    if (roles != null)
                    {
                        var roleName = roles.FirstOrDefault();

                        var role = _roleManager.FindByName(roleName);

                        return role.RoleType == (byte)RoleType.InternalRole;
                    }
                }
            }

            return false;
        }

        public static bool IsExternalRole(this IIdentity identity)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && identity.GetUserId() != null)
            {
                UserManager<ApplicationUser> userManager = DependencyResolver.Current.GetService<ApplicationUserManager>();

                ApplicationRoleManager _roleManager = DependencyResolver.Current.GetService<ApplicationRoleManager>();

                var user = userManager.FindById(identity.GetUserId());

                if (user != null)
                {
                    var roles = userManager.GetRoles(user.Id);

                    if (roles != null)
                    {
                        var roleName = roles.FirstOrDefault();

                        var role = _roleManager.FindByName(roleName);

                        return role.RoleType == (byte)RoleType.ExternalRole;
                    }
                }
            }

            return false;
        }

        public static bool IsCurrentUserAdministrator()
        {
            var currentUser = HttpContext.Current.User.Identity.Name;

            UserManager<ApplicationUser> userManager = DependencyResolver.Current.GetService<ApplicationUserManager>();

            if (currentUser == null)
                return false;

            var user = userManager.FindByName(currentUser);

            return user != null && userManager.IsInRole(user.Id, "Administrator");
        }

        public static string GetUserRoleName(this IIdentity identity)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated && identity.GetUserId() != null)
            {
                UserManager<ApplicationUser> userManager = DependencyResolver.Current.GetService<ApplicationUserManager>();

                var user = userManager.FindById(identity.GetUserId());

                if (user != null)
                {
                    var roles = userManager.GetRoles(user.Id);

                    if (roles != null)
                    {
                        return roles.FirstOrDefault();
                    }
                }
            }

            return string.Empty;
        }
    }
}