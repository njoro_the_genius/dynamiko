﻿using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Configuration;
using System.Configuration;
using System.IO;

namespace MvcApplication.Services
{
    public class WebConfigurationService : IWebConfigurationService
    {
        public ServiceHeader GetServiceHeader()
        {
            ServiceHeader serviceHeader = null;

            var apiAppConfigSection = (WebApplicationConfigSection)ConfigurationManager.GetSection("webApplicationConfiguration");

            foreach (var settingsItem in apiAppConfigSection.WebApplicationSettingsItems)
            {
                var webApiSettingsElement = (WebApplicationSettingsElement)settingsItem;

                serviceHeader = new ServiceHeader { ApplicationDomainName = webApiSettingsElement.UniqueId, Origin = (byte)ServiceOrigin.WebMVC };

                break;
            }

            return serviceHeader;
        }

        public string GetEmailTemplatePath(string template)
        {
            var apiAppConfigSection = (WebApplicationConfigSection)ConfigurationManager.GetSection("webApplicationConfiguration");

            var emailTemplatePath = string.Empty;

            foreach (var settingsItem in apiAppConfigSection.WebApplicationSettingsItems)
            {
                var webApiSettingsElement = (WebApplicationSettingsElement)settingsItem;

                emailTemplatePath = Path.Combine(webApiSettingsElement.EmailTemplatesPath, template);
            }

            return emailTemplatePath;
        }
    }
}