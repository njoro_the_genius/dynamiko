﻿using Infrastructure.Crosscutting.Framework.Utils;

namespace MvcApplication.Services
{
    public interface IWebConfigurationService
    {
        ServiceHeader GetServiceHeader();

        string GetEmailTemplatePath(string template);
    }
}
