﻿using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Owin;
using MvcApplication.EventHandler;
using Owin;
using System;
using System.Collections.Generic;

[assembly: OwinStartupAttribute(typeof(MvcApplication.Startup))]
namespace MvcApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            //app.UseHangfireAspNet(GetHangfireServers);

            //HangfireJobScheduler.ScheduleRecurringJobs();

            //app.UseHangfireDashboard();
           
        }

       
    }
}