﻿using System.Web.Mvc;

namespace MvcApplication.Attributes
{
    public class IgnoreAction : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }
    }
}