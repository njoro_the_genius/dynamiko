﻿using DistributedServices.MainBoundedContext.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Configuration;
using System.Security.Principal;
using System.Web.Mvc;

namespace MvcApplication.Attributes
{
    public class ForcePasswordChangeAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException(nameof(filterContext));

            IPrincipal user = filterContext.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                UserManager<ApplicationUser> userManager = new ApplicationUserManager(new ApplicationUserStore(DependencyResolver.Current.GetService<ApplicationDbContext>()));

                var applicationUser = userManager.FindById(user.Identity.GetUserId());

                //has the user ever changed their password ?
                if ((applicationUser != null && applicationUser.LastPasswordChangedDate == applicationUser.CreatedDate) || applicationUser.LastPasswordChangedDate?.AddDays(int.Parse(ConfigurationManager.AppSettings["PasswordExpiryDays"])) < DateTime.Now)
                {
                    UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);

                    filterContext.Result = new RedirectResult(urlHelper.Action("changepassword", "users", new { Area = "administration" }));

                    return;
                }
            }
        }
    }
}