﻿using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System.Web.Mvc;

namespace MvcApplication.Attributes
{
    public class RoleBasedAccessControlAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool isAuthorized = base.AuthorizeCore(filterContext.HttpContext);

            var urlHelper = new UrlHelper(filterContext.RequestContext);

            if (!isAuthorized)
            {
                filterContext.Result = new RedirectResult(urlHelper.Action("Login", "Account", new { Area = "Authentication", returnUrl = "" }));

                return;
            }

            var controllerName = $"{filterContext.ActionDescriptor.ControllerDescriptor.ControllerName}Controller";

            var actionName = filterContext.ActionDescriptor.ActionName;

            var serviceHeader = new ServiceHeader();

            var logger = DependencyResolver.Current.GetService<ILogger>();

            var channelService = new ChannelService(logger);

            var user = filterContext.HttpContext.User;

            var hasPermission = channelService.ValidatePermissionAsync(user.Identity.Name, controllerName, actionName, serviceHeader).Result;

            if (hasPermission)
            {
                return;
            }

            filterContext.Result = new RedirectResult(urlHelper.Action("Forbidden", "Errors", new { Area = "Errors" }));
        }
    }
}