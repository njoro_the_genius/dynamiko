﻿namespace WebApplication.Models
{
    public class DateRangeDTO
    {
        public string dateFrom { get; set; }

        public string dateTo { get; set; }
    }
}