﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class RoleDTO
    {
        public Guid Id { get; set; }

        [Display(Name = "Role Name")]
        public string Name { get; set; }

        [Display(Name = "Role Type")]
        public byte RoleType { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Is Enabled")]
        public bool IsEnabled { get; set; }

        [Display(Name = "Role Type Description")]
        public string RoleTypeDescription { get; set; }
    }
}