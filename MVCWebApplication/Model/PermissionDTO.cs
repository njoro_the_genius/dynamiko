﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class PermissionDTO
    {
        public Guid Id { get; set; }

        //public List<ControllerCollectionDTO> ControllerDto { get; set; }

        [Display(Name = "Role(s)")]
        public string Roles { get; set; }

        public bool ListRoles { get; set; }

        public List<RoleDTO> RoleList { get; set; }

        public string ActionName { get; set; }
    }
}