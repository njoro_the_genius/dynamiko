﻿using System;
using System.Collections.Generic;

namespace WebApplication.Models
{
    public class ControllerDTO
    {
        public Guid Id { get; set; }

        public string RoleName { get; set; }

        public List<ControllerCollectionDTO> ControllerCollection { get; set; }

        public List<ControllerDTO> controllerDTOs { get; set; }
    }

    public class ActionDTO
    {
        public Guid Id { get; set; }

        public String Name { get; set; }
    }

    public class ControllerCollectionDTO
    {
        public Guid Id { get; set; }

        public string ControllerName { get; set; }

        public List<ActionDTO> ActionList { get; set; }

        public string ActionName { get; set; }

        public bool ListRoles { get; set; }

        public string Commands { get; set; }
    }
}