﻿using System.Web.Mvc;

namespace WebApplication.Models
{
    public class RouteSelectListItem : SelectListItem
    {
        public string TransactionType { get; set; }
    }
}