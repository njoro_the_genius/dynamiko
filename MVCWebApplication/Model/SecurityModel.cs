﻿using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using System;
using System.Collections.Generic;

namespace WebApplication.Models
{
    public class SecurityModel
    {
        public Guid Id { get; set; }

        public ControllerDTO Module { get; set; }

        public List<ModuleNavigationItemInRoleDTO> SelectedModules { get; set; }

        public ApplicationRole Role { get; set; }
    }
}