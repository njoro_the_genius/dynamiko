﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Areas.HumanResource.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class NhifController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public NhifController(IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }
        // GET: HumanResource/Nhif
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.FindNhifOrderFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<NhifOrderDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(NhifOrderDTO nhifOrderDTO)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            if (ModelState.IsValid)
            {
                if (nhifOrderDTO.NhifGraduatedScaleDTOsList != null && nhifOrderDTO.NhifGraduatedScaleDTOsList.Any())
                {
                    nhifOrderDTO.NhifGraduatedScaleDTOs = nhifOrderDTO.NhifGraduatedScaleDTOsList;
                }

                var addNhifOrderDTO = await _channelService.AddNewNhifOrderAsync(nhifOrderDTO, _serviceHeader);

                if (addNhifOrderDTO != null)
                {
                    TempData["Success"] = "NhifOrder details created successfully";

                    return View("Index");
                }
            }

            TempData["Error"] = "Failed to create TourGuide details";

            return View("Create", nhifOrderDTO);
        }


        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);
            var nhifOrder = await _channelService.FindNhifOrderByIdAsync(id, _serviceHeader);
            nhifOrder.NhifGraduatedScaleDTOsList = nhifOrder.NhifGraduatedScaleDTOs.ToList();
            return View(nhifOrder);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(NhifOrderDTO nhifOrderDTO)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            if (ModelState.IsValid)
            {
                if (nhifOrderDTO.NhifGraduatedScaleDTOsList != null && nhifOrderDTO.NhifGraduatedScaleDTOsList.Any())
                {
                    nhifOrderDTO.NhifGraduatedScaleDTOs = nhifOrderDTO.NhifGraduatedScaleDTOsList;
                }

                var addNhifOrderDTO = await _channelService.UpdateNhifOrderAsync(nhifOrderDTO, _serviceHeader);

                if (addNhifOrderDTO != null)
                {
                    TempData["Success"] = "NhifOrder details edited successfully";

                    return View("Index");
                }
            }

            TempData["Error"] = "Failed to edit NHif details";

            return View("Create", nhifOrderDTO);
        }

    }
}