﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Areas.HumanResource.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class NssfController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public NssfController(IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }

        // GET: HumanResource/Nssf
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.FindNssfOrderFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<NssfOrderDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(NssfOrderDTO nssfOrderDTO)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            if (ModelState.IsValid)
            {
                if (nssfOrderDTO.NssfGraduatedScaleDTOsList != null && nssfOrderDTO.NssfGraduatedScaleDTOsList.Any())
                {
                    nssfOrderDTO.NssfGraduatedScaleDTOs = nssfOrderDTO.NssfGraduatedScaleDTOsList;
                }

                var addNssfOrderDTO = await _channelService.AddNewNssfOrderAsync(nssfOrderDTO, _serviceHeader);

                if (addNssfOrderDTO != null)
                {
                    TempData["Success"] = "NssfOrder details created successfully";

                    return View("Index");
                }
            }

            TempData["Error"] = "Failed to create NssfOrder details";

            return View("Create", nssfOrderDTO);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);
            var nssfOrder = await _channelService.FindNssfOrderByIdAsync(id, _serviceHeader);
            nssfOrder.NssfGraduatedScaleDTOsList = nssfOrder.NssfGraduatedScaleDTOs.ToList();
            return View(nssfOrder);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(NssfOrderDTO nssfOrderDTO)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            if (ModelState.IsValid)
            {
                if (nssfOrderDTO.NssfGraduatedScaleDTOsList != null && nssfOrderDTO.NssfGraduatedScaleDTOsList.Any())
                {
                    nssfOrderDTO.NssfGraduatedScaleDTOs = nssfOrderDTO.NssfGraduatedScaleDTOsList;
                }

                var addNssfOrderDTO = await _channelService.UpdateNssfOrderAsync(nssfOrderDTO, _serviceHeader);

                if (addNssfOrderDTO != null)
                {
                    TempData["Success"] = "NssfOrder details edited successfully";

                    return View("Index");
                }
            }

            TempData["Error"] = "Failed to Edit Nssf details";

            return View("Create", nssfOrderDTO);
        }


    }
}