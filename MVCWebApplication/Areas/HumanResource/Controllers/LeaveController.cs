﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Areas.HumanResource.Controllers
{
    [Authorize()]
    public class LeaveController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public LeaveController(
            IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }

        // GET: HumanResource/Leave
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            _serviceHeader.ApplicationUserName = currentUser.UserName;

            Guid? customer = currentUser.CustomerId;

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = new PageCollectionInfo<LeaveDTO>();

            if (roleName.Contains(role))
            {
                pageCollectionInfo = await _channelService.FindLeaveFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }
            else
            {
                pageCollectionInfo = await _channelService.FindLeaveFilterByEmployeeInPageAsync(Guid.Parse(currentUser.Id), jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<LeaveDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        public ActionResult AppliedLeave()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> AppliedLeave(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            _serviceHeader.ApplicationUserName = currentUser.UserName;

            Guid? customer = currentUser.CustomerId;

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = new PageCollectionInfo<LeaveDTO>();

            if (roleName.Contains(role))
            {
                pageCollectionInfo = await _channelService.FindLeaveFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }
            else if (roleName.Contains(EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.Administrator)))
            {
                pageCollectionInfo = await _channelService.FindLeaveFilterByCustomerInPageAsync(customer.Value, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }
            else
            {
                //pageCollectionInfo = await _channelService.FindLeaveFilterByEmployeeInPageAsync(Guid.Parse(currentUser.Id), jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<LeaveDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }


        public async Task<ActionResult> Create()
        {
            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            var leaveDTO = new LeaveDTO
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.Date,
            };

            return View(leaveDTO);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(LeaveDTO leaveDTO)
        {
            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            int result = DateTime.Compare(leaveDTO.StartDate, leaveDTO.EndDate);

            if (result > 0)
            {
                TempData["Error"] = "The  end date must be on or after the  start date.";

                return View(new LeaveDTO());
            }

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var employee = await _channelService.FindEmployeeByUserIdAsync(Guid.Parse(currentUser.Id), currentUser.CustomerId, _serviceHeader);

            if (employee != null)
            {
                leaveDTO.EmployeeId = employee.Id;

                leaveDTO.Period = (leaveDTO.EndDate - leaveDTO.StartDate).Days;

                //check number of days already used
                var appliedLeaves = await _channelService.FindLeavesByEmployeeIdAsync(leaveDTO.EmployeeId, _serviceHeader);

                //get a difference of eligible leave days and used leave days
                int leaveBalance = employee.AllocatedLeaveDays - appliedLeaves.Count;

                if (leaveBalance>0 && leaveBalance >= leaveDTO.Period)
                {
                    var addLeaveDTO = await _channelService.AddNewLeaveAsync(leaveDTO, _serviceHeader);

                    if (addLeaveDTO != null && addLeaveDTO.Item1 != null)
                    {
                        TempData["Result"] = string.Join(",", addLeaveDTO.Item2);

                        return RedirectToAction("Index");
                    }

                    TempData["Error"] = addLeaveDTO != null && addLeaveDTO.Item2.Any() ? string.Join(" | ", addLeaveDTO.Item2) : "Sorry, but an error has occured while creating customer.";
                }
                else
                {
                    TempData["Error"] = "You have 0 leave days left";

                    return View(leaveDTO);
                }


            }
            else
            {
                TempData["Error"] = "You dont exist as an employee";
            }

            return View(new LeaveDTO());
        }


        // GET: Registry/Customers/Create
        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            var appliedLeave = await _channelService.FindLeaveByIdAsync(id, _serviceHeader);

            if (appliedLeave != null)
                return View(appliedLeave);


            return View(new LeaveDTO());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(LeaveDTO leaveDTO)
        {
            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(leaveDTO);
            }

            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var employee = await _channelService.FindEmployeeByUserIdAsync(Guid.Parse(currentUser.Id), currentUser.CustomerId, _serviceHeader);

            if (employee != null)
            {
                leaveDTO.EmployeeId = employee.Id;

                leaveDTO.Period = (leaveDTO.EndDate - leaveDTO.StartDate).Days;

                var update = await _channelService.UpdateLeaveAsync(leaveDTO, _serviceHeader);

                if (update)
                {
                    TempData["Success"] = "Leave deatils updated successfully!";

                    return RedirectToAction("Index");
                }

                TempData["Error"] = "Failed to update leave details! Please try again. ";
            }
            else
            {
                TempData["Error"] = "You dont exist as an employee";
            }

            return View(new LeaveDTO());
        }

        [HttpGet]
        public async Task<ActionResult> Approve(Guid id)
        {
            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            var appliedLeave = await _channelService.FindLeaveByIdAsync(id, _serviceHeader);

            if (appliedLeave != null)
                return View(appliedLeave);


            return View(new LeaveDTO());
        }

        [HttpPost]
        public async Task<ActionResult> Approve(LeaveDTO leaveDTO)
        {
            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(leaveDTO);
            }

            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            if (roleName.Contains(role))
            {
                leaveDTO.RecordStatus = (byte)RecordStatus.Approved;

                leaveDTO.ApprovedBy = currentUser.UserName;

                leaveDTO.DateApproved = DateTime.Now;

                var update = await _channelService.UpdateLeaveAsync(leaveDTO, _serviceHeader);

                if (update)
                {
                    TempData["Success"] = "Leave application approved successfully!";

                    return RedirectToAction("Index");
                }

                TempData["Error"] = "Failed to approve the leave application! Please try again. ";

            }
            else
            {
                TempData["Error"] = "You don't have the rights to approve!";
            }



            return View(new LeaveDTO());
        }

        [HttpGet]
        public async Task<ActionResult> Reject(Guid id)
        {
            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            var appliedLeave = await _channelService.FindLeaveByIdAsync(id, _serviceHeader);

            if (appliedLeave != null)
                return View(appliedLeave);


            return View(new LeaveDTO());
        }

        [HttpPost]
        public async Task<ActionResult> Reject(LeaveDTO leaveDTO)
        {
            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(leaveDTO);
            }

            ViewBag.leaveTypesSelectList = GetLeaveSelectList(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            if (roleName.Contains(role))
            {

                leaveDTO.RecordStatus = (byte)RecordStatus.Rejected;

                leaveDTO.ApprovedBy = currentUser.UserName;

                leaveDTO.DateApproved = DateTime.Now;

                var update = await _channelService.UpdateLeaveAsync(leaveDTO, _serviceHeader);

                if (update)
                {
                    TempData["Success"] = "Leave application rejected successfully!";

                    return RedirectToAction("Index");
                }

                TempData["Error"] = "Failed to reject the leave application! Please try again. ";

            }
            else
            {
                TempData["Error"] = "You don't have the rights to reject!";
            }



            return View(new LeaveDTO());
        }

    }
}