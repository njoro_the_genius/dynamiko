﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Areas.HumanResource.Controllers
{
    [Authorize()]
    public class DeductionsController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public DeductionsController(IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }
        // GET: HumanResource/Deductions
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            _serviceHeader.ApplicationUserName = currentUser.UserName;

            Guid? customer = currentUser.CustomerId;

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            var pageCollectionInfo = new PageCollectionInfo<DeductionDTO>();

            if (roleName.Contains(role))
            {
                pageCollectionInfo = await _channelService.FindDeductionFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }
            else
            {
                pageCollectionInfo = await _channelService.FindDeductionByCustomerIdFilterInPageAsync(customer.Value, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<DeductionDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        public async Task<ActionResult> Create()
        {
            ViewBag.DeductionTypesSelectList = GetDeductionSelectList(string.Empty);
            ViewBag.FrequencyTypesSelectList = GetFrequencySelectList(string.Empty);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(DeductionDTO deductionDTO)
        {
            ViewBag.DeductionTypesSelectList = GetDeductionSelectList(string.Empty);

            ViewBag.FrequencyTypesSelectList = GetFrequencySelectList(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var employee = await _channelService.FindEmployeeByUserIdAsync(Guid.Parse(currentUser.Id), currentUser.CustomerId, _serviceHeader);

            if (employee != null)
            {
                try
                {
                    var addDeduction = await _channelService.AddNewDeductionAsync(deductionDTO, _serviceHeader);

                    if (addDeduction != null)
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception exception)
                {
                    TempData["Error"] = $"Sorry,{exception} error has occured while creating customer.";
                }

            }
            else
            {
                TempData["Error"] = "You dont exist as an employee";
            }



            return View();
        }

        public async Task<ActionResult> Edit()
        {
            ViewBag.DeductionTypesSelectList = GetDeductionSelectList(string.Empty);
            ViewBag.FrequencyTypesSelectList = GetFrequencySelectList(string.Empty);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(DeductionDTO deductionDTO)
        {
            ViewBag.DeductionTypesSelectList = GetDeductionSelectList(string.Empty);
            ViewBag.FrequencyTypesSelectList = GetFrequencySelectList(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var employee = await _channelService.FindEmployeeByUserIdAsync(Guid.Parse(currentUser.Id), currentUser.CustomerId, _serviceHeader);

            if (employee != null)
            {
                try
                {
                    var updateDeduction = await _channelService.UpdateDeductionAsync(deductionDTO, _serviceHeader);

                    if (updateDeduction)
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception exception)
                {
                    TempData["Error"] = $"Sorry,{exception} error has occured while creating customer.";
                }

            }
            else
            {
                TempData["Error"] = "You dont exist as an employee";
            }



            return View();
        }
    }
}