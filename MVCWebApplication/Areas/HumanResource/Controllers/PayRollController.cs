﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.HumanResource.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator,Standard User")]
    public class PayRollController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public PayRollController(IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }

        // GET: HumanResource/PayRoll
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            _serviceHeader.ApplicationUserName = currentUser.UserName;

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = new PageCollectionInfo<PayRollDTO>();

            if (roleName.Contains(role))
            {
                pageCollectionInfo = await _channelService.FindPayRollFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }
            else
            {
                pageCollectionInfo = await _channelService.FindPayRollByCustomerIdFilterInPageAsync(currentUser.CustomerId.Value, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);
            }
            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<PayRollDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        public ActionResult ItemsIndex(Guid id)
        {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> ItemsIndex(Guid id, JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.FindPayRollItemDTOFilterInPageAsync(id, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<PayRollItemDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        public ActionResult IndividualItemsIndex()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> IndividualItemsIndex(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            _serviceHeader.ApplicationUserName = currentUser.UserName;

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.FindPayRollItemByEmployeeIdDTOFilterInPageAsync(currentUser.CustomerId.Value, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, false, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<PayRollItemDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var payRollDTO = new PayRollDTO
            {
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.Date,
            };

            return View(payRollDTO);
        }

        [HttpPost]
        public async Task<ActionResult> Create(PayRollDTO payRoll)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            int result = DateTime.Compare(payRoll.StartDate, payRoll.EndDate);

            if (result > 0)
            {
                TempData["Error"] = "The  end date must be on or after the  start date.";

                return View(new PayRollDTO());
            }

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            payRoll.CustomerId = currentUser.CustomerId.Value;

            if (ModelState.IsValid)
            {
                var payRollDTO = await _channelService.AddNewPayRollAsync(payRoll, _serviceHeader);

                if (payRollDTO != null)
                {
                    TempData["Success"] = "Payroll details created successfully";

                    return View("Index");
                }
            }
            else
            {
                var errors = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                TempData["Error"] = errors;
            }



            return View("Create", payRoll);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            var leave = await _channelService.FindPayRollByIdAsync(id, _serviceHeader);

            return View(leave);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(PayRollDTO payRoll)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            payRoll.CustomerId = currentUser.CustomerId.Value;

            if (ModelState.IsValid)
            {
                var payRollDTO = await _channelService.UpdatePayRollAsync(payRoll, _serviceHeader);

                if (payRollDTO)
                {
                    TempData["Success"] = "Payroll details edited successfully";

                    return View("Index");
                }
            }
            else
            {
                var errors = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                TempData["Error"] = errors;
            }



            return View("Create", payRoll);
        }

        [HttpGet]
        public async Task<ActionResult> Approve(Guid id)
        {
            var leave = await _channelService.FindPayRollByIdAsync(id, _serviceHeader);

            return View(leave);
        }

        [HttpPost]
        public async Task<ActionResult> Approve(PayRollDTO payRoll)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            payRoll.CustomerId = currentUser.CustomerId.Value;
            payRoll.RecordStatus = (byte)RecordStatus.Approved;

            if (ModelState.IsValid)
            {
                var payRollDTO = await _channelService.UpdatePayRollAsync(payRoll, _serviceHeader);

                if (payRollDTO)
                {
                    TempData["Success"] = "Payroll details approved successfully";

                    return View("Index");
                }
            }
            else
            {
                var errors = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                TempData["Error"] = errors;
            }



            return View("Create", payRoll);
        }

        [HttpGet]
        public async Task<ActionResult> Reject(Guid id)
        {
            var leave = await _channelService.FindPayRollByIdAsync(id, _serviceHeader);

            return View(leave);
        }

        [HttpPost]
        public async Task<ActionResult> Reject(PayRollDTO payRoll)
        {
            ViewBag.chargeTypesSelectList = GetChargeType(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            payRoll.CustomerId = currentUser.CustomerId.Value;
            payRoll.RecordStatus = (byte)RecordStatus.Rejected;

            if (ModelState.IsValid)
            {
                var payRollDTO = await _channelService.UpdatePayRollAsync(payRoll, _serviceHeader);

                if (payRollDTO)
                {
                    TempData["Success"] = "Payroll details rejected successfully";

                    return View("Index");
                }
            }
            else
            {
                var errors = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                TempData["Error"] = errors;
            }



            return View("Create", payRoll);
        }
    }
}