﻿using System.Web.Mvc;

namespace MvcApplication.Areas.ApplicationSettings
{
    public class ApplicationSettingsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ApplicationSettings";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ApplicationSettings_default",
                "ApplicationSettings/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}