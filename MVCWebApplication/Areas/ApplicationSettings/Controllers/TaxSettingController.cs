﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.AspNet.Identity;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.ApplicationSettings.Controllers
{
    [Authorize(Roles = "Super Administrator")]
    public class TaxSettingController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _userManager;

        private readonly ApplicationRoleManager _applicationRoleManager;


        public TaxSettingController(IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            serviceHeader = webConfigurationService.GetServiceHeader();

            _userManager = userManager;

            _applicationRoleManager = roleManager;
        }


        // GET: ApplicationSettings/TaxSetting
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> TaxSettingList(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc";

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.FindTaxOrderFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending, serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(items: new List<TaxOrderDTO> { }, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(TaxOrderDTO taxOrderDTO)
        {
            if (!ModelState.IsValid)
            {
                string errorMessage = string.Join(" |", ModelState.Values.SelectMany(a => a.Errors).Select(b => b.ErrorMessage));

                TempData["ModelError"] = taxOrderDTO;

                return View("create", taxOrderDTO);
            }

            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            taxOrderDTO.CreatedBy = currentUser.UserName;

            var checkKeySetting = await _channelService.FindSettingByKeyAsync(taxOrderDTO.Description, serviceHeader);

            if (checkKeySetting != null)
            {
                TempData["Error"] = $"Tax  setting with the name {taxOrderDTO.Description} already exist!";

                return View(taxOrderDTO);
            }

            taxOrderDTO.CreatedBy = currentUser.UserName;

            var CreateResult = await _channelService.AddNewTaxOrderAsync(taxOrderDTO, serviceHeader);

            if (CreateResult != null)
            {
                TempData["Success"] = "Tax Setting created successfully";

                return RedirectToAction("index");
            }
            TempData["Error"] = "Tax Setting creation failed.";

            return View("create", taxOrderDTO);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            var findresult = await _channelService.FindTaxSettingByIdAsync(id, serviceHeader);

            findresult.TaxRangesItemList = await _channelService.FindGraduatedScalesAsync(id, serviceHeader);

            return View(findresult);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(TaxOrderDTO taxOrderDTO)
        {
            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            if (!ModelState.IsValid)
            {
                string errorMessage = string.Join(" |", ModelState.Values.SelectMany(a => a.Errors).Select(b => b.ErrorMessage));

                TempData["ModelError"] = taxOrderDTO;

                return View("edit", taxOrderDTO);
            }

            taxOrderDTO.CreatedBy = currentUser.UserName;

            var updateresult = await _channelService.UpdateTaxOrderAsync(taxOrderDTO, serviceHeader);

            if (updateresult)
            {
                TempData["Success"] = "tax setting updated successfully";

                return RedirectToAction("index");
            }

            TempData["Error"] = "tax setting update failed. Please try again later";

            return RedirectToAction("index");
        }

        public async Task<ActionResult> GetTaxSettingDetails(Guid id)
        {
            var staticSetting = await _channelService.FindTaxSettingByIdAsync(id, serviceHeader) ?? new TaxOrderDTO();

            staticSetting.TaxRangesItemList = await _channelService.FindGraduatedScalesAsync(id, serviceHeader);

            return PartialView("_taxSettingDetails", staticSetting);
        }
    }
}