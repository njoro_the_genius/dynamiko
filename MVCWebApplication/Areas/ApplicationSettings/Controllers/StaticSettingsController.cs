﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.AspNet.Identity;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.ApplicationSettings.Controllers
{
    [Authorize(Roles = "Super Administrator")]
    public class StaticSettingsController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _userManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public StaticSettingsController(IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            serviceHeader = webConfigurationService.GetServiceHeader();

            _userManager = userManager;

            _applicationRoleManager = roleManager;
        }

        // GET: ApplicationSettings/StaticSetting
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> StaticSettingList(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc";

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.FindSettingFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending, serviceHeader, 0.75d);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(items: new List<StaticSettingDTO> { }, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(StaticSettingDTO staticSettingDTO)
        {
            if (!ModelState.IsValid)
            {
                string errorMessage = string.Join(" |", ModelState.Values.SelectMany(a => a.Errors).Select(b => b.ErrorMessage));

                TempData["ModelError"] = staticSettingDTO;

                return View("create", staticSettingDTO);
            }

            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            staticSettingDTO.CreatedBy = currentUser.UserName;

            var checkKeySetting = await _channelService.FindSettingByKeyAsync(staticSettingDTO.Key, serviceHeader);

            if (checkKeySetting != null)
            {
                TempData["Error"] = $"Static setting with the name {staticSettingDTO.Key} already exist!";

                return View(staticSettingDTO);
            }

            staticSettingDTO.CreatedBy = currentUser.UserName;

            var CreateResult = await _channelService.AddNewSettingAsync(staticSettingDTO, serviceHeader);

            if (CreateResult != null)
            {
                TempData["Success"] = "Setting created successfully";

                return RedirectToAction("index", "staticsettings");
            }
            TempData["Error"] = "Setting creation failed.";

            return View("create", staticSettingDTO);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            var findresult = await _channelService.FindSettingByIdAsync(id, serviceHeader);

            return View(findresult);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(StaticSettingDTO settingDTO)
        {
            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            if (!ModelState.IsValid)
            {
                string errorMessage = string.Join(" |", ModelState.Values.SelectMany(a => a.Errors).Select(b => b.ErrorMessage));

                TempData["ModelError"] = settingDTO;

                return View("edit", settingDTO);
            }

            settingDTO.CreatedBy = currentUser.UserName;

            var updateresult = await _channelService.UpdateSettingAsync(settingDTO, serviceHeader);

            if (updateresult)
            {
                TempData["Success"] = "setting updated successfully";

                return RedirectToAction("index", "staticsettings");
            }

            TempData["Error"] = "setting update failed. Please try again later";

            return RedirectToAction("index", "staticsettings");
        }

        [HttpGet]
        public async Task<ActionResult> GetStaticSettingDetail(Guid id)
        {
            var staticSetting = await _channelService.FindSettingByIdAsync(id, serviceHeader);

            return View("_staticSettingDetails", staticSetting);
        }
    }
}