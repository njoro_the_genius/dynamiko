﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Analytics.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class ReportsController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly ApplicationUserManager _applicationUserManager;

        public ReportsController(
            IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager applicationUserManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(applicationUserManager, nameof(applicationUserManager));

            _applicationUserManager = applicationUserManager;

            _channelService = channelService;

            _serviceHeader = webConfigurationService.GetServiceHeader();
        }
        // GET: Reports/Reports
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Employees()
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var isSuperAdmin = User.Identity.IsUserInRole(new string[] { EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator) });

            var accountManager = await _channelService.FindApplicationUserAsync(currentUser.UserName, _serviceHeader);

            Guid customerId = isSuperAdmin ? Guid.Empty : accountManager?.CustomerId ?? Guid.Empty;

            ViewBag.CustomerList = await GetCustomersSelectList(_channelService, _serviceHeader, customerId.ToString());

            ViewBag.CustomerId = customerId.ToString();

            return View();
        }


        [HttpPost]
        public async Task<JsonResult> Employees(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var startDateString = Request["startDate"];

            var endDateString = Request["endDate"];

            var customerIdString = Request["CustomerId"];

            Guid customerId = Guid.Empty;

            string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy" };

            var startDate = DateTime.Today.AddDays(-14);

            var endDate = DateTime.Today;

            var totalRecordCount = 0;

            var searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            if (!string.IsNullOrWhiteSpace(customerIdString))
                Guid.TryParseExact(customerIdString, "D", out customerId);

            if (DateTime.TryParseExact($"{startDateString}", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate)
              && DateTime.TryParseExact($"{endDateString}", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate))
            {

                var isSuperAdmin = User.Identity.IsUserInRole(new string[] { EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator) });

                int claim = isSuperAdmin ? 1 : 2;

                var pageCollectionInfo = await _channelService.GetEmployeesReportAsync(claim, customerId, startDate, endDate, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, jQueryDataTablesModel.sSearch, _serviceHeader);

                if (pageCollectionInfo == null || !pageCollectionInfo.PageCollection.Any())
                    return this.DataTablesJson(new List<EmployeeDTO> { }, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);

                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<EmployeeDTO> { }, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        [HttpGet]
        public async Task<List<PaySlipDTO>> PaySlip(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var startDateString = Request["startDate"];

            var endDateString = Request["endDate"];

            var customerIdString = Request["employeeId"];

            Guid employeeId = Guid.Empty;

            string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy" };

            var startDate = DateTime.Today.AddDays(-14);

            int period = startDate.Month;



            if (!string.IsNullOrWhiteSpace(customerIdString))
                Guid.TryParseExact(customerIdString, "D", out employeeId);

            if (period > 0)
            {

                var isSuperAdmin = User.Identity.IsUserInRole(new string[] { EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator) });

                int claim = isSuperAdmin ? 1 : 2;

                var pageCollectionInfo = await _channelService.GetPaySlipAsync(employeeId, period, _serviceHeader);

                if (pageCollectionInfo == null || !pageCollectionInfo.PageCollection.Any())
                    return new List<PaySlipDTO>();
               
                return pageCollectionInfo.PageCollection;
            }

            return new List<PaySlipDTO>();
        }

        [HttpGet]
        public async Task<ActionResult> PayRoll()
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var isSuperAdmin = User.Identity.IsUserInRole(new string[] { EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator) });

            var accountManager = await _channelService.FindApplicationUserAsync(currentUser.UserName, _serviceHeader);

            Guid customerId = isSuperAdmin ? Guid.Empty : accountManager?.CustomerId ?? Guid.Empty;

            ViewBag.CustomerList = await GetCustomersSelectList(_channelService, _serviceHeader, customerId.ToString());

            ViewBag.CustomerId = customerId.ToString();

            return View();
        }

        [HttpPost]
        public async Task<JsonResult> PayRoll(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var startDateString = Request["startDate"];

            var endDateString = Request["endDate"];

            var customerIdString = Request["customerId"];

            Guid customerId = Guid.Empty;

            string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy" };

            var startDate = DateTime.Today.AddDays(-14);

            var endDate = DateTime.Today;

            var totalRecordCount = 0;

            var searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            if (!string.IsNullOrWhiteSpace(customerIdString))
                Guid.TryParseExact(customerIdString, "D", out customerId);

            if (DateTime.TryParseExact($"{startDateString}", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate)
              && DateTime.TryParseExact($"{endDateString}", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate))
            {

                var isSuperAdmin = User.Identity.IsUserInRole(new string[] { EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator) });

                int claim = isSuperAdmin ? 1 : 2;

                var pageCollectionInfo = await _channelService.GetPayRollAsync(claim, customerId, startDate, endDate, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, jQueryDataTablesModel.sSearch, _serviceHeader);

                if (pageCollectionInfo == null || !pageCollectionInfo.PageCollection.Any())
                    return this.DataTablesJson(new List<PaySlipDTO> { }, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);

                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<PaySlipDTO> { }, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }
    }
}