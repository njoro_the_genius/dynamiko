﻿using System.Web.Mvc;

namespace MvcApplication.Areas.Analytics
{
    public class AnalyticsAreaRegistration : AreaRegistration
    {
        public override string AreaName => "Analytics";

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Analytics_default",
                "Analytics/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}