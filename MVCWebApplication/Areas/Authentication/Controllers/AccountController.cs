﻿using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MvcApplication.Attributes;
using MvcApplication.Identity;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using static WebApplication.Models.AccountViewModels;

namespace MvcApplication.Areas.Authentication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(ForcePasswordChangeAttribute))]
    public class AccountController : Controller
    {
        private readonly IChannelService _channelService;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;
        private readonly ServiceHeader _serviceHeader;
        private readonly ApplicationSignInManager _signInManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IWebConfigurationService _webConfigurationService;

        public AccountController(
             IChannelService channelService,
             ApplicationSignInManager signInManager,
             IAuthenticationManager authenticationManager,
             ApplicationUserManager userManager,
             ApplicationRoleManager roleManager,
             IWebConfigurationService webConfigurationService)
        {
            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(signInManager, nameof(signInManager));

            Guard.ArgumentNotNull(authenticationManager, nameof(authenticationManager));

            _webConfigurationService = webConfigurationService;
            _serviceHeader = webConfigurationService.GetServiceHeader();
            _channelService = channelService;

            _userManager = userManager;

            _roleManager = roleManager;

            _signInManager = signInManager;

            _authenticationManager = authenticationManager;

            _userManager.RegisterTwoFactorProvider(TwoFactorProvidersCodes.EmailCode.ToString(), new EmailTokenProvider<ApplicationUser>());

        }

        // GET: Authentication/Account
        public ActionResult Index()
        {
            return View();
        }

      
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            // Store the originating URL so we can attach it to a form field
            var viewModel = new AccountLoginModel { ReturnUrl = returnUrl };

            return View(viewModel);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginModel viewModel, string returnUrl)
        {
            // Ensure we have a valid viewModel to work with
            if (!ModelState.IsValid)
                return View(viewModel);

            // Require the user to have a confirmed email before they can log on.
            var user = await _userManager.FindByNameAsync(viewModel.Username);

            if (user == null)
            {
                ModelState.AddModelError("", "Invalid login attempt.");

                return View(viewModel);
            }

            if (user.RecordStatus != (byte)RecordStatus.Approved || !user.IsEnabled)
            {
                ModelState.AddModelError("", "Sorry, your status does not allow you to login");

                return View(viewModel);
            }

            if (user != null)
            {
                //check if user is an api user
                var userRole = _userManager.GetRoles(user.Id).Any(x => x.Equals(EnumHelper.GetDescription(WellKnownUserRoles.APIAccount)));

                if (userRole)
                {
                    ModelState.AddModelError("", "Sorry, your role does not allow you to login");

                    return View(viewModel);
                }
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true

                var signInStatus = await _signInManager.PasswordSignInAsync(viewModel.Username, viewModel.Password, false, shouldLockout: true);

                switch (signInStatus)
                {
                    case SignInStatus.Success:

                        user.LastLoginDateTime = DateTime.Now;

                        await _userManager.UpdateAsync(user);

                        return RedirectToLocal(returnUrl);

                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    //case SignInStatus.RequiresVerification:
                    //    return RedirectToAction("TwoFactorAuthentication", new { ReturnUrl = returnUrl, RememberMe = false, cdx = user.Id, name = user.FirstName });
                    case SignInStatus.Failure:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(viewModel);
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(viewModel);
                }
            }

            ModelState.AddModelError("", "Invalid login attempt.");

            return View(viewModel);
        }


       
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword()
        {
            return View();
        }

        
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ResetPassword(PasswordResetModel passwordResetModel)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ModelState = "Invalid model state";

                return View(passwordResetModel);
            }

            bool result = await  _channelService.ResetPassword(passwordResetModel, _serviceHeader);

            if (result)
            {
                return RedirectToAction("Login");
            }
            else
            {
                ViewBag.ModelState = "Password reset failed! Please try again!";

                return View(passwordResetModel);
            }

        }

      
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            return View(new AccountForgotPasswordModel());
        }

       
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ForgotPassword(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                TempData["Error"] = "Please enter email address.";

                return View("ForgotPassword");
            }

            ApplicationUser applicationUser = await _userManager.FindByNameAsync(userName);

            if (applicationUser == null)
            {
                return View("ForgotPasswordConfirmation");
            }

            string resetToken = await _userManager.GenerateUserTokenAsync("ResetPassword", applicationUser.Id);

            string callBackUrl = Url.Action("ResetPassword", "Account", new { userId = applicationUser.Id, code = HttpUtility.HtmlDecode(resetToken) }, Request.Url.Scheme);

            var result = await _channelService.ResetPasswordLinkAsync(applicationUser.UserName, callBackUrl, _serviceHeader);

            if (!result)
                TempData["Error"] = "Unable to reset password at the moment.Please try again later";
            else
                return View("ForgotPasswordConfirmation");

            return RedirectToAction("ForgotPasswordConfirmation");

        }

        [IgnoreAction]
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            // First we clean the authentication ticket like always
            FormsAuthentication.SignOut();

            // Second we clear the principal to ensure the user does not retain any authentication
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            _authenticationManager.SignOut();

            Session.Clear();
            Session.Abandon();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();

            // Last we redirect to a controller/action that requires authentication to ensure a redirect takes place
            // this clears the Request.IsAuthenticated flag since this triggers a new request
            return RedirectLocal();
        }

        #region helpers

        private void EnsureLoggedOut()
        {
            // If the request is (still) marked as authenticated we send the user to the logout action
            if (Request.IsAuthenticated)
                Logout();
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            // If the return url starts with a slash "/" we assume it belongs to our site
            // so we will redirect to this "action"
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            // If we cannot verify if the url is local to our host we redirect to a default location
            return RedirectToAction("Dashboard", "Dashboard", new { Area = "" });
        }

        private ActionResult RedirectLocal(string returnUrl = "")
        {
            // If the return url starts with a slash "/" we assume it belongs to our site
            // so we will redirect to this "action"
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            // If we cannot verify if the url is local to our host we redirect to a default location
            return RedirectToAction("login", "account", new { Area = "Authentication" });
        }

        #endregion
    }
}