﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Registry.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class EmployeeController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;


        public EmployeeController(
           IChannelService channelService,
           IWebConfigurationService webConfigurationService,
           ApplicationUserManager userManager,
           ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }


        #region Employee

        // GET: Registry/Employee
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            _serviceHeader.ApplicationUserName = currentUser.UserName;

            Guid? customer = currentUser.CustomerId;

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc";

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            var pageCollectionInfo = new PageCollectionInfo<EmployeeDTO>();

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            if (roleName.Contains(role))
            {
                pageCollectionInfo = await _channelService.FindCustomerEmployeesFilterInPageAsync(jQueryDataTablesModel.sSearch, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, sortAscending, _serviceHeader);
            }
            else
            {
                pageCollectionInfo = await _channelService.FindEmployeeByCustomerIdAsync(customer, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, false, _serviceHeader);
            }
            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<EmployeeDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        // GET: Registry/Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Registry/Employee/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.RolesList = GetUserRoleInfo(string.Empty);

            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.DepartmentSelectListItems = GetDepartmentSelectListItems(string.Empty);

            ViewBag.GenderSelectListItems = GetGenderSelectListItems(string.Empty);

            ViewBag.BankSelectListItems = await GetBankSelectList(string.Empty);

            ViewBag.BranchSelectListItems = await GetBranchByBankSelectList(string.Empty);

            return View(new EmployeeDTO());
        }

        // POST: Registry/Employee/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(EmployeeDTO employeeDTO)
        {
            ViewBag.RolesList = GetUserRoleInfo(employeeDTO.RoleName);

            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.DepartmentSelectListItems = GetDepartmentSelectListItems(string.Empty);

            ViewBag.GenderSelectListItems = GetGenderSelectListItems(string.Empty);

            ViewBag.BankSelectListItems = await GetBankSelectList(string.Empty);

            ViewBag.BranchSelectListItems = await GetBranchByBankSelectList(string.Empty);

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            if (currentUser.UserName != "su")
            {
                employeeDTO.CustomerId = currentUser.CustomerId;
                employeeDTO.IsExternalUser = false;
            }
            else
            {
                employeeDTO.IsExternalUser = true;
            }

            employeeDTO.IsEnabled = true;

            employeeDTO.CreatedBy = currentUser.UserName;

            if(employeeDTO.AllocatedLeaveDays <= 0)
            {
                employeeDTO.AllocatedLeaveDays = 21;
            }

            var addNewEmployee = await _channelService.AddNewEmployeeAsync(employeeDTO, _serviceHeader);

            if (addNewEmployee != null && addNewEmployee.Item1 != null && addNewEmployee.Item2.Any())
            {
                TempData["Result"] = string.Join(",", addNewEmployee.Item2);

                return RedirectToAction("Index");
            }

            TempData["Error"] = addNewEmployee != null && addNewEmployee.Item2.Any() ? string.Join(" | ", addNewEmployee.Item2) : "Sorry, but an error has occured while creating customer-employee mapping.";

            return View();
        }

        // GET: Registry/Employee/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            var employee = await _channelService.FindEmployeeAsync(id, _serviceHeader) ?? new EmployeeDTO();

            ViewBag.BankSelectListItems = await GetBankSelectList(string.Empty);

            ViewBag.BranchSelectListItems = await GetBranchByBankSelectList(string.Empty);

            var applicationUser = await _applicationUserManager.FindByIdAsync(employee.UserId.ToString());

            var role = applicationUser.Roles.FirstOrDefault();

            var selectedRole = new ApplicationRole();

            if (role != null)
                selectedRole = await _applicationRoleManager.FindByIdAsync(role.RoleId);

            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.RolesList = GetUserRoleInfo(selectedRole.Name);

            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.DepartmentSelectListItems = GetDepartmentSelectListItems(string.Empty);

            ViewBag.GenderSelectListItems = GetGenderSelectListItems(string.Empty);

            return View(employee);
        }

        // POST: Registry/Employee/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EmployeeDTO employeeDTO)
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.RolesList = GetUserRoleInfo(employeeDTO.RoleName);

            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.DepartmentSelectListItems = GetDepartmentSelectListItems(string.Empty);

            ViewBag.GenderSelectListItems = GetGenderSelectListItems(string.Empty);

            ViewBag.BankSelectListItems = await GetBankSelectList(string.Empty);

            ViewBag.BranchSelectListItems = await GetBranchByBankSelectList(string.Empty);

            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(employeeDTO);
            }

            var update = await _channelService.UpdateEmployeeAsync(employeeDTO, _serviceHeader);

            if (update)
            {
                TempData["Result"] = "Employee updated successfully!";

                return RedirectToAction("Index");
            }

            TempData["Error"] = "Failed to update employee! Please try again.";

            return View(employeeDTO);
        }

        [HttpGet]
        public async Task<ActionResult> Details(Guid id)
        {
            var employee = await _channelService.FindEmployeeAsync(id, _serviceHeader) ?? new EmployeeDTO();

            return PartialView("_employeeDetails", employee);
        }

        #endregion

        #region Helpers
        [NonAction]
        private List<SelectListItem> GetUserRoleInfo(string selectedValue, bool filterRoles = true)
        {
            var rolesInfo = new List<SelectListItem>();

            rolesInfo.Add(new SelectListItem { Value = "", Selected = selectedValue == string.Empty, Text = "--Select Role--" });

            var roles = _applicationRoleManager.Roles.ToArray();

            Array.ForEach(roles,
                roleName =>
                {
                    rolesInfo.Add(new SelectListItem
                    {
                        Selected = roleName.Name == selectedValue,
                        Text = roleName.Name,
                        Value = roleName.Name
                    });
                });

            return rolesInfo;
        }

        protected async Task<List<SelectListItem>> GetBankSelectList(string selectedValue)
        {
            List<SelectListItem> banksSelectList = new List<SelectListItem> { };

            var defaultBank = new SelectListItem { Selected = (selectedValue == string.Empty), Text = @"Choose Bank", Value = string.Empty };

            banksSelectList.Add(defaultBank);

            //fetch from excel list

            var bankListItems = await _channelService.GetBanksListAsync(_serviceHeader);

            foreach (var bank in bankListItems)
            {
                banksSelectList.Add(new SelectListItem
                {
                    Text = bank.Name,
                    Value = bank.Code,
                    Selected = bank.Code == selectedValue,
                });
            }

            return banksSelectList;
        }

        protected async Task<List<SelectListItem>> GetBranchByBankSelectList(string selectedValue)
        {
            List<SelectListItem> branchesSelectList = new List<SelectListItem> { };

            var defaultBranch = new SelectListItem { Selected = (selectedValue == string.Empty), Text = @"Choose Branch", Value = string.Empty };

            branchesSelectList.Add(defaultBranch);

            //fetch from excel list

            var brachListItems = await _channelService.GetBranchesListAsync(selectedValue, _serviceHeader);

            if (brachListItems != null)
            {
                foreach (var branch in brachListItems)
                {
                    branchesSelectList.Add(new SelectListItem
                    {
                        Text = branch.Name,
                        Value = branch.Code,
                        Selected = branch.Code == selectedValue,
                    });
                }
            }

            return branchesSelectList;
        }

        public async Task<JsonResult> GetBranchSelectedBank(string selectedValue)
        {
            var list = await GetBranchByBankSelectList(selectedValue);

            return Json(list);
        }

        #endregion
    }
}