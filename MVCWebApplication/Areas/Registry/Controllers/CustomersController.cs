﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Registry.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class CustomersController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public CustomersController(
            IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager,
            ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }

        #region Customer

        // GET: Registry/Customers
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.FindCustomersByFullTextAsync(jQueryDataTablesModel.sSearch, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, false, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<CustomerDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        // GET: Registry/Customers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Registry/Customers/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            return View(new CustomerDTO());
        }

        // POST: Registry/Customers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CustomerDTO customerDTO)
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            if (string.IsNullOrEmpty(customerDTO.Name))
            {
                TempData["ModelError"] = "Customer name is required";

                return View(customerDTO);
            }

            var existingCustomer = await _channelService.FindCustomersByNameAsync(customerDTO.Name, _serviceHeader);

            if (existingCustomer != null && existingCustomer.Any())
            {
                TempData["Error"] = "Another customer with the name " + customerDTO.Name + "Already exists!";

                return View(customerDTO);
            }

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            //var customerDTO = customerBindingModel.ProjectedAs<CustomerDTO>();

            customerDTO.Type = byte.Parse(Request.Form["CustomerType"]);

            customerDTO.IsEnabled = true;

            var addNewCustomer = await _channelService.AddNewCustomerAsync(customerDTO, _serviceHeader);

            if (addNewCustomer != null && addNewCustomer.Item1 != null && addNewCustomer.Item2.Any())
            {
                TempData["Result"] = string.Join(",", addNewCustomer.Item2);

                return RedirectToAction("Index");
            }

            TempData["Error"] = addNewCustomer != null && addNewCustomer.Item2.Any() ? string.Join(" | ", addNewCustomer.Item2) : "Sorry, but an error has occured while creating customer.";

            return View();
        }

        // GET: Registry/Customers/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            var customer = await _channelService.FindCustomerAsync(id, _serviceHeader) ?? new CustomerDTO();

            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(customer.Type.ToString());

            return View(customer);
        }

        // POST: Registry/Customers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(CustomerDTO customerDTO)
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(customerDTO);
            }

            customerDTO.Type = byte.Parse(Request.Form["CustomerType"]);

            var update = await _channelService.UpdateCustomerAsync(customerDTO, _serviceHeader);

            if (update)
            {
                TempData["Result"] = "Customer updated successfully!";

                return RedirectToAction("Index");
            }

            TempData["Error"] = "Failed to update customer! Please try again. ";

            return View(customerDTO);
        }

        // GET: Registry/Customers/Delete/5
        public ActionResult Authorize(Guid id)
        {
            return View();
        }

        // POST: Registry/Customers/Delete/5
        [HttpPost]
        public ActionResult Authorize(int id, FormCollection collection)
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Details(Guid id)
        {
            var customer = await _channelService.FindCustomerAsync(id, _serviceHeader) ?? new CustomerDTO();

            return PartialView("_CustomerDetails", customer);
        }

        #endregion

        [NonAction]
        private List<SelectListItem> GetUserRoleInfo(string selectedValue, bool filterRoles = true)
        {
            var rolesInfo = new List<SelectListItem>();

            rolesInfo.Add(new SelectListItem { Value = "", Selected = selectedValue == string.Empty, Text = "--Select Role--" });

            var roles = _applicationRoleManager.Roles.ToArray();


            foreach (var role in roles)
            {
                if (role.RoleType == (byte)RoleType.InternalRole)
                    continue;

                rolesInfo.Add(new SelectListItem
                {
                    Text = role.Name,
                    Value = role.Name
                });
            }

            return rolesInfo;
        }

    }
}