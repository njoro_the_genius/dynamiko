﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcApplication.Areas.Registry.Models
{
    public class CustomerModel
    {
        #region primary details

        [Display(Name = "Customer Name")]
        public CustomerDTO Parent { get; set; }

        [Display(Name = "Customer Type")]
        public byte CustomerType { get; set; }


        #endregion

        #region Address

        [Display(Name = "Physical")]
        public string PhysicalAdderss { get; set; }

        [Display(Name = "Postal")]
        public string PostalCode { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Street")]
        public string EmailAddress { get; set; }

        [Display(Name = "Land Line")]
        public string LandLine { get; set; }

        [Display(Name = "Mobile Line")]
        public string MobileLine { get; set; }

        #endregion

        #region Users

        public List<ApplicationUserDTO> Users { get; set; }

        #endregion


    }
}