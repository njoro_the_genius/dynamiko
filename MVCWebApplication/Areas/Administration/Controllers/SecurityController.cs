﻿using Application.MainBoundedContext.DTO;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Services;
using WebApplication.Models;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Administration.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class SecurityController :  BaseController
    {
        private readonly ApplicationUserManager _applicationUserManager;

    private readonly IChannelService _channelService;

    private readonly ApplicationRoleManager _roleManager;

    private readonly ServiceHeader _serviceHeader;

    public SecurityController(
        IChannelService channelService,
         IWebConfigurationService webConfigurationService,
         ApplicationUserManager applicationUserManager,
         ApplicationRoleManager applicationRoleManager)
    {
        Guard.ArgumentNotNull(channelService, nameof(channelService));

        Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

        _channelService = channelService;

        _serviceHeader = webConfigurationService.GetServiceHeader();

        _roleManager = applicationRoleManager;

        _applicationUserManager = applicationUserManager;
    }

    [HttpGet]
    public async Task<ActionResult> Index(Guid id)
    {
        var modules = await GetModuleNavigationItemAsync(_channelService, _serviceHeader);

        var role = await _roleManager.FindByIdAsync(id.ToString());

        var selectedModules = await _channelService.FindModuleNavigationItemByRoleAsync(role.Id, _serviceHeader);

        var securityModel = new SecurityModel()
        {
            Id = id,
            Module = modules,
            Role = role,
            SelectedModules = selectedModules
        };

        return View(securityModel);
    }

    [HttpPost]
    public async Task<string> Index(Guid id, List<Guid> validGuids)
    {
        var modules = await GetModuleNavigationItemAsync(_channelService, _serviceHeader);

        var rolesInfo = new List<CheckBoxListInfo>();

        var roles = _roleManager.Roles.ToArray();

        var role = await _roleManager.FindByIdAsync(id.ToString());

        ViewBag.Roles = roles;

        ViewBag.Role = role;

        ViewBag.Id = id;

        Array.ForEach(roles, roleName => { rolesInfo.Add(new CheckBoxListInfo(roleName.Id, roleName.Name, false)); });

        ViewBag.RolesCheckBoxList = rolesInfo;

        ViewBag.Modules = modules;

        if (role != null)
        {
            var result = await _channelService.BulkInsertModuleNavigationItemAsync(validGuids, role.Id, _serviceHeader);

            if (result)
            {
                await _channelService.SeedSystemAccessRightAsync(role.Id, _serviceHeader);

                return "1";
            }

            return "0";
        }

        return "2";
    }
}
}