﻿using Application.MainBoundedContext.DTO;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Administration.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class RolesController : BaseController
    {
        private readonly ApplicationRoleManager _roleManager;

        private readonly ApplicationUserManager _applicationUserManager;

        public RolesController(
            ApplicationUserManager applicationUserManager,
            ApplicationRoleManager applicationRoleManager)
        {
            _roleManager = applicationRoleManager;

            _applicationUserManager = applicationUserManager;
        }

        // GET: Administration/Roles
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult RolesList(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var roles = _roleManager.Roles;

            var rolesList = new List<RoleDTO>();

            foreach (var items in roles)
            {
                List<ApplicationUserRole> applicationRoleUsers = _applicationUserManager.Users.SelectMany(a => a.Roles)
                .Where(b => b.RoleId == items.Id).ToList();

                bool hasUsers = applicationRoleUsers.Count > 0;

                rolesList.Add(new RoleDTO
                {
                    Id = Guid.Parse(items.Id),
                    Name = items.Name,
                    IsEnabled = items.IsEnabled,
                    Description = items.Description,
                    RoleType = items.RoleType,
                    RoleTypeDescription = EnumHelper.GetDescription((RoleType)items.RoleType)
                });
            }

            var searchRecordCount = 0;

            var totalRecordCount = 0;

            var searchText = jQueryDataTablesModel.sSearch.ToLower();

            if (!string.IsNullOrEmpty(searchText))
            {
                var filteredRoles = rolesList.Where(a => a.Name.Any(c => a.Name.ToLower().Contains(searchText))).ToList();

                rolesList = filteredRoles;
            }

            searchRecordCount = rolesList.Count;

            totalRecordCount = rolesList.Count;

            return this.DataTablesJson(rolesList, rolesList.Count, rolesList.Count, jQueryDataTablesModel.sEcho);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.RoleTypeSelectList = GetRoleTypeSelectList(string.Empty);

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoleDTO role)
        {
            ViewBag.RoleTypeSelectList = GetRoleTypeSelectList(role.RoleType.ToString());

            if (string.IsNullOrEmpty(role.Name))
            {
                TempData["ModelError"] = "Role name is required";

                return View("create");
            }

            var roleCreate = new ApplicationRole();

            roleCreate.Name = role.Name;

            roleCreate.IsEnabled = true;

            roleCreate.Description = role.Description;

            roleCreate.RoleType = role.RoleType;

            if (await _roleManager.RoleExistsAsync(roleCreate.Name))
            {
                TempData["ModelError"] = "Role exists already";

                return View();
            }

            var identityResult = await _roleManager.CreateAsync(roleCreate);

            if (identityResult != null)
            {
                TempData["Success"] = "role created successfully";

                return RedirectToAction("index");
            }

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Edit(string id)
        {
            var identityResult = await _roleManager.FindByIdAsync(id);

            if (identityResult == null)
            {
                TempData["Error"] = "Role not found";

                return RedirectToAction("index");
            }

            ViewBag.RoleTypeSelectList = GetRoleTypeSelectList(identityResult.RoleType.ToString());

            var roles = new RoleDTO
            {
                Id = Guid.Parse(id),
                Name = identityResult.Name,
                IsEnabled = identityResult.IsEnabled,
                RoleType = identityResult.RoleType,
                Description = identityResult.Description
            };

            return View("edit", roles);

        }

        [HttpPost]
        public async Task<ActionResult> Edit(RoleDTO role)
        {
            ViewBag.RoleTypeSelectList = GetRoleTypeSelectList(role.RoleType.ToString());

            if (string.IsNullOrEmpty(role.Name))
            {
                TempData["Error"] = "Role name is required.";

                return RedirectToAction("Roles");
            }

            var applicationRole = await _roleManager.FindByIdAsync(role.Id.ToString());

            if (applicationRole == null)
            {
                TempData["Error"] = "Unable to find a role with the specified id";

                return View("edit", role);
            }

            applicationRole.Name = role.Name;

            applicationRole.Description = role.Description;

            applicationRole.RoleType = role.RoleType;

            applicationRole.IsEnabled = role.IsEnabled;

            var updateResult = await _roleManager.UpdateAsync(applicationRole);

            if (updateResult == null)
            {
                TempData["Error"] = "Role could not be updated.Please try again";

                return View("edit", role);
            }

            TempData["Success"] = "Role updated successfully";

            return RedirectToAction("index");
        }

        [HttpGet]
        public ActionResult GetUserRoleInfo()
        {
            var rolesInfo = new List<SelectListItem>();

            rolesInfo.Add(new SelectListItem { Value = "", Selected = true, Text = "--Select Role--" });

            var roles = _roleManager.Roles.ToArray();

            Array.ForEach(roles,
                roleName =>
                {
                    rolesInfo.Add(new SelectListItem
                    {
                        Text = roleName.Name,
                        Value = roleName.Name
                    });
                });

            return new JsonResult() { Data = rolesInfo, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpGet]
        public ActionResult GetExternalUserRoleInfo()
        {
            var rolesInfo = new List<SelectListItem>();

            rolesInfo.Add(new SelectListItem { Value = "", Selected = true, Text = "--Select Role--" });

            var roles = _roleManager.Roles.ToArray();

            foreach (var role in roles)
            {
                if (role.RoleType == (byte)RoleType.InternalRole)
                    continue;

                rolesInfo.Add(new SelectListItem
                {
                    Text = role.Name,
                    Value = role.Name
                });
            }

            return new JsonResult() { Data = rolesInfo, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

    }
}