﻿using Application.MainBoundedContext.DTO;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Services;
using MvcApplication.Helpers;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MvcApplication.Attributes;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Microsoft.AspNet.Identity;

namespace MvcApplication.Areas.Administration.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        private readonly ApplicationUserManager _applicationUserManager;
        private readonly string _baseDirectoryPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data");
        private readonly IChannelService _channelService;
        private readonly ApplicationRoleManager _roleManager;
        private readonly ServiceHeader _serviceHeader;
        private readonly IWebConfigurationService _webConfigurationService;


        public UsersController(IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager applicationUserManager,
            ApplicationRoleManager applicationRoleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(applicationRoleManager, nameof(applicationRoleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _roleManager = applicationRoleManager;

            _applicationUserManager = applicationUserManager;
        }
        // GET: Administration/Users

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            Guid customerId = (Guid)currentUser.CustomerId;

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = new PageCollectionInfo<ApplicationUserDTO>();

            string wellKnownUserRolesDesc = EnumHelper.GetDescription((WellKnownUserRoles)1);

            if (roleName.Contains(wellKnownUserRolesDesc))
            {
                pageCollectionInfo = await _channelService.GetApplicationUserCollectionByFilterInPageAsync(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending, _serviceHeader);
            }
            else
            {
                pageCollectionInfo = await _channelService.GetApplicationUserCollectionByCustomerIdAsync(customerId, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending, _serviceHeader);
            }

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch)
                    ? pageCollectionInfo.PageCollection.Count
                    : totalRecordCount;

                return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
            }
            else
                return this.DataTablesJson(items: new List<ApplicationUserDTO> { }, totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }


        [HttpGet]
        public ActionResult Create()
        {
            var userInfoDTO = new ApplicationUserDTO();

            ViewBag.RolesList = GetUserRoleInfo(string.Empty);

            return View(userInfoDTO);
        }

        [HttpPost]
        public async Task<ActionResult> Create(ApplicationUserDTO applicationUserDTO)
        {
            ViewBag.RolesList = GetUserRoleInfo(applicationUserDTO.RoleName);

            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(applicationUserDTO);
            }

            var existingUser = await _applicationUserManager.FindByNameAsync(applicationUserDTO.UserName);

            if (existingUser != null && existingUser.CustomerId == applicationUserDTO.CustomerId)
            {
                TempData["Error"] = "Another user with the username " + applicationUserDTO.UserName + " already exists!";

                return View(applicationUserDTO);
            }else { }

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            applicationUserDTO.CreatedBy = currentUser.UserName;

            applicationUserDTO.CreatedDate = DateTime.Now;

            if (applicationUserDTO.CustomerId == null)
                applicationUserDTO.CustomerId = Guid.Empty;

            var created = await _channelService.CreateUserAsync(applicationUserDTO, _serviceHeader);

            if (created != null)
            {
                TempData["Success"] = "User details created successfully!";

                return RedirectToAction("Index");
            }

            TempData["Error"] = "Failed to create user!";

            return View(applicationUserDTO);
        }


        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            var applicationUser = await _applicationUserManager.FindByIdAsync(id.ToString());

            var role = applicationUser.Roles.FirstOrDefault();

            var selectedRole = new ApplicationRole();

            if (role != null)
                selectedRole = await _roleManager.FindByIdAsync(role.RoleId);

            var userInfoDTO = new ApplicationUserDTO()
            {
                UserName = applicationUser.UserName,
                Email = applicationUser.Email,
                RoleName = selectedRole == null ? string.Empty : selectedRole.Name,
                PhoneNumber = applicationUser.PhoneNumber,              
                CustomerId = applicationUser.CustomerId,
                LockoutEnabled = applicationUser.LockoutEnabled,
                LockoutEndDateUtc = applicationUser.LockoutEndDateUtc,
                IsExternalUser = applicationUser.IsExternalUser,
                CreatedDate = applicationUser.CreatedDate,
                IsEnabled = applicationUser.IsEnabled
            };

            ViewBag.RolesList = GetUserRoleInfo(selectedRole != null ? selectedRole.Name : string.Empty);

            return View(userInfoDTO);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ApplicationUserDTO applicationUserDTO)
        {

            ViewBag.RolesList = GetUserRoleInfo(applicationUserDTO.RoleName);

            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(applicationUserDTO);
            }

            var existingUser = await _applicationUserManager.FindByNameAsync(applicationUserDTO.UserName);

            if (existingUser != null && existingUser.Id != applicationUserDTO.Id)
            {
                TempData["Error"] = "Another user with the username " + applicationUserDTO.UserName + " already exists!";

                return View(applicationUserDTO);
            }

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            applicationUserDTO.RecordStatus = (byte)RecordStatus.Edited;

            applicationUserDTO.EditedBy = currentUser.UserName;

            applicationUserDTO.EditDate = DateTime.Now;

            var updated = await _channelService.UpdateAspnetIdentityUserAsync(applicationUserDTO, _serviceHeader);

            if (updated)
            {
                TempData["Success"] = "User details updated successfully!";

                return RedirectToAction("Index");
            }

            TempData["Error"] = "Failed to updated user details. Please try again!";

            return View(applicationUserDTO);
        }

        [HttpGet]
        public async Task<ActionResult> Approve(Guid id)
        {
            var applicationUser = await _applicationUserManager.FindByIdAsync(id.ToString());

            var role = applicationUser.Roles.FirstOrDefault();

            var selectedRole = await _roleManager.FindByIdAsync(role != null ? role.RoleId : string.Empty);


            var userInfoDTO = new ApplicationUserDTO()
            {
                UserName = applicationUser.UserName,
                Email = applicationUser.Email,
                Role = selectedRole != null ? (int)Enum.Parse(typeof(WellKnownUserRoles), selectedRole.Name, false) : 0,
                RoleName = selectedRole.Name,
                PhoneNumber = applicationUser.PhoneNumber,
                CustomerId = applicationUser.CustomerId,
                LockoutEnabled = applicationUser.LockoutEnabled,
                LockoutEndDateUtc = applicationUser.LockoutEndDateUtc,
                IsExternalUser = applicationUser.IsExternalUser,
                CreatedDate = applicationUser.CreatedDate,
                IsEnabled = applicationUser.IsEnabled
            };

            ViewBag.RolesList = GetUserRoleInfo(selectedRole != null ? selectedRole.Name : string.Empty);


            return View(userInfoDTO);
        }

        [HttpPost]
        public async Task<ActionResult> Approve(ApplicationUserDTO applicationUserDTO)
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(applicationUserDTO);
            }

            applicationUserDTO.RecordStatus = (byte)RecordStatus.Approved;

            applicationUserDTO.IsEnabled = true;

            applicationUserDTO.ApprovedBy = currentUser.UserName;

            applicationUserDTO.ApprovedDate = DateTime.Now;

            var updated = await _channelService.UpdateAspnetIdentityUserAsync(applicationUserDTO, _serviceHeader);

            ViewBag.RolesList = GetUserRoleInfo(applicationUserDTO.RoleName);

            if (updated)
            {
                TempData["Success"] = "User details approved successfully!";

                return RedirectToAction("Index");
            }

            TempData["Error"] = "Failed to approved user details. Please try again!";

            return View(applicationUserDTO);
        }

        [HttpGet]
        [ExcludeFilter(typeof(ForcePasswordChangeAttribute))]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ExcludeFilter(typeof(ForcePasswordChangeAttribute))]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            if (!ModelState.IsValid)
            {
                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));

                TempData["Error"] = messages;

                return View(changePasswordModel);
            }

            var applicationUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var applicationUserDTO = new ApplicationUserDTO()
            {
                UserName = applicationUser.UserName,
                Email = applicationUser.Email,
                PhoneNumber = applicationUser.PhoneNumber,
                CustomerId = applicationUser.CustomerId,
                LockoutEnabled = applicationUser.LockoutEnabled,
                LockoutEndDateUtc = applicationUser.LockoutEndDateUtc,
                IsExternalUser = applicationUser.IsExternalUser,
                CreatedDate = applicationUser.CreatedDate,
                IsEnabled = applicationUser.IsEnabled
            };



            IdentityResult identityResult = await _applicationUserManager.ChangePasswordAsync(applicationUser.Id, changePasswordModel.OldPassword, changePasswordModel.NewPassword);

            if (identityResult.Succeeded)
            {
                if (applicationUser != null)
                {
                    applicationUser.LastPasswordChangedDate = DateTime.Now;

                    await _applicationUserManager.UpdateAsync(applicationUser);

                    await _applicationUserManager.UpdateSecurityStampAsync(applicationUser.Id);
                    return RedirectToAction("Dashboard", "Dashboard", new { area = "" });
                }
                else
                {
                    TempData["Error"] = "Failed to change password! ";

                    return View(changePasswordModel);
                }
            }
            else
            {
                foreach (var i in identityResult.Errors)
                {
                    ModelState.AddModelError("IdentityResult", i);
                }

                string messages = string.Join("; ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));

                TempData["Error"] = messages;

                return View(changePasswordModel);
            }
        }
        #region Helpers
        [NonAction]
        private List<SelectListItem> GetUserRoleInfo(string selectedValue, bool filterRoles = true)
        {
            var rolesInfo = new List<SelectListItem>();

            rolesInfo.Add(new SelectListItem { Value = "", Selected = selectedValue == string.Empty, Text = "--Select Role--" });

            var roles = _roleManager.Roles.ToArray();

            Array.ForEach(roles,
                roleName =>
                {
                    rolesInfo.Add(new SelectListItem
                    {
                        Selected = roleName.Name == selectedValue,
                        Text = roleName.Name,
                        Value = roleName.Name
                    });
                });

            return rolesInfo;
        }
        #endregion
    }
}