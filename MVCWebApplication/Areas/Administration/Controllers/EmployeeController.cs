﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Administration.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator")]
    public class EmployeeController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        private readonly ApplicationRoleManager _applicationRoleManager;

        public EmployeeController(
           IChannelService channelService,
           IWebConfigurationService webConfigurationService,
           ApplicationUserManager userManager,
           ApplicationRoleManager roleManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            Guard.ArgumentNotNull(roleManager, nameof(roleManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;

            _applicationRoleManager = roleManager;
        }


        #region Employee

        // GET: Registry/Employee
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            _serviceHeader.ApplicationUserName = currentUser.UserName;

            Guid? customer = currentUser.CustomerId;

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc";

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var roleName = await _applicationUserManager.GetRolesAsync(currentUser.Id);

            var pageCollectionInfo = new PageCollectionInfo<EmployeeDTO>();

            var role = EnumHelper.GetDescription((WellKnownUserRoles)WellKnownUserRoles.SuperAdministrator);

            if (roleName.Contains(role))
            {
                pageCollectionInfo = await _channelService.FindCustomerEmployeesFilterInPageAsync(jQueryDataTablesModel.sSearch, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, sortAscending, _serviceHeader);
            }
            else
            {
                pageCollectionInfo = await _channelService.FindEmployeeByCustomerIdAsync(customer, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, false, _serviceHeader);
            }
            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(new List<EmployeeDTO>(), totalRecordCount, searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        // GET: Registry/Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Registry/Employee/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.DepartmentSelectListItems = GetDepartmentSelectListItems(string.Empty);

            return View();
        }

        // POST: Registry/Employee/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(EmployeeDTO employeeDTO)
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            ViewBag.DepartmentSelectListItems = GetDepartmentSelectListItems(string.Empty);

            var currentUser = await _applicationUserManager.FindByIdAsync(employeeDTO.UserId.ToString());

            employeeDTO.IsEnabled = true;

            var addNewEmployee = await _channelService.AddNewEmployeeAsync(employeeDTO, _serviceHeader);

            if (addNewEmployee != null && addNewEmployee.Item1 != null && addNewEmployee.Item2.Any())
            {
                TempData["Result"] = string.Join(",", addNewEmployee.Item2);

                return RedirectToAction("Index");
            }

            TempData["Error"] = addNewEmployee != null && addNewEmployee.Item2.Any() ? string.Join(" | ", addNewEmployee.Item2) : "Sorry, but an error has occured while creating customer-employee mapping.";

            return View(employeeDTO);
        }

        // GET: Registry/Employee/Edit/5
        [HttpGet]
        public async Task<ActionResult> Edit(Guid id)
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            var employee = await _channelService.FindEmployeeAsync(id, _serviceHeader) ?? new EmployeeDTO();

            return View(employee);
        }

        // POST: Registry/Employee/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EmployeeDTO employeeDTO)
        {
            ViewBag.CustomerTypesSelectList = GetCustomerTypesSelectList(string.Empty);

            if (!ModelState.IsValid)
            {
                TempData["Error"] = string.Join(" | ", ModelState.Values.SelectMany(a => a.Errors).Select(a => a.ErrorMessage));

                return View(employeeDTO);
            }

            var update = await _channelService.UpdateEmployeeAsync(employeeDTO, _serviceHeader);

            if (update)
            {
                TempData["Result"] = "Employee updated successfully!";

                return RedirectToAction("Index");
            }

            TempData["Error"] = "Failed to update employee! Please try again.";

            return View(employeeDTO);
        }

        [HttpGet]
        public async Task<ActionResult> Details(Guid id)
        {
            var employee = await _channelService.FindEmployeeAsync(id, _serviceHeader) ?? new EmployeeDTO();

            return PartialView("_employeeDetails", employee);
        }

        #endregion
    }
}