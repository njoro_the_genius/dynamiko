﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Controllers;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Messaging.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator,Standard User")]
    public class TextAlertsController : BaseController
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        public TextAlertsController(IChannelService channelService, IWebConfigurationService webConfigurationService)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();
        }

        // GET: Messaging/TextMessage

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var startDateString = Request["startDate"];

            var endDateString = Request["endDate"];

            string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy" };

            var startDate = DateTime.Today.AddDays(-14);

            var endDate = DateTime.Today;

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            if (DateTime.TryParseExact($"{startDateString}", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate)
              && DateTime.TryParseExact($"{endDateString}", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate))
            {
                var pageCollectionInfo =
               await _channelService.FindTextAlertsFilterInPageAsync(startDate, endDate, Guid.Empty, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending, _serviceHeader);

                if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
                {
                    totalRecordCount = pageCollectionInfo.ItemsCount;

                    searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch)
                        ? pageCollectionInfo.PageCollection.Count
                        : totalRecordCount;

                    return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount,
                        totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
                }

                else return this.DataTablesJson(items: new List<TextAlertDTO> { }, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(items: new List<TextAlertDTO> { }, totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }
    }
}