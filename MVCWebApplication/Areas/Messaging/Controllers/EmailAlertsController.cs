﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Attributes;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcApplication.Areas.Messaging.Controllers
{
    [Authorize(Roles = "Super Administrator,Administrator,Standard User")]
    public class EmailAlertsController : Controller
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        public EmailAlertsController(IChannelService channelService, IWebConfigurationService webConfigurationService)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _channelService = channelService;

        }

        // GET: Messaging/EmailMessage
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var pageCollectionInfo = await _channelService.FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(Guid.Empty, jQueryDataTablesModel.sSearch, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch)
                    ? pageCollectionInfo.PageCollection.Count
                    : totalRecordCount;

                return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
            }

            return this.DataTablesJson(items: new List<EmailAlertDTO> { }, totalRecords: totalRecordCount,
           totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }
    }
}