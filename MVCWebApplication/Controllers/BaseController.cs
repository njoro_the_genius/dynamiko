﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApplication.Models;

namespace MvcApplication.Controllers
{

    public class BaseController : Controller
    {
        private readonly ApplicationRoleManager _roleManager;

        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        public BaseController(IChannelService channelService, IWebConfigurationService webConfigurationService)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();
        }

        public BaseController()
        {
            var webConfigurationService = DependencyResolver.Current.GetService<IWebConfigurationService>();

            _roleManager = DependencyResolver.Current.GetService<ApplicationRoleManager>();
        }

        private static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        [NonAction]
        protected List<SelectListItem> GetRoleTypeSelectList(string selectedValue)
        {
            List<SelectListItem> roleTypeSelectList = new List<SelectListItem> { };

            var defaultSelectItem = new SelectListItem
            {
                Selected = (selectedValue == string.Empty),
                Text = @"Select Role Type",
                Value = string.Empty
            };

            roleTypeSelectList.Add(defaultSelectItem);

            var items = Enum.GetValues(typeof(RoleType)).Cast<RoleType>().Select(v => new SelectListItem
            {
                Text = StringExtensions.GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            roleTypeSelectList.AddRange(items);

            return roleTypeSelectList;
        }

        [NonAction]
        protected async Task<ControllerDTO> GetModuleNavigationItemAsync(IChannelService channelService,
           ServiceHeader serviceHeader)
        {
            var navigationItemDtos = await channelService.FindModuleNavigationItemsAsync(serviceHeader);

            var result = new ControllerDTO { ControllerCollection = new List<ControllerCollectionDTO>() };

            if (navigationItemDtos == null)
            {
                return null;
            }

            var groupResult = navigationItemDtos.GroupBy(x => x.ControllerName).Select(x => x.First());

            var itemDtos = groupResult as ModuleNavigationItemDTO[] ?? groupResult.ToArray();

            if (groupResult != null && groupResult.Any())
            {
                foreach (var item in itemDtos)
                {
                    result.Id = item.Id;

                    var actionList = navigationItemDtos.Where(x => x.ControllerName == item.ControllerName).ToList();

                    var actionDTOs = new List<ActionDTO>();

                    foreach (var action in actionList)
                    {
                        actionDTOs.Add(new ActionDTO()
                        {
                            Name = action.ActionName,
                            Id = action.Id
                        });
                    }

                    result.ControllerCollection.Add(new ControllerCollectionDTO()
                    {
                        ControllerName = item.ControllerName,
                        Id = item.Id,
                        ActionList = actionDTOs
                    });
                }
            }

            return result;
        }

        [NonAction]
        protected List<SelectListItem> GetCustomerTypesSelectList(string selectedValue)
        {
            List<SelectListItem> customerTypesSelectList = new List<SelectListItem> { };

            var defaultCustomerTypeItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = @"Choose Customer Type", Value = string.Empty };

            customerTypesSelectList.Add(defaultCustomerTypeItem);

            var items = Enum.GetValues(typeof(CustomerType)).Cast<CustomerType>().Select(v => new SelectListItem
            {
                Text = StringExtensions.GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            customerTypesSelectList.AddRange(items);

            return customerTypesSelectList;
        }

        [NonAction]
        protected async Task<List<SelectListItem>> GetCustomersSelectList(IChannelService channelService, ServiceHeader serviceHeader, string selectedValue)
        {
            Guard.ArgumentNotNull(channelService, "channelService");

            List<SelectListItem> customerSelectList = new List<SelectListItem> { };

            var defaultCompanyItem = new SelectListItem
            {
                Selected = (selectedValue == Guid.Empty.ToString("D")),
                Text = @"Select A customer",
                Value = Guid.Empty.ToString("D")
            };

            customerSelectList.Add(defaultCompanyItem);

            var Companies = (await channelService.FindCustomersAsync(serviceHeader));

            if (Companies != null)
            {
                foreach (var item in Companies)
                {
                    customerSelectList.Add(new SelectListItem
                    {
                        Selected = (item.Id == Guid.Parse(selectedValue == "" ? Guid.Empty.ToString() : selectedValue)),
                        Text = item.Name,
                        Value = item.Id.ToString("D")
                    });
                }
            }
            return customerSelectList;
        }


        [NonAction]
        protected List<SelectListItem> GetDepartmentSelectListItems(string selectedValue)
        {
            List<SelectListItem> departmentItems = new List<SelectListItem>()
            {
                new SelectListItem {Selected = selectedValue == string.Empty, Text = "--Department--", Value = ""}
            };

            var items = Enum.GetValues(typeof(Department)).Cast<Department>().Select(v => new SelectListItem
            {
                Text = GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            departmentItems.AddRange(items);

            return departmentItems;
        }

        protected List<SelectListItem> GetLeaveSelectList(string selectedValue)
        {
            List<SelectListItem> leaveTypesSelectList = new List<SelectListItem> { };

            var defaultLeaveTypeItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = @"Choose Leave Type", Value = string.Empty };

            leaveTypesSelectList.Add(defaultLeaveTypeItem);

            var items = Enum.GetValues(typeof(LeaveType)).Cast<LeaveType>().Select(v => new SelectListItem
            {
                Text = StringExtensions.GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            leaveTypesSelectList.AddRange(items);

            return leaveTypesSelectList;
        }

        protected List<SelectListItem> GetChargeType(string selectedValue)
        {
            List<SelectListItem> chargeTypeSelectList = new List<SelectListItem> { };

            var defaultLeaveTypeItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = @"Choose Charge Type", Value = string.Empty };

            chargeTypeSelectList.Add(defaultLeaveTypeItem);

            var items = Enum.GetValues(typeof(ChargeType)).Cast<ChargeType>().Select(v => new SelectListItem
            {
                Text = StringExtensions.GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            chargeTypeSelectList.AddRange(items);

            return chargeTypeSelectList;
        }

        protected List<SelectListItem> GetDeductionSelectList(string selectedValue)
        {
            List<SelectListItem> deductionTypesSelectList = new List<SelectListItem> { };

            var defaultLeaveTypeItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = @"Choose deduction Type", Value = string.Empty };

            deductionTypesSelectList.Add(defaultLeaveTypeItem);

            var items = Enum.GetValues(typeof(DeductionType)).Cast<DeductionType>().Select(v => new SelectListItem
            {
                Text = StringExtensions.GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            deductionTypesSelectList.AddRange(items);

            return deductionTypesSelectList;
        }

        [NonAction]
        protected List<SelectListItem> GetGenderSelectListItems(string selectedValue)
        {
            List<SelectListItem> genderList = new List<SelectListItem>()
            {
                new SelectListItem {Selected = selectedValue == string.Empty, Text = "--Gender--", Value = ""}
            };

            var items = Enum.GetValues(typeof(Gender)).Cast<Gender>().Select(v => new SelectListItem
            {
                Text = GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            genderList.AddRange(items);

            return genderList;
        }

        protected List<SelectListItem> GetFrequencySelectList(string selectedValue)
        {
            List<SelectListItem> frequencyTypesSelectList = new List<SelectListItem> { };

            var defaultfrequencyTypeItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = @"Choose Frequency", Value = string.Empty };

            frequencyTypesSelectList.Add(defaultfrequencyTypeItem);

            var items = Enum.GetValues(typeof(Frequency)).Cast<Frequency>().Select(v => new SelectListItem
            {
                Text = StringExtensions.GetEnumDescription(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).ToString() == selectedValue,
            }).ToList();

            frequencyTypesSelectList.AddRange(items);

            return frequencyTypesSelectList;
        }

        [HttpPost]
        public async Task<JsonResult> Branches(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var bankCode = Request["BankCode"];

            if (string.IsNullOrWhiteSpace(bankCode))
            {
                bankCode = ConfigurationManager.AppSettings["BankCode"];
            }

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc";

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.GetBranchesListFilterInPageAsync(bankCode, jQueryDataTablesModel.sSearch, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, true, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch)
                    ? pageCollectionInfo.PageCollection.Count
                    : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount,
                    searchRecordCount, jQueryDataTablesModel.sEcho);
            }
            else
                return this.DataTablesJson(new List<Branch> { }, totalRecordCount,
                    searchRecordCount, jQueryDataTablesModel.sEcho);
        }

        [HttpPost]
        public async Task<JsonResult> Banks(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc";

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = await _channelService.GetBanksListFilterInPageAsync(jQueryDataTablesModel.sSearch, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, true, _serviceHeader);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch)
                    ? pageCollectionInfo.PageCollection.Count
                    : totalRecordCount;

                return this.DataTablesJson(pageCollectionInfo.PageCollection, totalRecordCount,
                    searchRecordCount, jQueryDataTablesModel.sEcho);
            }
            else
                return this.DataTablesJson(new List<Bank> { }, totalRecordCount,
                    searchRecordCount, jQueryDataTablesModel.sEcho);
        }

    }
}