﻿using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.MainBoundedContext.Identity;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MvcApplication.Attributes;
using MvcApplication.Controllers;
using MvcApplication.Identity;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using static WebApplication.Models.AccountViewModels;

namespace DynamikoHRS.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(ForcePasswordChangeAttribute))]
    public class AccountController : BaseController
    {
        private readonly IChannelService _channelService;
        private readonly ApplicationUserManager _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private ServiceHeader serviceHeader;
        private readonly ApplicationSignInManager _signInManager;
        private readonly IAuthenticationManager _authenticationManager;

        public AccountController(
            IChannelService channelService,
            ApplicationUserManager userManager,
            RoleStore<IdentityRole> roleStore,
            ApplicationSignInManager signInManager,
            IAuthenticationManager authenticationManager)
        {
            _channelService = channelService;
            _userManager = userManager;
            _roleManager = new RoleManager<IdentityRole>(roleStore);
            _signInManager = signInManager;
            _authenticationManager = authenticationManager;
            serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            _userManager.RegisterTwoFactorProvider(TwoFactorProvidersCodes.EmailCode.ToString(), new EmailTokenProvider<ApplicationUser>());

        }
       
      
        public async Task<ActionResult> LockScreen()
        {
            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());

            ViewBag.Name = $"{currentUser.UserName}";

            ViewBag.UserName = currentUser.UserName;

            TempData["Username"] = currentUser.UserName;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LockScreen(AccountLockScreenModel viewModel, string returnUrl)
        {
            // Ensure we have a valid viewModel to work with
            //viewModel.Username = "su";
            string username = Request.Form["Username"];


            if (!ModelState.IsValid)
                return View(viewModel);



            // Require the user to have a confirmed email before they can log on.
            var user = await _userManager.FindByNameAsync(viewModel.Username);

            if (user != null)
            {
                if (user.RecordStatus != (int)RecordStatus.Approved)
                {
                    ModelState.AddModelError("", "Sorry, but your account is in invalid status.");

                    return View(viewModel);
                }

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                var signInStatus = await _signInManager.PasswordSignInAsync(viewModel.Username, viewModel.Password, false, shouldLockout: true);

                int passExpiryDays = 0;

                Int32.TryParse(ConfigurationManager.AppSettings["PasswordExpiryDays"], out passExpiryDays);

                switch (signInStatus)
                {
                    case SignInStatus.Success:
                        if (user.LastPasswordChangedDate != null && Convert.ToDateTime(user.LastPasswordChangedDate).AddDays(passExpiryDays) < DateTime.Now)
                        {
                            TempData["notification"] = "Your Password has Expired kindly reset";
                        }

                        //if the list exists, add this user to it
                        if (HttpRuntime.Cache["LoggedInUsers"] != null)
                        {
                            //get the list of logged in users from the cache
                            var loggedInUsers = (Dictionary<string, DateTime>)
                        HttpRuntime.Cache["LoggedInUsers"];

                            if (!loggedInUsers.ContainsKey(user.UserName))
                            {
                                //add this user to the list
                                loggedInUsers.Add(user.UserName, DateTime.Now);
                                //add the list back into the cache
                                HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;
                            }
                        }

                        //the list does not exist so create it
                        else
                        {
                            //create a new list
                            var loggedInUsers = new Dictionary<string, DateTime>();
                            //add this user to the list
                            loggedInUsers.Add(user.UserName, DateTime.Now);
                            //add the list into the cache
                            HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;
                        }

                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    //case SignInStatus.RequiresVerification:
                    //    //LoadAccessPermissions(viewModel.Username);
                    //    return RedirectToAction("TwoFactorAuthentication", new { ReturnUrl = returnUrl, RememberMe = false, cdx = user.Id, name = user.FirstName });
                    case SignInStatus.Failure:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(viewModel);
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(viewModel);
                }
            }

            ModelState.AddModelError("", "Invalid login attempt.");
            return View(viewModel);
        }

        [AllowAnonymous]
        public async Task<ActionResult> Lock()
        {
            var currentUser = await _userManager.FindByNameAsync(User.Identity.GetUserName());
            ViewBag.EmailAddress = currentUser.Email;

            ViewBag.Name = $"{currentUser.UserName}";

            return View(currentUser);
        }

        #region helpers

       

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            // If the return url starts with a slash "/" we assume it belongs to our site
            // so we will redirect to this "action"
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            // If we cannot verify if the url is local to our host we redirect to a default location
            return RedirectToAction("Dashboard", "Dashboard", new { Area = "" });
        }

        private ActionResult RedirectLocal(string returnUrl = "")
        {
            // If the return url starts with a slash "/" we assume it belongs to our site
            // so we will redirect to this "action"
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            // If we cannot verify if the url is local to our host we redirect to a default location
            return RedirectToAction("login", "account", new { Area = "Authentication" });
        }

        #endregion helpers
    }
}