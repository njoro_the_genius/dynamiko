﻿using Application.MainBoundedContext.DTO;
using DistributedServices.MainBoundedContext.Identity;
using Infrastructure.Crosscutting.Framework.Utils;
using MvcApplication.Helpers;
using MvcApplication.Services;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PyPay.WebApplication.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly IChannelService _channelService;

        private readonly ServiceHeader _serviceHeader;

        private readonly IWebConfigurationService _webConfigurationService;

        private readonly ApplicationUserManager _applicationUserManager;

        public DashboardController(
            IChannelService channelService,
            IWebConfigurationService webConfigurationService,
            ApplicationUserManager userManager)
        {
            Guard.ArgumentNotNull(channelService, nameof(channelService));

            Guard.ArgumentNotNull(webConfigurationService, nameof(webConfigurationService));

            Guard.ArgumentNotNull(userManager, nameof(userManager));

            _channelService = channelService;

            _webConfigurationService = webConfigurationService;

            _serviceHeader = webConfigurationService.GetServiceHeader();

            _applicationUserManager = userManager;
        }
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Dashboard()
        {
            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            var isSuperAdmin = User.Identity.IsUserInRole(new string[] { EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator) });

            var accountManager = await _channelService.FindApplicationUserAsync(currentUser.UserName, _serviceHeader);

            Guid customerId = isSuperAdmin ? Guid.Empty : accountManager?.CustomerId ?? Guid.Empty;

            int claim = isSuperAdmin ? 1 : 2;

            var dashboardWidgetDTO = await _channelService.FindDashboardWidgetsAsync(claim, customerId, _serviceHeader);

            Guid employeeId = Guid.Empty;

            employeeId = new Guid(currentUser.Id);

            int period = DateTime.Now.Month;

            //payslip data
            var pageCollectionInfo = await _channelService.GetPaySlipAsync(employeeId, period, _serviceHeader);

            if (pageCollectionInfo != null)
            {
                foreach (var item in pageCollectionInfo.PageCollection)
                {

                    var paySlip = new PaySlipDTO
                    {
                        Names = item.Names,

                        Unit = item.Unit,

                        Job = item.Job,

                        Station = item.Station,

                        EmployeePaySlip = item.EmployeePaySlip,

                        Company = item.Company,

                        Basicpay = item.Basicpay,

                        Grosspay = item.Grosspay,

                        LeaveAllowance = item.LeaveAllowance,

                        NetPay = item.NetPay,

                        AllowableDeductions = item.AllowableDeductions,

                        HealthInsurance = item.HealthInsurance,

                        NonCashBenefits = item.NonCashBenefits,

                        Pension = item.Pension,

                        Paye = item.Paye,

                        SaccoBenevoltFund = item.SaccoBenevoltFund,

                        SaccoSavings = item.SaccoSavings,

                        StaffWelfare = item.StaffWelfare,

                        PenisonWelfare = item.PenisonWelfare,

                        TaxablePay = item.TaxablePay,

                        TaxCharged = item.TaxCharged,

                        TaxRelief = item.TaxRelief,

                        TaxDeducted = item.TaxDeducted,

                        TotalDeductions = item.TotalDeductions,

                        PersonalRelief = item.PersonalRelief,

                        TaxableYPay = item.TaxableYPay,
                    };
                }

                ViewBag.PaySlip = pageCollectionInfo.PageCollection;
            }
            ViewBag.PaySlip = null;

            if (dashboardWidgetDTO != null)
            {
                ViewBag.TotalCustomers = dashboardWidgetDTO.TotalCustomer;

                ViewBag.CustomerName = dashboardWidgetDTO.CustomerName;

                ViewBag.TotalEMployees = dashboardWidgetDTO.TotalEMployees;

                ViewBag.TotalEmployeesOnLeave = dashboardWidgetDTO.TotalEmployeesOnLeave;

                ViewBag.TotalLeaveDays = dashboardWidgetDTO.TotalLeaveDays;

                ViewBag.TotalSalary = dashboardWidgetDTO.TotalSalary;

                ViewBag.TotalAverageSalary = dashboardWidgetDTO.TotalAverageSalary;
            }


            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetSalaryDashboardChart()
        {
            var user = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            Guid customerId = User.Identity.IsUserInRole(new string[] { WellKnownUserRoles.SuperAdministrator.ToString() }) ? Guid.Empty : user.CustomerId ?? Guid.Empty;

            int claim = User.Identity.IsUserInRole(new string[] { WellKnownUserRoles.SuperAdministrator.ToString() }) ? 1 : 2;

            var salaryDashboardChartDTO = await _channelService.GetDashboardGraphDataSetAsync(claim, customerId, _serviceHeader);

            //return Json(salaryDashboardChartDTO, JsonRequestBehavior.AllowGet);
            return new JsonResult() { Data = GetGraphDataSet(salaryDashboardChartDTO), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private DashboardGraphDataSetDTO GetGraphDataSet(List<DashboardGraphDTO> dashboardGraphDTOs)
        {
            var graphDataSet = new DashboardGraphDataSetDTO();

            var Quarter1 = new object[dashboardGraphDTOs.Count];
            var Quarter2 = new object[dashboardGraphDTOs.Count];
            var Quarter3 = new object[dashboardGraphDTOs.Count];
            var Quarter4 = new object[dashboardGraphDTOs.Count];

            var i = 0;

            foreach (var item in dashboardGraphDTOs)
            {
                var set = new object[2];

                set[0] = item.YearNo;

                set[1] = item.Quarter1;

                Quarter1[i] = set;

                var set2 = new object[2];

                set2[0] = item.YearNo;

                set2[1] = item.Quarter2;

                Quarter2[i] = set2;

                var set3 = new object[2];

                set3[0] = item.YearNo;

                set3[1] = item.Quarter3;

                Quarter3[i] = set3;

                var set4 = new object[2];

                set4[0] = item.YearNo;

                set4[1] = item.Quarter4;

                Quarter4[i] = set4;

                i++;
            }

            graphDataSet.Quater1 = Quarter1;

            graphDataSet.Quater2 = Quarter2;

            graphDataSet.Quater3 = Quarter3;

            graphDataSet.Quater4 = Quarter4;

            return graphDataSet;
        }

        [HttpGet]
        public async Task<List<PaySlipDTO>> PaySlip(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var startDateString = Request["startDate"];

            var endDateString = Request["endDate"];

            var customerIdString = Request["employeeId"];

            Guid employeeId = Guid.Empty;

            var currentUser = await _applicationUserManager.FindByNameAsync(User.Identity.Name);

            employeeId = new Guid(currentUser.Id);

            string[] formats = { "dd/MM/yyyy", "dd-MM-yyyy" };

            var startDate = DateTime.Today.AddDays(-14);

            int period = 0;

            if (period > 0 && DateTime.TryParseExact($"{startDateString}", formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate))
            {

                period = startDate.Month;

                var isSuperAdmin = User.Identity.IsUserInRole(new string[] { EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator) });

                int claim = isSuperAdmin ? 1 : 2;

                var pageCollectionInfo = await _channelService.GetPaySlipAsync(employeeId, period, _serviceHeader);

                if (pageCollectionInfo == null || !pageCollectionInfo.PageCollection.Any())
                    return new List<PaySlipDTO>();

                return pageCollectionInfo.PageCollection;
            }

            return new List<PaySlipDTO>();
        }

    }
}