# dynamiko HRM

Rich internet solution (RIA) designed on domain driven design (DDD) to handle human resource management- such as employee management,payroll, leave application,task management and payments integrations - mpesa and pesalink integration to disburse payments

## Technologies

The application is written in **Asp.Net MVC


Others:

- ASP.NET 
- EntityFramework
- ASP.NET Identity
- Bogus
- AutoMapper
- Serilog
-windows service - utilizing Managed Extensibility Framework (MEF)
-- Design patterns impelmented: 
-Factory design pattern
-Specification design pattern
-Chain of responsibility design pattern

### Running in Visual Studio

- Set Startup projects:
  - MvcApplication
  - DistributedServices.MainBoundedContext
  -Urion.WindowService
  
## EF & Data Access

- The solution uses this `DbContext`:

  - `ApplicationDbContext`
  
### Database providers:

- SqlServer

## Logging

- We are using `Serilog` with pre-definded following Sinks - white are available in `web.config`:

  - Console
  - File
  - MSSqlServer
  

## Email service

- It is possible to set up emails via:

### SendGrid

In WindowsService  project - in `app.config`:emailAlertDispatcherConfiguration section
```
"SendgridConfiguration": {
        "ApiKey"= "",
        "SourceEmail"= "",
        "SourceName"= ""
    }
```

### SMTP

```
"SmtpConfiguration": {
        "Host"= "",
        "username"= "",
        "Password"= ""
    }
```

**Identity Resources**

- Actions: Add, Update, Remove
- Entities:
  - Identity Claims
  - Identity Properties

## Asp.Net Identity

**Users**

- Actions: Add, Update, Delete
- Entities:
  - User Roles
  - User Logins
  - User Claims

**Roles**

- Actions: Add, Update, Delete
- Entities:
  - Role Claims