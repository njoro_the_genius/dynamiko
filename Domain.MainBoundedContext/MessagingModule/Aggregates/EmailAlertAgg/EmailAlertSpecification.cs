﻿using System;
using Domain.Seedwork.Specification;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg
{
    public static class EmailAlertSpecifications
    {
        public static Specification<EmailAlert> DefaultSpec()
        {
            Specification<EmailAlert> specification = new TrueSpecification<EmailAlert>();

            return specification;
        }

        public static Specification<EmailAlert> EmailAlertWithCustomer(Guid? CustomerId)
        {
            return new DirectSpecification<EmailAlert>(x => x.CustomerId == CustomerId);
        }

        public static Specification<EmailAlert> EmailAlertWithDLRStatus(int dlrStatus)
        {
            return new DirectSpecification<EmailAlert>(x => x.MailMessage.DLRStatus == dlrStatus);
        }

        public static Specification<EmailAlert> EmailAlertWithDLRStatus(Guid? CustomerId, int dlrStatus)
        {
            return new DirectSpecification<EmailAlert>(x => x.MailMessage.DLRStatus == dlrStatus && x.CustomerId == CustomerId);
        }

        public static Specification<EmailAlert> EmailAlertsWithRecipientOrSubjectOrBody(string searchText)
        {
            Specification<EmailAlert> specification = new TrueSpecification<EmailAlert>();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                var toSpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.To.Contains(searchText));

                var ccSpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.CC.Contains(searchText));

                var subjectSpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.Subject.Contains(searchText));

                var bodySpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.Body.Contains(searchText));

                specification &= toSpecification | ccSpecification | subjectSpecification | bodySpecification;
            }

            return specification;
        }

        public static Specification<EmailAlert> EmailAlertsWithRecipientOrSubjectOrBody(Guid? CustomerId, string searchText)
        {
            Specification<EmailAlert> specification = new DirectSpecification<EmailAlert>(x => x.CustomerId == CustomerId);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                var toSpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.To.Contains(searchText));

                var ccSpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.CC.Contains(searchText));

                var subjectSpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.Subject.Contains(searchText));

                var bodySpecification = new DirectSpecification<EmailAlert>(x => x.MailMessage.Body.Contains(searchText));

                specification &= toSpecification | ccSpecification | subjectSpecification | bodySpecification;
            }

            return specification;
        }
    }
}
