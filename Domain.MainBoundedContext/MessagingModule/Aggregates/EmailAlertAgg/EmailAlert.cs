﻿using Domain.MainBoundedContext.RegistryModule;
using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg
{
    public class EmailAlert : Entity
    {
        public Guid? CustomerId { get; set; }

        public virtual Customer Customer { get; private set; }

        public virtual MailMessage MailMessage { get; set; }
    }
}
