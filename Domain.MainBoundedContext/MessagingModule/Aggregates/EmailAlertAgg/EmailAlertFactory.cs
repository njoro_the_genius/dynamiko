﻿using System;
using Domain.MainBoundedContext.ValueObjects;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg
{
    public static class EmailAlertFactory
    {
        public static EmailAlert CreateEmailAlert(Guid? customerId, MailMessage mailMessage)
        {
            var emailMessage = new EmailAlert();

            emailMessage.GenerateNewIdentity();

            emailMessage.CustomerId = (customerId != null && customerId != Guid.Empty) ? customerId : null;

            emailMessage.MailMessage = mailMessage;

            return emailMessage;
        }
    }
}
