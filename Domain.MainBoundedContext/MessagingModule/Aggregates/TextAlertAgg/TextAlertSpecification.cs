﻿using System;
using System.Data.Entity.SqlServer;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Extensions;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.TextAlertAgg
{
    public static class TextAlertSpecification
    {
        public static Specification<TextAlert> DefaultSpecification()
        {
            Specification<TextAlert> defaultSpecification = new TrueSpecification<TextAlert>();

            return defaultSpecification;
        }

        public static Specification<TextAlert> TextAlertWithDlrStatus(int status)
        {
            Specification<TextAlert> dlrStatuSpecification = new TrueSpecification<TextAlert>();

            dlrStatuSpecification &= new DirectSpecification<TextAlert>(s => s.TextMessage.DlrStatus == status);

            return dlrStatuSpecification;
        }
        public static Specification<TextAlert> TextAlertWithDlrStatusAndOrigin(int status, int origin)
        {
            Specification<TextAlert> specification = new TrueSpecification<TextAlert>();

            specification &= new DirectSpecification<TextAlert>(a => a.TextMessage.Origin == origin && a.TextMessage.DlrStatus == status);

            return specification;
        }

        public static Specification<TextAlert> TextAlertWithRecipientMobileNumber(string mobileNumber, string text)
        {
            Specification<TextAlert> specification = new TrueSpecification<TextAlert>();

            specification &= new DirectSpecification<TextAlert>(a => a.TextMessage.Recipient == mobileNumber);

            if (!String.IsNullOrWhiteSpace(text))
            {
                text = text.SanitizePatIndexInput();
                var toSpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Recipient) > 0);
                var subjectSpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Reference) > 0);
                var bodySpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Body) > 0);

                specification &= (toSpec | subjectSpec | bodySpec);
            }

            return specification;
        }

        public static Specification<TextAlert> TextWithDrlStatusAndText(int dlrStatus, string text)
        {
            Specification<TextAlert> specification = new DirectSpecification<TextAlert>(x => x.TextMessage.DlrStatus == dlrStatus);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var toSpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Recipient) > 0);
                var subjectSpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Reference) > 0);
                var bodySpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Body) > 0);

                specification &= (toSpec | subjectSpec | bodySpec);
            }

            return specification;
        }

        public static Specification<TextAlert> TextAlertWithDatesAndText(DateTime startDate, DateTime endDate, string text)
        {
            Specification<TextAlert> specification = new DirectSpecification<TextAlert>(x => /*x.TextMessage.DlrStatus == dlrStatus &&*/ System.Data.Entity.DbFunctions.TruncateTime(x.CreatedDate) >= System.Data.Entity.DbFunctions.TruncateTime(startDate) && System.Data.Entity.DbFunctions.TruncateTime(x.CreatedDate) <= System.Data.Entity.DbFunctions.TruncateTime(endDate));

            if (!String.IsNullOrWhiteSpace(text))
            {
                text = text.SanitizePatIndexInput();

                var toSpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Recipient) > 0);

                var subjectSpec = new DirectSpecification<TextAlert>(c => SqlFunctions.PatIndex(text, c.TextMessage.Reference) > 0);

                specification &= (toSpec | subjectSpec);
            }

            return specification;
        }

       
    }
}
