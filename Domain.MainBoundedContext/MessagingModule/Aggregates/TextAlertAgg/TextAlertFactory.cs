﻿using System;
using Domain.MainBoundedContext.ValueObjects;

namespace Domain.MainBoundedContext.MessagingModule.Aggregates.TextAlertAgg
{
    public static class TextAlertFactory
    {
        public static TextAlert CreateTextAlert(TextMessage textMessage, string createdBy)
        {
            TextAlert textAlert = new TextAlert { TextMessage = textMessage, CreatedDate = DateTime.Now };

            textAlert.GenerateNewIdentity();

            textAlert.CreatedBy = createdBy;

            return textAlert;
        }
    }
}
