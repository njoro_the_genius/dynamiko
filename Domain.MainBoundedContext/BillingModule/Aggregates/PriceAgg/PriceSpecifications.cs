﻿using Domain.Seedwork.Specification;
using System;

namespace Domain.MainBoundedContext.BillingModule.Aggregates.PriceAgg
{
    public static class PriceSpecifications
    {
        public static Specification<Price> DefaultSpec(string text)
        {
            Specification<Price> specification = new TrueSpecification<Price>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var nameSpec = new DirectSpecification<Price>(c => c.Name.Contains(text));

                specification &= (nameSpec);
            }

            return specification;
        }
    }
}
