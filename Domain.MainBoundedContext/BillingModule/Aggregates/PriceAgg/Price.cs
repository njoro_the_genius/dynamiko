﻿using Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.BillingModule.Aggregates.PriceAgg
{
    public class Price : Entity
    {
        public string Name { get; set; }

        public byte PizzaSize { get; set; }

        public decimal Amount { get; set; }

        HashSet<PriceItem> _priceItems;

        public virtual ICollection<PriceItem> PriceItems
        {
            get
            {
                if (_priceItems == null)
                    _priceItems = new HashSet<PriceItem>();

                return _priceItems;
            }
            set
            {
                _priceItems = new HashSet<PriceItem>(value);
            }
        }

        public PriceItem AddNewPriceItem(byte topping, decimal amount)
        {
            var priceItem = new PriceItem
            {
                PriceId = this.Id,
                Topping=topping,
                Amount=amount,
                CreatedDate = this.CreatedDate,
                CreatedBy = this.CreatedBy
            };

            priceItem.GenerateNewIdentity();

            this.PriceItems.Add(priceItem);

            return priceItem;

        }

        
    }
}
