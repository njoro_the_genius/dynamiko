﻿using System;

namespace Domain.MainBoundedContext.BillingModule.Aggregates.PriceAgg
{
    public static class PriceFactory
    {
        public static Price CreatePrice(string name, byte pizzaSiza, decimal amount, string createdBy)
        {
            var price = new Price();
            price.GenerateNewIdentity();
            price.Name = name;
            price.PizzaSize = pizzaSiza;
            price.Amount = amount;
            price.CreatedBy = createdBy;
            price.CreatedDate = DateTime.Now;
            return price;

        }

        public static PriceItem CreatePriceItem(Guid priceId,byte topping, decimal amount, string createdBy)
        {
            var priceItem = new PriceItem
            {
                PriceId = priceId,
                Topping = topping,
                Amount = amount,
                CreatedDate = DateTime.Now,
                CreatedBy = createdBy
            };

            priceItem.GenerateNewIdentity();

            return priceItem;

        }
    }
}
