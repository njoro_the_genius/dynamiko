﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.BillingModule.Aggregates.PriceAgg
{
    public class PriceItem: Entity
    {
        public Guid PriceId { get; set; }

        public virtual Price Price { get; set; }

        public byte Topping { get; set; }

        public decimal Amount { get; set; }
    }
}
