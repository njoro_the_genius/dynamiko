﻿using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using System;
using System.Collections.Generic;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.TaxAgg
{
    public class TaxOrder : Entity
    {
        public string Description { get; set; }
        public bool IsActive { get; set; }

        HashSet<TaxGraduatedScale> _graduatedScales;

        public virtual ICollection<TaxGraduatedScale> GraduatedScales
        {
            get
            {
                if (_graduatedScales == null)
                    _graduatedScales = new HashSet<TaxGraduatedScale>();
                return _graduatedScales;
            }
            private set => _graduatedScales = new HashSet<TaxGraduatedScale>(value);
        }

        public TaxGraduatedScale AddNewGraduatedScale(Guid taxOrderId, byte type, decimal value, Range range, string createdBy)
        {
            var graduatedScale = new TaxGraduatedScale();

            graduatedScale.GenerateNewIdentity();

            graduatedScale.TaxOrderId = taxOrderId;

            graduatedScale.Type = type;

            graduatedScale.TaxValue = value;

            graduatedScale.Range = range;

            graduatedScale.CreatedBy = createdBy;

            GraduatedScales.Add(graduatedScale);

            return graduatedScale;
        }

        public override string ToString()
        {
            return "Tax, Name : " + Description;
        }
    }
}
