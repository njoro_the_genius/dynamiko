﻿using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.TaxAgg
{
    public class TaxGraduatedScale : Entity
    {
        public Guid TaxOrderId { get; set; }

        public virtual TaxOrder TaxOrder { get; set; }

        public virtual Range Range { get; set; }

        public byte Type { get; set; }

        public decimal TaxValue { get; set; }
    }
}
