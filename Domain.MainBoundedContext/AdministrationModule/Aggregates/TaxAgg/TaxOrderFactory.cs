﻿using Domain.MainBoundedContext.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.TaxAgg
{
    public class TaxOrderFactory
    {
        public static TaxOrder CreateTaxOrder(string description, bool isActive, string createdBy)
        {
            TaxOrder taxOrder = new TaxOrder();

            taxOrder.GenerateNewIdentity();

            taxOrder.Description = description;

            taxOrder.IsActive = isActive;

            taxOrder.CreatedBy = createdBy;

            taxOrder.CreatedDate = DateTime.Now;

            return taxOrder;
        }

        public static TaxGraduatedScale AddNewGraduatedScale(Guid taxOrderId, byte type, decimal value, Range range, string createdBy)
        {
            var graduatedScale = new TaxGraduatedScale();

            graduatedScale.GenerateNewIdentity();

            graduatedScale.TaxOrderId = taxOrderId;

            graduatedScale.Type = type;

            graduatedScale.TaxValue = value;

            graduatedScale.Range = range;

            graduatedScale.CreatedBy = createdBy;

            return graduatedScale;
        }
    }
}
