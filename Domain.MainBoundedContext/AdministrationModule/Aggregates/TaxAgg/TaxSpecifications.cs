﻿using Domain.Seedwork.Specification;
using System;
using System.Data.Entity.SqlServer;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.TaxAgg
{
    public class TaxSpecifications
    {
        public static ISpecification<TaxOrder> DefaultSpec()
        {
            return new TrueSpecification<TaxOrder>();
        }

        public static Specification<TaxOrder> WithNameLike(string text)
        {
            return new DirectSpecification<TaxOrder>(c => SqlFunctions.PatIndex(text, c.Description) > 0);
        }

        public static Specification<TaxOrder> TaxOrderWithFullText(string text)
        {
            Specification<TaxOrder> specification = new TrueSpecification<TaxOrder>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var nameSpec = new DirectSpecification<TaxOrder>(c => SqlFunctions.PatIndex(text, c.Description) > 0);

                specification = (nameSpec);
            }

            return specification;
        }

        public static Specification<TaxOrder> TaxOrderWithFullTextAndIsEnabled(string text, bool isEnabled)
        {
            Specification<TaxOrder> specification = new TrueSpecification<TaxOrder>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var nameSpec = new DirectSpecification<TaxOrder>(c => SqlFunctions.PatIndex(text, c.Description) > 0);

                specification &= (nameSpec);
            }

            var isEnabledSpec = new DirectSpecification<TaxOrder>(c => c.IsActive == isEnabled);

            return (specification & isEnabledSpec);
        }

        public static ISpecification<TaxGraduatedScale> GraduatedScaleWithTaxId(Guid taxOrderId)
        {
            Specification<TaxGraduatedScale> specification = new TrueSpecification<TaxGraduatedScale>();

            if (taxOrderId != Guid.Empty)
            {
                specification &= new DirectSpecification<TaxGraduatedScale>(x => x.TaxOrderId == taxOrderId);
            }

            return specification;
        }

        public static Specification<TaxOrder> ActiveSpec()
        {
            return new DirectSpecification<TaxOrder>(c => c.IsActive);
        }
    }
}
