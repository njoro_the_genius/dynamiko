﻿using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.DeductionsAgg
{
    public static class DeductionFactory
    {
        public static Deduction Create(Guid employeeId, int deductionType, decimal amount, int frequency,bool isActive,DateTime startDate,DateTime enddate)
        {
            var deduction = new Deduction();
            
            deduction.EmployeeId = employeeId;
            deduction.DeductionType = deductionType;
            deduction.Amount = amount;
            deduction.Frequency = frequency;
            deduction.IsActive = isActive;
            deduction.CreatedDate = DateTime.Now;
            deduction.StartDate = startDate;
            deduction.EndDate = enddate;

            return deduction;
        }
    }
}
