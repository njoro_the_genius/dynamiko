﻿using Domain.Seedwork.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.DeductionsAgg
{
    public static class DeductionSpecification
    {
        public static Specification<Deduction> DefaultSpec(string text)
        {
            Specification<Deduction> specification = new TrueSpecification<Deduction>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var createdSpec = new DirectSpecification<Deduction>(c => c.CreatedBy.Contains(text));

                var descriptionSpec = new DirectSpecification<Deduction>(c => c.Employee.FirstName.Contains(text));

                specification &= (createdSpec | descriptionSpec);
            }
            return specification;
        }

        public static Specification<Deduction> ActiveSpec()
        {
            return new DirectSpecification<Deduction>(c => c.IsActive);
        }

        public static Specification<Deduction> GetDeductionsByCustomerId(string text,Guid customerId)
        {
            Specification<Deduction> specification = DefaultSpec(text);
            var customerSpec= new DirectSpecification<Deduction>(c => c.Employee.CustomerId ==customerId);
            specification &= (customerSpec);
            return specification;
        }

        public static Specification<Deduction> GetDeductionsByCustomerIdInDateRange(string text, Guid customerId,Guid employeeId, DateTime? startDate, DateTime? endDate)
        {
            Specification<Deduction> specification = DefaultSpec(text);
            var customerSpec = new DirectSpecification<Deduction>(c => c.Employee.CustomerId == customerId);
            var employeeSpec = new DirectSpecification<Deduction>(c => c.EmployeeId == employeeId && c.StartDate > (startDate ?? DateTime.MinValue) && c.EndDate < (endDate ?? DateTime.MaxValue));
            specification &= (customerSpec | employeeSpec);
            return specification;
        }

    }
}
