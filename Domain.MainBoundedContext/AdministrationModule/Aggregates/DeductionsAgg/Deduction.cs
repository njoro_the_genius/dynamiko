﻿using Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.DeductionsAgg
{
    public class Deduction : Entity
    {
        public Guid EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public int DeductionType { get; set; }
        public decimal Amount { get; set; }
        public int Frequency { get; set; }
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}
