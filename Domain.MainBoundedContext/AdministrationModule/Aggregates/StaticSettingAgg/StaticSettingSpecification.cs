﻿using Domain.Seedwork.Specification;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.SettingAgg
{
    public class StaticSettingSpecification
    {
        public static Specification<StaticSetting> DefaultSpecification()
        {
            Specification<StaticSetting> specification = new TrueSpecification<StaticSetting>();

            return specification;
        }

        public static Specification<StaticSetting> SettingByKey(string text) 
        {
            Specification<StaticSetting> specification = new TrueSpecification<StaticSetting>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var keySpec = new DirectSpecification<StaticSetting>(c => c.Key.Contains(text.ToString()));

                specification &= (keySpec);
            }

            return specification;
        }
    }
}
