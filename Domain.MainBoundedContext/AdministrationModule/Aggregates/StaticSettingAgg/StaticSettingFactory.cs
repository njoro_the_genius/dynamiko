﻿using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.SettingAgg
{
    public class StaticSettingFactory
    {
        public static StaticSetting CreateSetting(string key, string value, string createdBy)
        {
            StaticSetting setting = new StaticSetting();

            setting.GenerateNewIdentity();

            setting.Key = !string.IsNullOrWhiteSpace(key) ? key.ToUpper() : key;

            setting.Value = value;

            setting.CreatedBy = createdBy;

            setting.CreatedDate = DateTime.Now;

            return setting;
        }
    }
}
