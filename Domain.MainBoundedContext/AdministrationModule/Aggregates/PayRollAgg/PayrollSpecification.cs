﻿using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.PayRollAgg
{
    public static class PayrollSpecification
    {
        public static Specification<PayRoll> DefaultSpec()
        {
            Specification<PayRoll> specification = new TrueSpecification<PayRoll>();

            return specification;
        }

        public static Specification<PayRollItem> PayRollItemByPayrollIdSpec(Guid payrollId)
        {
            Specification<PayRollItem> specification = new DirectSpecification<PayRollItem>(c => c.PayRollId == payrollId);

            return specification;
        }

        public static Specification<PayRollItem> PayRollItemByEmployeeIdSpec(Guid employeeId)
        {
            Specification<PayRollItem> specification = new DirectSpecification<PayRollItem>(c => c.EmployeeId == employeeId);

            return specification;
        }

        public static Specification<PayRoll> UnProcessedPayrollSpec()
        {
            Specification<PayRoll> specification = new DirectSpecification<PayRoll>(c => c.Status == (int)PayRollStatus.UnProcessed && c.RecordStatus == (byte)RecordStatus.Approved);

            return specification;
        }

        public static Specification<PayRoll> PayRollByCustomerId(Guid customerId)
        {
            Specification<PayRoll> specification = new DirectSpecification<PayRoll>(c => c.CustomerId == customerId);

            return specification;
        }
    }
}
