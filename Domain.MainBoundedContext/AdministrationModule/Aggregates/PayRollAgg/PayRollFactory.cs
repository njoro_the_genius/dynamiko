﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.PayRollAgg
{
    public static class PayRollFactory
    {
        public static PayRoll Create(Guid customerId,decimal grossPay, decimal nonCashBenefits, decimal pension, decimal totalPayee, decimal allowableDeductions, decimal netPay,decimal healthInsurance, DateTime startDate, DateTime endDate,int status,bool isQueued,string createdBy,byte recordStatus)
        {
            var payRoll = new PayRoll();

            payRoll.GenerateNewIdentity();

            payRoll.CustomerId = customerId;

            payRoll.TotalGrosspay = grossPay;

            payRoll.IsQueued = payRoll.IsQueued;

            payRoll.TotalNonCashBenefits = nonCashBenefits;

            payRoll.TotalPayee = totalPayee;

            payRoll.TotalPension = pension;

            payRoll.TotalAllowableDeductions = allowableDeductions;

            payRoll.TotalNetPay = netPay;

            payRoll.TotalHealthInsurance = healthInsurance;

            payRoll.Status = status;

            payRoll.StartDate = startDate;

            payRoll.EndDate = endDate;

            payRoll.CreatedBy = createdBy;

            payRoll.CreatedDate = DateTime.Now;

            payRoll.RecordStatus = recordStatus;

            return payRoll;
        }
    }
}
