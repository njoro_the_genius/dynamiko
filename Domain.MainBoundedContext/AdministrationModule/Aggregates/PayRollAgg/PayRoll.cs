﻿using Domain.MainBoundedContext.RegistryModule;
using Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.PayRollAgg
{
    public class PayRoll : Entity
    {
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public decimal TotalGrosspay { get; set; }

        public decimal TotalNonCashBenefits { get; set; }

        public decimal TotalPension { get; set; }

        public decimal TotalHealthInsurance { get; set; }

        public decimal TotalPayee { get; set; }

        public decimal TotalAllowableDeductions { get; set; }

        public decimal TotalNetPay { get; set; }

        public int Status { get; set; }

        public bool IsQueued { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        HashSet<PayRollItem> _payRollItems;

        public virtual ICollection<PayRollItem> PayRollItems
        {
            get
            {
                if (_payRollItems == null)
                    _payRollItems = new HashSet<PayRollItem>();

                return _payRollItems;
            }
            set
            {
                _payRollItems = new HashSet<PayRollItem>(value);
            }
        }

        public PayRollItem Create(Guid employeeId, decimal grossPay, decimal nonCashBenefits, decimal pension, decimal payee, decimal allowableDeductions, decimal netPay, decimal healthInsurance, string createdBy)
        {
            var payRoll = new PayRollItem();

            payRoll.GenerateNewIdentity();

            payRoll.PayRollId = this.Id;

            payRoll.EmployeeId = employeeId;

            payRoll.Payee = payee;

            payRoll.Grosspay = grossPay;

            payRoll.NonCashBenefits = nonCashBenefits;

            payRoll.Pension = pension;

            payRoll.AllowableDeductions = allowableDeductions;

            payRoll.NetPay = netPay;

            payRoll.HealthInsurance = healthInsurance;

            payRoll.CreatedBy = createdBy;

            payRoll.CreatedDate = DateTime.Now;

            this.PayRollItems.Add(payRoll);

            return payRoll;
        }
    }
}
