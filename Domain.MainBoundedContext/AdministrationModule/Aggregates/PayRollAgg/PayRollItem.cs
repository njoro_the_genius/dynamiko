﻿using Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.PayRollAgg
{
    public class PayRollItem :Entity
    {
        public Guid PayRollId { get; set; }

        public virtual PayRoll PayRoll { get; set; }

        public Guid EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }

        public decimal Grosspay { get; set; }

        public decimal NonCashBenefits { get; set; }

        public decimal HealthInsurance { get; set; }

        public decimal Pension { get; set; }

        public decimal Payee { get; set; }

        public decimal AllowableDeductions { get; set; }

        public decimal NetPay { get; set; }
    }
}
