﻿using Domain.Seedwork;
using System;
using System.Collections.Generic;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NSSFAgg
{
    public class NssfOrder : Entity
    {
        public string Description { get; set; }

        public bool IsActive { get; set; }

        HashSet<NssfGraduatedScale> _nssfGraduatedScales;

        public virtual ICollection<NssfGraduatedScale> NssfGraduatedScales
        {
            get
            {
                if (_nssfGraduatedScales == null)
                    _nssfGraduatedScales = new HashSet<NssfGraduatedScale>();

                return _nssfGraduatedScales;
            }
            set
            {
                _nssfGraduatedScales = new HashSet<NssfGraduatedScale>(value);
            }
        }

        public NssfGraduatedScale AddNewNssfGraduatedScale(Guid nssfOrderId, decimal lowerLimit, decimal upperLimit, byte type, decimal value, string createdBy)
        {
            var nssfGraduatedScale = new NssfGraduatedScale();
            nssfGraduatedScale.GenerateNewIdentity();
            nssfGraduatedScale.NssfOrderId = nssfOrderId;
            nssfGraduatedScale.LowerLimit = lowerLimit;
            nssfGraduatedScale.UpperLimit = upperLimit;
            nssfGraduatedScale.Type = type;
            nssfGraduatedScale.Value = value;
            nssfGraduatedScale.CreatedBy = createdBy;
            nssfGraduatedScale.CreatedDate = DateTime.Now;
            this.NssfGraduatedScales.Add(nssfGraduatedScale);
            return nssfGraduatedScale;
        }
    }
}
