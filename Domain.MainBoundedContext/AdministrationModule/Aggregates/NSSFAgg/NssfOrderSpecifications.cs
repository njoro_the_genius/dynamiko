﻿using Domain.Seedwork.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NSSFAgg
{
    public static class NssfOrderSpecifications
    {
        public static Specification<NssfOrder> DefaultSpec(string text)
        {
            Specification<NssfOrder> specification = new TrueSpecification<NssfOrder>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var createdSpec = new DirectSpecification<NssfOrder>(c => c.CreatedBy.Contains(text));

                var descriptionSpec = new DirectSpecification<NssfOrder>(c => c.Description.Contains(text));

                specification &= (createdSpec| descriptionSpec  );
            }

            return specification;
        }

        public static Specification<NssfOrder> ActiveSpec()
        {
            return new DirectSpecification<NssfOrder>(c => c.IsActive);
        }
    }
}
