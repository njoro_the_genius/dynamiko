﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NSSFAgg
{
    public static class NssfOrderFactory
    {
        public static NssfOrder GetNssfOrder(string description, bool isActive, byte recordStatus, string createdBy)
        {
            var nssfOrder = new NssfOrder();
            nssfOrder.GenerateNewIdentity();
            nssfOrder.Description = description;
            nssfOrder.IsActive = isActive;
            nssfOrder.RecordStatus = recordStatus;
            nssfOrder.CreatedBy = createdBy;
            nssfOrder.CreatedDate = DateTime.Now;
            return nssfOrder;
        }

        public static NssfGraduatedScale GetNewNssfGraduatedScale(Guid nssfOrderId, decimal lowerLimit, decimal upperLimit, byte type, decimal value, string createdBy)
        {
            var nssfGraduatedScale = new NssfGraduatedScale();
            nssfGraduatedScale.GenerateNewIdentity();
            nssfGraduatedScale.NssfOrderId = nssfOrderId;
            nssfGraduatedScale.LowerLimit = lowerLimit;
            nssfGraduatedScale.UpperLimit = upperLimit;
            nssfGraduatedScale.Type = type;
            nssfGraduatedScale.Value = value;
            nssfGraduatedScale.CreatedBy = createdBy;
            nssfGraduatedScale.CreatedDate = DateTime.Now;            
            return nssfGraduatedScale;
        }
    }
}
