﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NSSFAgg
{
    public class NssfGraduatedScale: Entity
    {
        public Guid NssfOrderId { get; set; }

        public virtual NssfOrder NssfOrder { get; set; }

        public decimal LowerLimit { get; set; }

        public decimal UpperLimit { get; set; }

        public byte Type { get; set; }

        public decimal Value { get; set; }
    }
}
