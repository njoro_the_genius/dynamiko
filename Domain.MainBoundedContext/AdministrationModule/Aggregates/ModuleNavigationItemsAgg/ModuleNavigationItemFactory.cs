﻿using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemAgg
{
    public static class ModuleNavigationItemFactory
    {
        public static ModuleNavigationItem AddModuleNavigationItem(string controllerName, string actionName, string createdBy)
        {
            var moduleNavigationItem = new ModuleNavigationItem();

            moduleNavigationItem.GenerateNewIdentity();

            moduleNavigationItem.ControllerName = controllerName;

            moduleNavigationItem.ActionName = actionName;

            moduleNavigationItem.CreatedBy = createdBy;

            moduleNavigationItem.CreatedDate = DateTime.Now;

            return moduleNavigationItem;
        }
    }
}
