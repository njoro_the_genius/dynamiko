﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemAgg
{
    public class ModuleNavigationItem : Entity
    {
        public string ControllerName { get; set; }

        public string ActionName { get; set; }
    }
}
