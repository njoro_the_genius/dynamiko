﻿using Domain.Seedwork.Specification;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemAgg
{
    public static class ModuleNavigationItemSpecifications
    {
        public static Specification<ModuleNavigationItem> DefaultSpecification()
        {
            Specification<ModuleNavigationItem> specification = new TrueSpecification<ModuleNavigationItem>();

            return specification;
        }

        public static Specification<ModuleNavigationItem> ModuleNavigationItemWithComtrollerAndActionNames(string controllerName, string actionName)
        {
            Specification<ModuleNavigationItem> specification = new TrueSpecification<ModuleNavigationItem>();

            if (string.IsNullOrEmpty(controllerName) || string.IsNullOrWhiteSpace(actionName)) return specification;
            Specification<ModuleNavigationItem> controllerNameSpec = new DirectSpecification<ModuleNavigationItem>(c => c.ControllerName.Equals(controllerName) && c.ActionName.Equals(actionName));

            specification &= controllerNameSpec;

            return specification;
        }

        public static Specification<ModuleNavigationItem> ModuleNavigationItemWithControllerName(string controllerName)
        {
            Specification<ModuleNavigationItem> specification = new TrueSpecification<ModuleNavigationItem>();

            if (string.IsNullOrEmpty(controllerName)) return specification;
            Specification<ModuleNavigationItem> controllerNameSpec =
                new DirectSpecification<ModuleNavigationItem>(c => c.ControllerName.Equals(controllerName));

            specification &= controllerNameSpec;

            return specification;
        }
    }
}
