﻿using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.LeaveAgg
{
    public static class LeaveFactory
    {
        public static Leave Create(Guid employeeId, int period, DateTime dateApplied, DateTime dateApproved, DateTime startDate, DateTime endDate, string approvedBy, string reason, string createdBy,byte recordStatus)
        {
            var leave = new Leave();
            leave.GenerateNewIdentity();
            leave.EmployeeId = employeeId;
            leave.Period = period;
            leave.DateApplied = dateApplied;
            leave.DateApproved = dateApproved;
            leave.StartDate = startDate;
            leave.EndDate = endDate;
            leave.RecordStatus = recordStatus;
            leave.ApprovedBy = approvedBy;
            leave.Reason = reason;
            leave.CreatedBy = createdBy;
            leave.CreatedDate = DateTime.Now;
            return leave;
        }
    }

}