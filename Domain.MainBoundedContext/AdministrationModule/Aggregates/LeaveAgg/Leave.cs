﻿using Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.LeaveAgg
{
    public class Leave : Entity
    {
        public Guid EmployeeId { get;  set; }
        public virtual Employee Employee { get; set; }
        public int Period { get; set; }
        public DateTime DateApplied { get; set; }
        public DateTime DateApproved { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ApprovedBy { get; set; }
        public string Reason { get; set; }
        public byte LeaveType { get; set; }
    }
}
