﻿using Domain.Seedwork.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.LeaveAgg
{
    public static class LeaveSpecification
    {
        public static Specification<Leave> DefaultSpec(string text)
        {
            Specification<Leave> specification = new TrueSpecification<Leave>();

            return specification;
        }

        public static Specification<Leave> GetLeaveByCustomerId(string text, Guid customerId)
        {
            Specification<Leave> specification = new TrueSpecification<Leave>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var defaultSpec = DefaultSpec(text);

                specification &= (defaultSpec);
            }

            var customerSpec = new DirectSpecification<Leave>(c => c.Employee.CustomerId == customerId);

            return (specification & customerSpec);
        }

        public static Specification<Leave> GetLeaveByEmployeeId(string text, Guid employeeId)
        {
            Specification<Leave> specification = new TrueSpecification<Leave>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var defaultSpec = DefaultSpec(text);

                specification &= (defaultSpec);
            }

            var customerSpec = new DirectSpecification<Leave>(c => c.Employee.UserId == employeeId);

            specification &= (customerSpec);

            return specification;
        }

        public static Specification<Leave> GetLeaveByEmployeeId(Guid employeeId)
        {
            return new DirectSpecification<Leave>(c => c.Employee.UserId == employeeId);
        }
    }
}
