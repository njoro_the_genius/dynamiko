﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NHIFAgg
{
    public static class NhifOrderFactory
    {
        public static NhifOrder GetNhifOrder(string description, bool isActive, byte recordStatus, string createdBy)
        {
            var nhifOrder = new NhifOrder();
            nhifOrder.GenerateNewIdentity();
            nhifOrder.Description = description;
            nhifOrder.IsActive = isActive;
            nhifOrder.RecordStatus = recordStatus;
            nhifOrder.CreatedBy = createdBy;
            nhifOrder.CreatedDate = DateTime.Now;
            return nhifOrder;
        }

        public static NhifGraduatedScale GetNewNhifGraduatedScale(Guid nhifOrderId, decimal lowerLimit, decimal upperLimit, byte type, decimal value, string createdBy)
        {
            var nhifGraduatedScale = new NhifGraduatedScale();
            nhifGraduatedScale.GenerateNewIdentity();
            nhifGraduatedScale.NhifOrderId = nhifOrderId;
            nhifGraduatedScale.LowerLimit = lowerLimit;
            nhifGraduatedScale.UpperLimit = upperLimit;
            nhifGraduatedScale.Type = type;
            nhifGraduatedScale.Value = value;
            nhifGraduatedScale.CreatedBy = createdBy;
            nhifGraduatedScale.CreatedDate = DateTime.Now;
            return nhifGraduatedScale;
        }
    }
}
