﻿using Domain.Seedwork;
using System;
using System.Collections.Generic;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NHIFAgg
{
    public class NhifOrder : Entity
    {
        public string Description { get; set; }

        public bool IsActive { get; set; }

        HashSet<NhifGraduatedScale> _nhifGraduatedScales;

        public virtual ICollection<NhifGraduatedScale> NhifGraduatedScales
        {
            get
            {
                if (_nhifGraduatedScales == null)
                    _nhifGraduatedScales = new HashSet<NhifGraduatedScale>();

                return _nhifGraduatedScales;
            }
            set
            {
                _nhifGraduatedScales = new HashSet<NhifGraduatedScale>(value);
            }
        }

        public NhifGraduatedScale AddNewNhifGraduatedScale(Guid nhifOrderId, decimal lowerLimit, decimal upperLimit, byte type, decimal value,string createdBy)
        {
            var nhifGraduatedScale = new NhifGraduatedScale();
            nhifGraduatedScale.GenerateNewIdentity();
            nhifGraduatedScale.NhifOrderId = nhifOrderId;
            nhifGraduatedScale.LowerLimit = lowerLimit;
            nhifGraduatedScale.UpperLimit = upperLimit;
            nhifGraduatedScale.Type = type;
            nhifGraduatedScale.Value = value;
            nhifGraduatedScale.CreatedBy = createdBy;
            nhifGraduatedScale.CreatedDate = DateTime.Now;
            this.NhifGraduatedScales.Add(nhifGraduatedScale);
            return nhifGraduatedScale;
        }

    }
}
