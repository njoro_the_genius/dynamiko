﻿using Domain.Seedwork.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NHIFAgg
{
    public class NhifOrderSpecification
    {
        public static Specification<NhifOrder> DefaultSpec(string text)
        {
            Specification<NhifOrder> specification = new TrueSpecification<NhifOrder>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var createdSpec = new DirectSpecification<NhifOrder>(c => c.CreatedBy.Contains(text));

                var descriptionSpec = new DirectSpecification<NhifOrder>(c => c.Description.Contains(text));

                specification &= (createdSpec | descriptionSpec);
            }

            return specification;
        }

        public static Specification<NhifOrder> ActiveSpec()
        {
            return new DirectSpecification<NhifOrder>(c => c.IsActive);
        }
    }
}
