﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.NHIFAgg
{
    public class NhifGraduatedScale : Entity
    {
        public Guid NhifOrderId { get; set; }

        public virtual NhifOrder NhifOrder { get; set; }

        public decimal LowerLimit { get; set; }

        public decimal UpperLimit { get; set; }

        public byte Type { get; set; }

        public decimal Value { get; set; }
    }
}
