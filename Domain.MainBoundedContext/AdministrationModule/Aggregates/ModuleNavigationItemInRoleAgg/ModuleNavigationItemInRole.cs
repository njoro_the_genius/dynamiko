﻿using Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemAgg;
using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemInRoleAgg
{
    public class ModuleNavigationItemInRole : Entity
    {
        public Guid ModuleNavigationItemId { get; set; }

        public virtual ModuleNavigationItem ModuleNavigationItem { get; set; }

        public string RoleId { get; set; }

        public void SetModuleNavigationItemId(Guid moduleNavigationItemId)
        {
            if (moduleNavigationItemId != Guid.Empty)
            {
                ModuleNavigationItemId = moduleNavigationItemId;
            }
        }
    }
}
