﻿using System;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemInRoleAgg
{
    public class ModuleNavigationItemInRoleFactory
    {
        public static ModuleNavigationItemInRole AddModuleNavigationItemInRole(Guid moduleNavigationItemId, string roleName, string createdBy)
        {
            var moduleNavigationItemInRole = new ModuleNavigationItemInRole();

            moduleNavigationItemInRole.GenerateNewIdentity();

            moduleNavigationItemInRole.SetModuleNavigationItemId(moduleNavigationItemId);

            moduleNavigationItemInRole.RoleId = roleName;

            moduleNavigationItemInRole.CreatedBy = createdBy;

            moduleNavigationItemInRole.CreatedDate = DateTime.Now;

            return moduleNavigationItemInRole;
        }
    }
}
