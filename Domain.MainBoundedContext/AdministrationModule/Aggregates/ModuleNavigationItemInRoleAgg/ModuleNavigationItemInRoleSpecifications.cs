﻿using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Extensions;
using System;
using System.Data.Entity.SqlServer;

namespace Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemInRoleAgg
{
    public static class ModuleNavigationItemInRoleSpecifications
    {
        public static Specification<ModuleNavigationItemInRole> DefaultSpecification()
        {
            Specification<ModuleNavigationItemInRole> specification = new TrueSpecification<ModuleNavigationItemInRole>();

            return specification;
        }

        public static Specification<ModuleNavigationItemInRole> ModuleNavigationItemWithRoleId(string roleId)
        {
            Specification<ModuleNavigationItemInRole> specification = new TrueSpecification<ModuleNavigationItemInRole>();

            if (string.IsNullOrEmpty(roleId)) return specification;

            Specification<ModuleNavigationItemInRole> controllerNameSpec =
                new DirectSpecification<ModuleNavigationItemInRole>(c => c.RoleId.Equals(roleId));

            specification &= controllerNameSpec;

            return specification;
        }

        public static Specification<ModuleNavigationItemInRole> ModuleNavigationItemWithItemIdAndRole(Guid itemId, string roleId)
        {
            Specification<ModuleNavigationItemInRole> specification = new TrueSpecification<ModuleNavigationItemInRole>();

            specification &= new DirectSpecification<ModuleNavigationItemInRole>(x => x.ModuleNavigationItemId == itemId && x.RoleId.Equals(roleId));

            return specification;
        }

        public static Specification<ModuleNavigationItemInRole> ModuleNavigationItemWithRoleIdAndModuleNavigationId(
            Guid moduleNavigationId)
        {
            Specification<ModuleNavigationItemInRole> specification = new TrueSpecification<ModuleNavigationItemInRole>();

            specification &= new DirectSpecification<ModuleNavigationItemInRole>(a => a.ModuleNavigationItemId == moduleNavigationId);

            return specification;
        }

        public static Specification<ModuleNavigationItemInRole> ModuleNavigationItemWithRoleIdAndModuleNavigationId(string roleId,
            Guid moduleNavigationId)
        {
            Specification<ModuleNavigationItemInRole> specification = new TrueSpecification<ModuleNavigationItemInRole>();

            specification &= new DirectSpecification<ModuleNavigationItemInRole>(a => a.ModuleNavigationItemId == moduleNavigationId && a.RoleId.Equals(roleId));

            return specification;
        }

        public static Specification<ModuleNavigationItemInRole> ModuleNavigationItemWithRoleIdAndControllerNameAndRoleId(string controllerName, string roleId)
        {
            Specification<ModuleNavigationItemInRole> specification = new TrueSpecification<ModuleNavigationItemInRole>();

            specification &= new DirectSpecification<ModuleNavigationItemInRole>(a => a.ModuleNavigationItem.ControllerName.Equals(controllerName) && a.RoleId.Equals(roleId));

            return specification;
        }
    }
}
