﻿using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using System.Collections.Generic;

namespace Domain.MainBoundedContext.RegistryModule
{
    public class Customer : Entity
    {
        public string Name { get; set; }

        public byte Type { get; set; }

        public virtual Address Address { get; set; }

        public bool IsEnabled { get; set; }

        public string LogoPath { get; set; }

        public override string ToString()
        {
            return "Customer, Name : " + Name;
        }

        public void Enable()
        {
            if (!IsEnabled)
                IsEnabled = true;
        }

        public void Disable()
        {
            if (IsEnabled)
                IsEnabled = false;
        }
    }
}
