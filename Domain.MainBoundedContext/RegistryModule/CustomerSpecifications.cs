﻿using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Extensions;
using System;
using System.Data.Entity.SqlServer;

namespace Domain.MainBoundedContext.RegistryModule
{
    public class CustomerSpecifications
    {

        public static ISpecification<Customer> DefaultSpec()
        {
            return new TrueSpecification<Customer>();
        }

        public static Specification<Customer> WithNameLike(string text)
        {
            return new DirectSpecification<Customer>(c => SqlFunctions.PatIndex(text, c.Name) > 0);
        }

        public static Specification<Customer> CustomerWithFullText(string text)
        {
            Specification<Customer> specification = new TrueSpecification<Customer>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var nameSpec = new DirectSpecification<Customer>(c => SqlFunctions.PatIndex(text, c.Name) > 0);

                var mobileNoSpec = new DirectSpecification<Customer>(c => SqlFunctions.PatIndex(text, c.Address.MobileLine) > 0);

                var emailSpec = new DirectSpecification<Customer>(c => SqlFunctions.PatIndex(text, c.Address.Email) > 0);

                specification = (nameSpec | mobileNoSpec | emailSpec);
            }

            return specification;
        }

        public static Specification<Customer> CustomerWithFullTextAndIsEnabled(string text, bool isEnabled)
        {
            Specification<Customer> specification = new TrueSpecification<Customer>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var nameSpec = new DirectSpecification<Customer>(c => SqlFunctions.PatIndex(text, c.Name) > 0);

                var mobileNoSpec = new DirectSpecification<Customer>(c => SqlFunctions.PatIndex(text, c.Address.MobileLine) > 0);

                var emailSpec = new DirectSpecification<Customer>(c => SqlFunctions.PatIndex(text, c.Address.Email) > 0);

                specification &= (nameSpec | mobileNoSpec | emailSpec);
            }

            var isEnabledSpec = new DirectSpecification<Customer>(c => c.IsEnabled == isEnabled);

            return (specification & isEnabledSpec);
        }

    }
}
