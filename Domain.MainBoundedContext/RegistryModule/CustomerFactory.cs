﻿using Domain.MainBoundedContext.ValueObjects;
using System;

namespace Domain.MainBoundedContext.RegistryModule
{
    public class CustomerFactory
    {
        public static Customer CreateCustomer(string name, byte type, Address address, byte recordStatus, bool isEnabled,string logoPath)
        {
            var customer = new Customer();

            customer.GenerateNewIdentity();

            customer.Address = address;

            customer.Type = type;

            customer.Name = name;

            customer.RecordStatus = recordStatus;

            customer.CreatedDate = DateTime.UtcNow;

            customer.IsEnabled = isEnabled;

            customer.LogoPath = logoPath;

            return customer;
        }
    }
}
