﻿using System;

namespace Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg
{
    public class EmployeeFactory
    {
        public static Employee CreateEmployee(Guid? customerId, Guid userId, string userName, int departmentId, string bankAccount, decimal grossSalary,int IdNumber,string KRAPin, string NHIFNumber, string NSSFNumber, byte recordStatus, bool isEnabled, bool DeductNSSFContribution, bool DeductNHIFContribution, string BankCode, string BranchCode, string AccountName, string JobNumber, DateTime DateOfEmployment, string JobTitle, int gender,DateTime dateOfBirth, string firstName, string otherNames, string PhoneNumber, string email,bool isExternalUser, int allocatedLeaveDays)
        {
            var employeeUser = new Employee();

            employeeUser.GenerateNewIdentity();

            employeeUser.SetTheCustomerForThisEmployee(customerId);

            employeeUser.UserId = userId;

            employeeUser.IsExternalUser = isExternalUser;

            employeeUser.UserName = userName;

            employeeUser.FirstName = firstName;

            employeeUser.OtherNames = otherNames;

            employeeUser.Gender = gender;

            employeeUser.DateOfBirth = dateOfBirth;

            employeeUser.DepartmentId = departmentId;

            employeeUser.PhoneNumber = PhoneNumber;

            employeeUser.IdNumber = IdNumber;

            employeeUser.Email = email;

            employeeUser.KRAPin = KRAPin;

            employeeUser.NHIFNumber = NHIFNumber;

            employeeUser.NSSFNumber = NSSFNumber;

            employeeUser.BankAccount = bankAccount;

            employeeUser.GrossSalary = grossSalary;

            employeeUser.RecordStatus = recordStatus;

            employeeUser.CreatedDate = DateTime.UtcNow;

            employeeUser.IsEnabled = isEnabled;

            employeeUser.DeductNSSFContribution = DeductNSSFContribution;

            employeeUser.DeductNHIFContribution = DeductNHIFContribution;

            employeeUser.BankCode = BankCode;

            employeeUser.BranchCode = BranchCode;

            employeeUser.AccountName = AccountName;

            employeeUser.JobNumber = JobNumber;

            employeeUser.DateOfEmployment = DateOfEmployment;

            employeeUser.JobTitle = JobTitle;

            employeeUser.AllocatedLeaveDays = allocatedLeaveDays;

            return employeeUser;
        }
    }
}
