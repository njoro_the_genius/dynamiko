﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg
{
    public class Employee : Entity
    {

        public Guid? CustomerId { get; private set; }
        public virtual Customer Customer { get; set; }
        public Guid UserId { get; set; }

        public string FirstName { get; set; }

        public string OtherNames { get; set; }

        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int DepartmentId { get; set; }

        public int IdNumber { get; set; }

        public string KRAPin { get; set; }

        public string NHIFNumber { get; set; }

        public string NSSFNumber { get; set; }

        public string BankAccount { get; set; }

        public Decimal GrossSalary { get; set; }

        public string UserName { get; set; }

        public int Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public bool DeductNSSFContribution { get; set; }

        public bool DeductNHIFContribution { get; set; }

        public string BankCode { get; set; }
        public bool IsExternalUser { get; set; }
        public string BranchCode { get; set; }
        public string AccountName { get; set; }
        public string JobNumber { get; set; }

        public DateTime DateOfEmployment { get; set; }
        public string JobTitle { get; set; }

        public bool IsEnabled { get; set; }

        public int AllocatedLeaveDays { get; set; }

        public void Enable()
        {
            if (!IsEnabled)
                IsEnabled = true;
        }

        public void Disable()
        {
            if (IsEnabled)
                IsEnabled = false;
        }

        public override string ToString()
        {
            return "Employee Primary Account Number : " + BankAccount;
        }

        public void SetTheCustomerForThisEmployee(Guid? id)
        {
            if (id == null || id == Guid.Empty)
            {
                CustomerId = null;

                Customer = null;

            }
            else
            {
                CustomerId = id;

            }

            
        }
    }
}
