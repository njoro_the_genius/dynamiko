﻿using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg
{
    public class EmployeeSpecifications
    {
        public static ISpecification<Employee> DefaultSpec(string text)
        {
            Specification<Employee> specification = new TrueSpecification<Employee>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                var customerName = new DirectSpecification<Employee>(c => c.Customer.Name.Contains(text));

                var bankAccount = new DirectSpecification<Employee>(c => c.BankAccount.Contains(text));

                specification &= (customerName | bankAccount);
            }

            return new TrueSpecification<Employee>();
        }
        public static Specification<Employee> MatchCustomerEmployeeWithUniqueCustomerId(Guid? customerId)
        {
            return new DirectSpecification<Employee>(x => x.CustomerId == customerId);
        }

        public static Specification<Employee> MatchCustomerEmployeeWithUniqueCustomerIdAndAreEnabled(Guid? customerId)
        {
            return new DirectSpecification<Employee>(x => x.CustomerId == customerId && x.IsEnabled );
        }

        public static Specification<Employee> EnabledEmployees()
        {
            return new DirectSpecification<Employee>(c => c.IsEnabled);
        }

        public static Specification<Employee> CustomerIdAndUserIdSpec(Guid? customerId, Guid userId)
        {
            return new DirectSpecification<Employee>(c => c.CustomerId == customerId && c.UserId == userId);
        }

        public static Specification<Employee> EmployeesWithMoreAllocatedLeaveDaysSpec(int allocatedLeaveDays)
        {
            return new DirectSpecification<Employee>(c => c.AllocatedLeaveDays >= allocatedLeaveDays);
        }

        public static Specification<Employee> EmployeesWithLessAllocatedLeaveDaysSpec(int allocatedLeaveDays)
        {
            return new DirectSpecification<Employee>(c => c.AllocatedLeaveDays <= allocatedLeaveDays);
        }

        public static Specification<Employee> EmployeesWithCustomerIds(List<Guid> customerIds)
        {
            var id = customerIds.ElementAt(0);

            Specification<Employee> specification = new DirectSpecification<Employee>(x => x.CustomerId == id);

            customerIds.RemoveAt(0);

            foreach (var customerId in customerIds)
            {
                specification |= new DirectSpecification<Employee>(x => x.CustomerId == customerId);
            }

            return specification;
        }

        

    }
}
