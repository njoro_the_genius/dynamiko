﻿using Domain.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class CommissionCharge : ValueObject<CommissionCharge>
    {
        public byte Mode { get; private set; }

        public decimal? PayeeToPayPartRate { get; private set; }

        public decimal? CustomerToPayPartRate { get; private set; }

        public CommissionCharge(byte mode,decimal? payeeToPayPartRate, decimal? customerToPayPartRate)
        {
            Mode = mode;

            if (mode == (byte)CommissionChargeMode.CustomerToPayPart)
            {
                PayeeToPayPartRate = payeeToPayPartRate;
                CustomerToPayPartRate = customerToPayPartRate;
            }
        }

        private CommissionCharge() { }
    }
}
