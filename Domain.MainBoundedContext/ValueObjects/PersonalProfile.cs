﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class PersonalProfile : ValueObject<PersonalProfile>
    {
        public int? CreditReferenceNumber { get; set; }

        public string Salutution { get; set; }

        public string Surname { get; set; }

        public string OtherNames { get; set; }

        public string FullNames { get; set; }

        public string NationalId { get; set; }

        public string Nationality { get; set; }

        public string PassportNumber { get; set; }

        public string DrivingLicenseNumber { get; set; }

        public string ServiceId { get; set; }

        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public string AlienId { get; set; }

        public string Pin { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public PersonalProfile(int creditReferenceNumber, string salutution, string surname,
          string otherNames, string fullNames, string nationalId, string passportNumber,
          string drivingLicenseNumber, string serviceId, string alienId, DateTime? dateOfBirth)
        {
            CreditReferenceNumber = creditReferenceNumber;
            Salutution = salutution;
            Surname = surname;
            OtherNames = otherNames;
            FullNames = fullNames;
            NationalId = nationalId;
            PassportNumber = passportNumber;
            DrivingLicenseNumber = drivingLicenseNumber;
            ServiceId = serviceId;
            AlienId = alienId;
            DateOfBirth = dateOfBirth;
        }

        public PersonalProfile()
        {
                
        }
    }
}
