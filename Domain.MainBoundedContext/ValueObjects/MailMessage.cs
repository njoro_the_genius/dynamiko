﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class MailMessage : ValueObject<MailMessage>
    {
        public string From { get; private set; }

        public string To { get; private set; }

        public string CC { get; private set; }

        public string Subject { get; private set; }

        public string Body { get; private set; }

        public bool IsBodyHtml { get; private set; }

        public byte DLRStatus { get; private set; }

        public byte Origin { get; private set; }

        public byte Priority { get; private set; }

        public byte SendRetry { get; private set; }

        public bool SecurityCritical { get; private set; }

        public string AttachmentFilePath { get; private set; }

        public MailMessage(string from, string to, string cc, string subject, string body, bool isBodyHtml, int dlrStatus, int origin, int priority, int sendRetry, bool securityCritical, string attachmentFilePath)
        {
            From = from;
            To = to;
            CC = cc;
            Subject = subject;
            Body = body;
            IsBodyHtml = isBodyHtml;
            DLRStatus = (byte)dlrStatus;
            Origin = (byte)origin;
            Priority = (byte)priority;
            SendRetry = (byte)sendRetry;
            SecurityCritical = securityCritical;
            AttachmentFilePath = attachmentFilePath;
        }

        private MailMessage()
        {

        }
    }
}
