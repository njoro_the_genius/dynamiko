﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class KYCRequest : ValueObject<KYCRequest>
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string OtherNames { get; set; }

        public string NationalID { get; set; }

        public string PassportNo { get; set; }

        public string ServiceID { get; set; }

        public string AlienID { get; set; }

        public string TaxID { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string PostalBoxNo { get; set; }

        public string PostalTown { get; set; }

        public string PostalCountry { get; set; }

        public string TelephoneWork { get; set; }

        public string TelephoneHome { get; set; }

        public string TelephoneMobile { get; set; }

        public string PhysicalAddress { get; set; }

        public string PhysicalTown { get; set; }

        public string PhysicalCountry { get; set; }

        public int ReportReason { get; set; }

        public byte Status { get; set; }

        public string CallBackURL { get; set; }

        public KYCRequest(string firstName, string middleName, string lastName, string otherNames, string nationalID,
            string passportNo, string serviceID, string alienID, string taxID, DateTime dateOfBirth, string postalBoxNo, string postalTown,
            string postalCountry, string telephoneWork, string telephoneHome, string telephoneMobile, string physicalAddress, string physicalTown,
            string physicalCountry, int reportReason, string callBackaURL)
        {
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            OtherNames = otherNames;
            NationalID = nationalID;
            PassportNo = passportNo;
            ServiceID = serviceID;
            AlienID = alienID;
            TaxID = taxID;
            DateOfBirth = dateOfBirth;
            PostalBoxNo = postalBoxNo;
            PostalTown = postalTown;
            PostalCountry = postalCountry;
            TelephoneWork = telephoneWork;
            TelephoneHome = telephoneHome;
            TelephoneMobile = telephoneMobile;
            PhysicalAddress = physicalAddress;
            PhysicalTown = physicalTown;
            PhysicalCountry = physicalCountry;
            ReportReason = reportReason;
            CallBackURL = callBackaURL;
        }

        public KYCRequest()
        {

        }
    }
}
