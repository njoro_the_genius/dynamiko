﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class Request : ValueObject<Request>
    {
        public string MessageTypeIdentification { get; set; }

        public string PrimaryAccountNumber { get; set; }

        public string SystemTraceAuditNumber { get; set; }

        public string RetrievalReferenceNumber { get; set; }

        public string MessageOut { get; set; }

        public string MessageIn { get; set; }

        public string ResponseCode { get; set; }

        public string ReferenceNumber { get; set; }

        public Request(string messageTypeIdentification, string primaryAccountNumber, string systemTraceAuditNumber, string retrievalReferenceNumber,
            string messageOut, string messageIn, string responseCode, string referenceNumber)
        {
            var request = new Request();

            request.MessageIn = messageIn;

            request.MessageOut = messageOut;

            request.MessageTypeIdentification = messageTypeIdentification;

            request.PrimaryAccountNumber = primaryAccountNumber;

            request.ReferenceNumber = referenceNumber;

            request.ResponseCode = responseCode;

            request.RetrievalReferenceNumber = retrievalReferenceNumber;

            request.SystemTraceAuditNumber = systemTraceAuditNumber;
        }

        public Request()
        {

        }
    }
}