﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class ScoreOutput : ValueObject<ScoreOutput>
    {
        public string MobiLoansScore { get; set; }

        public string Probability { get; set; }

        public string Grade { get; set; }

        public string ReasonCodeAARC1 { get; set; }

        public string ReasonCodeAARC2 { get; set; }

        public string ReasonCodeAARC3 { get; set; }

        public string ReasonCodeAARC4 { get; set; }

        public ScoreOutput(string mobiLoanScore, string probability, string grade, string reasonCodeAarc1,
            string reasonCodeAarc2, string reasonCodeAarc3, string reasonCodeAarc4)
        {
            MobiLoansScore = mobiLoanScore;
            Probability = probability;
            Grade = grade;
            ReasonCodeAARC1 = reasonCodeAarc1;
            ReasonCodeAARC2 = reasonCodeAarc2;
            ReasonCodeAARC3 = reasonCodeAarc3;
            ReasonCodeAARC4 = reasonCodeAarc4;
        }

        public ScoreOutput()
        {

        }
    }
}
