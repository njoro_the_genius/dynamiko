﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class Image : ValueObject<Image>
    {
        public byte[] Buffer { get; private set; }

        public Image(byte[] buffer)
        {
            Buffer = buffer;
        }

        private Image()
        { }
    }
}
