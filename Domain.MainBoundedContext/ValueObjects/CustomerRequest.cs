﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class CustomerRequest : ValueObject<CustomerRequest>
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string NationalId { get; set; }

        public string PassportNumber { get; set; }

        public string DrivingLicenseNumber { get; set; }


    }
}
