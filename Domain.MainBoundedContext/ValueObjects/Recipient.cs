﻿using Domain.Seedwork;
using System.Text.RegularExpressions;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class Recipient : ValueObject<Recipient>
    {
        public string Name { get; private set; }

        public string Address { get; private set; }

        public string EmailAddress { get; private set; }

        public string PhoneNumber { get; private set; }

        public string IDType { get; private set; }

        public string IDNumber { get; private set; }

        public string BankAccountNumber { get; private set; }

        public Recipient(string name, string address, string phoneNumber, string idType, string idNumber, string bankAccountNumber,string emailAddress)
        {
            this.Name = name;
            this.Address = address;
            this.PhoneNumber = string.IsNullOrWhiteSpace(phoneNumber) ? "" : Regex.Replace(phoneNumber, @"\s", "");
            this.IDType = idType;
            this.IDNumber = idNumber;
            this.BankAccountNumber = bankAccountNumber;
            this.EmailAddress = emailAddress;
        }

        private Recipient()
        { }
    }
}
