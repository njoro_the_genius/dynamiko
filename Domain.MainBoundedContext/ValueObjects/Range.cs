﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class Range : ValueObject<Range>
    {
        public decimal LowerLimit { get; private set; }

        public decimal UpperLimit { get; private set; }

        public Range(decimal lowerLimit, decimal upperLimit)
        {
            this.LowerLimit = lowerLimit;
            this.UpperLimit = upperLimit;
        }

        private Range()
        { }
    }
}
