﻿using Domain.Seedwork;
using System.Text.RegularExpressions;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class Remitter : ValueObject<Remitter>
    {
        public string Name { get; private set; }

        public string Address { get; private set; }

        public string PhoneNumber { get; private set; }

        public string IDType { get; private set; }

        public string IDNumber { get; private set; }

        public string Country { get; private set; }

        public string CCY { get; private set; }

        public string FinancialInstitution { get; private set; }

        public string SourceOfFunds { get; private set; }

        public string PrincipalActivity { get; private set; }

        public Remitter(string name, string address, string phoneNumber, string idType, string idNumber, string country, string ccy, string financialInstitution, string sourceOfFunds, string principalActivity)
        {
            this.Name = name;
            this.Address = address;
            this.PhoneNumber = string.IsNullOrWhiteSpace(phoneNumber) ? "" : Regex.Replace(phoneNumber, @"\s", "");
            this.IDType = idType;
            this.IDNumber = idNumber;
            this.Country = country;
            this.CCY = ccy;
            this.FinancialInstitution = financialInstitution;
            this.SourceOfFunds = sourceOfFunds;
            this.PrincipalActivity = principalActivity;
        }

        private Remitter()
        { }
    }
}
