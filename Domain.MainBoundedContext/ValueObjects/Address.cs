﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class Address : ValueObject<Address>
    {
        public string Physical { get; private set; }

        public string Postal { get; private set; }

        public string Street { get; private set; }

        public string PostalCode { get; private set; }

        public string City { get; private set; }

        public string Email { get; private set; }

        public string LandLine { get; private set; }

        public string MobileLine { get; private set; }

        public Address(string physicalAddress, string postalAddress, string street, string postalCode, string city, string email, string landLine, string mobileLine)
        {
            Physical = physicalAddress;
            Postal = postalAddress;
            Street = street;
            PostalCode = postalCode;
            City = city;
            Email = email;
            LandLine = landLine;
            MobileLine = mobileLine;
        }

        private Address()
        { }
    }
}
