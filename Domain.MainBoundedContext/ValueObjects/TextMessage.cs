﻿using Domain.Seedwork;

namespace Domain.MainBoundedContext.ValueObjects
{
    public class TextMessage : ValueObject<TextMessage>
    {
        public string Recipient { get; set; }

        public string Body { get; set; }

        public byte DlrStatus { get; set; }

        public string Reference { get; set; }

        public byte Origin { get; set; }

        public byte Priority { get; set; }

        public byte SendRetry { get; set; }

        public bool SecurityCritical { get; set; }

        public TextMessage(string recipient, string content, int dlrStatus, string reference, int origin, int priority, int sendRetry, bool securityCritical)
        {
            this.Recipient = recipient;
            this.Body = content;
            this.DlrStatus = (byte)dlrStatus;
            this.Reference = reference;
            this.Origin = (byte)origin;
            this.Priority = (byte)priority;
            this.SendRetry = (byte)sendRetry;
            this.SecurityCritical = securityCritical;
        }

        private TextMessage()
        {

        }
    }
}
