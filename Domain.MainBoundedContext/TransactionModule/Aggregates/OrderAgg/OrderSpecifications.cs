﻿using Domain.Seedwork.Specification;
using System;

namespace Domain.MainBoundedContext.TransactionModule.Aggregates.OrderAgg
{
    public static class OrderSpecifications
    {
        public static Specification<Order> DefaultSpec(string text)
        {
            Specification<Order> specification = new TrueSpecification<Order>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var nameSpec = new DirectSpecification<Order>(c => c.CreatedBy.Contains(text));

                specification &= (nameSpec);
            }

            return specification;
        }

        public static Specification<OrderLineItem> DefaultSpecOrdeline(Guid id, string text)
        {
            Specification<OrderLineItem> specification = new TrueSpecification<OrderLineItem>();


            var idSpec = new DirectSpecification<OrderLineItem>(c => c.OrderId == id);

            specification &= (idSpec);


            return specification;
        }
    }
}
