﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.TransactionModule.Aggregates.OrderAgg
{
    public class OrderLineItem : Entity
    {
        public Guid OrderId { get; set; }

        public virtual Order Order { get; set; }

        public byte PizzaSize { get; set; }

        public int Count { get; set; }

        public decimal Amount { get; set; }

        public string Toppings { get; set; }
    }
}
