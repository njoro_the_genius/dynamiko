﻿using Domain.Seedwork;
using System.Collections.Generic;

namespace Domain.MainBoundedContext.TransactionModule.Aggregates.OrderAgg
{
    public class Order: Entity
    {
        public decimal Amount { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal GST { get; set; }

        public bool IsPaid { get; set; }

        public string Receipt { get; set; }

        HashSet<OrderLineItem> _orderLineItems;

        public virtual ICollection<OrderLineItem> OrderLineItems
        {
            get
            {
                if (_orderLineItems == null)
                    _orderLineItems = new HashSet<OrderLineItem>();

                return _orderLineItems;
            }
            set
            {
                _orderLineItems = new HashSet<OrderLineItem>(value);
            }
        }

        public OrderLineItem AddNewOrderLineItem(byte pizzaSize,int count, decimal amount, string toppings)
        {
            var orderLine = new OrderLineItem();

            orderLine.OrderId = this.Id;

            orderLine.PizzaSize = pizzaSize;

            orderLine.Count = count;

            orderLine.Amount = amount;

            orderLine.Toppings = toppings;

            orderLine.CreatedBy = this.CreatedBy;

            orderLine.CreatedDate = this.CreatedDate;

            orderLine.GenerateNewIdentity();

            this.OrderLineItems.Add(orderLine);

            return orderLine;

        }

    }
}
