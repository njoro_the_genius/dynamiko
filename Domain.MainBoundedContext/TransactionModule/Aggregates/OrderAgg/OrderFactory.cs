﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.MainBoundedContext.TransactionModule.Aggregates.OrderAgg
{
    public static class OrderFactory
    {
        public static Order CreateOrder(decimal amount,decimal gst,decimal totalamount, bool isPaid, string receipt, string createdBy)
        {
            var order = new Order();

            order.GenerateNewIdentity();

            order.Amount = amount;

            order.IsPaid = isPaid;

            order.CreatedBy = createdBy;

            order.Receipt = receipt;

            order.GST = gst;

            order.TotalAmount = totalamount;

            order.CreatedDate = DateTime.Now;

            return order;

        }

        public static OrderLineItem AddNewOrderLineItem(Guid orderId,byte pizzaSize, int count, decimal amount, string toppings, string createdBy)
        {
            var orderLine = new OrderLineItem();

            orderLine.OrderId = orderId;

            orderLine.PizzaSize = pizzaSize;

            orderLine.Count = count;

            orderLine.Amount = amount;

            orderLine.Toppings = toppings;

            orderLine.CreatedBy = createdBy;

            orderLine.CreatedDate = DateTime.Now;

            orderLine.GenerateNewIdentity();

            return orderLine;

        }
    }
}
