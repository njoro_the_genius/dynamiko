﻿using Domain.Seedwork.Specification;

namespace Domain.MainBoundedContext.Aggregates.EnumerationAgg
{
    public static class EnumerationSpecifications
    {
        public static Specification<Enumeration> DefaultSpec()
        {
            Specification<Enumeration> specification = new TrueSpecification<Enumeration>();

            return specification;
        }

        public static Specification<Enumeration> EnumerationFullText(string text)
        {
            Specification<Enumeration> specification = new TrueSpecification<Enumeration>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                DirectSpecification<Enumeration> keySpec = new DirectSpecification<Enumeration>(c => c.Key.Contains(text));

                DirectSpecification<Enumeration> descriptionSpec = new DirectSpecification<Enumeration>(c => c.Description.Contains(text));

                specification &= (keySpec | descriptionSpec);
            }

            return specification;
        }
    }
}
