﻿using Domain.Seedwork;
using System;

namespace Domain.MainBoundedContext.Aggregates.AuditTrailAgg
{
    public class AuditTrail : Entity
    {
        public string EventType { get; set; }

        public string Activity { get; set; }

        public string AdditionalNarration { get; set; }

        public string ApplicationUserName { get; set; }

        public string ApplicationUserDesignation { get; set; }

        public string EnvironmentUserName { get; set; }

        public string EnvironmentMachineName { get; set; }

        public string EnvironmentDomainName { get; set; }

        public string EnvironmentOSVersion { get; set; }

        public string EnvironmentMACAddress { get; set; }

        public string EnvironmentMotherboardSerialNumber { get; set; }

        public string EnvironmentProcessorId { get; set; }

        public string EnvironmentIPAddress { get; set; }

        public Guid? CustomerId { get; set; }
    }
}
