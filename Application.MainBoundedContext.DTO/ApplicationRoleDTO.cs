﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Application.MainBoundedContext.DTO
{
    public class ApplicationRoleDTO
    {
        public Guid Id { get; set; }

        [Display(Name = "Role Name")]
        public string Name { get; set; }

        [Display(Name = "Role Description")]
        public string Description { get; set; }

        [Display(Name = "Is Enabled")]
        public bool IsEnabled { get; set; }

        [Display(Name = "Role Type")]
        public byte RoleType { get; set; }

    }
}
