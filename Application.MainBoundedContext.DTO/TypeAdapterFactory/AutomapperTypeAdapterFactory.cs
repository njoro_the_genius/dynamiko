﻿using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.MainBoundedContext.DTO.MessagingModule;
using Application.MainBoundedContext.DTO.RegistryModule;
using AutoMapper;
using Infrastructure.Crosscutting.Framework.Adapter;

namespace Application.MainBoundedContext.DTO.TypeAdapterFactory
{
    public class AutomapperTypeAdapterFactory : ITypeAdapterFactory
    {
        #region Constructor

        public AutomapperTypeAdapterFactory()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ApplicationProfile>();
                cfg.AddProfile<RegistryModuleProfile>();
                cfg.AddProfile<AdministrationModuleProfile>();
                cfg.AddProfile<MessagingModuleProfile>();                
            });
        }

        #endregion

        #region ITypeAdapterFactory Members

        public ITypeAdapter Create()
        {
            return new AutomapperTypeAdapter();
        }

        #endregion
    }
}