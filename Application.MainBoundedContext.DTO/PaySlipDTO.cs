﻿namespace Application.MainBoundedContext.DTO
{
    public class PaySlipDTO
    {
        public string Names { get; set; }

        public string Unit { get; set; }

        public string Job { get; set; }

        public string Station { get; set; }

        public string EmployeePaySlip { get; set; }

        public string Company { get; set; }

        public decimal Basicpay { get; set; }

        public decimal Grosspay { get; set; }

        public decimal LeaveAllowance { get; set; }

        public decimal NetPay { get; set; }

        public decimal AllowableDeductions { get; set; }

        public decimal HealthInsurance { get; set; }

        public decimal NonCashBenefits { get; set; }

        public decimal Pension { get; set; }

        public decimal Paye { get; set; }

        public decimal SaccoBenevoltFund { get; set; }

        public decimal SaccoSavings { get; set; }

        public decimal StaffWelfare { get; set; }

        public decimal PenisonWelfare { get; set; }

        public decimal TaxablePay { get; set; }

        public decimal TaxCharged { get; set; }

        public decimal TaxRelief { get; set; }

        public decimal TaxDeducted { get; set; }

        public decimal TotalDeductions { get; set; }

        public decimal PersonalRelief { get; set; }

        public decimal TaxableYPay { get; set; }

    }
}
