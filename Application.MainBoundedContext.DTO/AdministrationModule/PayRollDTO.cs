﻿using Application.Seedwork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class PayRollDTO : BindingModelBase<PayRollDTO>
    {
        public PayRollDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid CustomerId { get; set; }

        [DataMember]
        public decimal TotalGrosspay { get; set; }

        [DataMember]
        public decimal TotalNonCashBenefits { get; set; }

        [DataMember]
        public decimal TotalHealthInsurance { get; set; }

        [DataMember]
        public decimal TotalPension { get; set; }

        [DataMember]
        public decimal TotalAllowableDeductions { get; set; }

        [DataMember]
        public decimal TotalNetPay { get; set; }

        [DataMember]
        public decimal TotalPayee { get; set; }

        [DataMember]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public bool IsQueued { get; set; }

        [DataMember]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string ModifiedBy { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public byte RecordStatus { get; set; }


        HashSet<PayRollItemDTO> _payRollItems;

        [DataMember]
        public virtual ICollection<PayRollItemDTO> PayRollItemDTOs
        {
            get
            {
                if (_payRollItems == null)
                    _payRollItems = new HashSet<PayRollItemDTO>();

                return _payRollItems;
            }
            set
            {
                _payRollItems = new HashSet<PayRollItemDTO>(value);
            }
        }

        public IList<PayRollItemDTO> PayRollItemDTOsList { get; set; }
    }

    
}
