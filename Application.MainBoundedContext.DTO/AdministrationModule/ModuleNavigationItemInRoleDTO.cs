﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class ModuleNavigationItemInRoleDTO
    {
        [DataMember]
        public Guid Id { get; set; }

        [Display(Name = "Module Navigation Item")]
        public Guid ModuleNavigationItemId { get; set; }

        [Display(Name = "Role Name")]
        public string RoleId { get; set; }

        [Display(Name = "Controller Name")]
        public string ModuleNavigationItemControllerName { get; set; }

        [Display(Name = "Action Name")]
        public string ModuleNavigationItemActionName { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Commands")]
        public string Commands { get; set; }
    }
}
