﻿using System.ComponentModel.DataAnnotations;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class ChangePasswordModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "Old Password")]
        [Required(ErrorMessage = "Old Password is required")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [Required]
        public string NewPassword { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("NewPassword", ErrorMessage = "Passwords provided don't match")]
        [Required]
        public string ConfirmPassword { get; set; }
    }

    public class PasswordResetModel
    {
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        [Required(ErrorMessage = "New Password is required")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Passwords provided don't match")]
        [Required]
        public string ConfirmPassword { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public string Token { get; set; }
    }
}