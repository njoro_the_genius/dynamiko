﻿using System;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class PasswordHistoryDTO
    {
        public Guid UserId { get; set; }

        public string Password { get; set; }
    }
}
