﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class LeaveDTO: BindingModelBase<LeaveDTO>
    {
        public LeaveDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid EmployeeId { get; set; }

        [Display(Name = "Full Names")]
        public string EmployeeNames { get; set; }

        [DataMember]
        public int Period { get; set; }

        [DataMember]
        public DateTime DateApplied { get; set; }

        [DataMember]
        public DateTime DateApproved { get; set; }

        [DataMember]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DataMember]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string ApprovedBy { get; set; }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public byte LeaveType { get; set; }

        [Display(Name = "Record Status")]
        public string TypeDesc => EnumHelper.GetDescription((LeaveType)LeaveType);

        [Display(Name = "Record Status")]
        public byte RecordStatus { get; set; }

        [Display(Name = "Record Status")]
        public string RecordStatusDesc => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

    }
}
