﻿using Application.Seedwork;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class RolesDTO : BindingModelBase<RolesDTO>
    {
        public RolesDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [DataMember]
        [Display(Name = "Actions")]
        public string Actions { get; set; }

        [DataMember]
        [Display(Name = "Has Users")]
        public bool HasUsers { get; set; }
    }
}
