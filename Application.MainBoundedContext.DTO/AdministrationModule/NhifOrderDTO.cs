﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class NhifOrderDTO : BindingModelBase<NhifOrderDTO>
    {
        public NhifOrderDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        public byte RecordStatus { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string ModifiedBy { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        public string RecordStatusDescription => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        HashSet<NhifGraduatedScaleDTO> _nhifGraduatedScaleDTOs;

        [DataMember]
        public virtual ICollection<NhifGraduatedScaleDTO> NhifGraduatedScaleDTOs
        {
            get
            {
                if (_nhifGraduatedScaleDTOs == null)
                    _nhifGraduatedScaleDTOs = new HashSet<NhifGraduatedScaleDTO>();

                return _nhifGraduatedScaleDTOs;
            }
            set
            {
                _nhifGraduatedScaleDTOs = new HashSet<NhifGraduatedScaleDTO>(value);
            }
        }

        public IList<NhifGraduatedScaleDTO> NhifGraduatedScaleDTOsList { get; set; }
    }
}
