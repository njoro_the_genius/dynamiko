﻿using Application.Seedwork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class TaxOrderDTO : BindingModelBase<TaxOrderDTO>
    {
        public TaxOrderDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }

        public string Commands { get; set; }

        HashSet<TaxRangesDTO> _taxRangeItems;

        [DataMember]
        public virtual ICollection<TaxRangesDTO> TaxRangeItems
        {
            get
            {
                if (_taxRangeItems == null)
                    _taxRangeItems = new HashSet<TaxRangesDTO>();

                return _taxRangeItems;
            }
            set
            {
                _taxRangeItems = new HashSet<TaxRangesDTO>(value);
            }
        }

        public IList<TaxRangesDTO> TaxRangesItemList { get; set; }

    }
}
