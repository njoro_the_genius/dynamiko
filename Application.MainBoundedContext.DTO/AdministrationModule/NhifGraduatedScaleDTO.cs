﻿using Application.Seedwork;
using System;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class NhifGraduatedScaleDTO : BindingModelBase<NhifGraduatedScaleDTO>
    {
        public NhifGraduatedScaleDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid NhifOrderId { get; set; }
        [DataMember]
        public decimal LowerLimit { get; set; }
        [DataMember]
        public decimal UpperLimit { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public decimal Value { get; set; }
    }
}
