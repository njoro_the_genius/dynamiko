﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class DeductionDTO : BindingModelBase<DeductionDTO>
    {
        public DeductionDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid EmployeeId { get; set; }

        [DataMember]
        [Display(Name = "Employee Name")]
        public string EmployeeEmployeeNames { get; set; }
        [DataMember]
        public int DeductionType { get; set; }

        [Display(Name = "Record Status")]
        public string TypeDesc => EnumHelper.GetDescription((DeductionType)DeductionType);

        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public int Frequency { get; set; }
        [DataMember]
        public bool IsActive { get; set; }

        [Display(Name = "Record Status")]
        public byte RecordStatus { get; set; }

        [Display(Name = "Record Status")]
        public string RecordStatusDesc => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "StartDate")]
        public DateTime StartDate { get; set; }

        [Display(Name = "EndDate")]
        public DateTime EndDate { get; set; }



    }
}
