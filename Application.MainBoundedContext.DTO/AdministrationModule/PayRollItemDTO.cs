﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class PayRollItemDTO : BindingModelBase<PayRollItemDTO>
    {
        public PayRollItemDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid EmployeeId { get; set; }

        [DataMember]
        public Guid PayRollId { get; set; }

        [DataMember]
        public string EmployeeEmployeeNames { get; set; }

        [DataMember]
        public decimal Grosspay { get; set; }

        [DataMember]
        public decimal NonCashBenefits { get; set; }

        [DataMember]
        public decimal Pension { get; set; }

        [DataMember]
        public decimal Payee { get; set; }

        [DataMember]
        public decimal AllowableDeductions { get; set; }

        [DataMember]
        public decimal HealthInsurance { get; set; }

        [DataMember]
        public decimal NetPay { get; set; }

        [Display(Name = "Record Status")]
        public byte RecordStatus { get; set; }

        [Display(Name = "Record Status")]
        public string RecordStatusDesc => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "StartDate")]
        public DateTime StartDate { get; set; }

        [Display(Name = "EndDate")]
        public DateTime EndDate { get; set; }
    }
}
