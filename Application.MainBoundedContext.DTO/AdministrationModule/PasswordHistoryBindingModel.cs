﻿using Application.Seedwork;
using System;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class PasswordHistoryBindingModel : BindingModelBase<PasswordHistoryBindingModel>
    {
        public PasswordHistoryBindingModel()
        {
            AddAllAttributeValidators();
        }

        public Guid UserId { get; set; }

        public string Password { get; set; }
    }
}
