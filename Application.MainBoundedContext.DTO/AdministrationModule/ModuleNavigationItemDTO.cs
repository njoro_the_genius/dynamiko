﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class ModuleNavigationItemDTO
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Controller Name")]
        public string ControllerName { get; set; }

        [Required]
        [Display(Name = "Action Name")]
        public string ActionName { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }
    }
}
