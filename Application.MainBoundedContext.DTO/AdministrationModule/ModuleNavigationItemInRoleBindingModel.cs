﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class ModuleNavigationItemInRoleBindingModel : BindingModelBase<ModuleNavigationItemInRoleBindingModel>
    {
        public ModuleNavigationItemInRoleBindingModel()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [Display(Name = "Module Navigation Item")]
        [DataMember]
        [ValidGuid]
        public Guid ModuleNavigationItemId { get; set; }

        [Display(Name = "Role Name")]
        [DataMember]
        public string RoleName { get; set; }

        [Display(Name = "Controller Name")]
        [DataMember]
        public string ModuleNavigationItemControllerName { get; set; }

        [Display(Name = "Action Name")]
        [DataMember]
        public string ModuleNavigationItemActionName { get; set; }

        [Display(Name = "Created By")]
        [DataMember]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        [DataMember]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Commands")]
        [DataMember]
        public string Commands { get; set; }
    }
}
