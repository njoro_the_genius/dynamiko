﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class NssfOrderDTO : BindingModelBase<NssfOrderDTO>
    {
        public NssfOrderDTO()
        {
            AddAllAttributeValidators();    
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string Description { get; set; }

        [Display(Name = "Record Status")]
        public byte RecordStatus { get; set; }

        [Display(Name = "Record Status")]
        public string RecordStatusDesc => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        [DataMember]
        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public  DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        HashSet<NssfGraduatedScaleDTO> _nssfGraduatedScaleDTOs;

        [DataMember]
        public virtual ICollection<NssfGraduatedScaleDTO> NssfGraduatedScaleDTOs
        {
            get
            {
                if (_nssfGraduatedScaleDTOs == null)
                    _nssfGraduatedScaleDTOs = new HashSet<NssfGraduatedScaleDTO>();

                return _nssfGraduatedScaleDTOs;
            }
            set
            {
                _nssfGraduatedScaleDTOs = new HashSet<NssfGraduatedScaleDTO>(value);
            }
        }

        public IList<NssfGraduatedScaleDTO> NssfGraduatedScaleDTOsList { get; set; }
    }
}
