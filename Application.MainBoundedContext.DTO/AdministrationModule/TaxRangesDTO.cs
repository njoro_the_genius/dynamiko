﻿using Application.Seedwork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class TaxRangesDTO : BindingModelBase<TaxRangesDTO>
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public Guid TaxOrderId { get; set; }

        [Display(Name = "Tax Description")]
        public string TaxDescription { get; set; }

        [Display(Name = "Tax Value")]
        public decimal TaxValue { get; set; }

        [Display(Name = "Range Lower Limit")]
        public decimal RangeLowerLimit { get; set; }

        [Display(Name = "Range Upper Limit")]
        public decimal RangeUpperLimit { get; set; }
        public byte Type { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        public string CreatedDateToString => CreatedDate.ToString("g");

        public string Commands { get; set; }
    }
}
