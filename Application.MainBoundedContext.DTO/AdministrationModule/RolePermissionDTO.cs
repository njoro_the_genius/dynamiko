﻿using System.Collections.Generic;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class RolePermissionDTO
    {
        public string RoleId { get; set; }

        public string[] CurrentUserRoles { get; set; }

        public ICollection<ControllerAction> ControllerActionList { get; set; }
    }

    public class ControllerAction
    {
        public string ControllerName { get; set; }

        public string ActionName { get; set; }
    }
}
