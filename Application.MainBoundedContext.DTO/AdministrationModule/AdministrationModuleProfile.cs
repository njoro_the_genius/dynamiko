﻿using AutoMapper;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.DeductionsAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.LeaveAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemInRoleAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.NHIFAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.NSSFAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.PayRollAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.SettingAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.TaxAgg;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class AdministrationModuleProfile : Profile
    {
        public AdministrationModuleProfile()
        {
            CreateMap<ModuleNavigationItem, ModuleNavigationItemDTO>();

            CreateMap<ModuleNavigationItemDTO, ModuleNavigationItemBindingModel>();

            CreateMap<ModuleNavigationItemBindingModel, ModuleNavigationItemDTO>();

            CreateMap<ModuleNavigationItemInRole, ModuleNavigationItemInRoleDTO>();

            CreateMap<PasswordHistoryDTO, PasswordHistoryBindingModel>();

            CreateMap<PasswordHistoryBindingModel, PasswordHistoryDTO>();

            CreateMap<PayRoll, PayRollDTO>();

            CreateMap<PayRollItem, PayRollItemDTO>();

            CreateMap<Leave, LeaveDTO>();

            CreateMap<StaticSetting, StaticSettingDTO>();

            CreateMap<TaxOrder, TaxOrderDTO>();

            CreateMap<TaxGraduatedScale, TaxRangesDTO>();

            CreateMap<NhifOrder, NhifOrderDTO>();

            CreateMap<NhifGraduatedScale, NhifGraduatedScaleDTO>();

            CreateMap<NssfOrder, NssfOrderDTO>();

            CreateMap<NssfGraduatedScale, NssfGraduatedScaleDTO>();

            CreateMap<Deduction, DeductionDTO>();
        }
    }
}
