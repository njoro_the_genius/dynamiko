﻿using Application.Seedwork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class ModuleNavigationItemBindingModel : BindingModelBase<ModuleNavigationItemBindingModel>
    {
        public ModuleNavigationItemBindingModel()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Controller Name")]
        public string ControllerName { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Action Name")]
        public string ActionName { get; set; }

        [DataMember]
        [Display(Name = "Permission Type")]
        public int PermissionType { get; set; }

        [DataMember]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [DataMember]
        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        HashSet<ModuleNavigationItemInRoleDTO> _moduleNavigationItemInRoleDTO;

        [DataMember]
        public virtual ICollection<ModuleNavigationItemInRoleDTO> ModuleNavigationItemInRoles
        {
            get
            {
                if (_moduleNavigationItemInRoleDTO == null)
                    _moduleNavigationItemInRoleDTO = new HashSet<ModuleNavigationItemInRoleDTO>();
                return _moduleNavigationItemInRoleDTO;
            }
            set => _moduleNavigationItemInRoleDTO = new HashSet<ModuleNavigationItemInRoleDTO>(value);
        }

        public string Actions { get; set; }
    }
}
