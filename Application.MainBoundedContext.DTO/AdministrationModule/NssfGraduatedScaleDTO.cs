﻿using Application.Seedwork;
using System;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.AdministrationModule
{
    public class NssfGraduatedScaleDTO : BindingModelBase<NssfGraduatedScaleDTO>
    {
        public NssfGraduatedScaleDTO()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid NssfOrderId { get; set; }
        [DataMember]
        public decimal LowerLimit { get; set; }
        [DataMember]
        public decimal UpperLimit { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public decimal Value { get; set; }
    }
}
