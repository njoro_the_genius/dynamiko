﻿using Domain.MainBoundedContext.Aggregates.AuditTrailAgg;
using Domain.MainBoundedContext.Aggregates.EnumerationAgg;
using Infrastructure.Crosscutting.Framework.Models;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using AutoMapper;

namespace Application.MainBoundedContext.DTO
{
    public class ApplicationProfile : Profile
    {
        public ApplicationProfile()
        {
            //AuditTrail => AuditTrailDTO
            CreateMap<AuditTrail, AuditTrailDTO>();

            //Enumeration => EnumerationDTO
            CreateMap<Enumeration, EnumerationDTO>();
        }

        static string FlattenAuditInfo(string content)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(content) && content.StartsWith("<?xml"))
                {
                    var serializer = new XmlSerializer(typeof(AuditInfoWrapper));

                    var wrapper = (AuditInfoWrapper)serializer.Deserialize(new StringReader(content));

                    var sb = new StringBuilder();

                    if (wrapper.AuditInfoCollection != null && wrapper.AuditInfoCollection.Any())
                    {
                        foreach (var item in wrapper.AuditInfoCollection)
                            sb.AppendLine(
                                $"ColumnName: {item.ColumnName}, OriginalValue: {item.OriginalValue}, NewValue: {item.NewValue},");
                    }

                    return $"{sb}";
                }
                else return content;
            }
            catch { return content; }
        }
    }
}
