﻿using System;
using System.Collections.Generic;

namespace Application.MainBoundedContext.DTO
{
    public class DashboardGraphDataSetDTO
    {
        public object[] Quater1 { get; set; }

        public object[] Quater2 { get; set; }

        public object[] Quater3 { get; set; }

        public object[] Quater4 { get; set; }
    }

    public class DashboardGraphDTO
    {
        public string Customer { get; set; }

        public int? YearNo { get; set; }

        public int? Quarter1 { get; set; }

        public int? Quarter2 { get; set; }
        public int? Quarter3 { get; set; }
        public int? Quarter4 { get; set; }
    }

    public class DashboardGraphSideBarDTO
    {
        public int? TodaysTransactions { get; set; }

        public int? CurrentMonthTransactions { get; set; }

        public decimal? TotalValueOfTransactions { get; set; }
    }
}
