﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Application.MainBoundedContext.DTO
{
    public class ApplicationUserDTO
    {
        public string Id { get; set; }

        

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Customer")]
        public Guid? CustomerId { get; set; }

        [Display(Name = "Is Enabled")]
        public bool IsEnabled { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        public DateTime LastPasswordChangedDate { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\+(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "Invalid phone number")]
        [MinLength(13, ErrorMessage = "Invalid phone number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Role")]
        public ICollection<string> AssignedRole { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        public bool LockoutEnabled { get; set; }

        public int AccessFailedCount { get; set; }

        [Display(Name = "Is External User")]
        public bool IsExternalUser { get; set; }

        public byte RecordStatus { get; set; }

        public string RecordStatusDescription => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        public string CreatedBy { get; set; }

        public string ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string EditedBy { get; set; }

        public DateTime? EditDate { get; set; }

        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        public int Role { get; set; }

        public string RoleName { get; set; }

        public string PlainTextPassword { get; set; }

        public string LoginUrl { get; set; }

        public bool IsUserLocked => LockoutEndDateUtc >= DateTime.UtcNow;

        public string ApplicationDomainName { get; set; }

        public bool UserCreateSucceed { get; set; }

        public override string ToString()
        {
            return "User => " + "(" + UserName + ")";
        }
    }
}