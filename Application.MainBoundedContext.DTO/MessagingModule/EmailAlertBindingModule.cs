﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.MessagingModule
{
    public class EmailAlertBindingModel : BindingModelBase<EmailAlertBindingModel>
    {
        public EmailAlertBindingModel()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [DataMember]
        [Display(Name = "CustomerId")]
        public Guid CustomerId { get; set; }

        [DataMember]
        [Display(Name = "From")]
        [Required]
        public string MailMessageFrom { get; set; }

        [DataMember]
        [Display(Name = "Recipient")]
        [Required]
        public string MailMessageTo { get; set; }

        [DataMember]
        [Display(Name = "CC")]
        public string MailMessageCC { get; set; }

        [DataMember]
        [Display(Name = "Subject")]
        [Required]
        public string MailMessageSubject { get; set; }

        [DataMember]
        [Display(Name = "Message")]
        [Required]
        public string MailMessageBody { get; set; }

        [DataMember]
        [Display(Name = "Message")]
        public string MaskedMailMessageBody { get; set; }

        [DataMember]
        [Display(Name = "Is Body Html?")]
        public bool MailMessageIsBodyHtml { get; set; }

        [DataMember]
        [Display(Name = "DLR Status")]
        public byte MailMessageDLRStatus { get; set; }

        [DataMember]
        [Display(Name = "DLR Status")]
        public string MailMessageDLRStatusDescription => EnumHelper.GetDescription((DLRStatus)MailMessageDLRStatus);

        [DataMember]
        [Display(Name = "Origin")]
        public byte MailMessageOrigin { get; set; }

        [DataMember]
        [Display(Name = "Origin")]
        public string MailMessageOriginDescription => EnumHelper.GetDescription((MessageOrigin)MailMessageOrigin);

        [DataMember]
        [Display(Name = "Priority")]
        public byte MailMessagePriority { get; set; }

        [DataMember]
        [Display(Name = "Priority")]
        public string MailMessagePriorityDescription => EnumHelper.GetDescription((QueuePriority)MailMessagePriority);

        [DataMember]
        [Display(Name = "Send Retry")]
        public byte MailMessageSendRetry { get; set; }

        [DataMember]
        [Display(Name = "Security Critical")]
        public bool MailMessageSecurityCritical { get; set; }

        [DataMember]
        [Display(Name = "Has Attachment")]
        public bool MailMessageHasAttachment { get; set; }

        [DataMember]
        [Display(Name = "Attachment File Path")]
        public string MailMessageAttachmentFilePath { get; set; }

        [DataMember]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [DataMember]
        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [DataMember]

        public string Commands { get; set; }
    }
}
