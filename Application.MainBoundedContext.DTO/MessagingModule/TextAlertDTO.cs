﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.MessagingModule
{
    public class TextAlertDTO
    {
        [DataMember]
        public Guid Id { get; set; }

        public Guid CustomerId { get; set; }

        [DataMember]
        public string TextMessageRecipient { get; set; }

        [DataMember]
        public string TextMessageBody { get; set; }

        [DataMember]
        public string UnmaskedTextMessageBody { get; set; }

        [DataMember]
        public byte TextMessageDLRStatus { get; set; }

        [Display(Name = "Dlr Status")]
        public string DlrStatusDescription => EnumHelper.GetDescription((DLRStatus)TextMessageDLRStatus);

        [DataMember]
        public string TextMessageReference { get; set; }

        [DataMember]
        public byte TextMessageOrigin { get; set; }

        [DataMember]
        public string OriginDescription => EnumHelper.GetDescription((MessageOrigin)TextMessageOrigin);

        [DataMember]
        public byte TextMessagePriority { get; set; }

        [DataMember]
        public string PriorityDescription => EnumHelper.GetDescription((QueuePriority)TextMessagePriority);

        [DataMember]
        [Display(Name = "Send Retry")]

        public byte TextMessageSendRetry { get; set; }

        [DataMember]
        public bool TextMessageSecurityCritical { get; set; }

        [DataMember]
        public string PartnerSMSGatewayUsername { get; set; }

        [DataMember]
        public string PartnerSMSGatewayPassword { get; set; }

        [DataMember]
        public string PartnerSMSGatewaySenderId { get; set; }

        [DataMember]
        public string PartnerSMSGatewayUrl { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string Commands { get; set; }
    }
}
