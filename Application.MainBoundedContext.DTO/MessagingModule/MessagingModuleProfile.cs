﻿using AutoMapper;
using Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg;
using Domain.MainBoundedContext.MessagingModule.Aggregates.TextAlertAgg;

namespace Application.MainBoundedContext.DTO.MessagingModule
{
    public class MessagingModuleProfile : Profile
    {
        public MessagingModuleProfile()
        {
            CreateMap<EmailAlert, EmailAlertDTO>()
                .ForMember(dest => dest.MaskedMailMessageBody, opt => opt.MapFrom(src => src.MailMessage.SecurityCritical ? "Security Critical" : src.MailMessage.Body))
                .ForMember(dest => dest.MailMessageDLRStatusDescription, opt => opt.Ignore())
                .ForMember(dest => dest.MailMessageOriginDescription, opt => opt.Ignore());

            CreateMap<TextAlert, TextAlertDTO>()
                .ForMember(dest => dest.TextMessageBody, x => x.MapFrom(d => d.TextMessage.SecurityCritical ? "Security Critical" : d.TextMessage.Body))
                .ForMember(dest => dest.DlrStatusDescription, w => w.Ignore())
                .ForMember(dest => dest.OriginDescription, e => e.Ignore())
                .ForMember(dest => dest.PriorityDescription, e => e.Ignore());

            //TextAlertDTO => TextAlertBindingModel
            CreateMap<TextAlertDTO, TextAlertBindingModel>();

            CreateMap<EmailAlertDTO, EmailAlertBindingModel>();
        }
    }
}
