﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.MessagingModule
{
    public class TextAlertBindingModel : BindingModelBase<TextAlertBindingModel>
    {
        public TextAlertBindingModel()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [DataMember]
        [Display(Name = "CustomerId")]
        public Guid CustomerId { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Recipient")]
        public string TextMessageRecipient { get; set; }

        [DataMember]
        [Display(Name = "Message")]
        [Required]
        public string TextMessageBody { get; set; }

        [DataMember]
        [Display(Name = "Message")]
        //[Required]
        public string UnmaskedTextMessageBody { get; set; }

        [DataMember]
        [Display(Name = "Dlr Status")]
        public byte TextMessageDLRStatus { get; set; }

        [Display(Name = "Dlr Status")]
        public string DlrStatusDescription => EnumHelper.GetDescription((DLRStatus)TextMessageDLRStatus);

        [DataMember]
        [Display(Name = "Reference")]
        public string TextMessageReference { get; set; }

        [DataMember]
        [Display(Name = "Origin")]

        public byte TextMessageOrigin { get; set; }

        [DataMember]
        [Display(Name = "Origin")]
        public string OriginDescription => EnumHelper.GetDescription((MessageOrigin)TextMessageOrigin);

        [DataMember]
        [Display(Name = "Priority")]
        public byte TextMessagePriority { get; set; }

        [DataMember]
        [Display(Name = "Priority Description")]
        public string PriorityDescription => EnumHelper.GetDescription((QueuePriority)TextMessagePriority);

        [DataMember]
        [Display(Name = "Send Retry")]

        public byte TextMessageSendRetry { get; set; }

        [DataMember]
        [Display(Name = "Security Critical")]
        public bool TextMessageSecurityCritical { get; set; }

        [DataMember]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [DataMember]
        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        [Display(Name = "Created Date")]
        public string Commands { get; set; }
    }
}
