﻿namespace Application.MainBoundedContext.DTO
{
    public class DashBoardData
    {
        public int? TotalCustomer { get; set; }

        public string CustomerName { get; set; }

        public int? TotalEMployees { get; set; }

        public int TotalSalary { get; set; }

        public decimal TotalAverageSalary { get; set; }
     
    }
}
