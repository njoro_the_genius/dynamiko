﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.RegistryModule
{
    public class CustomerDTO
    {
        public Guid Id { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Type")]
        public byte Type { get; set; }

        [Display(Name = "Type")]
        public string TypeDesc => EnumHelper.GetDescription((CustomerType)Type);

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Record Status")]
        public byte RecordStatus { get; set; }

        [Display(Name = "Record Status")]
        public string RecordStatusDesc => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        [Display(Name = "Is Enabled?")]
        public bool IsEnabled { get; set; }

        [Display(Name = "Address Physical")]
        [Required]
        public string AddressPhysical { get; set; }

        [Display(Name = "Postal")]
        [Required]
        public string AddressPostal { get; set; }

        [Display(Name = "Street")]
        [Required]
        public string AddressStreet { get; set; }

        [Display(Name = "Postal Code")]
        [Required]
        public string AddressPostalCode { get; set; }

        [Display(Name = "City")]
        [Required]
        public string AddressCity { get; set; }

        [Display(Name = "Email")]
        public string AddressEmail { get; set; }

        [Display(Name = "Land Line")]
        public string AddressLandLine { get; set; }

        [Display(Name = "Mobile Line")]
        [Required]
        public string AddressMobileLine { get; set; }

        [Display(Name = "Logo Path")]
        public string LogoPath { get; set; }

        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }

        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }
        public string Commands { get; set; }
    }
}
