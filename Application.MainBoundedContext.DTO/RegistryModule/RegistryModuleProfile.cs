﻿using AutoMapper;
using Domain.MainBoundedContext.RegistryModule;
using Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg;

namespace Application.MainBoundedContext.DTO.RegistryModule
{
    public class RegistryModuleProfile : Profile
    {
        public RegistryModuleProfile()
        {
            CreateMap<Customer, CustomerDTO>();

            CreateMap<CustomerDTO, CustomerBindingModel>();

            CreateMap<Employee, EmployeeDTO>();
        }
    }
}
