﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.RegistryModule
{
    public class EmployeeDTO : BindingModelBase<EmployeeDTO>
    {
        public EmployeeDTO()
        {
            AddAllAttributeValidators();
        }
        public Guid Id { get; set; }
        [DataMember]
        public Guid? CustomerId { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        [Required]
        public string UserName { get; set; }
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Other Names")]
        public string OtherNames { get; set; }
        [Display(Name = "Full Names")]
        public string FullName => $"{FirstName} {OtherNames}";
        [Display(Name = "Full Names")]
        public string EmployeeNames { get; set; }
        [Display(Name = "Email")]
        [Required]
        public string Email { get; set; }
        [Display(Name = "Phone Number")]
        [RegularExpression(@"^\+(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "Invalid phone number")]
        [MinLength(13, ErrorMessage = "Invalid phone number")]
        [Required]
        public string PhoneNumber { get; set; }
        [DataMember]
        public int Gender { get; set; }
        [Display(Name = "Gender")]
        [Required]
        public string GenderDesc => EnumHelper.GetDescription((Gender)RecordStatus);
        [DataMember]
        [Display(Name = "Date of Birth")]
        [Required]
        public DateTime DateOfBirth { get; set; }
        [DataMember]
        public int DepartmentId { get; set; }
        [Display(Name = "Department")]
        [Required]
        public string DepartmentDesc => EnumHelper.GetDescription((Department)RecordStatus);
        [Display(Name = "Id Number")]
        [Required]
        public int IdNumber { get; set; }
        [Display(Name = "KRA Pin")]
        [Required]
        public string KRAPin { get; set; }
        [Display(Name = "NHIF Number")]
        [Required]
        public string NHIFNumber { get; set; }
        [Display(Name = "NSSF Number")]
        [Required]
        public string NSSFNumber { get; set; }
        [Display(Name = "Bank Account")]
        [Required]
        public string BankAccount { get; set; }
        [Display(Name = "Gross Salary")]
        [Required]
        public decimal GrossSalary { get; set; }
        [Display(Name = "Deduct NSSF Contribution?")]
        [Required]
        public bool DeductNSSFContribution { get; set; }
        [Display(Name = "Deduct NHIF Contribution?")]
        [Required]
        public bool DeductNHIFContribution { get; set; }
        public string BankName { get; set; }
        [Display(Name = "Bank Name")]
        [Required]
        public string BankCode { get; set; }     
        public string BranchName { get; set; }
        [Display(Name = "Branch Name")]
        [Required]
        public string BranchCode { get; set; }
        [Display(Name = "Account Name")]
        [Required]
        public string AccountName { get; set; }
        [Display(Name = "Job Number")]
        [Required]
        public string JobNumber { get; set; }
        [Display(Name = "Date of Employment")]
        [Required]
        public DateTime DateOfEmployment { get; set; }
        [Display(Name = "Job title")]
        [Required]
        public string JobTitle { get; set; }
        [Display(Name = "Allocated leave days")]
        [Required]
        public int AllocatedLeaveDays { get; set; }
        [Display(Name = "Record Status")]
        public byte RecordStatus { get; set; }
        [Display(Name = "Record Status")]
        public string RecordStatusDesc => EnumHelper.GetDescription((RecordStatus)RecordStatus);
        [Display(Name = "Is Enabled?")]
        public bool IsEnabled { get; set; }
        public bool IsExternalUser { get; set; }
        public int Role { get; set; }
        public string RoleName { get; set; }
        [Display(Name = "Role")]
        public ICollection<string> AssignedRole { get; set; }
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }
    }
}
