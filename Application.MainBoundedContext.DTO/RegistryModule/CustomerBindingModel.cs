﻿using Application.Seedwork;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Application.MainBoundedContext.DTO.RegistryModule
{
    public class CustomerBindingModel : BindingModelBase<CustomerBindingModel>
    {
        public CustomerBindingModel()
        {
            AddAllAttributeValidators();
        }

        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [DataMember]
        [Display(Name = "Type")]
        public byte Type { get; set; }

        public string TypeDesc => EnumHelper.GetDescription((CustomerType)Type);

        [DataMember]
        [Display(Name = "Record Status")]
        public byte RecordStatus { get; set; }

        [Display(Name = "Record Status Desc")]
        public string RecordStatusDesc => EnumHelper.GetDescription((RecordStatus)RecordStatus);

        [DataMember]
        [Display(Name = "Is Enabled?")]
        public bool IsEnabled { get; set; }

        [DataMember]
        [Display(Name = "Physical")]
        public string AddressPhysical { get; set; }

        [DataMember]
        [Display(Name = "Postal")]
        public string AddressPostal { get; set; }

        [DataMember]
        [Display(Name = "Street")]
        public string AddressStreet { get; set; }

        [DataMember]
        [Display(Name = "Postal Code")]
        public string AddressPostalCode { get; set; }

        [DataMember]
        [Display(Name = "City")]
        public string AddressCity { get; set; }

        [DataMember]
        [Display(Name = "Email")]
        public string AddressEmail { get; set; }

        [DataMember]
        [Display(Name = "Land Line")]
        public string AddressLandLine { get; set; }

        [DataMember]
        [Display(Name = "Mobile Line")]
        public string AddressMobileLine { get; set; }

        [DataMember]
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }

        [DataMember]
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }

        [DataMember]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [DataMember]
        [Display(Name = "Created Date")]
        public DateTime CreatedDate { get; set; }


    }
}
