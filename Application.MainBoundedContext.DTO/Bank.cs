﻿using System.Collections.Generic;

namespace Application.MainBoundedContext.DTO
{
    public class Bank
    {
        public string Code { get; set; }

        public string Name { get; set; }

        HashSet<Branch> _branches;

        public virtual ICollection<Branch> Branches
        {
            get
            {
                if (_branches == null)
                    _branches = new HashSet<Branch>();

                return _branches;
            }
            set => _branches = new HashSet<Branch>(value);
        }
    }

    public class Branch
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
