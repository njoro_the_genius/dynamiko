﻿using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using Urion.PayRoll.Configuration;

namespace Urion.PayRoll.QuartzJobs
{
    public class QueueingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;

        private readonly IChannelService _channelService;

        private readonly ILogger _logger;

        public QueueingJob(
              IMessageQueueService messageQueueService,
            IChannelService channelService,
            ILogger logger)
        {
            _messageQueueService = messageQueueService;

            _channelService = channelService;

            _logger = logger;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var gatewayDispatcherConfigurationSection = (PayRollConfigurationSection)ConfigurationManager.GetSection("payRollConfiguration");

                if (gatewayDispatcherConfigurationSection == null) return;

                foreach (var settingsItem in gatewayDispatcherConfigurationSection.GatewayDispatcherSettingsCollection)
                {
                    var settingElement = (PayRollSettingsElement)settingsItem;

                    if (settingElement != null && settingElement.Enabled == 1)
                    {
                        var serviceHeader = new ServiceHeader { ApplicationDomainName = settingElement.UniqueId };

                        var unProcessedPayRoll = await _channelService.FindUnProcessedPayRollFilterInPageAsync(settingElement.QueuePageIndex, settingElement.QueuePageSize, null, null, true, serviceHeader);

                        foreach (var payRoll in unProcessedPayRoll.PageCollection)
                        {
                            payRoll.IsQueued = true;

                            await QueuePayRoll(payRoll, gatewayDispatcherConfigurationSection, serviceHeader);
                        }
                    }
                }
            }
            catch (Exception exception)
            {

                _logger.LogError("Urion.PayRoll.QuartzJobs.QueueingJob.Execute", exception.Message);
            }
        }

        #region Helpers
        private async Task QueuePayRoll(PayRollDTO payRollDTO, PayRollConfigurationSection gatewayDispatcherConfigurationSection, ServiceHeader serviceHeader)
        {
            var queueDTO = new QueueDTO()
            {
                RecordId = payRollDTO.Id,
                AppDomainName = serviceHeader.ApplicationDomainName
            };

            if (await _channelService.UpdatePayRollAsync(payRollDTO, serviceHeader))
            {
                _messageQueueService.Send(gatewayDispatcherConfigurationSection.GatewayDispatcherSettingsCollection.QueuePath, queueDTO, MessageCategory.PayRollQueue, MessagePriority.High, 5);
            }
        }
        #endregion
    }
}
