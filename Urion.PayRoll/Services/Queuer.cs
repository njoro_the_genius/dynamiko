﻿using Infrastructure.Crosscutting.Framework.Logging;
using Presentation.Infrastructure.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urion.PayRoll.Configuration;
using Urion.PayRoll.QuartzJobs;

namespace Urion.PayRoll.Services
{
    [Export(typeof(IPlugin))]
    public class Queuer : IPlugin
    {
        private readonly ILogger _logger;

        [ImportingConstructor]
        public Queuer(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Guid Id => new Guid("{2765198E-D30C-47EC-B089-F6D85D1A936F}");

        public string Description => "PAYROLL QUEUER";

        public void DoWork(IScheduler scheduler, params string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    var GatewayDispatcherConfigSection = (PayRollConfigurationSection)ConfigurationManager.GetSection("payRollConfiguration");

                    if (GatewayDispatcherConfigSection == null)
                        throw new ArgumentNullException(nameof(GatewayDispatcherConfigSection));

                    // Define the Job to be scheduled
                    var jobDetail = JobBuilder.Create<QueueingJob>()
                        .WithIdentity("PayRollQueueingJob", "Urion")
                        .RequestRecovery()
                        .Build();

                    // Associate a trigger with the Job
                    var trigger = (ICronTrigger)TriggerBuilder.Create()
                        .WithIdentity("PayRollQueueingJob", "Urion")
                        .WithCronSchedule(GatewayDispatcherConfigSection.GatewayDispatcherSettingsCollection.QueueingJobCronExpression)
                        .StartAt(DateTime.UtcNow)
                        .WithPriority(1)
                        .Build();

                    // Validate that the job doesn't already exists
                    if (await scheduler.CheckExists(new JobKey("PayRollQueueingJob", "Urion")))
                        await scheduler.DeleteJob(new JobKey("PayRollQueueingJob", "Urion"));

                    var schedule = await scheduler.ScheduleJob(jobDetail, trigger);

                    _logger.Debug("Job '{0}' scheduled for '{1}'", "PayRollQueueingJob", schedule.ToString("r"));
                }
                catch (Exception ex)
                {
                    _logger.LogError("{0}->DoWork...", ex, Description);
                }
            });
        }

        public void Exit()
        {

        }
    }
}
