﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Logging;
using Presentation.Infrastructure.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urion.PayRoll.Configuration;
using Urion.PayRoll.QueueProcessors;

namespace Urion.PayRoll.Services
{
    [Export(typeof(IPlugin))]
    public class Dispatcher : IPlugin
    {
        private DispatcherMessageProcessor _messageProcessor;

        private readonly IChannelService _channelService;

        private readonly ILogger _logger;

        private readonly IHttpRequestAppService _httpRequestAppService;

        private readonly IMessageQueueService _messageQueueService;

        [ImportingConstructor]
        public Dispatcher(
          //IChannelService channelService,
          ILogger logger,
          IHttpRequestAppService httpRequestAppService,
          IMessageQueueService messageQueueService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _channelService = new ChannelService(_logger);

            _httpRequestAppService = httpRequestAppService ?? throw new ArgumentNullException(nameof(httpRequestAppService));

            _messageQueueService = messageQueueService ?? throw new ArgumentNullException(nameof(messageQueueService));
        }


        public Guid Id => new Guid("{B7D12915-8D63-4290-84F7-3BFEC868D0C3}");

        public string Description => "PayRoll DISPATCHER";

        public void DoWork(IScheduler scheduler, params string[] args)
        {
            try
            {
                var gatewayDispatcherConfigSection = (PayRollConfigurationSection)ConfigurationManager.GetSection("payRollConfiguration");

                if (gatewayDispatcherConfigSection != null)
                {
                    _messageProcessor = new DispatcherMessageProcessor(_channelService, _logger, gatewayDispatcherConfigSection, _messageQueueService);

                    _messageProcessor.Open();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{0}->DoWork...", ex, Description);
            }
        }

        public void Exit()
        {
            try
            {
                _messageProcessor?.Close();
            }
            catch (Exception ex)
            {
                _logger.LogInfo("{0}->Exit...", ex, Description);
            }
        }

    }
}
