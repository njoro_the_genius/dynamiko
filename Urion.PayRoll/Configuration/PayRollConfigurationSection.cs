﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urion.PayRoll.Configuration
{
    public class PayRollConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("payRollSettings")]
        public PayRollSettingsCollection GatewayDispatcherSettingsCollection => (PayRollSettingsCollection)base["payRollSettings"];
    }
}
