﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urion.PayRoll.Configuration
{
    public class PayRollSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("uniqueId", IsKey = true, IsRequired = true)]
        public string UniqueId
        {
            get => (string)base["uniqueId"];
            set => base["uniqueId"] = value;
        }

        [ConfigurationProperty("queuePageIndex", IsKey = false, IsRequired = true)]
        public int QueuePageIndex
        {
            get => (int)base["queuePageIndex"];
            set => base["queuePageIndex"] = value;
        }

        [ConfigurationProperty("queuePageSize", IsKey = false, IsRequired = true)]
        public int QueuePageSize
        {
            get => (int)base["queuePageSize"];
            set => base["queuePageSize"] = value;
        }

        [ConfigurationProperty("timeToBeReceived", IsKey = false, IsRequired = true)]
        public int TimeToBeReceived
        {
            get => (int)base["timeToBeReceived"];
            set => base["timeToBeReceived"] = value;
        }

        [ConfigurationProperty("hoursCap", IsKey = false, IsRequired = true)]
        public int HoursCap
        {
            get => (int)base["hoursCap"];
            set => base["hoursCap"] = value;
        }

        [ConfigurationProperty("timeout", IsKey = false, IsRequired = true)]
        public int Timeout
        {
            get => (int)base["timeout"];
            set => base["timeout"] = value;
        }

        [ConfigurationProperty("enabled", IsKey = false, IsRequired = true)]
        public int Enabled
        {
            get => (int)base["enabled"];
            set => base["enabled"] = value;
        }
        
    }
}
