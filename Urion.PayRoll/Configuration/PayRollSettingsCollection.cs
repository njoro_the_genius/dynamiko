﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urion.PayRoll.Configuration
{
    public class PayRollSettingsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new PayRollSettingsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PayRollSettingsElement)element).UniqueId;
        }

        public PayRollSettingsElement this[int index] => (PayRollSettingsElement)BaseGet(index);

        [ConfigurationProperty("name", IsRequired = false)]
        public string Name => (string)base["name"];

        [ConfigurationProperty("queuePath", IsRequired = true)]
        public string QueuePath => (string)base["queuePath"];

        [ConfigurationProperty("queueingJobCronExpression", IsRequired = true)]
        public string QueueingJobCronExpression => (string)base["queueingJobCronExpression"];

        [ConfigurationProperty("requeuingJobCronExpression", IsRequired = true)]
        public string RequeuingJobCronExpression => (string)base["requeuingJobCronExpression"];

        [ConfigurationProperty("queryingJobCronExpression", IsRequired = true)]
        public string QueryingJobCronExpression => (string)base["queryingJobCronExpression"];


        [ConfigurationProperty("templatePath", IsRequired = true)]
        public string TemplatePath => (string)base["templatePath"];

        [ConfigurationProperty("queueReceivers", IsRequired = true)]
        public int QueueReceivers => (int)base["queueReceivers"];

        [ConfigurationProperty("logEnabled", IsRequired = true)]
        public int LogEnabled => (int)base["logEnabled"];

    }
}
