﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urion.PayRoll.Configuration;
using Urion.PayRoll.DispatcherHandlers;

namespace Urion.PayRoll.QueueProcessors
{
    public class DispatcherMessageProcessor : MessageProcessor<QueueDTO>
    {
        private readonly IChannelService _channelService;

        private readonly IDispatcher _dispatcher;

        private readonly ILogger _logger;

        public DispatcherMessageProcessor(
           IChannelService channelService,
           ILogger logger,
           PayRollConfigurationSection gatewayDispatcherConfigurationSection,
           IMessageQueueService messageQueueService) : base(gatewayDispatcherConfigurationSection.GatewayDispatcherSettingsCollection.QueuePath, gatewayDispatcherConfigurationSection.GatewayDispatcherSettingsCollection.QueueReceivers)
        {
            _channelService = channelService;

            _logger = logger;

            _gatewayDispatcherConfigurationSection = gatewayDispatcherConfigurationSection;

            _dispatcher = new FakeDispatcher(new PayRollProcessorDispatcher(_channelService, _gatewayDispatcherConfigurationSection, messageQueueService, _logger));
        }

        private PayRollConfigurationSection _gatewayDispatcherConfigurationSection;

        protected override void LogError(Exception exception)
        {
            _logger.LogError("{0}->PayRollProcessorDispatcher...", exception, _gatewayDispatcherConfigurationSection.GatewayDispatcherSettingsCollection.QueuePath);
        }

        protected override Task Process(QueueDTO queueDTO, int appSpecific)
        {
            return _dispatcher.HandleRequest(queueDTO, appSpecific);
        }
    }
}
