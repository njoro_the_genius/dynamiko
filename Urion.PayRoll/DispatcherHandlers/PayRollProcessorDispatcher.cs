﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Urion.PayRoll.Configuration;

namespace Urion.PayRoll.DispatcherHandlers
{
    public class PayRollProcessorDispatcher : IDispatcher
    {
        private readonly PayRollConfigurationSection _gatewayDispatcherConfigurationSection;

        private readonly IChannelService _channelService;

        private readonly IMessageQueueService _messageQueueService;

        private readonly LimitedPool<HttpClient> _httpClientPool;

        public PayRollProcessorDispatcher(
            IChannelService channelService,
            PayRollConfigurationSection gatewayDispatcherConfigurationSection,
            IMessageQueueService messageQueueService, ILogger logger)
        {
            _channelService = channelService;

            _gatewayDispatcherConfigurationSection = gatewayDispatcherConfigurationSection;

            _messageQueueService = messageQueueService;
        }

        public Task HandleRequest(QueueDTO queueDTO, int appSpecific)
        {
            return ProcessAsync(queueDTO, appSpecific);
        }

        public async Task ProcessAsync(QueueDTO queueDto, int appSpecific)
        {
            foreach (var settingsItem in _gatewayDispatcherConfigurationSection.GatewayDispatcherSettingsCollection)
            {
                var settingElement = (PayRollSettingsElement)settingsItem;

                var serviceHeader = new ServiceHeader { ApplicationDomainName = queueDto.AppDomainName };


                if (settingElement != null && settingElement.Enabled == 1)
                {
                    var messageCategory = (MessageCategory)appSpecific;

                    switch (messageCategory)
                    {
                        case MessageCategory.PayRollQueue:

                            await _channelService.ProcessPayRoll(queueDto.RecordId, serviceHeader);

                            break;
                    }

                    }
                }
        }

        #region Helpers

        public async Task<Tuple<HttpStatusCode, string>> PostAsync(LimitedPool<HttpClient> httpClientPool, string payload, string url)
        {
            using (var httpClientContainer = httpClientPool.Get())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var httpContent = new StringContent(payload, Encoding.UTF8, "application/x-www-form-urlencoded");

                httpClientContainer.Value.DefaultRequestHeaders.Clear();

                var response = await httpClientContainer.Value.PostAsync(url, httpContent);

                var content = await response.Content.ReadAsStringAsync();

                return new Tuple<HttpStatusCode, string>(response.StatusCode, content);
            }
        }

        #endregion
    }
}
