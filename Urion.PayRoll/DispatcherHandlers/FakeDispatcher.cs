﻿using Infrastructure.Crosscutting.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urion.PayRoll.DispatcherHandlers
{
    public class FakeDispatcher : IDispatcher
    {
        private readonly IDispatcher _next;

        public FakeDispatcher(IDispatcher next)
        {
            _next = next;
        }

        public Task HandleRequest(QueueDTO queueDTO, int appSpecific)
        {
            return _next.HandleRequest(queueDTO, appSpecific);
        }
    }
}
