﻿using Infrastructure.Crosscutting.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urion.PayRoll.DispatcherHandlers
{
    public interface IDispatcher
    {
        Task HandleRequest(QueueDTO queueDto, int appSpecific);
    }
}
