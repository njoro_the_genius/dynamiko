﻿using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.ServiceModel;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IRedisClientService")]
    public interface IRedisClientService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginSeedSystemAccessRights(AsyncCallback callback, Object state);
        bool EndSeedSystemAccessRights(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginSeedSystemAccessRight(string role, AsyncCallback callback, Object state);
        bool EndSeedSystemAccessRight(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginValidatePermission(string username, string controllerName, string actionName, AsyncCallback callback, Object state);
        bool EndValidatePermission(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginRemoveAllRights(string[] roles, AsyncCallback callback, Object state);
        bool EndRemoveAllRights(IAsyncResult result);
    }
}
