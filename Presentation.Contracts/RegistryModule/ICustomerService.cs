﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts.RegistryModule
{
    [ServiceContract(Name = "ICustomerService")]
    public interface ICustomerService
    {
        #region CustomerDTO

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewCustomer(CustomerDTO customerDto, AsyncCallback callback, Object state);
        Tuple<CustomerDTO, List<string>> EndAddNewCustomer(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateCustomer(CustomerDTO customerDto, AsyncCallback callback, Object state);
        bool EndUpdateCustomer(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindCustomers(AsyncCallback callback, Object state);
        List<CustomerDTO> EndFindCustomers(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindCustomerById(Guid id, AsyncCallback callback, Object state);
        CustomerDTO EndFindCustomerById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindCustomersByFullText(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<CustomerDTO> EndFindCustomersByFullText(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindCustomersByFullTextAndIsEnabled(string searchString, bool isEnabled, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<CustomerDTO> EndFindCustomersByFullTextAndIsEnabled(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindCustomersByName(string name, AsyncCallback callback, Object state);
        List<CustomerDTO> EndFindCustomersByName(IAsyncResult result);

        #endregion
    }
}
