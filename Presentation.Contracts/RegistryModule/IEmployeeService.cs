﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts.RegistryModule
{
    [ServiceContract(Name = "IEmployeeService")]
    public interface IEmployeeService
    {
        #region EmployeeService

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewEmployee(EmployeeDTO employeeDTO, AsyncCallback callback, Object state);
        Tuple<EmployeeDTO, List<string>> EndAddNewEmployee(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateEmployee(EmployeeDTO employeeDTO, AsyncCallback callback, Object state);
        bool EndUpdateEmployee(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindEmployeeById(Guid id, AsyncCallback callback, Object state);
        EmployeeDTO EndFindEmployeeById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindCustomerEmployeesFilterInPage(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<EmployeeDTO> EndFindCustomerEmployeesFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindEmployeeByCustomerId(Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<EmployeeDTO> EndFindEmployeeByCustomerId(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindEmployeeByUserId(Guid userId, Guid? customerId, AsyncCallback callback, Object state);
        EmployeeDTO EndFindEmployeeByUserId(IAsyncResult result);
        #endregion
    }
}
