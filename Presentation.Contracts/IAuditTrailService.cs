﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IAuditTrailService")]
    public interface IAuditTrailService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddAuditTrails(List<AuditTrailDTO> auditTrailDTOs, AsyncCallback callback, Object state);
        bool EndAddAuditTrails(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddAuditTrail(AuditTrailDTO auditTrailDTO, AsyncCallback callback, Object state);
        AuditTrailDTO EndAddAuditTrail(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindAuditTrailsByDateRangeAndFilterInPage(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string filter, AsyncCallback callback, Object state);
        PageCollectionInfo<AuditTrailDTO> EndFindAuditTrailsByDateRangeAndFilterInPage(IAsyncResult result);
    }
}
