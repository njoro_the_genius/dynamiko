﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts.MessagingModule
{
    [ServiceContract(Name = "IEmailAlertService")]
    public interface IEmailAlertService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddEmailAlert(EmailAlertDTO emailAlertDTO, AsyncCallback asyncCallback, object state);
        EmailAlertDTO EndAddEmailAlert(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindEmailAlerts(Guid? customerId, int pageIndex, int pageSize, AsyncCallback asyncCallback, object state);
        PageCollectionInfo<EmailAlertDTO> EndFindEmailAlerts(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindEmailAlertsWithRecipientOrSubjectOrBody(Guid? customerId, string searchText, int pageIndex, int pageSize, AsyncCallback asyncCallback, object state);
        PageCollectionInfo<EmailAlertDTO> EndFindEmailAlertsWithRecipientOrSubjectOrBody(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindEmailAlertsWithDLRStatus(Guid? customerId, int dlrStatus, int pageIndex, int pageSize, AsyncCallback asyncCallback, object state);
        PageCollectionInfo<EmailAlertDTO> EndFindEmailAlertsWithDLRStatus(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindEmailAlertWithId(Guid id, AsyncCallback asyncCallback, object state);
        EmailAlertDTO EndFindEmailAlertWithId(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateEmailAlert(EmailAlertDTO emailAlertDTO, AsyncCallback asyncCallback, object state);
        bool EndUpdateEmailAlert(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGenerateEmail(String templatePath, IDictionary<string, object> expandoObject, string subject, string messageTo, bool mailMessageIsBodyHtml, bool mailMessageSecurityCritical, AsyncCallback asyncCallback, object state);
        bool EndGenerateEmail(IAsyncResult result);
    }
}
