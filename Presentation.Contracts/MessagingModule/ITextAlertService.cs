﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts.MessagingModule
{
    [ServiceContract(Name = "ITextAlertService")]
    public interface ITextAlertService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewText(TextAlertDTO textAlertDto, AsyncCallback callback, Object state);
        TextAlertDTO EndAddNewText(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddTextAlerts(List<TextAlertDTO> textAlertDTOs, AsyncCallback callback, Object state);
        bool EndAddTextAlerts(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateTextAlert(TextAlertDTO textAlertDTO, AsyncCallback callback, Object state);
        bool EndUpdateTextAlert(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlerts(AsyncCallback callback, Object state);
        List<TextAlertDTO> EndFindTextAlerts(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlertsByDLRStatus(int dlrStatus, AsyncCallback callback, Object state);
        List<TextAlertDTO> EndFindTextAlertsByDLRStatus(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlertsInPage(int pageIndex, int pageSize, List<string> sortFields, bool ascending, AsyncCallback callback, Object state);
        PageCollectionInfo<TextAlertDTO> EndFindTextAlertsInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlertsByFilterInPage(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, AsyncCallback callback, Object state);
        PageCollectionInfo<TextAlertDTO> EndFindTextAlertsByFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlertsByDateRangeAndFilterInPage(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, AsyncCallback callback, Object state);
        PageCollectionInfo<TextAlertDTO> EndFindTextAlertsByDateRangeAndFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlertsByDLRStatusAndOrigin(int dlrStatus, int origin, AsyncCallback callback, Object state);
        List<TextAlertDTO> EndFindTextAlertsByDLRStatusAndOrigin(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlert(Guid textAlertId, AsyncCallback callback, Object state);
        TextAlertDTO EndFindTextAlert(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlertsFilterInPage(DateTime startDate, DateTime endDate, Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<TextAlertDTO> EndFindTextAlertsFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTextAlertsByRecipientMobileNumberFilterInPage(string mobileNumber, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<TextAlertDTO> EndFindTextAlertsByRecipientMobileNumberFilterInPage(IAsyncResult result);
    }
}
