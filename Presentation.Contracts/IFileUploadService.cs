﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IFileUploadService")]
    public interface IFileUploadService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFileUpload(FileData fileData, AsyncCallback callback, object state);
        string EndFileUpload(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFileUploadDone(string filename, AsyncCallback callback, object state);
        bool EndFileUploadDone(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginPingNetwork(string hostNameOrAddress, AsyncCallback callback, object state);
        bool EndPingNetwork(IAsyncResult result);
    }
}
