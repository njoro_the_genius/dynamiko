﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "IAspnetIdentityManagerService")]
    public interface IAspnetIdentityManagerService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetApplicationUserCollectionByFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, object state);
        PageCollectionInfo<ApplicationUserDTO> EndGetApplicationUserCollectionByFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetApplicationUserCollectionByCustomerId(Guid customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, object state);
        PageCollectionInfo<ApplicationUserDTO> EndGetApplicationUserCollectionByCustomerId(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindApplicationUser(string userName, AsyncCallback callback, object state);
        ApplicationUserDTO EndFindApplicationUser(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindApplicationUserById(string id, AsyncCallback callback, object state);
        ApplicationUserDTO EndFindApplicationUserById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindApplicationRoleById(string id, AsyncCallback callback, object state);
        ApplicationRoleDTO EndFindApplicationRoleById(IAsyncResult result);


        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetApplicationUsersInRoles(string roleName, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, object state);
        PageCollectionInfo<ApplicationUserDTO> EndGetApplicationUsersInRoles(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindApplicationUsersInRoles(string roleName, AsyncCallback callback, object state);
        List<ApplicationUserDTO> EndFindApplicationUsersInRoles(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginCreateUser(ApplicationUserDTO applicationUserDto, AsyncCallback callback, object state);
        ApplicationUserDTO EndCreateUser(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateAspnetIdentityUser(ApplicationUserDTO applicationUserDto, AsyncCallback callback, object state);
        bool EndUpdateAspnetIdentityUser(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateApplicationRole(ApplicationRoleDTO applicationRoleDto, AsyncCallback callback, object state);
        bool EndUpdateApplicationRole(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetUserRoles(string username, AsyncCallback callback, object state);
        string[] EndGetUserRoles(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetApplicationUsers(AsyncCallback callback, object state);
        List<ApplicationUserDTO> EndGetApplicationUsers(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindApplicationUserByUsernameAndPassword(string username, string password, AsyncCallback callback, object state);
        ApplicationUserDTO EndFindApplicationUserByUsernameAndPassword(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetApplicationRoleCollectionByFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, object state);
        PageCollectionInfo<ApplicationRoleDTO> EndGetApplicationRoleCollectionByFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginCreateApplicationRole(ApplicationRoleDTO applicationRoleDto, AsyncCallback callback, object state);
        bool EndCreateApplicationRole(IAsyncResult result);


        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginResetPasswordLink(string userName, string callBackUrl, AsyncCallback callback, object state);
        bool EndResetPasswordLink(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginResetPassword(PasswordResetModel passwordResetModel, AsyncCallback callback, object state);
        bool EndResetPassword(IAsyncResult result);

    }
}
