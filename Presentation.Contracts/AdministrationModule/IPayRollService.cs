﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "IPayRollService")]
    public interface IPayRollService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewPayRoll(PayRollDTO payRollDTO, AsyncCallback callback, Object state);
        PayRollDTO EndAddNewPayRoll(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdatePayRoll(PayRollDTO payRollDTO, AsyncCallback callback, Object state);
        bool EndUpdatePayRoll(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPayRollById(Guid id, AsyncCallback callback, Object state);
        PayRollDTO EndFindPayRollById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPayRollFilterInPage(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<PayRollDTO> EndFindPayRollFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPayRollItemsFilterInPageAsync(Guid payRollId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<PayRollItemDTO> EndFindPayRollItemsFilterInPageAsync(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPayRolls(AsyncCallback callback, Object state);
        List<PayRollDTO> EndFindPayRolls(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginProcessPayRoll(Guid payRollId, AsyncCallback callback, Object state);
        PayRollDTO EndProcessPayRoll(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindUnProcessedPayRollFilterInPage(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<PayRollDTO> EndFindUnProcessedPayRollFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPayRollByCustomerIdFilterInPage(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<PayRollDTO> EndFindPayRollByCustomerIdFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindPayRollItemByEmployeeIdDTOFilterInPage(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<PayRollItemDTO> EndFindPayRollItemByEmployeeIdDTOFilterInPage(IAsyncResult result);
    }
}

