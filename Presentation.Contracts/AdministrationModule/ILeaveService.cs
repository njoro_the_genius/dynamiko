﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "ILeaveService")]
    public interface ILeaveService
    {
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewLeave(LeaveDTO leaveDTO, AsyncCallback callback, Object state);
        LeaveDTO EndAddNewLeave(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateLeave(LeaveDTO leaveDTO, AsyncCallback callback, Object state);
        bool EndUpdateLeave(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLeaveById(Guid id, AsyncCallback callback, Object state);
        LeaveDTO EndFindLeaveById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLeaveByEmployeeId(Guid employeeId, AsyncCallback callback, Object state);
        LeaveDTO EndFindLeaveByEmployeeId(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLeavesByEmployeeId(Guid employeeId, AsyncCallback callback, Object state);
        List<LeaveDTO> EndFindLeavesByEmployeeId(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLeaveFilterInPage(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<LeaveDTO> EndFindLeaveFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLeaveFilterByCustomerInPage(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<LeaveDTO> EndFindLeaveFilterByCustomerInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLeaveFilterByEmployeeInPage(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<LeaveDTO> EndFindLeaveFilterByEmployeeInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindLeaves(AsyncCallback callback, Object state);
        List<LeaveDTO> EndFindLeaves(IAsyncResult result);
    }
}
