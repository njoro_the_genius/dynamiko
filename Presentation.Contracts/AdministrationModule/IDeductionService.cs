﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "IDeductionService")]
    public interface IDeductionService
    {
        #region NssfOrderDTO
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewNssfOrder(NssfOrderDTO nssfOrderDTO, AsyncCallback callback, Object state);
        NssfOrderDTO EndAddNewNssfOrder(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateNssfOrder(NssfOrderDTO nssfOrderDTO, AsyncCallback callback, Object state);
        bool EndUpdateNssfOrder(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindNssfOrderById(Guid id, AsyncCallback callback, Object state);
        NssfOrderDTO EndFindNssfOrderById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindNssfOrderFilterInPage(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<NssfOrderDTO> EndFindNssfOrderFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindNssfOrders(AsyncCallback callback, Object state);
        List<NssfOrderDTO> EndFindNssfOrders(IAsyncResult result);
        #endregion


        #region NhifOrderDTO
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewNhifOrder(NhifOrderDTO nhifOrderDTO, AsyncCallback callback, Object state);
        NhifOrderDTO EndAddNewNhifOrder(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateNhifOrder(NhifOrderDTO nhifOrderDTO, AsyncCallback callback, Object state);
        bool EndUpdateNhifOrder(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindNhifOrderById(Guid id, AsyncCallback callback, Object state);
        NhifOrderDTO EndFindNhifOrderById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindNhifOrderFilterInPage(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<NhifOrderDTO> EndFindNhifOrderFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindNhifOrders(AsyncCallback callback, Object state);
        List<NhifOrderDTO> EndFindNhifOrders(IAsyncResult result);
        #endregion

        #region deductions

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewDeductionAsync(DeductionDTO deductionDTO, AsyncCallback callback, Object state);
        DeductionDTO EndAddNewDeductionAsync(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateDeductionAsync(DeductionDTO deductionDTO, AsyncCallback callback, Object state);
        bool EndUpdateDeductionAsync(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindDeductionByIdAsync(Guid id, AsyncCallback callback, Object state);
        DeductionDTO EndFindDeductionByIdAsync(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindDeductionFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<DeductionDTO> EndFindDeductionFilterInPageAsync(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindDeductionByCustomerIdFilterInPageAsync(Guid customerId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<DeductionDTO> EndFindDeductionByCustomerIdFilterInPageAsync(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindDeduction(AsyncCallback callback, Object state);
        List<DeductionDTO> EndFindDeduction(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindActiveDeductionAsync(AsyncCallback callback, Object state);
        DeductionDTO EndFindActiveDeductionAsync(IAsyncResult result);
        #endregion
    }
}
