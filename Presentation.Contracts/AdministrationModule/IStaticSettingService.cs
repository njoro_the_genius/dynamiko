﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "IStaticSettingService")]
    public interface IStaticSettingService
    {
        #region Static Setting

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewSetting(StaticSettingDTO staticSettingDTO, AsyncCallback callback, Object state);
        StaticSettingDTO EndAddNewSetting(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateSetting(StaticSettingDTO settingDTO, AsyncCallback callback, Object state);
        bool EndUpdateSetting(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindSettingFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<StaticSettingDTO> EndFindSettingFilterInPage(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindSettingByKey(string keyName, AsyncCallback callback, Object state);
        StaticSettingDTO EndFindSettingByKey(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindSettingById(Guid id, AsyncCallback callback, Object state);
        StaticSettingDTO EndFindSettingById(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginRemoveStaticSetting(Guid id, AsyncCallback callback, Object state);
        bool EndRemoveStaticSetting(IAsyncResult asyncResult);

        #endregion

    }
}
