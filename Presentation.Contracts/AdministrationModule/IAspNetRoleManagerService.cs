﻿using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "IAspNetRoleManagerService")]
    public interface IAspNetRoleManagerService
    {

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetRoles(AsyncCallback callback, Object state);
        List<string> EndGetRoles(IAsyncResult result);
    }
}
