﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "IModuleNavigationItemService")]
    public interface IModuleNavigationItemService
    {
        #region ModuleNavigationItem

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddModuleNavigationItem(List<ModuleNavigationItemDTO> moduleNavigationItem, AsyncCallback callback, Object state);
        bool EndAddModuleNavigationItem(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationItemById(Guid id, AsyncCallback callback, Object state);
        ModuleNavigationItemDTO EndFindModuleNavigationItemById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationItems(AsyncCallback callback, Object state);
        List<ModuleNavigationItemDTO> EndFindModuleNavigationItems(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationItemsFilterPageCollectionInfo(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<ModuleNavigationItemDTO> EndFindModuleNavigationItemsFilterPageCollectionInfo(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationActionControllerName(string controllerName, AsyncCallback callback, Object state);
        List<ModuleNavigationItemDTO> EndFindModuleNavigationActionControllerName(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginValidateAccessPermission(string controllerName, string actionName, string username, AsyncCallback callback, Object state);
        bool EndValidateAccessPermission(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginCacheRoleAccessPermissions(string[] currentUserRole, AsyncCallback callback, Object state);
        List<RolePermissionDTO> EndCacheRoleAccessPermissions(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginBulkInsertModuleNavigationItem(List<Guid> modulesNavigationIds, string roleId, AsyncCallback callback, Object state);
        bool EndBulkInsertModuleNavigationItem(IAsyncResult result);

        #endregion

        #region ModuleNavigationItemInRole

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddModuleNavigationItemInRole(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, AsyncCallback callback, Object state);
        bool EndAddModuleNavigationItemInRole(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationItemByRole(string roleId, AsyncCallback callback, Object state);
        List<ModuleNavigationItemInRoleDTO> EndFindModuleNavigationItemByRole(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationItemInRoleByModuleNavigationId(Guid moduleNavigationId, AsyncCallback callback, Object state);
        List<ModuleNavigationItemInRoleDTO> EndFindModuleNavigationItemInRoleByModuleNavigationId(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationItemByRolePageCollectionInfo(string roleId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<ModuleNavigationItemInRoleDTO> EndFindModuleNavigationItemByRolePageCollectionInfo(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindModuleNavigationItemInRoleById(Guid id, AsyncCallback callback, Object state);
        ModuleNavigationItemInRoleDTO EndFindModuleNavigationItemInRoleById(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginRemoveModuleNavigationItemInRole(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, AsyncCallback callback, Object state);
        bool EndRemoveModuleNavigationItemInRole(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginBulkInsertModuleNavigationItemInRoles(List<Guid> moduleNavigationItemInRole, string roleId, AsyncCallback callback, Object state);
        bool EndBulkInsertModuleNavigationItemInRoles(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateModuleNavigationItemInRoleList(ModuleNavigationItemInRoleDTO moduleNavigationItemInRole, AsyncCallback callback, Object state);
        bool EndUpdateModuleNavigationItemInRoleList(IAsyncResult result);

        #endregion
    }
}
