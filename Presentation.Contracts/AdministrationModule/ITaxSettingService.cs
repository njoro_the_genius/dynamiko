﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Contracts.AdministrationModule
{
    [ServiceContract(Name = "ITaxSettingService")]
    public interface ITaxSettingService
    {

        #region TaxSettingService

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginAddNewTaxOrder(TaxOrderDTO taxOrderDTO, AsyncCallback callback, Object state);
        TaxOrderDTO EndAddNewTaxOrder(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginUpdateTaxOrder(TaxOrderDTO taxOrderDTO, AsyncCallback callback, Object state);
        bool EndUpdateTaxOrder(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTaxSettingById(Guid id, AsyncCallback callback, Object state);
        TaxOrderDTO EndFindTaxSettingById(IAsyncResult asyncResult);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindTaxOrderFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, AsyncCallback callback, Object state);
        PageCollectionInfo<TaxOrderDTO> EndFindTaxOrderFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindGraduatedScales(Guid chargeId, AsyncCallback callback, Object state);
        List<TaxRangesDTO> EndFindGraduatedScales(IAsyncResult result);

        #endregion
    }
}
