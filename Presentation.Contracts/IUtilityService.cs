﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace Presentation.Contracts
{
    [ServiceContract(Name = "IUtilityService")]
    public interface IUtilityService
    {
      
        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginConfigureApplicationDatabase(AsyncCallback callback, object state);
        bool EndConfigureApplicationDatabase(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginConfigureAspNetIdentityDatabase(AsyncCallback callback, object state);
        bool EndConfigureAspNetIdentityDatabase(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginFindDashboardWidgets(int claim, Guid? customerId, AsyncCallback callback, Object state);
        DashboardWidgetDTO EndFindDashboardWidgets(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetDashboardGraphDataSet(int claim, Guid? customerId, AsyncCallback callback, Object state);
        List<DashboardGraphDTO> EndGetDashboardGraphDataSet(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetEmployeesReport(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, AsyncCallback callback, object state);
        PageCollectionInfo<EmployeeDTO> EndGetEmployeesReport(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetBanksList(AsyncCallback callback, object state);
        List<Bank> EndGetBanksList(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetBranchesList(string bankCode, AsyncCallback callback, object state);
        List<Branch> EndGetBranchesList(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetBankByBankCode(string bankCode, AsyncCallback callback, object state);
        Bank EndGetBankByBankCode(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetBrancheByBranchCode(string bankCode, string branchCode, AsyncCallback callback, object state);
        Branch EndGetBrancheByBranchCode(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetBranchesListFilterInPage(string bankCode, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, AsyncCallback callback, object state);
        PageCollectionInfo<Branch> EndGetBranchesListFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetBanksListFilterInPage(string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, AsyncCallback callback, object state);
        PageCollectionInfo<Bank> EndGetBanksListFilterInPage(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetPaySlip(Guid employeeId, int period, AsyncCallback callback, object state);
        PageCollectionInfo<PaySlipDTO> EndGetPaySlip(IAsyncResult result);

        [OperationContract(AsyncPattern = true)]
        [FaultContract(typeof(ApplicationServiceError))]
        IAsyncResult BeginGetPayRoll(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, AsyncCallback callback, object state);
        PageCollectionInfo<PaySlipDTO> EndGetPayRoll(IAsyncResult result);
    }
}
