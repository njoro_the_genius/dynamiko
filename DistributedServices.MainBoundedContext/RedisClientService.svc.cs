﻿using Application.MainBoundedContext.Services;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using DistributedServices.Seedwork.ErrorHandlers;
using Infrastructure.Crosscutting.Framework.Utils;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RedisClientService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RedisClientService.svc or RedisClientService.svc.cs at the Solution Explorer and start debugging.
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class RedisClientService : IRedisClientService
    {
        private readonly IRedisClientAppService _redisClientAppService;

        private readonly IAspNetRoleManagerService _aspNetRoleManagerService;

        public RedisClientService(IRedisClientAppService redisClientAppService, IAspNetRoleManagerService aspNetRoleManagerService)
        {
            Guard.ArgumentNotNull(redisClientAppService, nameof(redisClientAppService));

            Guard.ArgumentNotNull(aspNetRoleManagerService, nameof(aspNetRoleManagerService));

            _redisClientAppService = redisClientAppService;

            _aspNetRoleManagerService = aspNetRoleManagerService;
        }

        public async Task<bool> SeedSystemAccessRightAsync(string role)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _redisClientAppService.SeedSystemAccessRightAsync(role, serviceHeader);
        }

        public async Task<bool> SeedSystemAccessRightsAsync()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            var roles = _aspNetRoleManagerService.GetRoles();

            return await _redisClientAppService.SeedSystemAccessRightsAsync(roles.ToArray(), serviceHeader);
        }

        public async Task<bool> ValidatePermissionAsync(string username, string controllerName, string actionName)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            var getUserRoles = _aspNetRoleManagerService.GetUserRoles(username);

            return await _redisClientAppService.ValidatePermissionAsync(getUserRoles, controllerName, actionName, serviceHeader);
        }

        public async Task<bool> RemoveAllRightsAsync(string[] roles)
        {
            return await _redisClientAppService.RemoveAllRightsAsync(roles);
        }
    }
}
