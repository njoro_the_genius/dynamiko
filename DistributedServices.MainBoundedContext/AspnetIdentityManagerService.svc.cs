﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.MainBoundedContext.RegistryModule;
using Application.MainBoundedContext.Services;
using Application.Seedwork;
using AutoMapper;
using DistributedServices.MainBoundedContext.Identity;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Web;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AspnetIdentityManagerService : IAspnetIdentityManagerService
    {
        private readonly IAuditTrailAppService _auditTrailAppService;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;
        private readonly IEmailAppService _emailAppService;
        
        private readonly string _baseDirectoryPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data");

        public AspnetIdentityManagerService(
            IAuditTrailAppService auditTrailAppService,
            ApplicationUserManager applicationUserManager,
            ApplicationRoleManager applicationRoleManager,
            IEmailAppService emailAppService,
            IEmployeeAppService employeeAppService
         )
        {
            Guard.ArgumentNotNull(emailAppService, nameof(emailAppService));

            _auditTrailAppService = auditTrailAppService;
            _userManager = applicationUserManager;
            _roleManager = applicationRoleManager;
            _emailAppService = emailAppService;
        }

        public async Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns,
            string searchstring, bool sortAscending)
        {
            var allUsers = _userManager.Users.ToList();

            var project = Util.ProjectToApplicationUserDTO(allUsers);

            await FetchApplicationUserRole(project);

            return ProjectionsExtensionMethods.AllMatchingPaged(project.AsQueryable(), Util.ApplicationUserFullText(searchstring), startIndex, pageSize, sortedColumns, sortAscending);
        }

        public PageCollectionInfo<ApplicationRoleDTO> GetApplicationRoleCollectionByFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns,
            string searchstring, bool sortAscending)
        {
            var allRoles = _roleManager.Roles.ToList();

            var project = Util.ProjectToApplicationRoleDTO(allRoles);

            return ProjectionsExtensionMethods.AllMatchingPaged(project.AsQueryable(), Util.ApplicationRoleName(searchstring), startIndex, pageSize, sortedColumns, sortAscending);
        }

        public async Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByCustomerIdAsync(Guid customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchstring, bool sortAscending)
        {
            var allUsers = _userManager.Users.Where(x => x.CustomerId == customerId).ToList();

            var project = Util.ProjectToApplicationUserDTO(allUsers);

            await FetchApplicationUserRole(project);

            return ProjectionsExtensionMethods.AllMatchingPaged(project.AsQueryable(), Util.ApplicationUserFullText(searchstring), startIndex, pageSize, new List<string> { "Username", "FirstName", "OtherNames" }, sortAscending);
        }

        public async Task<ApplicationUserDTO> FindApplicationUserAsync(string userName)
        {
            var applicationUser = await _userManager.FindByNameAsync(userName);

            return Util.ProjectToApplicationUserDTO(applicationUser);
        }

        public async Task<ApplicationUserDTO> FindApplicationUserByIdAsync(string id)
        {
            var applicationUser = await _userManager.FindByIdAsync(id);

            return Util.ProjectToApplicationUserDTO(applicationUser);
        }

        public async Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUsersInRolesAsync(string roleName, int startIndex, int pageSize, IList<string> sortedColumns, string searchstring, bool sortAscending)
        {
            var role = await _roleManager.FindByNameAsync(roleName);

            if (role == null)
                return null;

            var usersInRoles = _userManager.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id));

            var project = Util.ProjectToApplicationUserDTO(usersInRoles.ToList());

            return ProjectionsExtensionMethods.AllMatchingPaged(project.AsQueryable(), Util.ApplicationUserFullText(searchstring), startIndex, pageSize, new List<string> { "Username", "FirstName", "OtherNames" }, sortAscending);
        }

        public async Task<List<ApplicationUserDTO>> FindApplicationUsersInRolesAsync(string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);

            var usersInRoles = _userManager.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id));

            var project = Util.ProjectToApplicationUserDTO(usersInRoles.ToList());

            return project;
        }

        public async Task<ApplicationUserDTO> CreateUserAsync(ApplicationUserDTO applicationUserDto)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            serviceHeader.ApplicationDomainName = serviceHeader.ApplicationDomainName.Equals("?") ? applicationUserDto.ApplicationDomainName : serviceHeader.ApplicationDomainName;

            await _auditTrailAppService.AddNewAuditTrailAsync(new AuditTrailDTO
            {
                EventType = "Entity_Added",
                AdditionalNarration = $"CreateUser->Username = {applicationUserDto.UserName}",
            }, serviceHeader);

            var createdDate = DateTime.Now;

            byte recordStatus = 0;

            bool iSApproved = false;

            //if created by su insert as approved and generate email
            if (applicationUserDto.CreatedBy == "su")
            {
                recordStatus = (byte)RecordStatus.Approved;

                iSApproved = true;
            }
            else
            {
                recordStatus = (byte)RecordStatus.New;
                

                iSApproved = false;
            }

            var user = new ApplicationUser
            {
                UserName = applicationUserDto.UserName,
                Email = applicationUserDto.Email,
                EmailConfirmed = true,
                TwoFactorEnabled = applicationUserDto.TwoFactorEnabled,
                PhoneNumber = applicationUserDto.PhoneNumber,
                PhoneNumberConfirmed = true,
                LockoutEnabled = true,
                LastPasswordChangedDate = createdDate,
                CreatedDate = createdDate,
                IsEnabled = iSApproved,
                IsExternalUser = applicationUserDto.IsExternalUser,
                CustomerId = applicationUserDto.CustomerId,
                RecordStatus = recordStatus,
                CreatedBy = applicationUserDto.CreatedBy,
            };

           
            var identityResult = await _userManager.CreateAsync(user);

            if (identityResult.Succeeded)
            {                

                var applicationUser = await FindApplicationUserAsync(applicationUserDto.UserName);

                applicationUser.UserCreateSucceed = true;

                if (!string.IsNullOrEmpty(applicationUserDto.RoleName))
                {
                    var result = await _userManager.AddToRoleAsync(applicationUser.Id, applicationUserDto.RoleName);

                    if (result.Succeeded && applicationUserDto.CreatedBy != "su")
                    {
                        var queueDTO = ApprovalRequestMessageFactory.CreateApprovalRequestMessage(Guid.Parse(applicationUser.Id), serviceHeader.ApplicationDomainName, "AspNetUsers", serviceHeader.ApplicationUserName, "New User, Username : " + applicationUser.UserName, (int)EntityState.Added);

                        ApprovalRequestMessageFactory.SendMessage(queueDTO, MessageCategory.ApprovalRequest);
                    }
                    else if (applicationUserDto.CreatedBy == "su")
                    {
                        // //no approval required generate email

                        string newPassword = PasswordStore.GenerateRandomPassword(new PasswordOptions
                        {
                            RequiredLength = int.Parse(ConfigurationManager.AppSettings["RequiredPasswordLength"]),
                            RequireNonLetterOrDigit = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireNonLetterOrDigit"]),
                            RequireDigit = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireDigit"]),
                            RequireLowercase = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireLowercase"]),
                            RequireUppercase = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireUppercase"]),
                            RequireNonAlphanumeric = true,
                            RequiredUniqueChars = 1
                        });

                        var passwordResetToken = _userManager.GeneratePasswordResetToken(applicationUser.Id);

                        var resetPassword = _userManager.ResetPassword(applicationUser.Id, passwordResetToken, newPassword);


                        string templatePath = Path.Combine(_baseDirectoryPath, @"EmailMessageTemplate\LoginCredentials_EmailTemplate.cshtml");

                        dynamic expando = new ExpandoObject();

                        var loginCredentialsModel = expando as IDictionary<string, object>;

                        loginCredentialsModel.Add("Name", applicationUser.UserName);

                        loginCredentialsModel.Add("UserName", applicationUser.UserName);

                        loginCredentialsModel.Add("Password", newPassword);

                        loginCredentialsModel.Add("LoginURL", ConfigurationManager.AppSettings["WebClientUrl"]);

                        loginCredentialsModel.Add("LogoURL", ConfigurationManager.AppSettings["AspnetMvcLogoURL"]);

                        string subject = applicationUser.UserName + " Login Details";

                        await _emailAppService.GenerateEmailAsync(templatePath, loginCredentialsModel, subject, applicationUser.Email, true, true, serviceHeader);

                    }
                    else
                    {

                    }
                }

                return applicationUser;
            }
            else
            {
                throw  new Exception(string.Join(",",identityResult.Errors.ToArray()));
            }

        }

        public async Task<bool> CreateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto)
        {
            ApplicationRole applicationRole = new ApplicationRole()
            {
                Name = applicationRoleDto.Name
            };

            var identityResult = await _roleManager.CreateAsync(applicationRole);

            return identityResult.Succeeded;
        }

        public async Task<bool> UpdateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto)
        {
            if (applicationRoleDto != null)
            {
                ApplicationRole applicationRole = await _roleManager.FindByIdAsync(applicationRoleDto.Id.ToString());

                applicationRole.Name = applicationRoleDto.Name;

                var identityResult = await _roleManager.UpdateAsync(applicationRole);

                return identityResult.Succeeded;
            }

            return false;
        }

        public async Task<bool> UpdateAspnetIdentityUserAsync(ApplicationUserDTO applicationUserDto)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            await _auditTrailAppService.AddNewAuditTrailAsync(new AuditTrailDTO
            {
                EventType = "Entity_Modified",
                AdditionalNarration = $"UpdateUser->Username = {applicationUserDto.UserName}",
            }, serviceHeader);

            var applicationUser = await _userManager.FindByNameAsync(applicationUserDto.UserName);

            var updateResult = new IdentityResult();

            if (applicationUser != null)
            {
                switch ((RecordStatus)applicationUser.RecordStatus)
                {
                    case RecordStatus.New:
                        applicationUser.RecordStatus = (byte)applicationUserDto.RecordStatus;
                        applicationUser.ApprovedBy = applicationUserDto.ApprovedBy;
                        applicationUser.LockoutEnabled = true;
                        applicationUser.PhoneNumber = applicationUserDto.PhoneNumber;
                        applicationUser.IsExternalUser = applicationUserDto.IsExternalUser;
                        applicationUser.ApprovedDate = DateTime.Now;
                        applicationUser.Remarks = applicationUserDto.Remarks;
                        applicationUser.IsEnabled = applicationUserDto.IsEnabled;
                        applicationUser.CustomerId = applicationUserDto.CustomerId;
                       

                        updateResult = await _userManager.UpdateAsync(applicationUser);

                        if (updateResult.Succeeded && !string.IsNullOrWhiteSpace(applicationUserDto.RoleName))
                        {
                            var currentUserRoles = await _userManager.GetRolesAsync(applicationUserDto.Id);

                            if (currentUserRoles != null && currentUserRoles.Any())
                            {
                                Array.ForEach(currentUserRoles.ToArray(), roleName =>
                                {
                                    _userManager.RemoveFromRole(applicationUser.Id, roleName);
                                });
                            }

                            var role = await _roleManager.FindByNameAsync(applicationUserDto.RoleName);

                            if (role != null)
                                _userManager.AddToRole(applicationUserDto.Id, applicationUserDto.RoleName);
                        }


                        if (updateResult.Succeeded && applicationUser.RecordStatus == (byte)RecordStatus.Approved)
                        {
                            string newPassword = PasswordStore.GenerateRandomPassword(new PasswordOptions
                            {
                                RequiredLength = int.Parse(ConfigurationManager.AppSettings["RequiredPasswordLength"]),
                                RequireNonLetterOrDigit = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireNonLetterOrDigit"]),
                                RequireDigit = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireDigit"]),
                                RequireLowercase = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireLowercase"]),
                                RequireUppercase = bool.Parse(ConfigurationManager.AppSettings["PasswordRequireUppercase"]),
                                RequireNonAlphanumeric = true,
                                RequiredUniqueChars = 1
                            });

                            var passwordResetToken = _userManager.GeneratePasswordResetToken(applicationUser.Id);

                            var resetPassword = _userManager.ResetPassword(applicationUser.Id, passwordResetToken, newPassword);

                            if (resetPassword.Succeeded)
                            {
                                string templatePath = Path.Combine(_baseDirectoryPath, @"EmailMessageTemplate\LoginCredentials_EmailTemplate.cshtml");

                                dynamic expando = new ExpandoObject();

                                var loginCredentialsModel = expando as IDictionary<string, object>;

                                //loginCredentialsModel.Add("Name", applicationUser.FirstName + " " + applicationUser.OtherNames);

                                loginCredentialsModel.Add("UserName", applicationUser.UserName);

                                loginCredentialsModel.Add("Password", newPassword);

                                loginCredentialsModel.Add("LoginURL", ConfigurationManager.AppSettings["WebClientUrl"]);

                                loginCredentialsModel.Add("LogoURL", ConfigurationManager.AppSettings["AspnetMvcLogoURL"]);

                                string subject = applicationUser.UserName + " Login Details";

                                await _emailAppService.GenerateEmailAsync(templatePath, loginCredentialsModel, subject, applicationUser.Email, true, true, serviceHeader);

                            }
                        }

                        return updateResult.Succeeded;

                    case RecordStatus.Rejected:
                    case RecordStatus.Edited:
                    case RecordStatus.Approved:

                        applicationUser.RecordStatus = applicationUserDto.RecordStatus;
                        applicationUser.ApprovedBy = applicationUserDto.ApprovedBy;
                        applicationUser.LockoutEnabled = true;
                        applicationUser.PhoneNumber = applicationUserDto.PhoneNumber;
                        applicationUser.IsExternalUser = applicationUserDto.IsExternalUser;
                        applicationUser.ApprovedDate = DateTime.Now;
                        applicationUser.Remarks = applicationUserDto.Remarks;
                        applicationUser.IsEnabled = applicationUserDto.IsEnabled;
                        

                        updateResult = await _userManager.UpdateAsync(applicationUser);

                        if (updateResult.Succeeded && !string.IsNullOrWhiteSpace(applicationUserDto.RoleName))
                        {
                            var currentUserRoles = await _userManager.GetRolesAsync(applicationUserDto.Id);

                            if (currentUserRoles != null && currentUserRoles.Any())
                            {
                                Array.ForEach(currentUserRoles.ToArray(), roleName =>
                                {
                                    _userManager.RemoveFromRole(applicationUser.Id, roleName);
                                });
                            }

                            var role = await _roleManager.FindByNameAsync(applicationUserDto.RoleName);

                            if (role != null)
                                _userManager.AddToRole(applicationUserDto.Id, applicationUserDto.RoleName);
                        }

                        if (updateResult.Succeeded && applicationUserDto.RecordStatus != (byte)RecordStatus.Approved)
                        {
                            var queueDTO = ApprovalRequestMessageFactory.CreateApprovalRequestMessage(Guid.Parse(applicationUser.Id), serviceHeader.ApplicationDomainName, "AspNetUsers", serviceHeader.ApplicationUserName, "Modified User, Username : " + applicationUserDto.UserName, (int)EntityState.Modified);

                            ApprovalRequestMessageFactory.SendMessage(queueDTO, MessageCategory.ApprovalRequest);
                        }

                        return updateResult.Succeeded;
                }

                return false;
            }

            return false;
        }

        public async Task<bool> ResetPassword(PasswordResetModel passwordResetModel)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            bool tokenValidateResult = await _userManager.VerifyUserTokenAsync(passwordResetModel.UserId, "ResetPassword", HttpUtility.HtmlDecode(passwordResetModel.Token));

            if (!tokenValidateResult)
            {
                return false;
            }

            ApplicationUser applicationUser = await _userManager.FindByIdAsync(passwordResetModel.UserId);

            IdentityResult passwordResetResult = await _userManager.ResetPasswordAsync(passwordResetModel.UserId, passwordResetModel.Token, passwordResetModel.Password);

            if (passwordResetResult.Errors != null && passwordResetResult.Errors.Any())
            {
                return false;
            }

            var passwordChangeEmailTemplatePath = Path.Combine(_baseDirectoryPath, @"EmailMessageTemplate\PasswordChange_EmailTemplate.cshtml");

            dynamic expando = new ExpandoObject();

            var passwordChangeModel = expando as IDictionary<string, object>;

            string LogoURL = Path.Combine(_baseDirectoryPath, @"Logo\logo.gif");

            passwordChangeModel.Add("ApplicationUserName", applicationUser.UserName );

            //passwordChangeModel.Add("LogoURL", ConfigurationManager.AppSettings["AspnetMvcLogoURL"]);

            passwordChangeModel.Add("LogoURL", LogoURL); // ConfigurationManager.AppSettings["AspnetMvcLogoURL"]

            await _emailAppService.GenerateEmailAsync(passwordChangeEmailTemplatePath, passwordChangeModel, "Password Change", applicationUser.Email, true, true, serviceHeader);

            return true;
        }

        public async Task<bool> ResetPasswordLink(string userName, string callBackUrl)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            ApplicationUser applicationUser = await _userManager.FindByNameAsync(userName);

            if (applicationUser == null)
            {
                return false;
            }

            var passwordResetEmailTemplatePath = Path.Combine(_baseDirectoryPath, @"EmailMessageTemplate\PasswordReset_EmailTemplate.cshtml");

            dynamic expando = new ExpandoObject();

            var passwordResetModel = expando as IDictionary<string, object>;

            passwordResetModel.Add("ApplicationUserName", applicationUser.UserName);

            passwordResetModel.Add("PasswordResetLink", callBackUrl);

            passwordResetModel.Add("LogoURL", ConfigurationManager.AppSettings["AspnetMvcLogoURL"]);

            await _emailAppService.GenerateEmailAsync(passwordResetEmailTemplatePath, passwordResetModel, "Password Reset", applicationUser.Email, true, true, serviceHeader);

            return true;
        }

        public async Task<string[]> GetUserRolesAsync(string username)
        {
            if (!string.IsNullOrWhiteSpace((username)))
            {
                var roles = await _userManager.GetRolesAsync(username);

                return roles.ToArray();
            }
            return new[] { "" };
        }

        public List<ApplicationUserDTO> GetApplicationUsers()
        {
            var users = _userManager.Users.ToList();

            return Util.ProjectToApplicationUserDTO(users);
        }

        public async Task<ApplicationUserDTO> FindApplicationUserByUsernameAndPasswordAsync(string username, string password)
        {
            var applicationUser = await _userManager.FindAsync(username, password);

            if (applicationUser != null)
            {
                return Util.ProjectToApplicationUserDTO(applicationUser);
            }

            return null;
        }

        public async Task<ApplicationRoleDTO> FindApplicationRoleByIdAsync(string id)
        {
            var applicationRole = await _roleManager.FindByIdAsync(id);

            if (applicationRole != null)
            {
                return Util.ProjectToApplicationRoleDTO(applicationRole);
            }

            return null;
        }

        public async Task FetchApplicationUserRole(List<ApplicationUserDTO> applicationUserDTOs)
        {
            foreach (var applicationUserDTO in applicationUserDTOs)
            {
                var roles = await _userManager.GetRolesAsync(applicationUserDTO.Id);

                var currentRole = roles.FirstOrDefault();

                applicationUserDTO.RoleName = currentRole;
            }
        }
    }

    public static class Util
    {
        public static Specification<ApplicationUserDTO> ApplicationUserFullText(string text)
        {
            Specification<ApplicationUserDTO> specification = new TrueSpecification<ApplicationUserDTO>();

            if (!string.IsNullOrWhiteSpace(text))
            {
                text = text.ToLower();

                var nameSpec = new DirectSpecification<ApplicationUserDTO>(c => c.UserName.ToLower().Contains(text));

                var emailSpec = new DirectSpecification<ApplicationUserDTO>(c => c.Email.ToLower().Contains(text));

                //var firstNameSpec = new DirectSpecification<ApplicationUserDTO>(c => c.FirstName.ToLower().Contains(text));

               // var otherNamesSpec = new DirectSpecification<ApplicationUserDTO>(c => c.OtherNames.ToLower().Contains(text));

                specification &= (nameSpec | emailSpec);
            }

            return specification;
        }

        public static Specification<ApplicationRoleDTO> ApplicationRoleName(string name)
        {
            Specification<ApplicationRoleDTO> specification = new TrueSpecification<ApplicationRoleDTO>();

            if (!string.IsNullOrWhiteSpace(name))
            {
                name = name.ToLower();

                var nameSpecification = new DirectSpecification<ApplicationRoleDTO>(c => c.Name.ToLower().Contains(name));

                specification &= nameSpecification;
            }

            return specification;
        }

        public static ApplicationUserDTO ProjectToApplicationUserDTO(ApplicationUser applicationUser)
        {
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<ApplicationUser, ApplicationUserDTO>(); });

            IMapper iMapper = config.CreateMapper();

            var destination = iMapper.Map<ApplicationUser, ApplicationUserDTO>(applicationUser);

            return destination;
        }

        public static ApplicationRoleDTO ProjectToApplicationRoleDTO(ApplicationRole applicationRole)
        {
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<ApplicationRole, ApplicationRoleDTO>(); });

            IMapper iMapper = config.CreateMapper();

            var destination = iMapper.Map<ApplicationRole, ApplicationRoleDTO>(applicationRole);

            return destination;
        }

        public static List<ApplicationUserDTO> ProjectToApplicationUserDTO(List<ApplicationUser> applicationUser)
        {
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<ApplicationUser, ApplicationUserDTO>(); });

            IMapper iMapper = config.CreateMapper();

            var destination = iMapper.Map<List<ApplicationUser>, List<ApplicationUserDTO>>(applicationUser);

            return destination;
        }

        public static List<ApplicationRoleDTO> ProjectToApplicationRoleDTO(List<ApplicationRole> applicationRoles)
        {
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<ApplicationRole, ApplicationRoleDTO>(); });

            IMapper iMapper = config.CreateMapper();

            var destination = iMapper.Map<List<ApplicationRole>, List<ApplicationRoleDTO>>(applicationRoles);

            return destination;
        }
    }
}
