﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Application.MainBoundedContext.RegistryModule.Services;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerAppService _customerAppService;

        private readonly IAspnetIdentityManagerService _aspnetIdentityManagerService;

        public CustomerService(
            ICustomerAppService customerAppService,
            IAspnetIdentityManagerService aspnetIdentityManagerService)
        {
            Guard.ArgumentNotNull(customerAppService, nameof(customerAppService));

            Guard.ArgumentNotNull(aspnetIdentityManagerService, nameof(aspnetIdentityManagerService));

            _customerAppService = customerAppService;

            _aspnetIdentityManagerService = aspnetIdentityManagerService;
        }

        public async Task<Tuple<CustomerDTO, List<string>>> AddNewCustomerAsync(CustomerDTO customerDto)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            var addCustomers = await _customerAppService.AddNewCustomerAsync(customerDto, serviceHeader);

            return addCustomers;
        }

        public async Task<bool> UpdateCustomerAsync(CustomerDTO customerDto)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _customerAppService.UpdateCustomerAsync(customerDto, serviceHeader);
        }

        public async Task<List<CustomerDTO>> FindCustomersAsync()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _customerAppService.FindCustomersAsync(serviceHeader);
        }

        public async Task<CustomerDTO> FindCustomerByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _customerAppService.FindCustomerAsync(id, serviceHeader);
        }


        public async Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _customerAppService.FindCustomersByFullTextAsync(searchString, startIndex, pageSize, sortedColumns, sortAscending, serviceHeader);
        }

        public async Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAndIsEnabledAsync(string searchString, bool isEnabled, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _customerAppService.FindCustomersByFullTextAndIsEnabledAsync(searchString, isEnabled, startIndex, pageSize, sortedColumns, sortAscending, serviceHeader);
        }

        public async Task<List<CustomerDTO>> FindCustomersByNameAsync(string name)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _customerAppService.FindCustomersByNameAsync(name, serviceHeader);
        }
    }
}
