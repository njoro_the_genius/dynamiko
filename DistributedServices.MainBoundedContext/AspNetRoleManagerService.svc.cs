﻿using DistributedServices.MainBoundedContext.Identity;
using DistributedServices.MainBoundedContext.InstanceProviders;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute] // Manage all unhandled exceptions
    [UnityInstanceProviderServiceBehavior] // Create instance and inject dependencies using unity container
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AspNetRoleManagerService : IAspNetRoleManagerService
    {
        readonly ApplicationUserManager _userManager;

        readonly ApplicationRoleManager _roleManager;

        public AspNetRoleManagerService(
            ApplicationUserManager applicationUserManager,
            ApplicationRoleManager applicationRoleManager)
        {
            _userManager = applicationUserManager;

            _roleManager = applicationRoleManager;
        }

        public List<string> GetRoles()
        {
            List<string> systemRoles = new List<string>();

            var roles = _roleManager.Roles;

            if (roles.Any())
            {
                foreach (var role in roles)
                {
                    systemRoles.Add(role.Id);
                }
            }

            return systemRoles;
        }

        public string[] GetUserRoles(string username)
        {
            List<string> userRoles = new List<string>();

            if (!string.IsNullOrWhiteSpace((username)))
            {
                var user = _userManager.FindByName(username);

                if (user != null)
                {
                    var roles = _userManager.GetRoles(user.Id);

                    foreach (var roleName in roles)
                    {
                        var role = _roleManager.FindByName(roleName);

                        userRoles.Add(role.Id);
                    }

                    return userRoles.ToArray();
                }

                return new[] { "" };
            }

            return new[] { "" };
        }

        public string[] GetUsersInRole(string role)
        {
            var roleId = _roleManager.FindByName(role);

            var users = _userManager.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(roleId.Id)).Select(x => x.UserName).ToArray();

            return users;
        }

        public bool IsInRole(string userName, string role)
        {
            var user = _userManager.FindByName(userName);

            return user != null && _userManager.IsInRole(user.Id, role);
        }
    }
}
