﻿using Application.MainBoundedContext.AdministrationModule;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class TaxSettingService : ITaxSettingService
    {
        public readonly ITaxSettingAppService _taxSettingAppService;

        public TaxSettingService(ITaxSettingAppService taxSettingAppService)
        {
            Guard.ArgumentNotNull(taxSettingAppService, "taxSettingAppService");

            _taxSettingAppService = taxSettingAppService;
        }

        #region TaxOrder

        public async Task<TaxOrderDTO> AddNewTaxOrder(TaxOrderDTO taxOrderDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _taxSettingAppService.AddNewTaxOrder(taxOrderDTO, serviceHeader);
        }

        public async Task<bool> UpdateTaxOrder(TaxOrderDTO taxOrderDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _taxSettingAppService.UpdateTaxOrder(taxOrderDTO, serviceHeader);
        }

        public TaxOrderDTO FindTaxSettingById(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _taxSettingAppService.FindTaxSettingById(id, serviceHeader);
        }

        public Task<PageCollectionInfo<TaxOrderDTO>> FindTaxOrderFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString,
            bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _taxSettingAppService.FindTaxOrderFilterInPagesAsync(startIndex, pageSize, sortedColumns, searchString, sortAscending,
                serviceHeader);
        }

        public async Task<List<TaxRangesDTO>> FindGraduatedScalesAsync(Guid taxOrderId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _taxSettingAppService.FindGraduatedScalesAsync(taxOrderId, serviceHeader);
        }

        #endregion
    }
}
