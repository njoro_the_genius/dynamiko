﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Application.MainBoundedContext.AdministrationModule;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class DeductionService : IDeductionService
    {
        private readonly IDeductionAppService _deductionAppService;

        public DeductionService(IDeductionAppService deductionAppService)
        {
            Guard.ArgumentNotNull(deductionAppService, nameof(deductionAppService));
            _deductionAppService = deductionAppService;
        }

        public async Task<NhifOrderDTO> AddNewNhifOrderAsync(NhifOrderDTO nhifOrderDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.AddNewNhifOrderAsync(nhifOrderDTO, serviceHeader);
        }

        public async Task<NssfOrderDTO> AddNewNssfOrderAsync(NssfOrderDTO nssfOrderDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.AddNewNssfOrderAsync(nssfOrderDTO, serviceHeader);
        }

        public async Task<NhifOrderDTO> FindNhifOrderByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindNhifOrderByIdAsync(id, serviceHeader);
        }

        public async Task<PageCollectionInfo<NhifOrderDTO>> FindNhifOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindNhifOrderFilterInPageAsync(pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<List<NhifOrderDTO>> FindNhifOrdersAsync()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindNhifOrders(serviceHeader);
        }

        public async Task<NssfOrderDTO> FindNssfOrderByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindNssfOrderByIdAsync(id, serviceHeader);
        }

        public async Task<PageCollectionInfo<NssfOrderDTO>> FindNssfOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindNssfOrderFilterInPageAsync(pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<List<NssfOrderDTO>> FindNssfOrdersAsync()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindNssfOrders(serviceHeader);
        }


        public async Task<bool> UpdateNhifOrderAsync(NhifOrderDTO nhifOrderDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.UpdateNhifOrderAsync(nhifOrderDTO, serviceHeader);
        }


        public async Task<bool> UpdateNssfOrderAsync(NssfOrderDTO nssfOrderDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.UpdateNssfOrderAsync(nssfOrderDTO, serviceHeader);
        }

        public async Task<bool> UpdateDeductionAsync(DeductionDTO deductionDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.UpdateDeductionAsync(deductionDTO, serviceHeader);
        }

        public async Task<DeductionDTO> AddNewDeductionAsync(DeductionDTO deductionDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.AddNewDeductionAsync(deductionDTO, serviceHeader);
        }

        public async Task<DeductionDTO> FindActiveDeductionAsync()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindActiveDeductionAsync(serviceHeader);
        }

        public async Task<List<DeductionDTO>> FindDeduction()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindDeduction(serviceHeader);
        }

        public async Task<DeductionDTO> FindDeductionByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindDeductionByIdAsync(id,serviceHeader);
        }

        public async Task<PageCollectionInfo<DeductionDTO>> FindDeductionFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindDeductionFilterInPageAsync(pageIndex,pageSize,sortedColumns,searchString,sortAscending, serviceHeader);
        }

        public async Task<PageCollectionInfo<DeductionDTO>> FindDeductionByCustomerIdFilterInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _deductionAppService.FindDeductionByCustomerIdFilterInPageAsync(customerId,pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }
    }
}

