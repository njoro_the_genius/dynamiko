﻿using DistributedServices.MainBoundedContext.InstanceProviders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.NetworkInformation;
using System.ServiceModel;
using System.Text;
using Application.MainBoundedContext.DTO;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class FileUploadService : IFileUploadService
    {
        private static Dictionary<string, FileStream> _wcfFileStreams = new Dictionary<string, FileStream>();

        private static string _lastUpdatedFilename;

        public string FileUpload(FileData fileData)
        {
            FileStream fileStream;

            string filepath = Path.Combine(ConfigurationManager.AppSettings["FileUploadDirectory"], fileData.Filename);

            try
            {
                lock (_wcfFileStreams)
                {
                    _wcfFileStreams.TryGetValue(filepath, out fileStream);

                    if (fileStream == null)
                    {
                        fileStream = File.Open(filepath, FileMode.Create, FileAccess.ReadWrite);

                        _wcfFileStreams.Add(filepath, fileStream);
                    }
                }

                fileStream.Write(fileData.Buffer, 0, fileData.Buffer.Length);

                fileStream.Flush();

                return string.Empty;
            }
            catch (Exception ex)
            {
                FileUploadDone(filepath);

                return ex.ToString();
            }
        }

        public bool FileUploadDone(string filename)
        {
            string filepath = Path.Combine(ConfigurationManager.AppSettings["FileUploadDirectory"], filename);

            lock (_wcfFileStreams)
            {
                _wcfFileStreams.TryGetValue(filepath, out FileStream fileStream);

                if (fileStream != null)
                {
                    _wcfFileStreams.Remove(filepath);

                    _lastUpdatedFilename = filepath;

                    fileStream.Close();

                    return true;
                }
            }

            return false;
        }

        public bool PingNetwork(string hostNameOrAddress)
        {
            bool pingStatus = false;

            using (Ping ping = new Ping())
            {
                byte[] buffer = Encoding.ASCII.GetBytes("BROKER");

                int timeout = 120;

                try
                {
                    PingReply reply = ping.Send(hostNameOrAddress, timeout, buffer);

                    pingStatus = (reply.Status == IPStatus.Success);
                }
                catch (Exception)
                {
                    pingStatus = false;
                }
            }

            return pingStatus;
        }
    }
}
