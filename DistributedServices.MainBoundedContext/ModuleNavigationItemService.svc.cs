﻿using Application.MainBoundedContext.AdministrationModule;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.Identity;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ModuleNavigationItemService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ModuleNavigationItemService.svc or ModuleNavigationItemService.svc.cs at the Solution Explorer and start debugging.
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class ModuleNavigationItemService : IModuleNavigationItemService
    {
        private readonly IModuleNavigationItemAppService _moduleNavigationItemAppService;

        private readonly ApplicationUserManager _userManager;

        private readonly ApplicationRoleManager _roleManager;

        public ModuleNavigationItemService(
            ModuleNavigationItemAppService moduleNavigationItemAppService,
            ApplicationUserManager applicationUserManager,
            ApplicationRoleManager applicationRoleManager)
        {
            Guard.ArgumentNotNull("moduleNavigationItemAppService", nameof(moduleNavigationItemAppService));

            _moduleNavigationItemAppService = moduleNavigationItemAppService;

            _roleManager = applicationRoleManager;

            _userManager = applicationUserManager;
        }

        #region ModuleNavigationItem

        public async Task<bool> AddModuleNavigationItemAsync(List<ModuleNavigationItemDTO> moduleNavigationItem)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.AddModuleNavigationItemAsync(moduleNavigationItem, serviceHeader);
        }

        public async Task<ModuleNavigationItemDTO> FindModuleNavigationItemByIdAsync(Guid id)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationItemByIdAsync(id, serviceHeader);
        }

        public async Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemsAsync()
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationItemsAsync(serviceHeader);
        }

        public async Task<PageCollectionInfo<ModuleNavigationItemDTO>> FindModuleNavigationItemsFilterPageCollectionInfoAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationItemsFilterPageCollectionInfoAsync(startIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<List<ModuleNavigationItemDTO>> FindModuleNavigationActionControllerNameAsync(string controllerName)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationActionControllerNameAsync(controllerName, serviceHeader);
        }

        public async Task<bool> ValidateAccessPermissionAsync(string controllerName, string actionName, string username)
        {
            var userDetails = _userManager.FindByName(username);

            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            if (userDetails != null)
            {
                var roles = _userManager.GetRoles(userDetails.Id);

                if (roles != null)
                {
                    var rolesInArray = roles.ToArray();

                    return await _moduleNavigationItemAppService.ValidateAccessPermissionAsync(controllerName, actionName, rolesInArray, serviceHeader);
                }

                return false;
            }

            return false;
        }

        public async Task<bool> BulkInsertModuleNavigationItemAsync(List<Guid> modulesNavigationIds, string roleId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.BulkInsertModuleNavigationItemAsync(modulesNavigationIds, roleId, serviceHeader);
        }

        #endregion

        #region ModuleNavigationItemInRole

        public async Task<bool> AddModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.AddModuleNavigationItemInRoleAsync(moduleNavigationItemInRole, serviceHeader);
        }

        public async Task<bool> RemoveModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.RemoveModuleNavigationItemInRoleAsync(moduleNavigationItemInRole, serviceHeader);
        }

        public async Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRoleAsync(string role)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationItemInRoleByRoleAsync(role, serviceHeader);
        }

        public async Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationIdAsync(Guid moduleNavigationId)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationItemInRoleByModuleNavigationIdAsync(moduleNavigationId, serviceHeader);
        }

        public async Task<PageCollectionInfo<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRolePageCollectionInfoAsync(string roleId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationItemByRolePageCollectionInfoAsync(roleId, startIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<List<RolePermissionDTO>> CacheRoleAccessPermissionsAsync(string[] currentUserRole)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            var applicationDomainWrapperList = new List<RolePermissionDTO>();

            var roles = _roleManager.Roles;

            if (roles != null && roles.Any())
            {
                var rolesList = new List<string>();

                var rolesArray = rolesList.ToArray();

                foreach (var item in roles)
                {
                    rolesList.Add(item.Id);

                    rolesArray = rolesList.ToArray();
                }

                var permissionsList = await _moduleNavigationItemAppService.GetAccessPermissionsPerRolesAsync(rolesArray, currentUserRole, serviceHeader);

                if (permissionsList != null && permissionsList.Any())
                {
                    applicationDomainWrapperList.AddRange(permissionsList);
                }
            }

            return applicationDomainWrapperList;
        }

        public async Task<bool> BulkInsertModuleNavigationItemInRolesAsync(List<Guid> moduleNavigationItemInRole, string roleId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.BulkInsertModuleNavigationItemInRolesAsync(moduleNavigationItemInRole, roleId, serviceHeader);
        }

        public async Task<ModuleNavigationItemInRoleDTO> FindModuleNavigationItemInRoleByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.FindModuleNavigationItemInRoleByIdAsync(id, serviceHeader);
        }

        public async Task<bool> UpdateModuleNavigationItemInRoleListAsync(ModuleNavigationItemInRoleDTO moduleNavigationItemInRole)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _moduleNavigationItemAppService.UpdateModuleNavigationItemInRoleListAsync(moduleNavigationItemInRole, serviceHeader);
        }

        #endregion
    }
}
