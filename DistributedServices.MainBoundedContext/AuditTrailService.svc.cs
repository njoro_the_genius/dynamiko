﻿using Application.MainBoundedContext.DTO;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Application.MainBoundedContext.Services;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AuditTrailService : IAuditTrailService
    {
        private readonly IAuditTrailAppService _auditTrailAppService;
        
        public AuditTrailService(IAuditTrailAppService auditTrailAppService)
        {
            Guard.ArgumentNotNull(auditTrailAppService, nameof(auditTrailAppService));

            _auditTrailAppService = auditTrailAppService;
        }

        public async Task<bool> AddAuditTrailsAsync(List<AuditTrailDTO> auditTrailDTOs)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _auditTrailAppService.AddNewAuditTrailsAsync(auditTrailDTOs, serviceHeader);
        }

        public async Task<AuditTrailDTO> AddAuditTrailAsync(AuditTrailDTO auditTrailDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _auditTrailAppService.AddNewAuditTrailAsync(auditTrailDTO, serviceHeader);
        }

        public async Task<PageCollectionInfo<AuditTrailDTO>> FindAuditTrailsByDateRangeAndFilterInPageAsync(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string filter)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _auditTrailAppService.FindAuditTrailsByDateRangeAndFilterAsync(pageIndex, pageSize, startDate, endDate, filter, serviceHeader);
        }
    }
}
