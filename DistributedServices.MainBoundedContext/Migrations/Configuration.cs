namespace DistributedServices.MainBoundedContext.Migrations
{
    using Identity;
    using Infrastructure.Crosscutting.Framework.Utils;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            CommandTimeout = 3600;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new ApplicationUserStore(context));

            var roleManager = new ApplicationRoleManager(new ApplicationRoleStore(context));

            var roleValues = Enum.GetValues(typeof(WellKnownUserRoles));

            foreach (var roleValue in roleValues)
            {
                if (roleManager.FindByName(EnumHelper.GetDescription((WellKnownUserRoles)roleValue)) == null)
                {
                    if (EnumHelper.GetDescription((WellKnownUserRoles)roleValue) == (EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator)) || EnumHelper.GetDescription((WellKnownUserRoles)roleValue) == (EnumHelper.GetDescription(WellKnownUserRoles.HumanResource)))
                    {
                        var seedRole = new ApplicationRole
                        {
                            Name = EnumHelper.GetDescription((WellKnownUserRoles)roleValue),
                            IsEnabled = true,
                            RoleType = (byte)RoleType.InternalRole,
                            Description = EnumHelper.GetDescription((WellKnownUserRoles)roleValue)
                        };

                        roleManager.Create(seedRole);
                    }
                    else
                    {
                        var seedRole = new ApplicationRole
                        {
                            Name = EnumHelper.GetDescription((WellKnownUserRoles)roleValue),
                            IsEnabled = true,
                            RoleType = (byte)RoleType.ExternalRole,
                            Description = EnumHelper.GetDescription((WellKnownUserRoles)roleValue)
                        };

                        roleManager.Create(seedRole);
                    }
                }
            }

            var role = roleManager.FindByName(EnumHelper.GetDescription(WellKnownUserRoles.SuperAdministrator));

            //seed super-user
            if (userManager.FindByName(DefaultSettings.Instance.RootUser) == null)
            {
                var user = new ApplicationUser
                {
                    UserName = DefaultSettings.Instance.RootUser,
                    Email = DefaultSettings.Instance.RootEmail,
                    EmailConfirmed = true,
                    CreatedDate = DateTime.Now,
                    //FirstName = "Super",
                    //OtherNames = "User",
                    RecordStatus = (byte)RecordStatus.Approved,
                    IsEnabled = true,
                    IsExternalUser = false,
                    LastPasswordChangedDate = DateTime.Now
                };

                userManager.Create(user, DefaultSettings.Instance.RootPassword);

                if (role != null)
                {
                    var applicationUserRole = new ApplicationUserRole()
                    {
                        UserId = user.Id,
                        RoleId = role.Id
                    };

                    //add super-user to super-admin role
                    user.Roles.Add(applicationUserRole);
                }
            }

            if (userManager.FindByName(DefaultSettings.Instance.RootAuthoriser) == null)
            {
                var authorizer = new ApplicationUser
                {
                    UserName = DefaultSettings.Instance.RootAuthoriser,
                    Email = DefaultSettings.Instance.RootEmail,
                    EmailConfirmed = true,
                    CreatedDate = DateTime.Now,
                    //FirstName = "Super",
                    //OtherNames = "Authoriser",
                    RecordStatus = (byte)RecordStatus.Approved,
                    IsEnabled = true,
                    IsExternalUser = false,
                    LastPasswordChangedDate = DateTime.Now
                };

                userManager.Create(authorizer, DefaultSettings.Instance.RootAuthoriserPassword);

                if (role != null)
                {

                    var authorizerRole = new ApplicationUserRole()
                    {
                        UserId = authorizer.Id,
                        RoleId = role.Id
                    };

                    //add super-user to super-admin role
                    authorizer.Roles.Add(authorizerRole);
                }
            }
        }
    }

    public static class ApplicationDomainName
    {
        public static string CurrentDomainName { get; set; }
    }
}