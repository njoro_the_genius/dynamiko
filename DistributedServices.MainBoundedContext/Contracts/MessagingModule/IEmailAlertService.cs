﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IEmailAlertService
    {
        #region EmailAlert

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool AddEmailAlerts(List<EmailAlertDTO> emailAlertDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO emailAlertDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsAsync(Guid? customerId, int pageIndex, int pageSize);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(Guid? customerId, string searchText, int pageIndex, int pageSize);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithDLRStatusAsync(Guid? customerId, int dlrStatus, int pageIndex, int pageSize);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<EmailAlertDTO> FindEmailAlertWithIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateEmailAlertAsync(EmailAlertDTO emailAlertDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> GenerateEmailAsync(String templatePath, IDictionary<string, object> expandoObject, string subject, string messageTo, bool mailMessageIsBodyHtml, bool mailMessageSecurityCritical);

        #endregion
    }
}
