﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface ITextAlertService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<TextAlertDTO> AddNewTextAsync(TextAlertDTO textAlertDto);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateTextAlertAsync(TextAlertDTO textAlertDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<TextAlertDTO>> FindTextAlertsAsync();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAsync(int dlrStatus);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsInPageAsync(int pageIndex, int pageSize, List<string> sortFields, bool ascending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByFilterInPageAsync(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByDateRangeAndFilterInPageAsync(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAndOriginAsync(int dlrStatus, int origin);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<TextAlertDTO> FindTextAlertAsync(Guid textAlertId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsFilterInPageAsync(DateTime startDate, DateTime endDate, Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByRecipientMobileNumberFilterInPageAsync(string mobileNumber, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);
    }
}
