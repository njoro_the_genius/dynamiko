﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface ITaxSettingService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<TaxOrderDTO> AddNewTaxOrder(TaxOrderDTO taxOrderDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateTaxOrder(TaxOrderDTO taxOrderDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        TaxOrderDTO FindTaxSettingById(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<TaxOrderDTO>> FindTaxOrderFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<TaxRangesDTO>> FindGraduatedScalesAsync(Guid taxOrderId);

    }
}
