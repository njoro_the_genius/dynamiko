﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IModuleNavigationItemService" in both code and config file together.
    [ServiceContract]
    public interface IModuleNavigationItemService
    {
        #region ModuleNavigationItem

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> AddModuleNavigationItemAsync(List<ModuleNavigationItemDTO> moduleNavigationItem);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<ModuleNavigationItemDTO> FindModuleNavigationItemByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemsAsync();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<ModuleNavigationItemDTO>> FindModuleNavigationItemsFilterPageCollectionInfoAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<ModuleNavigationItemDTO>> FindModuleNavigationActionControllerNameAsync(string controllerName);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> ValidateAccessPermissionAsync(string controllerName, string actionName, string username);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> BulkInsertModuleNavigationItemAsync(List<Guid> modulesNavigationIds, string roleId);

        #endregion

        #region ModuleNavigationItemInRole

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> AddModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationIdAsync(Guid moduleNavigationId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> RemoveModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<ModuleNavigationItemInRoleDTO> FindModuleNavigationItemInRoleByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRoleAsync(string roleId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRolePageCollectionInfoAsync(string roleId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<RolePermissionDTO>> CacheRoleAccessPermissionsAsync(string[] currentUserRole);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> BulkInsertModuleNavigationItemInRolesAsync(List<Guid> moduleNavigationItemInRole, string roleId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateModuleNavigationItemInRoleListAsync(ModuleNavigationItemInRoleDTO moduleNavigationItemInRole);

        #endregion
    }
}
