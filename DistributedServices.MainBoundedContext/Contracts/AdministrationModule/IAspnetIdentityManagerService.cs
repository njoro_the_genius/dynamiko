﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IAspnetIdentityManagerService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByCustomerIdAsync(Guid customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<ApplicationUserDTO> FindApplicationUserAsync(string userName);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<ApplicationUserDTO> FindApplicationUserByIdAsync(string id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<ApplicationRoleDTO> FindApplicationRoleByIdAsync(string id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUsersInRolesAsync(string roleName, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<ApplicationUserDTO>> FindApplicationUsersInRolesAsync(string roleName);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<ApplicationUserDTO> CreateUserAsync(ApplicationUserDTO applicationUserDto);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateAspnetIdentityUserAsync(ApplicationUserDTO applicationUserDto);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<string[]> GetUserRolesAsync(string username);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        List<ApplicationUserDTO> GetApplicationUsers();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<ApplicationUserDTO> FindApplicationUserByUsernameAndPasswordAsync(string username, string password);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<ApplicationRoleDTO> GetApplicationRoleCollectionByFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> CreateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> ResetPassword(PasswordResetModel passwordResetModel);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> ResetPasswordLink(string userName, string callBackUrl);
    }
}