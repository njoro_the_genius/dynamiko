﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILeaveService" in both code and config file together.
    [ServiceContract]
    public interface ILeaveService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<LeaveDTO> AddNewLeaveAsync(LeaveDTO leaveDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateLeaveAsync(LeaveDTO leaveDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<LeaveDTO> FindLeaveByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<LeaveDTO> FindLeaveByEmployeeIdAsync(Guid employeeId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<LeaveDTO>> FindLeavesByEmployeeIdAsync(Guid employeeId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByCustomerInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByEmployeeInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<LeaveDTO>> FindLeaves();
    }
}
