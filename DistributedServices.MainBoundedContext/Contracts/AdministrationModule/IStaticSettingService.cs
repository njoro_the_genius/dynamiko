﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IStaticSettingService" in both code and config file together.
    [ServiceContract]
    public interface IStaticSettingService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        StaticSettingDTO AddNewSetting(StaticSettingDTO staticSettingDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool UpdateSetting(StaticSettingDTO settingDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<StaticSettingDTO> FindSettingFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        StaticSettingDTO FindSettingByKey(string keyName);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        StaticSettingDTO FindSettingById(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool RemoveStaticSetting(Guid id);

    }
}
