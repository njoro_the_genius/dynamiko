﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDeductionService" in both code and config file together.
    [ServiceContract]
    public interface IDeductionService
    {
        #region NssfOrderDTO
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<NssfOrderDTO> AddNewNssfOrderAsync(NssfOrderDTO nssfOrderDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateNssfOrderAsync(NssfOrderDTO nssfOrderDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<NssfOrderDTO> FindNssfOrderByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<NssfOrderDTO>> FindNssfOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<NssfOrderDTO>> FindNssfOrdersAsync();
        #endregion

        #region NhifOrderDTO
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]

        Task<NhifOrderDTO> AddNewNhifOrderAsync(NhifOrderDTO nhifOrderDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateNhifOrderAsync(NhifOrderDTO nhifOrderDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<NhifOrderDTO> FindNhifOrderByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<NhifOrderDTO>> FindNhifOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<NhifOrderDTO>> FindNhifOrdersAsync();
        #endregion

        #region DeductionDTO
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<DeductionDTO> AddNewDeductionAsync(DeductionDTO deductionDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateDeductionAsync(DeductionDTO deductionDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<DeductionDTO> FindDeductionByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<DeductionDTO>> FindDeductionFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<DeductionDTO>> FindDeductionByCustomerIdFilterInPageAsync(Guid customerId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<DeductionDTO>> FindDeduction();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<DeductionDTO> FindActiveDeductionAsync();
        #endregion
    }
}
