﻿using DistributedServices.Seedwork.ErrorHandlers;
using Infrastructure.Crosscutting.Framework.Utils;
using System.Collections.Generic;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IAspNetRoleManagerService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        string[] GetUsersInRole(string role);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool IsInRole(string userName, string role);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        List<string> GetRoles();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        string[] GetUserRoles(string username);
    }
}
