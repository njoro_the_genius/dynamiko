﻿using System.ServiceModel;
using Application.MainBoundedContext.DTO;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IFileUploadService
    {
        [OperationContract]
        string FileUpload(FileData fileData);

        [OperationContract]
        bool FileUploadDone(string filename);

        [OperationContract]
        bool PingNetwork(string hostNameOrAddress);
    }
}
