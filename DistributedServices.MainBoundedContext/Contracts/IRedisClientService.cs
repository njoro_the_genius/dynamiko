﻿using DistributedServices.Seedwork.ErrorHandlers;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IRedisClientService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> SeedSystemAccessRightAsync(string role);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> SeedSystemAccessRightsAsync();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> ValidatePermissionAsync(string username, string controllerName, string actionName);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> RemoveAllRightsAsync(string[] roles);
    }
}