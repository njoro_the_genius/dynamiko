﻿using Application.MainBoundedContext.DTO;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IAuditTrailService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> AddAuditTrailsAsync(List<AuditTrailDTO> auditTrailDTOs);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<AuditTrailDTO> AddAuditTrailAsync(AuditTrailDTO auditTrailDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<AuditTrailDTO>> FindAuditTrailsByDateRangeAndFilterInPageAsync(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string filter);
    }
}
