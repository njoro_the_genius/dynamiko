﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IEmployeeService
    {
        #region EmployeeService

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<Tuple<EmployeeDTO, List<string>>> AddNewEmployeeAsync(EmployeeDTO employeeDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateEmployeeAsync(EmployeeDTO employeeDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<EmployeeDTO> FindEmployeeByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<EmployeeDTO>> FindCustomerEmployeesFilterInPageAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending);


        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<EmployeeDTO>> FindEmployeeByCustomerIdAsync(Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<EmployeeDTO> FindEmployeeByUserIdAsync(Guid userId, Guid? customerId);

        #endregion
    }
}
