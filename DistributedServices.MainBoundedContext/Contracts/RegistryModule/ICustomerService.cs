﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.Seedwork.ErrorHandlers;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface ICustomerService
    {
        #region CustomerDTO

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<Tuple<CustomerDTO, List<string>>> AddNewCustomerAsync(CustomerDTO customerDto);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<bool> UpdateCustomerAsync(CustomerDTO customerDto);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<CustomerDTO>> FindCustomersAsync();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<CustomerDTO> FindCustomerByIdAsync(Guid id);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending);
     
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAndIsEnabledAsync(string searchString, bool isEnabled, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending);

        [OperationContract]
        [FaultContract((typeof(ApplicationServiceError)))]
        Task<List<CustomerDTO>> FindCustomersByNameAsync(string name);

        #endregion
    }
}
