﻿using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.ServiceModel;
using Application.MainBoundedContext.DTO;

namespace DistributedServices.MainBoundedContext
{
    [ServiceContract]
    public interface IMediaService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        string MediaUpload(FileData fileData);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool MediaUploadDone(string filename);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        MediaDTO GetMedia(Guid sku);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool PostFile(MediaDTO mediaDTO);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool PostImage(MediaDTO mediaDTO);
    }
}
