﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.Seedwork.ErrorHandlers;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{

    [ServiceContract]
    public interface IUtilityService
    {
        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool ConfigureApplicationDatabase();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        bool ConfigureAspNetIdentityDatabase();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        List<Bank> GetBanksList();

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        List<Branch> GetBranchesList(string bankCode);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Bank GetBankByBankCode(string bankCode);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Branch GetBrancheByBranchCode(string bankCode, string branchCode);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<Branch> GetBranchesListFilterInPage(string bankCode, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        PageCollectionInfo<Bank> GetBanksListFilterInPage(string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending);


        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<DashboardWidgetDTO> FindDashboardWidgetsAsync(int claim, Guid? customerId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<List<DashboardGraphDTO>> GetDashboardGraphDataSetAsync(int claim, Guid? customerId);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<EmployeeDTO>> GetEmployeesReportAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text);

        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<PaySlipDTO>> GetPayRollAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text);


        [OperationContract]
        [FaultContract(typeof(ApplicationServiceError))]
        Task<PageCollectionInfo<PaySlipDTO>> GetPaySlipAsync(Guid employeeId, int period);

    }
}
