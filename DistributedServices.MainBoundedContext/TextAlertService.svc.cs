﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Application.MainBoundedContext.MessagingModule.Services;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class TextAlertService : ITextAlertService
    {
        private readonly ITextAlertAppService _textAlertAppService;

        public TextAlertService(ITextAlertAppService textAlertAppService)
        {
            Guard.ArgumentNotNull(textAlertAppService, nameof(textAlertAppService));

            _textAlertAppService = textAlertAppService;
        }

        public async Task<TextAlertDTO> AddNewTextAsync(TextAlertDTO textAlertDto)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await  _textAlertAppService.AddNewTextAlertAsync(textAlertDto, serviceHeader);
        }

        public async Task<bool> UpdateTextAlertAsync(TextAlertDTO textAlertDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.UpdateTextAlertAsync(textAlertDTO, serviceHeader);
        }

        public async Task<List<TextAlertDTO>> FindTextAlertsAsync()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsAsync(serviceHeader);
        }

        public async Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAsync(int dlrStatus)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsByDLRStatusAsync(dlrStatus, serviceHeader);
        }

        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsInPageAsync(int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsAsync(pageIndex, pageSize, sortFields, ascending, serviceHeader);
        }

        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByFilterInPageAsync(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsAsync(dlrStatus, text, pageIndex, pageSize, sortFields, ascending, serviceHeader);
        }

        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByDateRangeAndFilterInPageAsync(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsAsync(dlrStatus, startDate, endDate, text, pageIndex, pageSize, sortFields, ascending, serviceHeader);
        }

        public async Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAndOriginAsync(int dlrStatus, int origin)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsByDLRStatusAndOriginAsync(dlrStatus, origin, serviceHeader);
        }

        public async Task<TextAlertDTO> FindTextAlertAsync(Guid textAlertId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertAsync(textAlertId, serviceHeader);
        }

        public async  Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsFilterInPageAsync(DateTime startDate, DateTime endDate, Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsFilterInPageAsync(startDate, endDate, customerId, startIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByRecipientMobileNumberFilterInPageAsync(string mobileNumber,
            int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _textAlertAppService.FindTextAlertsByRecipientMobileNumberFilterInPageAsync(mobileNumber, startIndex, pageSize, sortedColumns, searchString, true, serviceHeader);
        }
    }
}
