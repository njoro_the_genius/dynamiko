﻿using Application.MainBoundedContext.AdministrationModule;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute] // Manage all unhandled exceptions
    [UnityInstanceProviderServiceBehavior] // Create instance and inject dependencies using unity container
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class StaticSettingService : IStaticSettingService
    {
        public readonly IStaticSettingAppService _staticSettingAppService;

        public StaticSettingService(IStaticSettingAppService staticSettingAppService)
        {
            Guard.ArgumentNotNull(staticSettingAppService, "staticSettingAppService");

            _staticSettingAppService = staticSettingAppService;
        }

        #region Settings

        public StaticSettingDTO AddNewSetting(StaticSettingDTO staticSettingDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _staticSettingAppService.AddNewStaticSetting(staticSettingDTO, serviceHeader);
        }

        public StaticSettingDTO FindSettingById(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _staticSettingAppService.FindstaticSettingById(id, serviceHeader);
        }

        public StaticSettingDTO FindSettingByKey(string keyName)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _staticSettingAppService.FindSettingByKey(keyName, serviceHeader);
        }

        public PageCollectionInfo<StaticSettingDTO> FindSettingFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _staticSettingAppService.FindStaticSettingsFilterInPage(startIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public bool RemoveStaticSetting(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _staticSettingAppService.RemoveStaticSetting(id, serviceHeader);
        }

        public bool UpdateSetting(StaticSettingDTO settingDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _staticSettingAppService.UpdateStaticSetting(settingDTO, serviceHeader);
        }

        #endregion
    }
}
