﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Application.MainBoundedContext.MessagingModule.Services;
using Application.MainBoundedContext.Services;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class EmailAlertService : IEmailAlertService
    {
        private readonly IEmailAlertAppService _emailAlertAppService;

        private readonly IEmailAppService _emailAppService;

        public EmailAlertService(IEmailAlertAppService emailAlertAppService, IEmailAppService emailAppService)
        {
            Guard.ArgumentNotNull(emailAlertAppService, nameof(emailAlertAppService));

            _emailAlertAppService = emailAlertAppService;

            _emailAppService = emailAppService;
        }

        public bool AddEmailAlerts(List<EmailAlertDTO> emailAlertDTOs)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _emailAlertAppService.AddEmailAlerts(emailAlertDTOs, serviceHeader);
        }

        public async Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO emailAlertDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _emailAlertAppService.AddEmailAlertAsync(emailAlertDTO, serviceHeader);
        }

        public async Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsAsync(Guid? customerId, int pageIndex, int pageSize)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _emailAlertAppService.FindEmailAlertsAsync(customerId, pageIndex, pageSize, serviceHeader);
        }

        public async Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(Guid? customerId, string searchText, int pageIndex, int pageSize)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _emailAlertAppService.FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(customerId, searchText, pageIndex, pageSize, serviceHeader);
        }

        public async Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithDLRStatusAsync(Guid? customerId, int dlrStatus, int pageIndex, int pageSize)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _emailAlertAppService.FindEmailAlertsWithDLRStatusAsync(customerId, dlrStatus, pageIndex, pageSize, serviceHeader);
        }

        public async Task<EmailAlertDTO> FindEmailAlertWithIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _emailAlertAppService.FindEmailAlertWithIdAsync(id, serviceHeader);
        }

        public async Task<bool> UpdateEmailAlertAsync(EmailAlertDTO emailAlertDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _emailAlertAppService.UpdateEmailAlertAsync(emailAlertDTO, serviceHeader);
        }

        public async Task<bool> GenerateEmailAsync(string templatePath, IDictionary<string, object> expandoObject, string subject, string messageTo, bool mailMessageIsBodyHtml, bool mailMessageSecurityCritical)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _emailAppService.GenerateEmailAsync(templatePath, expandoObject, subject, messageTo, mailMessageIsBodyHtml, mailMessageSecurityCritical, serviceHeader);
        }
    }
}
