﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DistributedServices.MainBoundedContext.Identity
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, string, ApplicationUserRole>
    {
        public ApplicationRoleStore(ApplicationDbContext context) : base(context)
        {

        }
    }
}