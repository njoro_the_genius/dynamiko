﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext.Identity
{
    public class ApplicationUser : IdentityUser<string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>
    {
        public ApplicationUser()
        {
            Id = Guid.NewGuid().ToString();
        }

        public DateTime CreatedDate { get; set; }

        public Guid? CustomerId { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsExternalUser { get; set; }

        public byte RecordStatus { get; set; }

        public string CreatedBy { get; set; }

        public string ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string EditedBy { get; set; }

        public DateTime? EditDate { get; set; }

        public string Remarks { get; set; }

        public DateTime? LastPasswordChangedDate { get; set; }

        public DateTime? LastLoginDateTime { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here
            userIdentity.AddClaim(new Claim(ApplicationUserProperties.CustomerId, CustomerId.ToString()));

            return userIdentity;
        }
    }

    public class ApplicationUserProperties
    {
        public const string CustomerId = "CustomerId";
    }
}