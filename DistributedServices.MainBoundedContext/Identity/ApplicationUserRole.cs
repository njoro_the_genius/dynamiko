﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DistributedServices.MainBoundedContext.Identity
{
    public class ApplicationUserRole : IdentityUserRole<string>
    {
        public ApplicationUserRole() : base()
        {
        }

        public virtual ApplicationRole Role { get; set; }
    }
}