﻿using Infrastructure.Crosscutting.Framework.Utils;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DistributedServices.MainBoundedContext.Identity
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserLogin, ApplicationUserRole, IdentityUserClaim>
    {
        public ApplicationDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        { }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable($"{DefaultSettings.Instance.TablePrefix}AspNetUsers");
            modelBuilder.Entity<ApplicationRole>().ToTable($"{DefaultSettings.Instance.TablePrefix}AspNetRoles");

            modelBuilder.Entity<ApplicationUserRole>().HasRequired(ur => ur.Role).WithMany().HasForeignKey(ur => ur.RoleId);
            modelBuilder.Entity<ApplicationUserRole>().ToTable($"{DefaultSettings.Instance.TablePrefix}AspNetUserRoles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable($"{DefaultSettings.Instance.TablePrefix}AspNetUserClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable($"{DefaultSettings.Instance.TablePrefix}AspNetUserLogins");
        }
    }
}