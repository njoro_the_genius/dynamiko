﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace DistributedServices.MainBoundedContext.Identity
{
    public class ApplicationRole : IdentityRole<string, ApplicationUserRole>
    {
        public ApplicationRole() : base()
        {
            Id = Guid.NewGuid().ToString();
        }

        public byte RoleType { get; set; }

        public string Description { get; set; }

        public bool IsEnabled { get; set; }
    }
}