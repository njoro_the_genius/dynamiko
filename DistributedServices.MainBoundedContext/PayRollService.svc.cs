﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Application.MainBoundedContext.AdministrationModule;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.MainBoundedContext.RegistryModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PayRollService : IPayRollService
    {
        private readonly IPayRollAppService _payRollAppService;
        private readonly IEmployeeAppService _employeeAppService;
        private readonly IDeductionAppService _deductionAppService;
        private readonly ITaxSettingAppService _taxSettingAppService;

        public PayRollService(IPayRollAppService payRollAppService, IEmployeeAppService employeeAppService, IDeductionAppService deductionAppService, ITaxSettingAppService taxSettingAppService)
        {
            Guard.ArgumentNotNull(payRollAppService, nameof(payRollAppService));
            Guard.ArgumentNotNull(employeeAppService, nameof(employeeAppService));
            Guard.ArgumentNotNull(deductionAppService, nameof(deductionAppService));
            Guard.ArgumentNotNull(taxSettingAppService, nameof(taxSettingAppService));
            _payRollAppService = payRollAppService;
            _employeeAppService = employeeAppService;
            _deductionAppService = deductionAppService;
            _taxSettingAppService = taxSettingAppService;
        }

        public async Task<PayRollDTO> AddNewPayRollAsync(PayRollDTO payRollDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.AddNewPayRollAsync(payRollDTO, serviceHeader);
        }

        public async Task<PageCollectionInfo<PayRollDTO>> FindPayRollByCustomerIdFilterInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.FindPayRollByCustomerIdFilterInPageAsync(customerId, pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<PayRollDTO> FindPayRollByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.FindPayRollByIdAsync(id, serviceHeader);
        }

        public async Task<PageCollectionInfo<PayRollDTO>> FindPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.FindPayRollFilterInPageAsync(pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemByEmployeeIdDTOFilterInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.FindPayRollItemByEmployeeIdDTOFilterInPageAsync(employeeId, pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemsFilterInPageAsync(Guid payRollId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.FindPayRollItemDTOFilterInPageAsync(payRollId, pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<List<PayRollDTO>> FindPayRolls()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.FindPayRolls(serviceHeader);
        }

        public async Task<PageCollectionInfo<PayRollDTO>> FindUnProcessedPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.FindUnProcessedPayRollFilterInPageAsync(pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<PayRollDTO> ProcessPayRoll(Guid payRollId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            var payroll = await _payRollAppService.FindPayRollByIdAsync(payRollId, serviceHeader);

            if (payroll != null)
            {
                var enabledEmployees = await _employeeAppService.FindEnabledEmployeesByCustomerIdAsync(payroll.CustomerId, serviceHeader);
                var enabledNhif = await _deductionAppService.FindActiveNhifOrderAsync(serviceHeader);
                var enabledNssf = await _deductionAppService.FindActiveNssfOrderAsync(serviceHeader);
                var enabledTaxRecord = await _taxSettingAppService.FindActiveTaxOrderAsync(serviceHeader);
                var orderTaxRates = enabledTaxRecord.TaxRangeItems.OrderBy(taxitem => taxitem.RangeUpperLimit).ToList();
                var relief = decimal.Parse((ConfigurationManager.AppSettings["TaxRelief"]));

                foreach (var employee in enabledEmployees)
                {
                    if (employee.GrossSalary <= 0)
                        continue;

                    var employeeDeduction = await _deductionAppService.FindDeductionByCustomerIdDue(payroll.CustomerId, employee.Id, payroll.StartDate, payroll.EndDate, serviceHeader);
                    var nssfScale = enabledNssf.NssfGraduatedScaleDTOs.Where(x => x.LowerLimit <= employee.GrossSalary && x.UpperLimit >= employee.GrossSalary).FirstOrDefault();
                    var nhifScale = enabledNhif.NhifGraduatedScaleDTOs.Where(x => x.LowerLimit <= employee.GrossSalary && x.UpperLimit >= employee.GrossSalary).FirstOrDefault();
                    var upperTaxRange = enabledTaxRecord.TaxRangeItems.Where(x => x.RangeLowerLimit <= employee.GrossSalary && x.RangeUpperLimit >= employee.GrossSalary).FirstOrDefault();
                    orderTaxRates.RemoveAll(o => o.RangeUpperLimit > upperTaxRange?.RangeUpperLimit);
                    decimal payee = 0, nhifAmount = 0, nssfAmount = 0, netpay = 0, totalOtherDeduction = 0;
                    foreach (var taxRange in orderTaxRates)
                    {
                        if (taxRange.RangeUpperLimit != upperTaxRange?.RangeUpperLimit)
                        {
                            var difference = taxRange.RangeUpperLimit - taxRange.RangeLowerLimit;
                            payee += taxRange.Type == (byte)ChargeType.Percentage ? Decimal.Divide(taxRange.TaxValue, 100) * difference : taxRange.TaxValue;
                        }
                        else
                        {
                            var difference = employee.GrossSalary - taxRange.RangeLowerLimit;
                            payee += taxRange.Type == (byte)ChargeType.Percentage ? Decimal.Divide(taxRange.TaxValue, 100) * difference : taxRange.TaxValue;
                            break;
                        }
                    }

                    if (employeeDeduction != null && employeeDeduction.Count > 0)
                    {
                        foreach (var deduction in employeeDeduction)
                        {
                            totalOtherDeduction += deduction.Amount;
                        }
                    }


                    if (payee > relief)
                        payee -= relief;

                    if (employee.DeductNHIFContribution && nhifScale != null)
                        nhifAmount = nhifScale.Type == (byte)ChargeType.Percentage ? Decimal.Divide(nhifScale.Value, 100) * employee.GrossSalary : nhifScale.Value;
                    if (employee.DeductNSSFContribution && nssfScale != null)
                        nssfAmount = nssfScale.Type == (byte)ChargeType.Percentage ? Decimal.Divide(nssfScale.Value, 100) * employee.GrossSalary : nssfScale.Value;

                    netpay = employee.GrossSalary - (payee + nhifAmount + nssfAmount + totalOtherDeduction);
                    payroll.TotalGrosspay += employee.GrossSalary;
                    payroll.TotalPayee += payee < 0 ? 0 : payee;
                    payroll.TotalHealthInsurance += nhifAmount < 0 ? 0 : nhifAmount;
                    payroll.TotalPension += nssfAmount < 0 ? 0 : nssfAmount;
                    payroll.TotalNetPay += netpay < 0 ? 0 : netpay;
                    payroll.TotalAllowableDeductions += nssfAmount < 0 ? 0 : nssfAmount;

                    if (netpay > 0)
                    {
                        payroll.PayRollItemDTOs.Add(
                            new PayRollItemDTO
                            {
                                EmployeeId = employee.Id,
                                Grosspay = employee.GrossSalary,
                                Payee = payee,
                                NetPay = netpay,
                                HealthInsurance = nhifAmount,
                                Pension = nssfAmount,
                                AllowableDeductions = nssfAmount,
                            });
                    }


                }
                payroll.Status = (int)PayRollStatus.Processed;
            }

            var result = await _payRollAppService.UpdatePayRollAsync(payroll, serviceHeader);
            return payroll;
        }

        public async Task<bool> UpdatePayRollAsync(PayRollDTO payRollDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _payRollAppService.UpdatePayRollAsync(payRollDTO, serviceHeader);
        }

    }
}
