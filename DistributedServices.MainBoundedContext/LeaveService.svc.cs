﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Application.MainBoundedContext.AdministrationModule;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class LeaveService : ILeaveService
    {
        private readonly ILeaveAppService _leaveAppService;

        public LeaveService(ILeaveAppService leaveAppService)
        {
            Guard.ArgumentNotNull(leaveAppService, nameof(leaveAppService));
            _leaveAppService = leaveAppService;
        }

        public async Task<LeaveDTO> AddNewLeaveAsync(LeaveDTO leaveDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.AddNewLeaveAsync(leaveDTO, serviceHeader);
        }

        public async Task<LeaveDTO> FindLeaveByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.FindLeaveByIdAsync(id, serviceHeader);
        }

        public async Task<LeaveDTO> FindLeaveByEmployeeIdAsync(Guid employeeId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.FindLeaveByEmployeeIdAsync(employeeId, serviceHeader);
        }

        public async Task<List<LeaveDTO>> FindLeavesByEmployeeIdAsync(Guid employeeId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.FindLeavesByEmployeeIdAsync(employeeId, serviceHeader);
        }

        public async Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByCustomerInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.FindLeaveFilterByCustomerInPageAsync(customerId,pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByEmployeeInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.FindLeaveFilterByEmployeeInPageAsync(employeeId, pageIndex, pageSize, sortedColumns, searchString, sortAscending, serviceHeader);
        }

        public async Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.FindLeaveFilterInPageAsync(pageIndex,pageSize,sortedColumns,searchString,sortAscending,serviceHeader);
        }

        public async Task<List<LeaveDTO>> FindLeaves()
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.FindLeaves(serviceHeader);
        }

        public async Task<bool> UpdateLeaveAsync(LeaveDTO leaveDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _leaveAppService.UpdateLeaveAsync(leaveDTO,serviceHeader);
        }
    }
}
