﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Application.MainBoundedContext.RegistryModule;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.Seedwork.EndpointBehaviors;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandler]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeAppService _employeeAppService;


        private readonly IAspnetIdentityManagerService _aspnetIdentityManagerService;

        public EmployeeService(
            IEmployeeAppService employeeAppService,
            IAspnetIdentityManagerService aspnetIdentityManagerService)
        {
            Guard.ArgumentNotNull(employeeAppService, nameof(employeeAppService));

            Guard.ArgumentNotNull(aspnetIdentityManagerService, nameof(aspnetIdentityManagerService));

            _employeeAppService = employeeAppService;

            _aspnetIdentityManagerService = aspnetIdentityManagerService;
        }

        public async Task<Tuple<EmployeeDTO, List<string>>> AddNewEmployeeAsync(EmployeeDTO employeeDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            byte recordStatus = 0;

            bool isApproved = false;

            //if created by su insert as approved and generate email
            if (employeeDTO.CreatedBy == "su")
            {
                recordStatus = (byte)RecordStatus.Approved;

                isApproved = true;
            }
            else
            {
                recordStatus = (byte)RecordStatus.New; isApproved = false;

                isApproved = false;
            }

            //create user object
            var userInfoDTO = new ApplicationUserDTO()
            {
                UserName = employeeDTO.UserName,
                Email = employeeDTO.Email,
                RoleName = employeeDTO.RoleName,
                PhoneNumber = employeeDTO.PhoneNumber,
                CustomerId = employeeDTO.CustomerId,
                LockoutEnabled = false,
                LockoutEndDateUtc = DateTime.UtcNow,
                IsExternalUser = employeeDTO.IsExternalUser,
                CreatedDate = employeeDTO.CreatedDate,
                IsEnabled = isApproved,
                RecordStatus = recordStatus,
                CreatedBy = employeeDTO.CreatedBy
            };

            employeeDTO.RecordStatus = recordStatus;
            employeeDTO.IsEnabled = isApproved;

            //insert user into user table

            var user = await _aspnetIdentityManagerService.CreateUserAsync(userInfoDTO);

            //if user created successfull create employee

            if (user.UserCreateSucceed)
            {
                employeeDTO.UserId = new Guid(user.Id);

                var addEmployees  = await _employeeAppService.AddNewEmployeeAsync(employeeDTO, serviceHeader);

                return addEmployees;

            }
            
            return new Tuple<EmployeeDTO, List<string>>(employeeDTO, null);
        }

        public async Task<bool> UpdateEmployeeAsync(EmployeeDTO employeeDTO)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            //create user object
            var userInfoDTO = new ApplicationUserDTO()
            {
                Id = employeeDTO.UserId.ToString(),
                UserName = employeeDTO.UserName,
                Email = employeeDTO.Email,
                RoleName = employeeDTO.RoleName,
                PhoneNumber = employeeDTO.PhoneNumber,
                CustomerId = employeeDTO.CustomerId,
                LockoutEnabled = false,
                LockoutEndDateUtc = DateTime.UtcNow,
                IsExternalUser = employeeDTO.IsExternalUser,
                CreatedDate = employeeDTO.CreatedDate,
                IsEnabled = employeeDTO.IsEnabled
            };

            //insert user into user table

            var user = await _aspnetIdentityManagerService.UpdateAspnetIdentityUserAsync(userInfoDTO);

            return await _employeeAppService.UpdateEmployeeAsync(employeeDTO, serviceHeader);
        }

        public async Task<EmployeeDTO> FindEmployeeByIdAsync(Guid id)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _employeeAppService.FindEmployeeAsync(id, serviceHeader);
        }

        public Task<PageCollectionInfo<EmployeeDTO>> FindCustomerEmployeesFilterInPageAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return _employeeAppService.FindCustomerEmployeesFilterInPageAsync(searchString, startIndex, pageSize, sortedColumns, sortAscending,
                serviceHeader);
        }

        public async Task<PageCollectionInfo<EmployeeDTO>> FindEmployeeByCustomerIdAsync(Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _employeeAppService.FindEmployeeByCustomerIdAsync(customerId, startIndex, pageSize, sortedColumns, sortAscending, serviceHeader);
        }

        public async Task<EmployeeDTO> FindEmployeeByUserIdAsync(Guid userId, Guid? customerId)
        {
            var serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
            return await _employeeAppService.FindEmployeeByUserIdAsync(userId, customerId, serviceHeader);
        }
    }
}
