﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Application.MainBoundedContext.Services;
using Application.Seedwork;
using DistributedServices.MainBoundedContext.InstanceProviders;
using DistributedServices.MainBoundedContext.Migrations;
using DistributedServices.Seedwork.EndpointBehaviors;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Infrastructure.Data.MainBoundedContext.Migrations;
using Infrastructure.Data.MainBoundedContext.UnitOfWork;
using LazyCache;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;

namespace DistributedServices.MainBoundedContext
{
    [ApplicationErrorHandlerAttribute]
    [UnityInstanceProviderServiceBehavior]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class UtilityService : IUtilityService
    {
        private readonly ISqlCommandAppService _sqlCommandAppService;
        private readonly IEnumerationAppService _enumerationAppService;
        private readonly IAppCache _appCache;
        private readonly IDbContextFactory _dbContextFactory;
        private readonly string _baseDirectoryPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data");

        public UtilityService(IEnumerationAppService enumerationAppService,
            IAppCache appCache,
            IDbContextFactory dbContextFactory,
            ISqlCommandAppService sqlCommandAppService)
        {
            Guard.ArgumentNotNull(enumerationAppService, nameof(enumerationAppService));
            Guard.ArgumentNotNull(appCache, nameof(appCache));
            Guard.ArgumentNotNull(dbContextFactory, nameof(dbContextFactory));
            Guard.ArgumentNotNull(sqlCommandAppService, nameof(sqlCommandAppService));

            _enumerationAppService = enumerationAppService;
            _appCache = appCache;
            _dbContextFactory = dbContextFactory;
            _sqlCommandAppService = sqlCommandAppService;
        }
        public bool ConfigureApplicationDatabase()
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            AutoConfiguration autoConfiguration = new AutoConfiguration(true);

            BoundedContextUnitOfWork context = _dbContextFactory.CreateDbContext<BoundedContextUnitOfWork>(serviceHeader);

            autoConfiguration.InitializeDatabase(context);

            return true;
        }

        public bool ConfigureAspNetIdentityDatabase()
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            ApplicationDomainName.CurrentDomainName = serviceHeader.ApplicationDomainName;

            DbMigrator databaseMigrator = new DbMigrator(new Migrations.Configuration());

            databaseMigrator.Update();

            return true;
        }

        public async Task<DashboardWidgetDTO> FindDashboardWidgetsAsync(int claim, Guid? customerId)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _sqlCommandAppService.FindDashboardWidgetsAsync(claim, customerId, serviceHeader);
        }
        public async Task<List<DashboardGraphDTO>> GetDashboardGraphDataSetAsync(int claim, Guid? customerId)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _sqlCommandAppService.GetDashboardGraphDataSetAsync(claim, customerId, serviceHeader);
        }

        public async Task<PageCollectionInfo<EmployeeDTO>> GetEmployeesReportAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _sqlCommandAppService.GetEmployeesReportAsync(claim, customerId, startDate, endDate, pageIndex, pageSize, text, serviceHeader);
        }

        public List<Bank> GetBanksList()
        {
            var bankCodes = new List<string>();

            var bankNames = new List<string>();

            var branchCodes = new List<string>();

            var branchNames = new List<string>();

            var branchIndexes = new List<int>();

            var bankBranchesFileName = Path.Combine(_baseDirectoryPath, "BanksBranches.xls");

            using (FileStream file = new FileStream(bankBranchesFileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    IEnumerator rows = sheet?.GetRowEnumerator();

                    if (rows != null)
                    {
                        rows.MoveNext(); // skip header row

                        int rowCounter = default(int);

                        while (rows.MoveNext())
                        {
                            IRow row = (HSSFRow)rows.Current;

                            ICell cell1Value = row.GetCell(0);  // BankCode
                            ICell cell2Value = row.GetCell(1);  // BranchCode
                            ICell cell3Value = row.GetCell(2);  // BankName
                            ICell cell4Value = row.GetCell(3);  // BranchName
                            ICell cell5Value = row.GetCell(4);  // Status

                            object col1Value = cell1Value?.ToString();
                            object col2Value = cell2Value?.ToString();
                            object col3Value = cell3Value?.ToString();
                            object col4Value = cell4Value?.ToString();
                            object col5Value = cell5Value?.ToString();

                            if (!bankCodes.Contains(col1Value.ToString().Trim()))
                            {
                                bankCodes.Add(col1Value.ToString().Trim());

                                branchIndexes.Add(rowCounter);
                            }

                            if (!bankNames.Contains(col3Value.ToString().Trim()))
                                bankNames.Add(col3Value.ToString().Trim());

                            branchCodes.Add(col2Value.ToString().Trim());

                            branchNames.Add(col4Value.ToString().Trim());

                            rowCounter += 1;
                        }
                    }
                }
            }

            #region branches

            var tupleList = new List<Tuple<long, long>>();

            var indexCounter = 0;

            foreach (var item in branchIndexes)
            {
                if (indexCounter + 1 < branchIndexes.Count)
                {
                    var tuple = new Tuple<long, long>(branchIndexes[indexCounter], branchIndexes[indexCounter + 1]);

                    tupleList.Add(tuple);
                }
                else
                {
                    var tuple = new Tuple<long, long>(branchIndexes.Last(), branchCodes.Count);

                    tupleList.Add(tuple);
                }

                indexCounter += 1;
            }

            var branchNameSets = new List<string[]>();

            var branchCodeSets = new List<string[]>();

            foreach (var tuple in tupleList)
            {
                var skip = (int)tuple.Item1;

                var take = (int)tuple.Item2 - (int)tuple.Item1;

                var targetBranchNames = branchNames.Skip(skip).Take(take).ToArray();

                branchNameSets.Add(targetBranchNames);

                var targetBranchCodes = branchCodes.Skip(skip).Take(take).ToArray();

                branchCodeSets.Add(targetBranchCodes);
            }

            #endregion

            #region banks

            var banksList = new List<Bank>();

            var bankCounter = 0;

            foreach (var bankName in bankNames)
            {
                if (bankCounter < bankCodes.Count)
                {
                    var bank = new Bank { Code = $"{bankCodes[bankCounter]}".PadLeft(2, '0'), Name = bankName };

                    var branchCounter = 0;

                    foreach (var branchName in branchNameSets[bankCounter])
                    {
                        var branch = new Branch { Code = $"{branchCodeSets[bankCounter][branchCounter]}".PadLeft(3, '0'), Name = branchName };

                        bank.Branches.Add(branch);

                        branchCounter += 1;
                    }

                    banksList.Add(bank);

                    bankCounter += 1;
                }
            }

            #endregion

            return banksList.OrderBy(x => x.Name).ToList();
        }

        public List<Branch> GetBranchesList(string bankCode)
        {
            var banks = GetBanksList();

            if (banks != null)
            {
                var targetBank = (from b in banks
                                  where b.Code == bankCode
                                  select b).FirstOrDefault();

                return targetBank?.Branches.OrderBy(x => x.Name).ToList();
            }

            return null;
        }

        public PageCollectionInfo<Branch> GetBranchesListFilterInPage(string bankCode, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var branches = GetBranchesList(bankCode).AsQueryable();

            return ProjectionsExtensionMethods.AllMatchingPaged(branches, BranchFullText(text), pageIndex, pageSize, new List<string> { "Name" }, ascending);
        }
        public Bank GetBankByBankCode(string bankCode)
        {
            var banks = GetBanksList().AsQueryable();

            return banks?.Where(x => x.Code == bankCode)?.FirstOrDefault();
        }

        public Branch GetBrancheByBranchCode(string bankCode, string branchCode)
        {
            var branches = GetBranchesList(bankCode).AsQueryable();

            return branches?.Where(x => x.Code == branchCode)?.FirstOrDefault();
        }

        public PageCollectionInfo<Bank> GetBanksListFilterInPage(string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending)
        {
            var banks = GetBanksList().AsQueryable();

            return ProjectionsExtensionMethods.AllMatchingPaged(banks, BankFullText(text), pageIndex, pageSize, new List<string> { "Name" }, ascending);
        }

        public static Specification<Branch> BranchFullText(string text)
        {
            Specification<Branch> specification = new TrueSpecification<Branch>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                text = text.ToLower();

                var nameSpec = new DirectSpecification<Branch>(c => c.Name.ToLower().Contains(text));

                var codeSpec = new DirectSpecification<Branch>(c => c.Code.ToLower().Contains(text));

                specification &= (nameSpec | codeSpec);
            }

            return specification;
        }

        public static Specification<Bank> BankFullText(string text)
        {
            Specification<Bank> specification = new TrueSpecification<Bank>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                text = text.ToLower();

                var nameSpec = new DirectSpecification<Bank>(c => c.Name.ToLower().Contains(text));

                var codeSpec = new DirectSpecification<Bank>(c => c.Code.ToLower().Contains(text));

                specification &= (nameSpec | codeSpec);
            }

            return specification;
        }

        public async Task<PageCollectionInfo<PaySlipDTO>> GetPayRollAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _sqlCommandAppService.GetPayRollAsync(claim, customerId, startDate, endDate, pageIndex, pageSize, text, serviceHeader);
        }

        public async Task<PageCollectionInfo<PaySlipDTO>> GetPaySlipAsync(Guid employeeId, int period)
        {
            ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);

            return await _sqlCommandAppService.GetPaySlipAsync(employeeId, period,  serviceHeader);
        }
    }
}
