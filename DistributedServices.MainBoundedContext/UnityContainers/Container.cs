﻿using Application.MainBoundedContext.AdministrationModule;
using Application.MainBoundedContext.DTO.TypeAdapterFactory;
using Application.MainBoundedContext.MessagingModule.Services;
using Application.MainBoundedContext.RegistryModule;
using Application.MainBoundedContext.RegistryModule.Services;
using Application.MainBoundedContext.Services;
using DistributedServices.MainBoundedContext.Identity;
using Domain.Seedwork;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Services.RedisClient;
using Infrastructure.Data.MainBoundedContext.Repositories;
using Infrastructure.Data.MainBoundedContext.UnitOfWork;
using LazyCache;
using Microsoft.AspNet.Identity;
using Numero3.EntityFramework.Implementation;
using Numero3.EntityFramework.Interfaces;
using System.Runtime.Caching;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace DistributedServices.MainBoundedContext.UnityContainers
{
    /// <summary>
    /// DI container accessor
    /// </summary>
    public static class Container
    {
        #region Properties

        /// <summary>
        /// Get the current configured container
        /// </summary>
        /// <returns>Configured container</returns>
        public static IUnityContainer Current { get; private set; }

        #endregion

        #region Constructor

        static Container()
        {
            ConfigureContainer();
            ConfigureFactories();
        }

        #endregion

        #region Methods

        private static void ConfigureContainer()
        {
            /*
             * Add here the code configuration or the call to configure the container 
             * using the application configuration file
             */

            Current = new UnityContainer();

            //-> Caching
            Current.RegisterType<IAppCache, CachingService>(new ContainerControlledLifetimeManager(), new InjectionConstructor(MemoryCache.Default));

            //-> DbContext
            Current.RegisterType<IDbContextFactory, RuntimeContextFactory>(new ContainerControlledLifetimeManager());
            Current.RegisterType<IDbContextScopeFactory, DbContextScopeFactory>(new ContainerControlledLifetimeManager());
            Current.RegisterType<IAmbientDbContextLocator, AmbientDbContextLocator>(new ContainerControlledLifetimeManager());

            //-> Logging
            Current.RegisterType<ILogger, SerilogLogger>(new ContainerControlledLifetimeManager());
            Current.RegisterType<IAuditTrailAppService, AuditTrailAppService>(new ContainerControlledLifetimeManager());

            //-> Adapters
            Current.RegisterType<ITypeAdapterFactory, AutomapperTypeAdapterFactory>(new ContainerControlledLifetimeManager());
            Current.RegisterType<ILoggerFactory, SerilogLoggerFactory>(new ContainerControlledLifetimeManager());

            //-> Identity Framework
            Current.RegisterType<ApplicationDbContext>(new TransientLifetimeManager(), new InjectionConstructor("AuthStore"));

            Current.RegisterType<IUserStore<ApplicationUser>, ApplicationUserStore>(new InjectionConstructor(typeof(ApplicationDbContext)));

            Current.RegisterType<ApplicationUserManager>(new InjectionConstructor(typeof(ApplicationUserStore)));

            Current.RegisterType<IRoleStore<ApplicationRole, string>, ApplicationRoleStore>(new InjectionConstructor(typeof(ApplicationDbContext)));

            Current.RegisterType<ApplicationRoleManager>(new InjectionConstructor(typeof(ApplicationRoleStore)));

            Current.RegisterType<IAspnetIdentityManagerService, AspnetIdentityManagerService>();

            Current.RegisterType<IAspNetRoleManagerService, AspNetRoleManagerService>();

            //-> Messaging Module Service
            Current.RegisterType<IEmailAlertAppService, EmailAlertAppService>();
            Current.RegisterType<ITextAlertAppService, TextAlertAppService>();
            Current.RegisterType<IBrokerService, BrokerService>();
            Current.RegisterType<IMessageQueueService, MessageQueueService>();
            Current.RegisterType<IEmailAppService, EmailAppService>();


            Current.RegisterType<IEnumerationAppService, EnumerationAppService>();
            Current.RegisterType<ISqlCommandAppService, SqlCommandAppService>();
            Current.RegisterType<IHttpRequestAppService, HttpRequestAppService>();
            

            //->Administration Module
            Current.RegisterType<IModuleNavigationItemAppService, ModuleNavigationItemAppService>();
            Current.RegisterType<IRedisClientAppService, RedisClientAppService>();
            Current.RegisterType<ILeaveAppService, LeaveAppService>();
            Current.RegisterType<IPayRollAppService, PayRollAppService>();
            Current.RegisterType<IStaticSettingAppService, StaticSettingAppService>();
            Current.RegisterType<ITaxSettingAppService, TaxSettingAppService>();
            Current.RegisterType<IDeductionAppService, DeductionAppService>();

            //-> Repositories	
            Current.RegisterType(typeof(IRedisRepository<>), typeof(RedisRepository<>));
            Current.RegisterType(typeof(IRepository<>), typeof(Repository<>));

            //-> Registry Module Service
            Current.RegisterType<ICustomerAppService, CustomerAppService>();
            Current.RegisterType<IEmployeeAppService, EmployeeAppService>();

        }

        private static void ConfigureFactories()
        {
            var loggerFactory = Current.Resolve<ILoggerFactory>();
            LoggerFactory.SetCurrent(loggerFactory);

            var typeAdapterFactory = Current.Resolve<ITypeAdapterFactory>();
            TypeAdapterFactory.SetCurrent(typeAdapterFactory);
        }

        #endregion
    }
}