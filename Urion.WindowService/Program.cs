﻿using System.ServiceProcess;
using System.Threading;

namespace Urion.WindowService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if (DEBUG)
            MainService serviceBase = new MainService();
            serviceBase.StartDebugging(null);
            Thread.Sleep(Timeout.Infinite);
#else
            

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new MainService() 
			};
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
