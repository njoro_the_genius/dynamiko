﻿using System.Configuration;

namespace TextAlertDispatcher.Configuration
{
    public class TextAlertDispatcherConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("textAlertDispatcherSettings")]
        public TextAlertDispatcherSettingsCollection TextAlertDispatcherSettingsItems => (TextAlertDispatcherSettingsCollection)base["textAlertDispatcherSettings"];

    }
}
