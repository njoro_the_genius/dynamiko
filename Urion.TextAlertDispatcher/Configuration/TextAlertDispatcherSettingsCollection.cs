﻿using System.Configuration;

namespace TextAlertDispatcher.Configuration
{
    public class TextAlertDispatcherSettingsCollection : ConfigurationElementCollection
    {
        public TextAlertDispatcherSettingsElement this[int index] => (TextAlertDispatcherSettingsElement)BaseGet(index);

        [ConfigurationProperty("name", IsRequired = false)]
        public string Name => (string)base["name"];

        [ConfigurationProperty("queuePath", IsRequired = true)]
        public string QueuePath => (string)base["queuePath"];

        [ConfigurationProperty("queueingJobCronExpression", IsRequired = true)]
        public string QueueingJobCronExpression => (string)base["queueingJobCronExpression"];

        [ConfigurationProperty("queueReceivers", IsRequired = true)]
        public int QueueReceivers => (int)base["queueReceivers"];

        [ConfigurationProperty("logPath", IsRequired = true)]
        public string LogPath => (string)base["logPath"];

        [ConfigurationProperty("logEnabled", IsRequired = true)]
        public int LogEnabled => (int)base["logEnabled"];

        [ConfigurationProperty("serviceProvider", IsRequired = true)]
        public int ServiceProvider => (int)base["serviceProvider"];

        protected override ConfigurationElement CreateNewElement()
        {
            return new TextAlertDispatcherSettingsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TextAlertDispatcherSettingsElement)element).UniqueId;
        }
    }
}
