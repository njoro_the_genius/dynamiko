﻿using Infrastructure.Crosscutting.Framework.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace TextAlertDispatcher.Configuration
{
    public class AfricasTalkingGatewayException : Exception
    {
        public AfricasTalkingGatewayException(string message)
            : base(message)
        {

        }
    }

    public class AfricasTalkingGateway
    {
        private static readonly string VOICE_URLString = "https://voice.africastalking.com";
        private static readonly string SUBSCRIPTION_URLString = "https://api.africastalking.com/subscription";
        private static readonly string USERDATA_URLString = "https://api.africastalking.com/version1/user";
        private static readonly string AIRTIME_URLString = "https://api.africastalking.com/version1/airtime";
        private readonly string _apiKey;
        private readonly string _username;

        //Change the debug flag to true to view the full response
        private readonly bool DEBUG = false;
        private int responseCode;
        private readonly JavaScriptSerializer serializer;

        public AfricasTalkingGateway(string username_, string apiKey_)
        {
            _username = username_;
            _apiKey = apiKey_;
            serializer = new JavaScriptSerializer();
        }

        public dynamic sendMessage(string to_, string message_, string urlString_, string from_ = null,
            int bulkSMSMode_ = 1, Hashtable options_ = null)
        {
            var data = new Hashtable();
            data["username"] = _username;
            data["to"] = to_;
            data["message"] = message_;

            if (from_ != null)
            {
                data["from"] = from_;
                data["bulkSMSMode"] = Convert.ToString(bulkSMSMode_);

                if (options_ != null)
                {
                    if (options_.Contains("keyword")) data["keyword"] = options_["keyword"];

                    if (options_.Contains("linkId")) data["linkId"] = options_["linkId"];

                    if (options_.Contains("enqueue")) data["enqueue"] = options_["enqueue"];

                    if (options_.Contains("retryDurationInHours"))
                        data["retryDurationInHours"] = options_["retryDurationInHours"];
                }
            }

            var response = sendPostRequest(data, urlString_);
            if (responseCode == (int)HttpStatusCode.Created)
            {
                var json = serializer.Deserialize<dynamic>(response);
                var recipients = json["SMSMessageData"]["Recipients"];
                return recipients;
            }

            throw new AfricasTalkingGatewayException(response);
        }

        public dynamic fetchMessages(int lastReceivedId_, string urlString_)
        {
            var url = urlString_ + "?username=" + _username + "&lastReceivedId=" + Convert.ToString(lastReceivedId_);
            var response = sendGetRequest(url);
            if (responseCode == (int)HttpStatusCode.OK)
            {
                dynamic json = serializer.DeserializeObject(response);
                return json["SMSMessageData"]["Messages"];
            }

            throw new AfricasTalkingGatewayException(response);
        }

        public dynamic createSubscription(string phoneNumber_, string shortCode_, string keyword_)
        {
            if (phoneNumber_.Length == 0 || shortCode_.Length == 0 || keyword_.Length == 0)
                throw new AfricasTalkingGatewayException("Please supply phone number, short code and keyword");
            var data_ = new Hashtable();
            data_["username"] = _username;
            data_["phoneNumber"] = phoneNumber_;
            data_["shortCode"] = shortCode_;
            data_["keyword"] = keyword_;
            var urlString = SUBSCRIPTION_URLString + "/create";
            var response = sendPostRequest(data_, urlString);
            if (responseCode == (int)HttpStatusCode.Created)
            {
                var json = serializer.Deserialize<dynamic>(response);
                return json;
            }

            throw new AfricasTalkingGatewayException(response);
        }

        public dynamic deleteSubscription(string phoneNumber_, string shortCode_, string keyword_)
        {
            if (phoneNumber_.Length == 0 || shortCode_.Length == 0 || keyword_.Length == 0)
                throw new AfricasTalkingGatewayException("Please supply phone number, short code and keyword");
            var data_ = new Hashtable
            {
                ["username"] = _username,
                ["phoneNumber"] = phoneNumber_,
                ["shortCode"] = shortCode_,
                ["keyword"] = keyword_
            };
            var urlString = SUBSCRIPTION_URLString + "/delete";
            var response = sendPostRequest(data_, urlString);
            if (responseCode == (int)HttpStatusCode.Created)
            {
                var json = serializer.Deserialize<dynamic>(response);
                return json;
            }

            throw new AfricasTalkingGatewayException(response);
        }

        public dynamic call(string from_, string to_)
        {
            var data = new Hashtable();
            data["username"] = _username;
            data["from"] = from_;
            data["to"] = to_;

            var urlString = VOICE_URLString + "/call";
            var response = sendPostRequest(data, urlString);

            var json = serializer.Deserialize<dynamic>(response);
            if ((string)json["errorMessage"] == "None") return json["entries"];
            throw new AfricasTalkingGatewayException(json["errorMessage"]);
        }

        public int getNumQueuedCalls(string phoneNumber_, string queueName_ = null)
        {
            var data = new Hashtable();
            data["username"] = _username;
            data["phoneNumbers"] = phoneNumber_;
            if (queueName_ != null)
                data["queueName"] = queueName_;

            var urlString = VOICE_URLString + "/queueStatus";
            var response = sendPostRequest(data, urlString);
            var json = serializer.Deserialize<dynamic>(response);
            if ((string)json["errorMessage"] == "None") return json["entries"];
            throw new AfricasTalkingGatewayException(json["errorMessage"]);
        }

        public void uploadMediaFile(string url_)
        {
            var data = new Hashtable();
            data["username"] = _username;
            data["url"] = url_;

            var urlString = VOICE_URLString + "/mediaUpload";
            var response = sendPostRequest(data, urlString);
            var json = serializer.Deserialize<dynamic>(response);
            if ((string)json["errorMessage"] != "None")
                throw new AfricasTalkingGatewayException(json["errorMessage"]);
        }

        public dynamic sendAirtime(ArrayList recipients_)
        {
            var urlString = AIRTIME_URLString + "/send";
            var recipients = new JavaScriptSerializer().Serialize(recipients_);
            var data = new Hashtable();
            data["username"] = _username;
            data["recipients"] = recipients;
            var response = sendPostRequest(data, urlString);
            if (responseCode == (int)HttpStatusCode.Created)
            {
                var json = serializer.Deserialize<dynamic>(response);
                if (json["responses"].Count > 0)
                    return json["responses"];
                throw new AfricasTalkingGatewayException(json["errorMessage"]);
            }

            throw new AfricasTalkingGatewayException(response);
        }

        public dynamic getUserData()
        {
            var urlString = USERDATA_URLString + "?username=" + _username;
            var response = sendGetRequest(urlString);
            if (responseCode == (int)HttpStatusCode.OK)
            {
                var json = serializer.Deserialize<dynamic>(response);
                return json["UserData"];
            }

            throw new AfricasTalkingGatewayException(response);
        }

        private string sendPostRequest(Hashtable dataMap_, string urlString_)
        {
            try
            {
                var dataStr = "";
                foreach (string key in dataMap_.Keys)
                {
                    if (dataStr.Length > 0) dataStr += "&";
                    var value = (string)dataMap_[key];
                    dataStr += HttpUtility.UrlEncode(key, Encoding.UTF8);
                    dataStr += "=" + HttpUtility.UrlEncode(value, Encoding.UTF8);
                }

                var byteArray = Encoding.UTF8.GetBytes(dataStr);

                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationCallback;
                var webRequest = (HttpWebRequest)WebRequest.Create(urlString_);

                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = byteArray.Length;
                webRequest.Accept = "application/json";

                webRequest.Headers.Add("apiKey", _apiKey);

                var webpageStream = webRequest.GetRequestStream();
                webpageStream.Write(byteArray, 0, byteArray.Length);
                webpageStream.Close();

                var httpResponse = (HttpWebResponse)webRequest.GetResponse();
                responseCode = (int)httpResponse.StatusCode;
                var webpageReader = new StreamReader(httpResponse.GetResponseStream());
                var response = webpageReader.ReadToEnd();

                if (DEBUG)
                    Console.WriteLine("Full response: " + response);

                return response;
            }
            catch (WebException ex)
            {
                if (ex.Response == null)
                    throw new AfricasTalkingGatewayException(ex.Message);
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var response = reader.ReadToEnd();

                    if (DEBUG)
                        Console.WriteLine("Full response: " + response);

                    return response;
                }
            }

            catch (AfricasTalkingGatewayException ex)
            {
                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
                throw ex;
            }
        }

        private string sendGetRequest(string urlString_)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationCallback;

                var webRequest = (HttpWebRequest)WebRequest.Create(urlString_);
                webRequest.Method = "GET";
                webRequest.Accept = "application/json";
                webRequest.Headers.Add("apiKey", _apiKey);

                var httpResponse = (HttpWebResponse)webRequest.GetResponse();
                responseCode = (int)httpResponse.StatusCode;
                var webpageReader = new StreamReader(httpResponse.GetResponseStream());

                var response = webpageReader.ReadToEnd();

                if (DEBUG)
                    Console.WriteLine("Full response: " + response);

                return response;
            }

            catch (WebException ex)
            {
                if (ex.Response == null)
                    throw new AfricasTalkingGatewayException(ex.Message);

                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var response = reader.ReadToEnd();

                    if (DEBUG)
                        Console.WriteLine("Full response: " + response);

                    return response;
                }
            }

            catch (AfricasTalkingGatewayException ex)
            {
                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
                throw ex;
            }
        }

        private bool RemoteCertificateValidationCallback(object sender_, X509Certificate certificate, X509Chain chain_,
            SslPolicyErrors errors_)
        {
            return true;
        }

        public class SendAirtime
        {
            public string errorMessage { get; set; }

            public int numSent { get; set; }

            public string totalAmount { get; set; }

            public string totalDiscount { get; set; }

            public IList<responses> responses { get; set; }
        }

        public class responses
        {
            public string phoneNumber { get; set; }

            public string errorMessage { get; set; }

            public string amount { get; set; }

            public string status { get; set; }

            public string requestId { get; set; }

            public string discount { get; set; }
        }
    }
}
