﻿
using System.Configuration;

namespace TextAlertDispatcher.Configuration
{
    public class TextAlertDispatcherSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("uniqueId", IsKey = true, IsRequired = true)]
        public string UniqueId
        {
            get => (string)base["uniqueId"];
            set => base["uniqueId"] = value;
        }

        [ConfigurationProperty("queuePageSize", IsKey = false, IsRequired = true)]
        public int QueuePageSize
        {
            get => (int)base["queuePageSize"];
            set => base["queuePageSize"] = value;
        }

        [ConfigurationProperty("timeToBeReceived", IsKey = false, IsRequired = true)]
        public int TimeToBeReceived
        {
            get => (int)base["timeToBeReceived"];
            set => base["timeToBeReceived"] = value;
        }

        [ConfigurationProperty("enabled", IsKey = false, IsRequired = true)]
        public int Enabled
        {
            get => (int)base["enabled"];
            set => base["enabled"] = value;
        }

        [ConfigurationProperty("callBackUrl", IsKey = false, IsRequired = true)]
        public string CallBackUrl
        {
            get => (string)base["callBackUrl"];
            set => base["callBackUrl"] = value;
        }

        [ConfigurationProperty("fikiwaUrl", IsKey = false, IsRequired = true)]
        public string FikiwaUrl
        {
            get => (string)base["fikiwaUrl"];
            set => base["fikiwaUrl"] = value;
        }
    }
}
