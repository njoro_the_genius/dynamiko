﻿using Infrastructure.Crosscutting.Framework.Logging;
using Quartz;
using Quartz.Impl;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;
using TextAlertDispatcher.QuartzJobs;
using Presentation.Infrastructure.Services;

namespace TextAlertDispatcher.Services
{
    [Export(typeof(IPlugin))]
    public class Queuer : IPlugin
    {
        private readonly ILogger _logger;

        [ImportingConstructor]
        public Queuer(ILogger logger)
        {
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
            // Get a reference to the scheduler
            var sf = new StdSchedulerFactory();

            //logger
            _logger = logger;
        }

        #region IPlugin

        public Guid Id => new Guid("{C47A9837-5CB8-4AB8-B4A8-0073190E878F}");

        public string Description => "TEXT QUEUER";

        public void DoWork(IScheduler scheduler, params string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    var textDispatcherConfigSection = (TextAlertDispatcherConfigurationSection)ConfigurationManager.GetSection("textAlertDispatcherConfiguration");

                    if (textDispatcherConfigSection != null)
                    {
                        // Define the Job to be scheduled
                        var job = JobBuilder.Create<QueueingJob>()
                            .WithIdentity("TextQueueingJob", "Urion")
                            .RequestRecovery()
                            .Build();

                        // Associate a trigger with the Job
                        var cronExpression = textDispatcherConfigSection.TextAlertDispatcherSettingsItems.QueueingJobCronExpression;

                        var trigger = (ICronTrigger)TriggerBuilder.Create()
                            .WithIdentity("TextQueueingJob", "Urion")
                            .WithCronSchedule(cronExpression)
                            .StartAt(DateTime.Now)
                            .WithPriority(1)
                            .Build();

                        // Validate that the job doesn't already exists
                        if (await scheduler.CheckExists(new JobKey("TextQueueingJob", "Urion")))
                        {
                            await scheduler.DeleteJob(new JobKey("TextQueueingJob", "Urion"));
                        }

                        var schedule = await scheduler.ScheduleJob(job, trigger);

                        _logger.LogInfo("Job '{0}' scheduled for '{1}'", "TextQueueingJob", schedule.ToString("r"));
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("{0}->DoWork...", ex, Description);
                }
            });
        }

        public void Exit()
        {

        }

        #endregion
    }
}
