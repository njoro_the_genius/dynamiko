﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Logging;
using Presentation.Infrastructure.Services;
using Quartz;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using TextAlertDispatcher.Configuration;
using TextAlertDispatcher.QueueProcessors;

namespace TextAlertDispatcher.Services
{
    [Export(typeof(IPlugin))]
    public class Dispatcher : IPlugin
    {
        private readonly IChannelService _channelService;

        private readonly IMessageQueueService _messageQueueService;

        private DispatcherMessageProcessor _messageProcessor;

        private readonly ILogger _logger;

        [ImportingConstructor]
        public Dispatcher(
            IChannelService channelService,
            IMessageQueueService messageQueueService, ILogger logger)
        {

            _channelService = channelService;

            _messageQueueService = messageQueueService ?? throw new ArgumentNullException(nameof(messageQueueService));

            _logger = logger;
        }

        #region IPlugin

        public Guid Id => new Guid("{EE092E59-317F-44AC-95B9-1B57FD8613A5}");

        public string Description => "Text Alert Dispatcher";

        public void DoWork(IScheduler scheduler, params string[] args)
        {

            try
            {
                var textDispatcherConfigSection = (TextAlertDispatcherConfigurationSection)ConfigurationManager.GetSection("textAlertDispatcherConfiguration");

                if (textDispatcherConfigSection != null)
                {
                    _messageProcessor = new DispatcherMessageProcessor(_channelService, _messageQueueService, textDispatcherConfigSection, _logger);

                    _messageProcessor.Open();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{0}->DoWork...", ex, Description);
            }
        }

        public void Exit()
        {
            try
            {
                _messageProcessor?.Close();
            }
            catch (Exception ex)
            {
                _logger.LogError("{0}->Exit...", ex, Description);
            }
        }

        #endregion
    }
}
