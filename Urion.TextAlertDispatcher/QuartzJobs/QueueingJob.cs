﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using Quartz;
using System;
using System.Configuration;
using System.Messaging;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;

namespace TextAlertDispatcher.QuartzJobs
{
    public class QueueingJob : IJob
    {
        private readonly IChannelService _channelService;

        private readonly IMessageQueueService _messageQueueService;

        private readonly ILogger _logger;

        public QueueingJob(IMessageQueueService messageQueueService, IChannelService channelService, ILogger logger)
        {
            _messageQueueService = messageQueueService;

            _channelService = channelService;

            _logger = logger;
        }

        #region IJob

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.Debug("{0}****{0}Job {1} fired @ {2} next scheduled for {3}{0}***{0}", Environment.NewLine, context.JobDetail.Key, context.FireTimeUtc.ToString(), context.NextFireTimeUtc.Value.ToString("r"));

            try
            {
                var textDispatcherConfigSection = (TextAlertDispatcherConfigurationSection)ConfigurationManager.GetSection("textDispatcherConfiguration");

                if (textDispatcherConfigSection != null)
                    foreach (var settingsItem in textDispatcherConfigSection.TextAlertDispatcherSettingsItems)
                    {
                        var textDispatcherSettingsElement = (TextAlertDispatcherSettingsElement)settingsItem;

                        if (textDispatcherSettingsElement != null && textDispatcherSettingsElement.Enabled == 1)
                        {
                            var serviceHeader = new ServiceHeader();

                            // 3. Retrieve messages whose DLR status is Pending
                            var textAlertsWithDLRStatusPending = await _channelService.FindTextAlertsByFilterInPageAsync((int)DLRStatus.Pending, null, 0, textDispatcherSettingsElement.QueuePageSize, null, true, serviceHeader);


                            _logger.Debug("{0}****{0}textAlertsWithDLRStatusPending {1}{0}***{0}", Environment.NewLine, textAlertsWithDLRStatusPending != null && textAlertsWithDLRStatusPending.PageCollection != null ? textAlertsWithDLRStatusPending.PageCollection.Count : -1);

                            // 4. Send the messages to msmq - Normal priority
                            if (textAlertsWithDLRStatusPending != null)
                            {
                                foreach (var item in textAlertsWithDLRStatusPending.PageCollection)
                                {
                                    if (item.TextMessageSendRetry == 0)
                                    {
                                        var queueDTO = new QueueDTO
                                        {
                                            RecordId = item.Id,
                                            ServiceProvider = textDispatcherConfigSection.TextAlertDispatcherSettingsItems.ServiceProvider,
                                            BulkTextSenderId = item.PartnerSMSGatewaySenderId,
                                            BulkTextUsername = item.PartnerSMSGatewayUsername.Decrypt(),
                                            BulkTextPassword = item.PartnerSMSGatewayPassword.Decrypt(),
                                            BulkTextUrl = item.PartnerSMSGatewayUrl
                                        };

                                        _messageQueueService.Send(textDispatcherConfigSection.TextAlertDispatcherSettingsItems.QueuePath, queueDTO, MessageCategory.TextAlert, (MessagePriority)item.TextMessagePriority, textDispatcherSettingsElement.TimeToBeReceived);
                                    }
                                }
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                _logger.LogError("Urion.TextAlertDispatcher.Configuration.AfricasTalking_Queueing.Execute", ex);
            }
        }
        #endregion
    }
}
