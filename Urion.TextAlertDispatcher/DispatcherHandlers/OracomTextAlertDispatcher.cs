﻿using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Newtonsoft.Json;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;

namespace TextAlertDispatcher.DispatcherHandlers
{
    public class OracomTextAlertDispatcher : ITextAlertDispatcher
    {
        private readonly IChannelService _channelService;

        private readonly TextAlertDispatcherConfigurationSection _textAlertDispatcherConfigurationSection;

        private readonly ILogger _logger;

        private readonly ITextAlertDispatcher _next;

        public OracomTextAlertDispatcher(IChannelService channelService,
            TextAlertDispatcherConfigurationSection textAlertDispatcherConfigurationSection, ILogger logger)
        {
            _channelService = channelService;

            _textAlertDispatcherConfigurationSection = textAlertDispatcherConfigurationSection;

            _logger = logger;
        }

        public Task HandleRequest(QueueDTO queueDto, int appSpecific)
        {
            return SendAsync(queueDto, appSpecific);
        }

        private async Task SendAsync(QueueDTO queueDto, int appSpecific)
        {
            try
            {
                foreach (var settingsItem in _textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems)
                {
                    var textDispatcherSettingsElement = (TextAlertDispatcherSettingsElement)settingsItem;

                    if (textDispatcherSettingsElement != null && textDispatcherSettingsElement.Enabled == 1)
                    {
                        var messageCategory = (MessageCategory)appSpecific;

                        var serviceHeader = new ServiceHeader();

                        switch (messageCategory)
                        {
                            case MessageCategory.TextAlert:

                                #region SMS Alert

                                var smsAlert = await _channelService.FindTextAlertAsync(queueDto.RecordId, serviceHeader);

                                if (smsAlert != null)
                                    switch ((DLRStatus)smsAlert.TextMessageDLRStatus)
                                    {
                                        case DLRStatus.UnKnown:
                                        case DLRStatus.Pending:

                                            if (!string.IsNullOrWhiteSpace(smsAlert.TextMessageRecipient) && !string.IsNullOrWhiteSpace(smsAlert.UnmaskedTextMessageBody))
                                            {
                                                var msisdn = smsAlert.TextMessageRecipient.Trim();

                                                var textMessage = smsAlert.UnmaskedTextMessageBody.Trim();

                                                var request = new request
                                                {
                                                    authentication = new authentication
                                                    {
                                                        username = smsAlert.PartnerSMSGatewayUsername.Decrypt(),
                                                        password = smsAlert.PartnerSMSGatewayPassword.Decrypt()
                                                    },
                                                    messages = new List<message>
                                                {
                                                    new message
                                                    {
                                                        sender = smsAlert.PartnerSMSGatewaySenderId,
                                                        text = textMessage,
                                                        recipients = new List<recipient>
                                                        {
                                                            new recipient
                                                            {
                                                                gsm = msisdn.Replace("+", string.Empty)
                                                            }
                                                        }
                                                    }
                                                }
                                                };

                                                string jsonConvert = JsonConvert.SerializeObject(request);

                                                HttpWebResponse webResponse = Post(System.Text.Encoding.Default.GetBytes(jsonConvert));

                                                if (webResponse.StatusCode == HttpStatusCode.OK)
                                                {
                                                    using (Stream responseStream = webResponse.GetResponseStream())
                                                    {
                                                        using (StreamReader reader = new StreamReader(responseStream))
                                                        {
                                                            string json = reader.ReadToEnd();

                                                            response response = JsonConvert.DeserializeObject<response>(json);

                                                            foreach (var item in response.results)
                                                            {
                                                                switch (item.status)
                                                                {
                                                                    case "0":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Delivered;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|ALL_RECIPIENTS_PROCESSED";
                                                                        break;
                                                                    case "-1":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|SEND_ERROR";
                                                                        break;
                                                                    case "-2":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|NOT_ENOUGH_CREDITS";
                                                                        break;
                                                                    case "-3":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|NETWORK_NOTCOVERED";
                                                                        break;
                                                                    case "-5":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|INVALID_USER_OR_PASS";
                                                                        break;
                                                                    case "-6":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|MISSING_DESTINATION_ADDRESS";
                                                                        break;
                                                                    case "-10":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|MISSING_USERNAME";
                                                                        break;
                                                                    case "-11":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|MISSING_PASSWORD";
                                                                        break;
                                                                    case "-13":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|INVALID_DESTINATION_ADDRESS";
                                                                        break;
                                                                    case "-22":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|SYNTAX_ERROR";
                                                                        break;
                                                                    case "-23":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|ERROR_PROCESSING";
                                                                        break;
                                                                    case "-26":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|COMMUNICATION_ERROR";
                                                                        break;
                                                                    case "-27":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|INVALID_SENDDATETIME";
                                                                        break;
                                                                    case "-28":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|INVALID_DELIVERY_REPORT_PUSH_URL";
                                                                        break;
                                                                    case "-30":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|INVALID_CLIENT_APPID";
                                                                        break;
                                                                    case "-33":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|DUPLICATE_MESSAGEID";
                                                                        break;
                                                                    case "-34":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|SENDER_NOT_ALLOWED";
                                                                        break;
                                                                    case "-99":
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = $"{item.messageid}|GENERAL_ERROR";
                                                                        break;
                                                                    default:
                                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;
                                                                        smsAlert.TextMessageSendRetry = 1;
                                                                        smsAlert.TextMessageReference = json;
                                                                        break;
                                                                }

                                                                break;
                                                            }

                                                            smsAlert.TextMessageSendRetry = 1;

                                                            await _channelService.UpdateTextAlertAsync(smsAlert, serviceHeader);
                                                        }
                                                    }
                                                }
                                            }
                                            break;
                                        default:
                                            break;
                                    }

                                #endregion

                                break;
                        }
                    }
                }
            }

            catch (Exception exception)
            {
                _logger.LogError("{0}->OracomTextAlertDispatcher...", exception, _textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueuePath);
            }
        }

        #region Helpers

        private HttpWebResponse Post(byte[] buffer)
        {
            var webRequest = WebRequest.Create(requestUriString: "http://107.20.199.106/api/v3/sendsms/json") as HttpWebRequest;

            if (webRequest == null) throw new NullReferenceException(message: "request is not a http request");

            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = buffer.Length;

            using (Stream requestStream = webRequest.GetRequestStream())
            {
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Close();
            }

            return webRequest.GetResponse() as HttpWebResponse;
        }

        #endregion
    }

    public class request
    {
        public authentication authentication { get; set; }

        public List<message> messages { get; set; }
    }

    public class response
    {
        public List<result> results { get; set; }
    }

    public class authentication
    {
        public string username { get; set; }

        public string password { get; set; }
    }

    public class message
    {
        public string sender { get; set; }

        public string text { get; set; }

        public List<recipient> recipients { get; set; }
    }

    public class recipient
    {
        public string gsm { get; set; }
    }

    public class result
    {
        public string status { get; set; }

        public string messageid { get; set; }

        public string destination { get; set; }
    }
}
