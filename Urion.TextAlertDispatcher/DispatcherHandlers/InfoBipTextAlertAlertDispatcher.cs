﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Newtonsoft.Json;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;

namespace TextAlertDispatcher.DispatcherHandlers
{
    class InfoBipTextAlertAlertDispatcher : ITextAlertDispatcher
    {
        private readonly IChannelService _channelService;

        private readonly IMessageQueueService _messageQueueService;

        private readonly TextAlertDispatcherConfigurationSection _textDispatcherConfigurationSection;

        private readonly ILogger _logger;

        public InfoBipTextAlertAlertDispatcher(
            IChannelService channelService,
            IMessageQueueService messageQueueService,
            TextAlertDispatcherConfigurationSection textAlertDispatcherConfigurationSection, ILogger logger)
        {
            _channelService = channelService;

            _messageQueueService = messageQueueService;

            _textDispatcherConfigurationSection = textAlertDispatcherConfigurationSection;

            _logger = logger;
        }

        public Task HandleRequest(QueueDTO queueDto, int appSpecific)
        {
            return SendAsync(queueDto, appSpecific);
        }

        private async Task SendAsync(QueueDTO queueDto, int appSpecific)
        {
            try
            {
                foreach (var settingsItem in _textDispatcherConfigurationSection.TextAlertDispatcherSettingsItems)
                {
                    var textDispatcherSettingsElement = (TextAlertDispatcherSettingsElement)settingsItem;

                    if (textDispatcherSettingsElement != null && textDispatcherSettingsElement.Enabled == 1)
                    {
                        var messageCategory = (MessageCategory)appSpecific;

                        var serviceHeader = new ServiceHeader();

                        switch (messageCategory)
                        {
                            case MessageCategory.TextAlert:

                                #region SMS Alert

                                var smsAlert = await _channelService.FindTextAlertAsync(queueDto.RecordId, serviceHeader);

                                if (smsAlert != null)
                                    switch ((DLRStatus)smsAlert.TextMessageDLRStatus)
                                    {
                                        case DLRStatus.UnKnown:
                                        case DLRStatus.Pending:

                                            if (!string.IsNullOrWhiteSpace(smsAlert.TextMessageRecipient) &&
                                                !string.IsNullOrWhiteSpace(smsAlert.UnmaskedTextMessageBody))
                                            {
                                                var msisdn = smsAlert.TextMessageRecipient.Trim();

                                                var textMessage = smsAlert.UnmaskedTextMessageBody.Trim();

                                                var requestInfobip = new InfobipMessage
                                                {
                                                    from = "",
                                                    to = msisdn,
                                                    text = textMessage
                                                };

                                                var jsonConvert = JsonConvert.SerializeObject(requestInfobip);

                                                var webResponse = Post(Encoding.Default.GetBytes($"{jsonConvert}"), "", "", "");

                                                if (webResponse.StatusCode == HttpStatusCode.OK)
                                                    using (var responseStream = webResponse.GetResponseStream())
                                                    {
                                                        using (var reader = new StreamReader(responseStream))
                                                        {
                                                            var json = reader.ReadToEnd();

                                                            _logger.Debug("{0}****{0}json-in {1}{0}***{0}",
                                                                Environment.NewLine, json);

                                                            var response =
                                                                JsonConvert.DeserializeObject<InfobipResponse>(json);

                                                            smsAlert.TextMessageSendRetry = 1;

                                                            smsAlert.TextMessageDLRStatus = (int)DLRStatus.Delivered;

                                                            smsAlert.TextMessageReference =
                                                                $"{response.messages[0].status.description}- {response.messages[0].messageId}";

                                                            await _channelService.UpdateTextAlertAsync(smsAlert,
                                                                serviceHeader);
                                                        }
                                                    }
                                            }

                                            break;
                                        default:
                                            break;
                                    }

                                #endregion

                                break;
                        }
                    }
                }
            }

            catch (Exception exception)
            {
                _logger.LogError("{0}->InfoBipTextAlertAlertDispatcher...", exception, _textDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueuePath);
            }
        }

        #region Helpers

        private HttpWebResponse Post(byte[] buffer, string url, string username, string password)
        {
            var webRequest = WebRequest.Create(url) as HttpWebRequest;

            if (webRequest == null) throw new NullReferenceException("request is not a http request");

            webRequest.Method = "POST";

            webRequest.ContentType = "application/json";

            webRequest.Headers.Add("Authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");

            using (var requestStream = webRequest.GetRequestStream())
            {
                requestStream.Write(buffer, 0, buffer.Length);

                requestStream.Close();
            }

            return webRequest.GetResponse() as HttpWebResponse;
        }

        #endregion
    }

    public class InfobipMessage
    {
        public string from { get; set; }

        public string to { get; set; }

        public string text { get; set; }
    }

    public class InfobipResponse
    {
        public List<messages> messages;
    }

    public class messages
    {
        public string to { get; set; }

        public status status { get; set; }

        public int smsCount { get; set; }

        public string messageId { get; set; }
    }

    public class status
    {
        public string groupId { get; set; }

        public string groupName { get; set; }

        public string id { get; set; }

        public string name { get; set; }

        public string description { get; set; }
    }
}
