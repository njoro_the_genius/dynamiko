﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Models;
using Presentation.Infrastructure.Services;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;

namespace TextAlertDispatcher.DispatcherHandlers
{
    public class FakeTextAlertDispatcher : ITextAlertDispatcher
    {
        private readonly ITextAlertDispatcher _next;

        public FakeTextAlertDispatcher(
            IChannelService channelService,
            IMessageQueueService messageQueueService,
            TextAlertDispatcherConfigurationSection textDispatcherConfigurationSection,
            ITextAlertDispatcher next)
        {
            _next = next;
        }

        public Task HandleRequest(QueueDTO queueDTO, int appSpecific)
        {
            return _next.HandleRequest(queueDTO, appSpecific);
        }
    }
}
