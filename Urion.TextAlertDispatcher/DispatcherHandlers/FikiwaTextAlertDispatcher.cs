﻿using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Newtonsoft.Json;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;
using Utility = TextAlertDispatcher.Configuration.Utility;

namespace TextAlertDispatcher.DispatcherHandlers
{
    public class FikiwaTextAlertDispatcher : ITextAlertDispatcher
    {
        private readonly IChannelService _channelService;

        private readonly ITextAlertDispatcher _next;

        private readonly TextAlertDispatcherConfigurationSection _TextAlertDispatcherConfigurationSection;

        private readonly LimitedPool<HttpClient> _httpClientPool;

        private readonly ILogger _logger;

        public FikiwaTextAlertDispatcher(IChannelService channelService, TextAlertDispatcherConfigurationSection TextAlertDispatcherConfigurationSection, ILogger logger, ITextAlertDispatcher next)
        {
            _channelService = channelService;

            _TextAlertDispatcherConfigurationSection = TextAlertDispatcherConfigurationSection;

            _logger = logger;

            _httpClientPool = new LimitedPool<HttpClient>(() => new HttpClient(), client => client.Dispose());

            _next = next;
        }

        public Task HandleRequest(QueueDTO queueDto, int appSpecific)
        {
            if (queueDto.ServiceProvider == (int)SMSServiceProvider.Fikiwa)
                return SendAsync(queueDto, appSpecific);

            return _next.HandleRequest(queueDto, appSpecific);
        }

        private async Task SendAsync(QueueDTO queueDto, int appSpecific)
        {
            try
            {
                foreach (var settingsItem in _TextAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems)
                {
                    var TextAlertDispatcherSettingsElement = (TextAlertDispatcherSettingsElement)settingsItem;

                    if (TextAlertDispatcherSettingsElement != null && TextAlertDispatcherSettingsElement.Enabled == 1)
                    {
                        var messageCategory = (MessageCategory)appSpecific;

                        var serviceHeader = new ServiceHeader();

                        switch (messageCategory)
                        {
                            case MessageCategory.TextAlert:

                                #region SMSAlert

                                var smsAlert = await _channelService.FindTextAlertAsync(queueDto.RecordId, serviceHeader);

                                if (smsAlert != null)
                                    switch ((DLRStatus)smsAlert.TextMessageDLRStatus)
                                    {
                                        case DLRStatus.UnKnown:
                                        case DLRStatus.Pending:

                                            if (!string.IsNullOrWhiteSpace(smsAlert.TextMessageRecipient) && !string.IsNullOrWhiteSpace(smsAlert.UnmaskedTextMessageBody))
                                            {
                                                var msisdn = smsAlert.TextMessageRecipient.Trim();

                                                if (!msisdn.Contains("+"))
                                                {
                                                    msisdn = "+" + msisdn;
                                                }

                                                var textMessage = smsAlert.UnmaskedTextMessageBody.Trim();

                                                var uniqueTransactionIdentifier = StringExtensions.TransactionReferenceNumber();

                                                var requestHeader = new RequestHeader
                                                {
                                                    transactionType = 1,
                                                    uniqueTransactionIdentifier = uniqueTransactionIdentifier,
                                                    callbackURL = TextAlertDispatcherSettingsElement.CallBackUrl,
                                                    messages = new List<Message>
                                                    {
                                                        new Message
                                                        {
                                                            from = smsAlert.PartnerSMSGatewaySenderId,
                                                            to = msisdn,
                                                            Text = textMessage,
                                                            securityCritical = smsAlert.TextMessageSecurityCritical
                                                        }
                                                    }
                                                };

                                                var jsonConvert = JsonConvert.SerializeObject(requestHeader);

                                                var post = await Utility.PostAsync(_httpClientPool, jsonConvert, TextAlertDispatcherSettingsElement.FikiwaUrl, smsAlert.PartnerSMSGatewayUsername.Decrypt(), smsAlert.PartnerSMSGatewayPassword.Decrypt());

                                                if (post.Item1 == HttpStatusCode.OK)
                                                {
                                                    smsAlert.TextMessageDLRStatus = (int)DLRStatus.Submitted;

                                                    smsAlert.TextMessageSendRetry = 1;

                                                    smsAlert.TextMessageReference = uniqueTransactionIdentifier;

                                                    await _channelService.UpdateTextAlertAsync(smsAlert, serviceHeader);
                                                }
                                            }

                                            break;
                                    }

                                #endregion

                                break;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.LogError("{0}->FikiwaTextAlertDispatcher...", exception, _TextAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueuePath);
            }
        }
    }

    public class RequestHeader
    {
        public int transactionType { get; set; }

        public string uniqueTransactionIdentifier { get; set; }

        public string callbackURL { get; set; }

        public List<Message> messages { get; set; }
    }

    public class Message
    {
        public string from { get; set; }

        public string to { get; set; }

        public string Text { get; set; }

        public bool securityCritical { get; set; }
    }
}
