﻿using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;

namespace TextAlertDispatcher.DispatcherHandlers
{
    public class AfricasTalkingTextAlertDispatcher : ITextAlertDispatcher
    {
        private readonly IChannelService _channelService;

        private readonly ITextAlertDispatcher _next;

        private readonly TextAlertDispatcherConfigurationSection _textAlertDispatcherConfigurationSection;

        private readonly ILogger _logger;

        public AfricasTalkingTextAlertDispatcher(
            IChannelService channelService,
            TextAlertDispatcherConfigurationSection textAlertDispatcherConfigurationSection,
            ITextAlertDispatcher next, ILogger logger)
        {
            _channelService = channelService;

            _textAlertDispatcherConfigurationSection = textAlertDispatcherConfigurationSection;

            _next = next;

            _logger = logger;
        }

        public Task HandleRequest(QueueDTO queueDto, int appSpecific)
        {
            if (queueDto.ServiceProvider == (int)SMSServiceProvider.AfricasTalking)
                return SendAsync(queueDto, appSpecific);

            return _next.HandleRequest(queueDto, appSpecific);
        }

        private async Task SendAsync(QueueDTO queueDto, int appSpecific)
        {
            try
            {
                foreach (var settingsItem in _textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems)
                {
                    var textDispatcherSettingsElement = (TextAlertDispatcherSettingsElement)settingsItem;

                    if (textDispatcherSettingsElement != null && textDispatcherSettingsElement.Enabled == 1)
                    {
                        var messageCategory = (MessageCategory)appSpecific;

                        var serviceHeader = new ServiceHeader { ApplicationDomainName = queueDto.AppDomainName };

                        switch (messageCategory)
                        {
                            case MessageCategory.TextAlert:

                                #region Africa's Talking SMS Alert

                                var smsAlert = await _channelService.FindTextAlertAsync(queueDto.RecordId, serviceHeader);

                                if (smsAlert != null)
                                    switch ((DLRStatus)smsAlert.TextMessageDLRStatus)
                                    {
                                        case DLRStatus.UnKnown:
                                        case DLRStatus.Pending:

                                            if (!string.IsNullOrWhiteSpace(smsAlert.TextMessageRecipient) && !string.IsNullOrWhiteSpace(smsAlert.UnmaskedTextMessageBody))
                                            {
                                                var msisdn = smsAlert.TextMessageRecipient.Trim();

                                                var textMessage = smsAlert.UnmaskedTextMessageBody.Trim();

                                                var gateway = new AfricasTalkingGateway(smsAlert.PartnerSMSGatewayUsername, smsAlert.PartnerSMSGatewayPassword);

                                                dynamic results = null;

                                                if (string.IsNullOrWhiteSpace(queueDto.BulkTextSenderId))
                                                    results = gateway.sendMessage(msisdn, textMessage,
                                                        queueDto.BulkTextUrl);
                                                else
                                                    results = gateway.sendMessage(msisdn, textMessage, smsAlert.PartnerSMSGatewayUrl, smsAlert.PartnerSMSGatewaySenderId);

                                                foreach (var result in results)
                                                {
                                                    var status = result["status"].ToString();

                                                    if (status.ToLower() == "Success".ToLower())
                                                        smsAlert.TextMessageDLRStatus = (int)DLRStatus.Delivered;
                                                    else smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;

                                                    smsAlert.TextMessageReference = status;

                                                    await _channelService.UpdateTextAlertAsync(smsAlert, serviceHeader);

                                                    break;
                                                }
                                            }

                                            break;
                                    }

                                #endregion

                                break;
                        }
                    }
                }
            }

            catch (Exception exception)
            {
                _logger.LogError("{0}->AfricasTalkingTextAlertDispatcher...", exception, _textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueuePath);
            }
        }
    }
}
