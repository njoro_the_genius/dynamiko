﻿using Infrastructure.Crosscutting.Framework.Models;
using System.Threading.Tasks;

namespace TextAlertDispatcher.DispatcherHandlers
{
    public interface ITextAlertDispatcher
    {
        Task HandleRequest(QueueDTO queueDTO, int appSpecific);
    }
}
