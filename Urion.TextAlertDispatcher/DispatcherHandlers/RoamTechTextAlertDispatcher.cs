﻿using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Newtonsoft.Json;
using Presentation.Infrastructure.Services;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;

namespace TextAlertDispatcher.DispatcherHandlers
{
    public class RoamTechTextAlertDispatcher : ITextAlertDispatcher
    {
        private readonly IChannelService _channelService;

        private readonly ITextAlertDispatcher _next;

        private readonly TextAlertDispatcherConfigurationSection _textAlertDispatcherConfigurationSection;

        private readonly ILogger _logger;

        public RoamTechTextAlertDispatcher(
            IChannelService channelService,
            TextAlertDispatcherConfigurationSection textAlertDispatcherConfigurationSection,
            ITextAlertDispatcher next, ILogger logger)
        {
            _channelService = channelService;

            _textAlertDispatcherConfigurationSection = textAlertDispatcherConfigurationSection;

            _next = next;

            _logger = logger;
        }

        public Task HandleRequest(QueueDTO queueDto, int appSpecific)
        {
            if (queueDto.ServiceProvider == (int)SMSServiceProvider.RoamTech)
                return SendAsync(queueDto, appSpecific);

            return _next.HandleRequest(queueDto, appSpecific);
        }

        private async Task SendAsync(QueueDTO queueDto, int appSpecific)
        {
            try
            {
                foreach (var settingsItem in _textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems)
                {
                    var textDispatcherSettingsElement = (TextAlertDispatcherSettingsElement)settingsItem;

                    if (textDispatcherSettingsElement != null && textDispatcherSettingsElement.Enabled == 1)
                    {
                        var messageCategory = (MessageCategory)appSpecific;

                        var serviceHeader = new ServiceHeader { ApplicationDomainName = queueDto.AppDomainName };

                        switch (messageCategory)
                        {
                            case MessageCategory.TextAlert:

                                #region SMS Alert

                                var smsAlert = await _channelService.FindTextAlertAsync(queueDto.RecordId, serviceHeader);

                                if (smsAlert != null)
                                    switch ((DLRStatus)smsAlert.TextMessageDLRStatus)
                                    {
                                        case DLRStatus.UnKnown:
                                        case DLRStatus.Pending:

                                            if (!string.IsNullOrWhiteSpace(smsAlert.TextMessageRecipient) && !string.IsNullOrWhiteSpace(smsAlert.UnmaskedTextMessageBody))
                                            {
                                                var msisdn = smsAlert.TextMessageRecipient.Trim();

                                                var textMessage = smsAlert.UnmaskedTextMessageBody.Trim();

                                                var smsBodyHash = string.Empty;

                                                var encoding = Encoding.UTF8;

                                                var keyByte = encoding.GetBytes(queueDto.BulkTextPassword);

                                                using (var hmacsha256 = new HMACSHA256(keyByte))
                                                {
                                                    hmacsha256.ComputeHash(encoding.GetBytes(textMessage));

                                                    smsBodyHash = ByteToString(hmacsha256.Hash);
                                                }

                                                var request = new Request
                                                {
                                                    api_uid = smsBodyHash,
                                                    cust_acc = smsAlert.PartnerSMSGatewayUsername,
                                                    msg = textMessage,
                                                    bulk_msisdn = new[] { msisdn },
                                                    source = smsAlert.PartnerSMSGatewaySenderId,
                                                };

                                                var jsonString = JsonConvert.SerializeObject(request);

                                                var jsonData = Encoding.UTF8.GetBytes(jsonString);

                                                var response = Post(jsonData);

                                                //0: Accepted for delivery

                                                if (response != null && !string.IsNullOrWhiteSpace(response.status))
                                                {
                                                    smsAlert.TextMessageDLRStatus = !string.IsNullOrWhiteSpace(response.status) && response.status.Equals("1") ? (byte)DLRStatus.Delivered : (byte)DLRStatus.Failed;
                                                }
                                                else smsAlert.TextMessageDLRStatus = (int)DLRStatus.Failed;

                                                smsAlert.TextMessageReference = response?.rawresponse;

                                                await _channelService.UpdateTextAlertAsync(smsAlert, serviceHeader);
                                            }

                                            break;
                                        default:
                                            break;
                                    }

                                #endregion

                                break;
                        }
                    }
                }
            }

            catch (Exception exception)
            {
                _logger.LogError("{0}->RoamTechTextAlertDispatcher...", exception, _textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueuePath);
            }
        }

        private static string ByteToString(byte[] bufferBytes)
        {
            var sbinary = "";
            foreach (var t in bufferBytes)
                sbinary += t.ToString("x2");
            return sbinary;
        }

        private static Response Post(byte[] buffer)
        {
            var webRequest = WebRequest.Create("http://107.20.199.106/api/v3/sendsms/json") as HttpWebRequest;

            if (webRequest == null) throw new NullReferenceException("request is not a http request");

            webRequest.Method = "POST";

            webRequest.ContentType = "application/json";

            webRequest.KeepAlive = true;

            using (var requestStream = webRequest.GetRequestStream())
            {
                requestStream.Write(buffer, 0, buffer.Length);

                requestStream.Close();
            }

            var webResponse = webRequest.GetResponse() as HttpWebResponse;

            if (webResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var receiveStream = webResponse.GetResponseStream())
                {
                    using (var reader = new StreamReader(receiveStream, new UTF8Encoding()))
                    {
                        var response = reader.ReadToEnd();

                        var deserialize = JsonConvert.DeserializeObject<Response>(response);

                        return deserialize;
                    }
                }
            }

            return null;
        }
    }

    public class Request
    {
        /// <summary>
        ///     The client's customer account assigned to them by SMSLEO
        /// </summary>
        public string cust_acc { get; set; }

        /// <summary>
        ///     A digest created by applying a hashing function to the message using the api_key (issued to the client by SMSLeo)
        ///     as the key
        /// </summary>
        public string api_uid { get; set; }

        /// <summary>
        ///     Mobile number to which the message is destined
        /// </summary>
        public string[] bulk_msisdn { get; set; }

        /// <summary>
        ///     Sender Name / Shortcode that is sending the message.
        /// </summary>
        public string source { get; set; }

        /// <summary>
        ///     The text content of the message being sent.
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        ///     Set this to 2 when sending Unicode characters in the message.
        /// </summary>
        public string coding { get; set; }

        /// <summary>
        ///     Used to send replies to users that interact with a short code. It is only required for Safaricom shortcode
        ///     replies.
        /// </summary>
        public string link_id { get; set; }

        /// <summary>
        ///     The endpoint (url) to which we shall submit the delivery report. The end point should ideally include a message id
        ///     parameter that identifies the message uniquely from your end.
        /// </summary>
        public string dlr_url { get; set; }
    }

    public class Response
    {
        public string status { get; set; }

        public string desc { get; set; }

        public string rawresponse { get; set; }
    }
}
