﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Threading.Tasks;
using TextAlertDispatcher.Configuration;
using TextAlertDispatcher.DispatcherHandlers;

namespace TextAlertDispatcher.QueueProcessors
{
    public class DispatcherMessageProcessor : MessageProcessor<QueueDTO>
    {
        private readonly ITextAlertDispatcher _textAlertDispatcher;

        private readonly TextAlertDispatcherConfigurationSection _textAlertDispatcherConfigurationSection;

        private readonly ILogger _logger;

        public DispatcherMessageProcessor(IChannelService channelService, IMessageQueueService messageQueueService,
            TextAlertDispatcherConfigurationSection textAlertDispatcherConfigurationSection, ILogger logger)
            : base(textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueuePath, textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueueReceivers)
        {
            _textAlertDispatcher =
                new FakeTextAlertDispatcher(channelService, messageQueueService, textAlertDispatcherConfigurationSection,
                    new OracomTextAlertDispatcher(channelService, textAlertDispatcherConfigurationSection, _logger));


            _textAlertDispatcherConfigurationSection = textAlertDispatcherConfigurationSection;

            _logger = logger;
        }

        protected override void LogError(Exception exception)
        {
            _logger.LogError("{0}->DispatcherMessageProcessor...", exception, _textAlertDispatcherConfigurationSection.TextAlertDispatcherSettingsItems.QueuePath);
        }

        protected override Task Process(QueueDTO queueDto, int appSpecific)
        {
            return _textAlertDispatcher.HandleRequest(queueDto, appSpecific);
        }
    }
}
