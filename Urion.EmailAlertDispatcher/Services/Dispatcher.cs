﻿using Application.MainBoundedContext.Services;
using EmailAlertDispatcher.Configuration;
using EmailAlertDispatcher.QueueProcessors;
using Infrastructure.Crosscutting.Framework.Logging;
using Presentation.Infrastructure.Services;
using Quartz;
using System;
using System.ComponentModel.Composition;
using System.Configuration;

namespace EmailAlertDispatcher.Services
{
    [Export(typeof(IPlugin))]
    public class Dispatcher : IPlugin
    {
        private DispatcherMessageProcessor _messageProcessor;
        private readonly IChannelService _channelService;
        private readonly ISmtpService _smtpService;
        private readonly ILogger _logger;

        [ImportingConstructor]
        public Dispatcher(
            //IChannelService channelService,
            ISmtpService smtpService,
            ILogger logger
            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _channelService = new ChannelService(_logger);
            _smtpService = smtpService ?? throw new ArgumentNullException(nameof(smtpService));
        }

        #region IPlugin

        public Guid Id => new Guid("{B71996D3 -DB37 - 407C - 832F - C5C8FDCAEF64}");

        public string Description => "EMAIL ALERT DISPATCHER";

        public void DoWork(IScheduler scheduler, params string[] args)
        {
            try
            {
                var emailDispatcherConfigSection = (EmailAlertDispatcherConfigurationSection)ConfigurationManager.GetSection("emailAlertDispatcherConfiguration");

                if (emailDispatcherConfigSection != null)
                {
                    _messageProcessor = new DispatcherMessageProcessor(_channelService, _smtpService, emailDispatcherConfigSection, _logger);

                    _messageProcessor.Open();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("{0}->DoWork...", ex, Description);
            }
        }

        public void Exit()
        {
            try
            {
                _messageProcessor?.Close();
            }
            catch (Exception ex)
            {
                _logger.LogInfo("{0}->Exit...", ex, Description);
            }
        }

        #endregion
    }
}
