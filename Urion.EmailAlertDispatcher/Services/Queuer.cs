﻿using EmailAlertDispatcher.Configuration;
using EmailAlertDispatcher.QuartzJobs;
using Infrastructure.Crosscutting.Framework.Logging;
using Presentation.Infrastructure.Services;
using Quartz;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Threading.Tasks;

namespace EmailAlertDispatcher.Services
{
    [Export(typeof(IPlugin))]
    public class Queuer : IPlugin
    {
        private readonly ILogger _logger;

        [ImportingConstructor]
        public Queuer(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        #region Plugin

        public Guid Id => new Guid("{707DB5A9-40BF-4E84-B5BE-650090E65342}");

        public string Description => "EMAIL ALERT QUEUER";

        public void DoWork(IScheduler scheduler, params string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    var emailDispatcherConfigSection = (EmailAlertDispatcherConfigurationSection)ConfigurationManager.GetSection("emailAlertDispatcherConfiguration");

                    if (emailDispatcherConfigSection == null)
                        throw new ArgumentNullException(nameof(emailDispatcherConfigSection));

                    // Define the Job to be scheduled
                    var jobDetail = JobBuilder.Create<QueuingJob>()
                        .WithIdentity("EmailAlertQueueingJob", "Urion")
                        .RequestRecovery()
                        .Build();

                    // Associate a trigger with the Job
                    var trigger = (ICronTrigger)TriggerBuilder.Create()
                        .WithIdentity("EmailAlertQueueingJob", "Urion")
                        .WithCronSchedule(emailDispatcherConfigSection.EmailAlertDispatcherSettingItems.QueueingJobCronExpression)
                        .StartAt(DateTime.UtcNow)
                        .WithPriority(1)
                        .Build();

                    // Validate that the job doesn't already exists
                    if (await scheduler.CheckExists(new JobKey("EmailAlertQueueingJob", "Urion")))
                        await scheduler.DeleteJob(new JobKey("EmailAlertQueueingJob", "Urion"));

                    var schedule = await scheduler.ScheduleJob(jobDetail, trigger);

                    _logger.Debug("Job '{0}' scheduled for '{1}'", "EmailAlertQueueingJob", schedule.ToString("r"));
                }
                catch (Exception ex)
                {
                    _logger.LogError("{0}->DoWork...", ex, Description);
                }
            });
        }

        public void Exit()
        {

        }

        #endregion
    }
}
