﻿using Infrastructure.Crosscutting.Framework.Models;
using System.Threading.Tasks;

namespace EmailAlertDispatcher.DispatcherHandlers
{
    public interface IEmailAlertDispatcher
    {
        Task HandleRequestAsync(QueueDTO queueDTO, int appSpecific);
    }
}
