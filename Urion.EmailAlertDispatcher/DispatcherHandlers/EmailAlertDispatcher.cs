﻿using Application.MainBoundedContext.Services;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Net.Mail;
using System.Threading.Tasks;
using EmailAlertDispatcher.Configuration;

namespace EmailAlertDispatcher.DispatcherHandlers
{
    public class EmailAlertDispatcher : IEmailAlertDispatcher
    {
        private readonly IChannelService _channelService;
        private readonly ISmtpService _smtpService;
        private readonly EmailAlertDispatcherConfigurationSection _emailAlertDispatcherConfigSection;

        public EmailAlertDispatcher(
            IChannelService channelService,
            ISmtpService smtpService,
            EmailAlertDispatcherConfigurationSection emailAlertDispatcherConfigSection)
        {
            _channelService = channelService;
            _smtpService = smtpService;
            _emailAlertDispatcherConfigSection = emailAlertDispatcherConfigSection;
        }

        public Task HandleRequestAsync(QueueDTO queueDTO, int @int)
        {
            return SendAsync(queueDTO, @int);
        }

        public async Task SendAsync(QueueDTO queueDTO, int appSpecific)
        {
            foreach (var settingsItem in _emailAlertDispatcherConfigSection.EmailAlertDispatcherSettingItems)
            {
                var emailDispatcherSettingsElement = (EmailAlertDispatcherSettingsElement)settingsItem;

                if (emailDispatcherSettingsElement != null && emailDispatcherSettingsElement.Enabled == 1)
                {
                    var messageCategory = (MessageCategory)appSpecific;

                    var serviceHeader = new ServiceHeader { ApplicationDomainName = queueDTO.AppDomainName };

                    switch (messageCategory)
                    {
                        case MessageCategory.EmailAlert:

                            #region EmailAlert

                            var emailMessageDTO = await _channelService.FindEmailAlertWithIdAsync(queueDTO.RecordId, serviceHeader);

                            if (emailMessageDTO == null)
                                continue;

                            if (emailMessageDTO.MailMessageDLRStatus != (byte)DLRStatus.Delivered)
                            {
                                var fromAddress = new MailAddress(emailDispatcherSettingsElement.MailMessageFrom, "");

                                var toAddress = new MailAddress(emailMessageDTO.MailMessageTo, "");

                                string fromPassword = emailDispatcherSettingsElement.SmtpPassword;

                                string subject = emailMessageDTO.MailMessageSubject;

                                string body = emailMessageDTO.MailMessageBody;

                                using (var message = new MailMessage(fromAddress, toAddress)
                                {
                                    Subject = subject,

                                    Body = body,

                                    IsBodyHtml = emailMessageDTO.MailMessageIsBodyHtml

                                })
                                {
                                    if (emailMessageDTO.Id != null && emailMessageDTO.Id != Guid.Empty)
                                    {
                                        _smtpService.SendEmail(emailDispatcherSettingsElement.SmtpHost, emailDispatcherSettingsElement.SmtpPort, emailDispatcherSettingsElement.SmtpEnableSsl == 1, emailDispatcherSettingsElement.SmtpUsername, emailDispatcherSettingsElement.SmtpPassword, message);
                                       
                                        emailMessageDTO.MailMessageDLRStatus = (byte)DLRStatus.Delivered;

                                        await _channelService.UpdateEmailAlertAsync(emailMessageDTO, serviceHeader);
                                    }
                                }
                            }

                            #endregion

                            break;
                    }
                }
            }
        }
    }
}
