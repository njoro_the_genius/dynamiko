﻿using System.Configuration;

namespace EmailAlertDispatcher.Configuration
{
    public class EmailAlertDispatcherConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("emailAlertDispatcherSettings")]
        public EmailAlertDispatcherSettingsCollection EmailAlertDispatcherSettingItems => ((EmailAlertDispatcherSettingsCollection)base["emailAlertDispatcherSettings"]);
    }
}
