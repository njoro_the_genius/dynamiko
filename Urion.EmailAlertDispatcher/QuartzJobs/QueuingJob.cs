﻿using Application.MainBoundedContext.Services;
using EmailAlertDispatcher.Configuration;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using System.Messaging;

namespace EmailAlertDispatcher.QuartzJobs
{
    public class QueuingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly IChannelService _channelService;
        private readonly ILogger _logger;

        public QueuingJob(
            IMessageQueueService messageQueueService,
            IChannelService channelService,
            ILogger logger)
        {
            _messageQueueService = messageQueueService ?? throw new ArgumentNullException(nameof(messageQueueService));
            _channelService = channelService ?? throw new ArgumentNullException(nameof(channelService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                _logger.Debug("{0}****{0}Job {1} fired @ {2} next scheduled for {3}{0}***{0}", Environment.NewLine, context.JobDetail.Key, context.FireTimeUtc.ToString("r"), context.NextFireTimeUtc.Value.ToString("r"));

                var emailDispatcherConfigSection = (EmailAlertDispatcherConfigurationSection)ConfigurationManager.GetSection("emailAlertDispatcherConfiguration");

                if (emailDispatcherConfigSection != null)
                {
                    foreach (var settingsItem in emailDispatcherConfigSection.EmailAlertDispatcherSettingItems)
                    {
                        var emailDispatcherSettingsElement = (EmailAlertDispatcherSettingsElement)settingsItem;

                        if (emailDispatcherSettingsElement != null && emailDispatcherSettingsElement.Enabled == 1)
                        {
                            var serviceHeader = new ServiceHeader { ApplicationDomainName = emailDispatcherSettingsElement.UniqueId };

                            #region Queable Email Message

                            var emailMessageWithStatusPending = await _channelService.FindEmailAlertsWithDLRStatusAsync(null, (int)DLRStatus.Pending, emailDispatcherSettingsElement.QueuePageIndex, emailDispatcherSettingsElement.QueuePageSize, serviceHeader);

                            if (emailMessageWithStatusPending != null && emailMessageWithStatusPending.PageCollection.Any())
                            {
                                foreach (var item in emailMessageWithStatusPending.PageCollection)
                                {
                                    var queueDTO = new QueueDTO
                                    {
                                        RecordId = item.Id,
                                        AppDomainName = serviceHeader.ApplicationDomainName,
                                    };

                                    if (item.Id != null && item.Id != Guid.Empty)
                                    {
                                        _messageQueueService.Send(emailDispatcherConfigSection.EmailAlertDispatcherSettingItems.QueuePath, queueDTO, MessageCategory.EmailAlert, (MessagePriority.Highest), emailDispatcherSettingsElement.TimeToBeReceived);
                                    }
                                }
                            }

                            #endregion

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Urion.EmailAlertDispatcher_QueueingJob.Execute", ex);
            }
        }
    }
}
