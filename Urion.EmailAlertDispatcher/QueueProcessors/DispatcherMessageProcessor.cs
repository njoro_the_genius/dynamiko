﻿using Application.MainBoundedContext.Services;
using EmailAlertDispatcher.Configuration;
using EmailAlertDispatcher.DispatcherHandlers;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Services;
using System;
using System.Threading.Tasks;

namespace EmailAlertDispatcher.QueueProcessors
{
    public class DispatcherMessageProcessor : MessageProcessor<QueueDTO>
    {
        private readonly IEmailAlertDispatcher _emailAlertDispatcher;
        private readonly EmailAlertDispatcherConfigurationSection _emailAlertDispatcherConfigSection;

        public DispatcherMessageProcessor(
            IChannelService channelService,
            ISmtpService smtpService,
            EmailAlertDispatcherConfigurationSection emailAlertDispatcherConfigSection,
            ILogger logger) : base(emailAlertDispatcherConfigSection.EmailAlertDispatcherSettingItems.QueuePath, emailAlertDispatcherConfigSection.EmailAlertDispatcherSettingItems.QueueReceivers)
        {
            _emailAlertDispatcherConfigSection = emailAlertDispatcherConfigSection;
            _emailAlertDispatcher = new DispatcherHandlers.EmailAlertDispatcher(channelService, smtpService, emailAlertDispatcherConfigSection);
        }

        protected override void LogError(Exception exception)
        {
            LoggerFactory.CreateLog().LogError("{0}->DispatcherMessageProcessor...", exception, _emailAlertDispatcherConfigSection.EmailAlertDispatcherSettingItems.QueuePath);
        }

        protected override Task Process(QueueDTO queueDTO, int appSpecific)
        {
            return _emailAlertDispatcher.HandleRequestAsync(queueDTO, appSpecific);
        }
    }
}
