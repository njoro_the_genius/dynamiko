﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Application.MainBoundedContext.Services;
using Domain.MainBoundedContext.MessagingModule.Aggregates.EmailAlertAgg;
using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;

namespace Application.MainBoundedContext.MessagingModule.Services
{
    public class EmailAlertAppService : IEmailAlertAppService
    {
        public readonly IDbContextScopeFactory _dbContextScopeFactory;

        public readonly IRepository<EmailAlert> _emailAlertRepository;

        public readonly IBrokerService _brokerService;

        public EmailAlertAppService(
            IDbContextScopeFactory dbContextScopeFactory,
            IRepository<EmailAlert> emailAlertRepository,
            IBrokerService brokerService)
        {
            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));

            _emailAlertRepository = emailAlertRepository ?? throw new ArgumentNullException(nameof(emailAlertRepository));

            _brokerService = brokerService ?? throw new ArgumentNullException(nameof(brokerService));
        }

        public async Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader)
        {
            if (emailAlertDTO != null)
            {
                var emailAlertBindingModel = emailAlertDTO.ProjectedAs<EmailAlertBindingModel>();

                emailAlertBindingModel.ValidateAll();

                if (emailAlertBindingModel.HasErrors)
                    throw new InvalidOperationException(string.Join(Environment.NewLine, emailAlertBindingModel.ErrorMessages));

                var dlrStatus = DLRStatus.UnKnown;

                switch ((MessageOrigin)emailAlertDTO.MailMessageOrigin)
                {
                    case MessageOrigin.Within:
                    case MessageOrigin.Without:
                        dlrStatus = DLRStatus.Pending;
                        break;
                    case MessageOrigin.Other:
                        dlrStatus = DLRStatus.NotApplicable;
                        break;
                    default:
                        break;
                }

                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var mailMessage = new MailMessage(emailAlertDTO.MailMessageFrom, emailAlertDTO.MailMessageTo, emailAlertDTO.MailMessageCC, emailAlertDTO.MailMessageSubject, emailAlertDTO.MailMessageBody, emailAlertDTO.MailMessageIsBodyHtml, (int)dlrStatus, emailAlertDTO.MailMessageOrigin, emailAlertDTO.MailMessagePriority, emailAlertDTO.MailMessageSendRetry, emailAlertDTO.MailMessageSecurityCritical, emailAlertDTO.MailMessageAttachmentFilePath);

                    var EmailAlert = EmailAlertFactory.CreateEmailAlert(emailAlertDTO.CustomerId, mailMessage);

                    _emailAlertRepository.Add(EmailAlert, serviceHeader);

                    if (await dbContextScope.SaveChangesAsync(serviceHeader) > 0)
                    {
                        var projection = EmailAlert.ProjectedAs<EmailAlertDTO>();

                       // _brokerService.ProcessEmailAlertsAsync(DMLCommand.Insert, serviceHeader, projection);

                        return projection;
                    }
                }
            }

            return null;
        }

        public bool AddEmailAlerts(List<EmailAlertDTO> emailAlertDTOs, ServiceHeader serviceHeader)
        {
            var result = default(bool);

            if (emailAlertDTOs != null && emailAlertDTOs.Any())
            {
                emailAlertDTOs.ForEach(async item =>
                {
                    await AddEmailAlertAsync(item, serviceHeader);
                });

                result = true;
            }

            return result;
        }

        public async Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsAsync(Guid? customerId, int pageIndex, int pageSize, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                ISpecification<EmailAlert> specification = customerId == null ? EmailAlertSpecifications.DefaultSpec() : EmailAlertSpecifications.EmailAlertWithCustomer(customerId);

                var sortFields = new List<string> { "CreatedDate" };

                return await _emailAlertRepository.AllMatchingPagedAsync<EmailAlertDTO>(specification, pageIndex, pageSize, sortFields, true, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithDLRStatusAsync(Guid? customerId, int dlrStatus, int pageIndex, int pageSize, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                ISpecification<EmailAlert> specification = customerId == null ? EmailAlertSpecifications.EmailAlertWithDLRStatus(dlrStatus) : EmailAlertSpecifications.EmailAlertWithDLRStatus(customerId, dlrStatus);

                var sortFields = new List<string> { "CreatedDate" };

                return await _emailAlertRepository.AllMatchingPagedAsync<EmailAlertDTO>(specification, pageIndex, pageSize, sortFields, true, serviceHeader);
            }
        }

        public async Task<EmailAlertDTO> FindEmailAlertWithIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return await _emailAlertRepository.GetAsync<EmailAlertDTO>(id, serviceHeader);
            }
        }
        public async Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(Guid? customerId, string searchText, int pageIndex, int pageSize, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                ISpecification<EmailAlert> specification = customerId == Guid.Empty ? EmailAlertSpecifications.EmailAlertsWithRecipientOrSubjectOrBody(searchText) : EmailAlertSpecifications.EmailAlertsWithRecipientOrSubjectOrBody(customerId, searchText);

                var sortFields = new List<string> { "CreatedDate" };

                return await _emailAlertRepository.AllMatchingPagedAsync<EmailAlertDTO>(specification, pageIndex, pageSize, sortFields, false, serviceHeader);
            }
        }

        public async Task<bool> UpdateEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader)
        {
            var result = default(bool);

            if (emailAlertDTO == null || emailAlertDTO.Id == Guid.Empty)
                return result;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = await _emailAlertRepository.GetAsync(emailAlertDTO.Id, serviceHeader);

                if (persisted != null)
                {
                    var mailMessage = new MailMessage(emailAlertDTO.MailMessageFrom, emailAlertDTO.MailMessageTo, emailAlertDTO.MailMessageCC, emailAlertDTO.MailMessageSubject, emailAlertDTO.MailMessageBody, emailAlertDTO.MailMessageIsBodyHtml, emailAlertDTO.MailMessageDLRStatus, emailAlertDTO.MailMessageOrigin, emailAlertDTO.MailMessagePriority, emailAlertDTO.MailMessageSendRetry, emailAlertDTO.MailMessageSecurityCritical, emailAlertDTO.MailMessageAttachmentFilePath);

                    var current = EmailAlertFactory.CreateEmailAlert(emailAlertDTO.CustomerId, mailMessage);

                    _emailAlertRepository.Merge(persisted, current, serviceHeader);

                    result = await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;
                }
            }

            return result;
        }
    }
}
