﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Application.MainBoundedContext.MessagingModule.Services
{
    public interface ITextAlertAppService
    {
        Task<TextAlertDTO> AddNewTextAlertAsync(TextAlertDTO textAlertDto, ServiceHeader serviceHeader);

        bool AddNewTextAlerts(List<TextAlertDTO> textAlertDto, ServiceHeader serviceHeader);

        Task<bool> UpdateTextAlertAsync(TextAlertDTO textAlertDTO, ServiceHeader serviceHeader);

        Task<List<TextAlertDTO>> FindTextAlertsAsync(ServiceHeader serviceHeader);

        Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAsync(int dlrStatus, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsAsync(int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsAsync(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsAsync(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAndOriginAsync(int dlrStatus, int origin, ServiceHeader serviceHeader);

        Task<TextAlertDTO> FindTextAlertAsync(Guid textAlertId, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsFilterInPageAsync(DateTime startDate, DateTime endDate, Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByRecipientMobileNumberFilterInPageAsync(string mobileNumber, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

    }
}
