﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Application.MainBoundedContext.MessagingModule.Services
{
    public interface IEmailAlertAppService
    {
        Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO EmailAlertDTO, ServiceHeader serviceHeader);

        bool AddEmailAlerts(List<EmailAlertDTO> EmailAlertDTOs, ServiceHeader serviceHeader);

        Task<bool> UpdateEmailAlertAsync(EmailAlertDTO EmailAlertDTO, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsAsync(Guid? customerId, int pageIndex, int pageSize, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithDLRStatusAsync(Guid? customerId, int dlrStatus, int pageIndex, int pageSize, ServiceHeader serviceHeader);

        Task<EmailAlertDTO> FindEmailAlertWithIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(Guid? customerId, string searchText, int pageIndex, int pageSize, ServiceHeader serviceHeader);
    }
}
