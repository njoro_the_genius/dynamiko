﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.MessagingModule;
using Application.MainBoundedContext.Services;
using Domain.MainBoundedContext.MessagingModule.Aggregates.TextAlertAgg;
using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;

namespace Application.MainBoundedContext.MessagingModule.Services
{
    public class TextAlertAppService : ITextAlertAppService
    {
        private readonly IRepository<TextAlert> _textAlertRepository;

        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public TextAlertAppService(IDbContextScopeFactory dbContextScopeFactory, IRepository<TextAlert> textAlertRepository
           )
        {
            if (dbContextScopeFactory == null)
                throw new ArgumentNullException(nameof(dbContextScopeFactory));

            if (textAlertRepository == null)
                throw new ArgumentNullException(nameof(textAlertRepository));

            _textAlertRepository = textAlertRepository;

            _dbContextScopeFactory = dbContextScopeFactory;
        }



        public async Task<List<TextAlertDTO>> FindTextAlertsAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return await _textAlertRepository.GetAllAsync<TextAlertDTO>(serviceHeader);
            }
        }

        public async Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAsync(int dlrStatus, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                Specification<TextAlert> filter = TextAlertSpecification.TextAlertWithDlrStatus(dlrStatus);

                ISpecification<TextAlert> spec = filter;

                IEnumerable<TextAlert> textAlerts = await _textAlertRepository.AllMatchingAsync(spec, serviceHeader);

                if (textAlerts != null && textAlerts.Any())
                {
                    return textAlerts.ProjectedAsCollection<TextAlertDTO>();
                }
                else return null;
            }
        }

        public async  Task<TextAlertDTO> FindTextAlertAsync(Guid textAlertId, ServiceHeader serviceHeader)
        {
            if (textAlertId != Guid.Empty)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var textAlert = await _textAlertRepository.GetAsync(textAlertId, serviceHeader);

                    return textAlert != null ? textAlert.ProjectedAs<TextAlertDTO>() : null;
                }
            }

            return null;
        }

        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsAsync(int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                Specification<TextAlert> filter = TextAlertSpecification.DefaultSpecification();

                ISpecification<TextAlert> spec = filter;

                return await _textAlertRepository.AllMatchingPagedAsync<TextAlertDTO>(spec, pageIndex, pageSize, sortFields, ascending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsAsync(int dlrStatus, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                Specification<TextAlert> filter = TextAlertSpecification.TextWithDrlStatusAndText(dlrStatus, text);

                ISpecification<TextAlert> spec = filter;

                return await _textAlertRepository.AllMatchingPagedAsync<TextAlertDTO>(spec, pageIndex, pageSize, sortFields, ascending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsAsync(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                Specification<TextAlert> filter = TextAlertSpecification.TextAlertWithDatesAndText(startDate, endDate, text);

                ISpecification<TextAlert> spec = filter;

                return await _textAlertRepository.AllMatchingPagedAsync<TextAlertDTO>(spec, pageIndex, pageSize, sortFields, ascending, serviceHeader);
            }
        }

        public async Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAndOriginAsync(int dlrStatus, int origin, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                Specification<TextAlert> filter = TextAlertSpecification.TextAlertWithDlrStatusAndOrigin(dlrStatus, origin);

                ISpecification<TextAlert> spec = filter;

                return await _textAlertRepository.AllMatchingAsync<TextAlertDTO>(spec, serviceHeader);
            }
        }

      
        public async Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByRecipientMobileNumberFilterInPageAsync(string mobileNumber, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = TextAlertSpecification.TextAlertWithRecipientMobileNumber(mobileNumber, searchString);

                ISpecification<TextAlert> spec = filter;

                var sortFields = new List<string> { "CreatedDate" };

                return await _textAlertRepository.AllMatchingPagedAsync<TextAlertDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public Task<TextAlertDTO> AddNewTextAlertAsync(TextAlertDTO textAlertDto, ServiceHeader serviceHeader)
        {
            throw new NotImplementedException();
        }

        public bool AddNewTextAlerts(List<TextAlertDTO> textAlertDto, ServiceHeader serviceHeader)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateTextAlertAsync(TextAlertDTO textAlertDTO, ServiceHeader serviceHeader)
        {
            throw new NotImplementedException();
        }

        public Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsFilterInPageAsync(DateTime startDate, DateTime endDate, Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            throw new NotImplementedException();
        }
    }
}
