﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.ComponentModel.Composition;
using System.Messaging;

namespace Application.MainBoundedContext.Services
{
    [Export(typeof(IMessageQueueService))]
    public class MessageQueueService : IMessageQueueService
    {
        public void Send(string queuePath, object data, MessageCategory messageCategory, MessagePriority priority, int timeToBeReceived)
        {
            if (string.IsNullOrWhiteSpace(queuePath))
                throw new ArgumentNullException(nameof(queuePath));

            if (!MessageQueue.Exists(queuePath))
                MessageQueue.Create(queuePath, true);

            using (MessageQueue messageQueue = new MessageQueue(queuePath, QueueAccessMode.Send))
            {
                messageQueue.Formatter = new BinaryMessageFormatter();

                messageQueue.MessageReadPropertyFilter.SetAll();

                using (MessageQueueTransaction mqt = new MessageQueueTransaction())
                {
                    mqt.Begin();

                    using (Message message = new Message(data, new BinaryMessageFormatter()))
                    {
                        message.Label =
                            $"{EnumHelper.GetDescription(messageCategory)}|{EnumHelper.GetDescription(priority)}";
                        message.AppSpecific = (int)messageCategory;
                        message.Priority = priority;
                        message.TimeToBeReceived = TimeSpan.FromMinutes(timeToBeReceived);

                        messageQueue.Send(message, mqt);
                    }

                    mqt.Commit();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queuePath">Path to the message queuing queue</param>
        /// <param name="data">App specific payload being sent to the queue</param>
        /// <param name="messageCategory">App specific classification of the message</param>
        /// <param name="priority">Where in the queue the message is placed</param>
        /// <param name="timeToBeReceived">Maximum amount of time for the message to be received from the destination queue</param>
        /// <param name="type">App specific information i.e transaction type</param>
        public void Send(string queuePath, object data, MessageCategory messageCategory, MessagePriority priority, int timeToBeReceived, int type)
        {
            if (string.IsNullOrWhiteSpace(queuePath))
                throw new ArgumentNullException(nameof(queuePath));

            if (!MessageQueue.Exists(queuePath))
                MessageQueue.Create(queuePath, true);

            using (MessageQueue messageQueue = new MessageQueue(queuePath, QueueAccessMode.Send))
            {
                messageQueue.Formatter = new BinaryMessageFormatter();

                messageQueue.MessageReadPropertyFilter.SetAll();

                using (MessageQueueTransaction mqt = new MessageQueueTransaction())
                {
                    mqt.Begin();

                    using (Message message = new Message(data, new BinaryMessageFormatter()))
                    {
                        message.Label =
                            $"{EnumHelper.GetDescription(messageCategory)}|{EnumHelper.GetDescription(priority)}";
                        message.AppSpecific = type;
                        message.Priority = priority;
                        message.TimeToBeReceived = TimeSpan.FromMinutes(timeToBeReceived);

                        messageQueue.Send(message, mqt);
                    }

                    mqt.Commit();
                }
            }
        }
    }
}