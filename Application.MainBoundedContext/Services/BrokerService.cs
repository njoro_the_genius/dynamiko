﻿using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Configuration;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Configuration;
using System.Linq;
using System.Messaging;

namespace Application.MainBoundedContext.Services
{
    public class BrokerService : IBrokerService
    {
        private readonly IMessageQueueService _messageQueueService;

        private readonly ServiceBrokerConfigSection _serviceBrokerConfigSection;

        public BrokerService(IMessageQueueService messageQueueService)
        {
            _messageQueueService = messageQueueService ?? throw new ArgumentNullException(nameof(messageQueueService));

            _serviceBrokerConfigSection = GetConfiguration();
        }

        public bool ProcessAuditLogs(DMLCommand command, ServiceHeader serviceHeader, params AuditLogBCP[] data)
        {
            var result = default(bool);

            if (data != null && _serviceBrokerConfigSection != null)
            {
                switch (command)
                {
                    case DMLCommand.Insert:

                        _messageQueueService.Send(_serviceBrokerConfigSection.ServiceBrokerSettingsItems.AuditLogDispatcherQueuePath, data.ToList(), MessageCategory.AuditLog, MessagePriority.High, _serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);

                        result = true;

                        break;
                    default:
                        break;
                }
            }

            return result;
        }

        public bool ProcessEmailAlertsAsync(DMLCommand command, ServiceHeader serviceHeader, params EmailAlertDTO[] data)
        {
            var result = default(bool);

            if (data != null)
            {
                switch (command)
                {
                    case DMLCommand.Insert:

                        Array.ForEach(data, (EmailAlertDTO) =>
                        {
                            switch ((DLRStatus)EmailAlertDTO.MailMessageDLRStatus)
                            {
                                case DLRStatus.UnKnown:
                                case DLRStatus.Pending:

                                    if (EmailAlertDTO.MailMessageSendRetry == 0)
                                    {
                                        var queueDTO = new QueueDTO
                                        {
                                            RecordId = EmailAlertDTO.Id,
                                            AppDomainName = serviceHeader.ApplicationDomainName
                                        };

                                        _messageQueueService.Send(_serviceBrokerConfigSection.ServiceBrokerSettingsItems.EmailDispatcherQueuePath, queueDTO, MessageCategory.EmailAlert, (MessagePriority)EmailAlertDTO.MailMessagePriority, _serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);
                                    }

                                    break;
                                default:
                                    break;
                            }
                        });

                        result = true;

                        break;
                    default:
                        break;
                }
            }

            return result;
        }

        public bool ProcessTextAlerts(DMLCommand command, ServiceHeader serviceHeader, params TextAlertDTO[] data)
        {
            var result = default(bool);

            if (data != null && _serviceBrokerConfigSection != null)
            {
                switch (command)
                {
                    case DMLCommand.Insert:

                        Array.ForEach(data, (textAlert) =>
                        {
                            switch ((DLRStatus)textAlert.TextMessageDLRStatus)
                            {
                                case DLRStatus.UnKnown:
                                case DLRStatus.Pending:

                                    if (textAlert.TextMessageSendRetry == 0)
                                    {
                                        var queueDTO = new QueueDTO
                                        {
                                            RecordId = textAlert.Id,
                                            AppDomainName = serviceHeader.ApplicationDomainName,
                                        };

                                        _messageQueueService.Send(_serviceBrokerConfigSection.ServiceBrokerSettingsItems.TextDispatcherQueuePath, queueDTO, MessageCategory.TextAlert, (MessagePriority)textAlert.TextMessagePriority, _serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);
                                    }

                                    break;
                            }
                        });

                        result = true;

                        break;
                }
            }

            return result;
        }

        public bool ProcessApproval(DMLCommand command, ServiceHeader serviceHeader, string entityName, byte recordStatus, Guid recordId)
        {
            var result = default(bool);

            if (!string.IsNullOrWhiteSpace(entityName) && recordId != null && recordId != Guid.Empty && _serviceBrokerConfigSection != null)
            {
                switch (command)
                {
                    case DMLCommand.Insert:


                        var queueDTO = new QueueDTO
                        {
                            RecordId = recordId,
                            EntityName = entityName,
                            AppDomainName = serviceHeader.ApplicationDomainName,
                            RecordStatus = recordStatus
                        };

                        _messageQueueService.Send(_serviceBrokerConfigSection.ServiceBrokerSettingsItems.ApprovalDispatcherQueuePath, queueDTO, MessageCategory.Approvals, MessagePriority.High, _serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);

                        result = true;

                        break;
                    default:
                        break;
                }
            }

            return result;
        }

        public bool ProcessApproval(DMLCommand command, ServiceHeader serviceHeader, string entityName, Guid recordId)
        {
            var result = default(bool);

            if (!string.IsNullOrWhiteSpace(entityName) && recordId != null && recordId != Guid.Empty && _serviceBrokerConfigSection != null)
            {
                switch (command)
                {
                    case DMLCommand.Insert:


                        var queueDTO = new QueueDTO
                        {
                            RecordId = recordId,
                            EntityName = entityName,
                            AppDomainName = serviceHeader.ApplicationDomainName
                        };

                        _messageQueueService.Send(_serviceBrokerConfigSection.ServiceBrokerSettingsItems.ApprovalDispatcherQueuePath, queueDTO, MessageCategory.Approvals, MessagePriority.High, _serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);

                        result = true;

                        break;
                    default:
                        break;
                }
            }

            return result;
        }

        public bool ProcessOrderLine(DMLCommand command, ServiceHeader serviceHeader, Guid orderLineId)
        {
            var result = false;

            if (orderLineId != Guid.Empty && orderLineId != null && _serviceBrokerConfigSection != null)
            {
                switch (command)
                {
                    case DMLCommand.Insert:

                        var queueDTO = new QueueDTO
                        {
                            RecordId = orderLineId,
                            AppDomainName = serviceHeader.ApplicationDomainName,
                            Gateway = _serviceBrokerConfigSection.ServiceBrokerSettingsItems.OrderGateway
                        };

                        _messageQueueService.Send(_serviceBrokerConfigSection.ServiceBrokerSettingsItems.GatewayDispatcherQueuePath, queueDTO, MessageCategory.AirtimePurchaseRequest, MessagePriority.High, _serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);

                        result = true;

                        break;
                    default:
                        break;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool ProcessKYCRecord(DMLCommand command, ServiceHeader serviceHeader, Guid kycRecordId)
        {
            var result = false;

            if (kycRecordId != Guid.Empty && kycRecordId != null && _serviceBrokerConfigSection != null)
            {
                switch (command)
                {
                    case DMLCommand.Insert:

                        var queueDTO = new QueueDTO
                        {
                            RecordId = kycRecordId,
                            AppDomainName = serviceHeader.ApplicationDomainName,
                        };

                        _messageQueueService.Send(_serviceBrokerConfigSection.ServiceBrokerSettingsItems.TransUnionDispatcherQueuePath, queueDTO, MessageCategory.CRBPosting, MessagePriority.High, _serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);

                        result = true;

                        break;
                    default:
                        break;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        private static ServiceBrokerConfigSection GetConfiguration()
        {
            try
            {
                return (ServiceBrokerConfigSection)ConfigurationManager.GetSection("serviceBrokerConfiguration");
            }
            catch { return null; }
        }
    }
}