﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Domain.MainBoundedContext.Aggregates.AuditTrailAgg;
using Domain.Seedwork;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public class SqlCommandAppService : ISqlCommandAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        private readonly IRepository<AuditTrail> _repository;
      
        private readonly ILogger _logger;

        public SqlCommandAppService(
            IDbContextScopeFactory dbContextScopeFactory,
            IRepository<AuditTrail> repository,
         
            ILogger logger)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));

            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));
          

            _logger = logger;
        }

        public bool BulkInsert<T>(string tableName, IList<T> list, ServiceHeader serviceHeader)
        {
            var result = default(bool);

            using (var bulkCopy = new SqlBulkCopy(ConfigurationManager.ConnectionStrings[serviceHeader.ApplicationDomainName].ConnectionString, SqlBulkCopyOptions.FireTriggers))
            {
                bulkCopy.BulkCopyTimeout = 300;

                bulkCopy.BatchSize = 5000;

                bulkCopy.DestinationTableName = tableName;

                var table = new DataTable();

                var props = TypeDescriptor.GetProperties(typeof(T))
                                           //Dirty hack to make sure we only have system data types 
                                           //i.e. filter out the relationships/collections
                                           .Cast<PropertyDescriptor>()
                                           .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System") || propertyInfo.PropertyType.Namespace.Equals("Domain.MainBoundedContext.ValueObjects"))
                                           .ToArray();

                var propsList = new List<PropertyDescriptor>();

                var valueObjectPropsList = new List<PropertyDescriptor>();

                foreach (var propertyInfo in props)
                {
                    if (propertyInfo.PropertyType.Namespace.Equals("Domain.MainBoundedContext.ValueObjects"))
                    {
                        valueObjectPropsList.Add(propertyInfo);

                        var valueObjectProps = propertyInfo.GetChildProperties()
                                                   //Dirty hack to make sure we only have system data types 
                                                   //i.e. filter out the relationships/collections
                                                   .Cast<PropertyDescriptor>()
                                                   .Where(voPropertyInfo => voPropertyInfo.PropertyType.Namespace.Equals("System"))
                                                   .ToArray();

                        propsList.AddRange(valueObjectProps.ToList());

                        foreach (var valueObjectPropertyInfo in valueObjectProps)
                        {
                            bulkCopy.ColumnMappings.Add(propertyInfo.Name + "_" + valueObjectPropertyInfo.Name, propertyInfo.Name + "_" + valueObjectPropertyInfo.Name);

                            table.Columns.Add(propertyInfo.Name + "_" + valueObjectPropertyInfo.Name, Nullable.GetUnderlyingType(valueObjectPropertyInfo.PropertyType) ?? valueObjectPropertyInfo.PropertyType);
                        }                        
                    }
                    else
                    {
                        propsList.Add(propertyInfo);

                        bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);

                        table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                    }
                }

                props = propsList.ToArray();

                var values = new object[props.Length];

                foreach (var item in list)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        if (props[i].ComponentType.Namespace.Equals("Domain.MainBoundedContext.ValueObjects"))
                        {
                            object valueObject = valueObjectPropsList.Where(x => x.PropertyType.Name.Equals(props[i].ComponentType.Name)).FirstOrDefault().GetValue(item);

                            values[i] = props[i].GetValue(valueObject);
                        }
                        else
                        {
                            values[i] = props[i].GetValue(item);
                        }
                    }

                    table.Rows.Add(values);
                }

                bulkCopy.WriteToServer(table);

                result = true;
            }

            return result;
        }

        public async Task<DashboardWidgetDTO> FindDashboardWidgetsAsync(int claim, Guid? CustomerId, ServiceHeader serviceHeader)
        {
            DashboardWidgetDTO dashboardWidgetDTO = new DashboardWidgetDTO();

            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var query = await _repository.DatabaseSqlQueryAsync<DashboardWidgetDTO>("EXEC Sp_DashBoardStatistics @claim,@customerId", serviceHeader, new SqlParameter("Claim", claim), new SqlParameter("CustomerId", CustomerId));

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        dashboardWidgetDTO = new DashboardWidgetDTO()
                        {
                            TotalCustomer = item.TotalCustomer,
                            TotalEMployees = item.TotalEMployees,
                            TotalSalary = item.TotalSalary,
                            TotalAverageSalary = item.TotalAverageSalary,
                            CustomerName = item.CustomerName,
                            TotalEmployeesOnLeave = item.TotalEmployeesOnLeave,
                            TotalLeaveDays = item.TotalLeaveDays
                        };
                    }
                }
            }

            return dashboardWidgetDTO;
        }

        public async Task<List<DashboardGraphDTO>> GetDashboardGraphDataSetAsync(int claim, Guid? customerId, ServiceHeader serviceHeader)
        {
            List<DashboardGraphDTO> dashboardGraphDTOs = new List<DashboardGraphDTO>();

            DashboardGraphDTO dashboardWidgetDTO  = new DashboardGraphDTO();

            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var query = await _repository.DatabaseSqlQueryAsync<DashboardGraphDTO>("EXEC Sp_GetSalaryStatistics_GraphData @Claim,@CustomerId", serviceHeader, new SqlParameter("Claim", claim), new SqlParameter("CustomerId", customerId));

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        dashboardWidgetDTO = new DashboardGraphDTO()
                        {
                            Customer = item.Customer,
                            Quarter1 = item.Quarter1,
                            Quarter2 = item.Quarter2,
                            Quarter3 = item.Quarter3,
                            Quarter4 = item.Quarter4,
                            YearNo = item.YearNo
                        };

                        dashboardGraphDTOs.Add(dashboardWidgetDTO);
                    }
                }
            }

            return dashboardGraphDTOs;
        }

       

        public async Task<PageCollectionInfo<EmployeeDTO>> GetEmployeesReportAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var query = await _repository.DatabaseSqlQueryAsync<EmployeeDTO>("EXEC Sp_GetEmployees_Report @Claim,@StartDate,@EndDate,@CustomerId,@StartIndex,@PageSize,@SearchString",
                    serviceHeader,
                    new SqlParameter("Claim", claim),
                    new SqlParameter("StartDate", startDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("EndDate", endDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("CustomerId", customerId),
                    new SqlParameter("StartIndex", pageIndex),
                    new SqlParameter("pageSize", pageSize),
                    new SqlParameter("SearchString", text));

                if (query != null)
                {
                    PageCollectionInfo<EmployeeDTO> pageCollectionInfo = new PageCollectionInfo<EmployeeDTO>()
                    {
                        ItemsCount = query.Count(),
                        PageIndex = pageIndex,
                        PageSize = pageSize,
                        PageCollection = query,
                    };

                    return pageCollectionInfo;
                }
            }

            return null;
        }

        public async Task<PageCollectionInfo<PaySlipDTO>> GetPaySlipAsync(Guid employeeId, int period, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var query = await _repository.DatabaseSqlQueryAsync<PaySlipDTO>("EXEC Sp_ViewPaySlip @period,@employeeId",
                    serviceHeader,                  
                    new SqlParameter("period", period),
                    new SqlParameter("employeeId", employeeId));

                if (query != null)
                {
                    PageCollectionInfo<PaySlipDTO> pageCollectionInfo = new PageCollectionInfo<PaySlipDTO>()
                    {
                        ItemsCount = query.Count(),
                        PageCollection = query,
                    };

                    return pageCollectionInfo;
                }
            }

            return null;
        }

        public async Task<PageCollectionInfo<PaySlipDTO>> GetPayRollAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var query = await _repository.DatabaseSqlQueryAsync<PaySlipDTO>("EXEC Sp_PayRollItems @Claim,@StartDate,@EndDate,@CustomerId,@StartIndex,@PageSize,@SearchString",
                    serviceHeader,
                    new SqlParameter("Claim", claim),
                    new SqlParameter("StartDate", startDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("EndDate", endDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("CustomerId", customerId),
                    new SqlParameter("StartIndex", pageIndex),
                    new SqlParameter("pageSize", pageSize),
                    new SqlParameter("SearchString", text));

                if (query != null)
                {
                    PageCollectionInfo<PaySlipDTO> pageCollectionInfo = new PageCollectionInfo<PaySlipDTO>()
                    {
                        ItemsCount = query.Count(),
                        PageIndex = pageIndex,
                        PageSize = pageSize,
                        PageCollection = query,
                    };

                    return pageCollectionInfo;
                }
            }

            return null;
        }

    }
}