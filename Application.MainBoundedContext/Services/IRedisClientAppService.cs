﻿using Infrastructure.Crosscutting.Framework.Utils;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public interface IRedisClientAppService
    {
        Task<bool> SeedSystemAccessRightAsync(string role, ServiceHeader serviceHeader);

        Task<bool> SeedSystemAccessRightsAsync(string[] roles, ServiceHeader serviceHeader);

        Task<bool> ValidatePermissionAsync(string[] roles, string controllerName, string actionName, ServiceHeader serviceHeader);

        Task<bool> RemoveAllRightsAsync(string[] roles);
    }
}
