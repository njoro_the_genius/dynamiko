﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public interface ISqlCommandAppService
    {
        bool BulkInsert<T>(string tableName, IList<T> list, ServiceHeader serviceHeader);

        
        Task<List<DashboardGraphDTO>> GetDashboardGraphDataSetAsync(int claim, Guid? customerId, ServiceHeader serviceHeader);

        Task<DashboardWidgetDTO> FindDashboardWidgetsAsync(int claim, Guid? customerId, ServiceHeader serviceHeader);

       
        Task<PageCollectionInfo<EmployeeDTO>> GetEmployeesReportAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PaySlipDTO>> GetPaySlipAsync(Guid employeeId, int period, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PaySlipDTO>> GetPayRollAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader);



    }
}