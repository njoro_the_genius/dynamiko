﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;

namespace Application.MainBoundedContext.Services
{
    public static class Helper
    {
        public static ClientCode GetClientCode(string code)
        {
            var clientCode = ClientCode.SBM;

            Enum.TryParse<ClientCode>(code, true, out clientCode);

            return clientCode;
        }
    }
}
