﻿using System.IO;
using System.Security.Cryptography;
using System.Text;
using RazorEngine;
using RazorEngine.Templating;

namespace Application.MainBoundedContext.Services
{
    public class RazorTemplateService
    {
        public static string GenerateMessageBody<T>(ref T template, string fullTemplatePath)
        {
            #region RazorSmsGeneration

            string rawTemplate = File.ReadAllText(fullTemplatePath);

            return Engine.Razor.RunCompile(rawTemplate, GetMd5Hash(rawTemplate), typeof(T), template);

            #endregion
        }

        private static string GetMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (byte t in hash)
            {
                sb.Append(t.ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
