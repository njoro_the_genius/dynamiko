﻿using Application.MainBoundedContext.DTO;
using Application.Seedwork;
using Domain.MainBoundedContext.Aggregates.AuditTrailAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public class AuditTrailAppService : IAuditTrailAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IRepository<AuditTrail> _auditTrailRepository;

        public AuditTrailAppService(
           IDbContextScopeFactory dbContextScopeFactory,
           IRepository<AuditTrail> auditTrailRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));

            _auditTrailRepository = auditTrailRepository ?? throw new ArgumentNullException(nameof(auditTrailRepository));
        }

        public async Task<AuditTrailDTO> AddNewAuditTrailAsync(AuditTrailDTO auditTrailDTO, ServiceHeader serviceHeader)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var auditTrail = AuditTrailFactory.CreateAuditTrail(auditTrailDTO.EventType, auditTrailDTO.Activity, auditTrailDTO.AdditionalNarration, auditTrailDTO.ApplicationUserName, auditTrailDTO.ApplicationUserDesignation, auditTrailDTO.EnvironmentUserName, auditTrailDTO.EnvironmentMachineName, auditTrailDTO.EnvironmentDomainName, auditTrailDTO.EnvironmentOSVersion,
                        auditTrailDTO.EnvironmentMACAddress, auditTrailDTO.EnvironmentMotherboardSerialNumber, auditTrailDTO.EnvironmentProcessorId, auditTrailDTO.EnvironmentIPAddress, serviceHeader, auditTrailDTO.CustomerId);

                _auditTrailRepository.Add(auditTrail, serviceHeader);

                return await dbContextScope.SaveChangesAsync(serviceHeader) > 0 ? auditTrail.ProjectedAs<AuditTrailDTO>() : null;
            }
        }

        public async Task<bool> AddNewAuditTrailsAsync(List<AuditTrailDTO> auditTrailDTOs, ServiceHeader serviceHeader)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                foreach (var auditTrailDTO in auditTrailDTOs)
                {
                    var auditTrail = AuditTrailFactory.CreateAuditTrail(auditTrailDTO.EventType, auditTrailDTO.Activity, auditTrailDTO.AdditionalNarration, auditTrailDTO.ApplicationUserName, auditTrailDTO.ApplicationUserDesignation, auditTrailDTO.EnvironmentUserName, auditTrailDTO.EnvironmentMachineName, auditTrailDTO.EnvironmentDomainName, auditTrailDTO.EnvironmentOSVersion,
                            auditTrailDTO.EnvironmentMACAddress, auditTrailDTO.EnvironmentMotherboardSerialNumber, auditTrailDTO.EnvironmentProcessorId, auditTrailDTO.EnvironmentIPAddress, serviceHeader, auditTrailDTO.CustomerId);

                    _auditTrailRepository.Add(auditTrail, serviceHeader);
                }

                return await dbContextScope.SaveChangesAsync(serviceHeader) > 0;
            }
        }

        public async Task<PageCollectionInfo<AuditTrailDTO>> FindAuditTrailsByDateRangeAndFilterAsync(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string text, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = AuditTrailSpecifications.AuditTrailWithDateRangeAndFullText(startDate, endDate, text);

                ISpecification<AuditTrail> spec = filter;

                var sortFields = new List<string> { "SequentialId" };

                return await _auditTrailRepository.AllMatchingPagedAsync<AuditTrailDTO>(spec, pageIndex, pageSize, sortFields, true, serviceHeader);
            }
        }
    }
}
