﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public interface IEmailAppService
    {
        Task<bool> GenerateEmailAsync(String templatePath, IDictionary<string, object> expandoObject, string subject,string messageTo, bool mailMessageIsBodyHtml, bool mailMessageSecurityCritical,ServiceHeader serviceHeader);
    }
}
