﻿using Application.MainBoundedContext.DTO.MessagingModule;
using Application.MainBoundedContext.MessagingModule.Services;
using Infrastructure.Crosscutting.Framework.Utils;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public class EmailAppService : IEmailAppService
    {
        private readonly EmailAlertAppService _emailAlertAppService;

        public EmailAppService(EmailAlertAppService emailAlertAppService)
        {
            _emailAlertAppService = emailAlertAppService ?? throw new ArgumentNullException(nameof(emailAlertAppService));
        }

        public async Task<bool> GenerateEmailAsync(string templatePath, IDictionary<string, object> expandoObject, string subject, string messageTo, bool mailMessageIsBodyHtml, bool mailMessageSecurityCritical, ServiceHeader serviceHeader)
        {
            var template = System.IO.File.ReadAllText(templatePath);

            var emailBody = Engine.Razor.RunCompile(template, $"{Guid.NewGuid()}_EmailTemplate", null, expandoObject);

            var emailAlertDTO = new EmailAlertDTO
            {
                MailMessageFrom = DefaultSettings.Instance.RootEmail,
                MailMessageTo = messageTo,
                MailMessageSubject = subject,
                MailMessageBody = emailBody.Trim(),
                MailMessageIsBodyHtml = mailMessageIsBodyHtml,
                MailMessageOrigin = (int)MessageOrigin.Within,
                MailMessageSecurityCritical = mailMessageSecurityCritical,
                MailMessagePriority = (int)QueuePriority.VeryHigh,
                MailMessageDLRStatus = (byte)DLRStatus.Pending
            };

            var emailAlert = await _emailAlertAppService.AddEmailAlertAsync(emailAlertDTO, serviceHeader);

            return emailAlert != null && emailAlert.Id != null && emailAlert.Id != Guid.Empty;
        }
    }
}
