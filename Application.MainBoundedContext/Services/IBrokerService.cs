﻿using Application.MainBoundedContext.DTO.MessagingModule;
using Infrastructure.Crosscutting.Framework.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using System;

namespace Application.MainBoundedContext.Services
{
    public interface IBrokerService
    {
        bool ProcessAuditLogs(DMLCommand command, ServiceHeader serviceHeader, params AuditLogBCP[] data);

        bool ProcessEmailAlertsAsync(DMLCommand command, ServiceHeader serviceHeader, params EmailAlertDTO[] data);

        bool ProcessTextAlerts(DMLCommand command, ServiceHeader serviceHeader, params TextAlertDTO[] data);

        bool ProcessApproval(DMLCommand command, ServiceHeader serviceHeader, string entityName, byte recordStatus, Guid recordId);

        bool ProcessApproval(DMLCommand command, ServiceHeader serviceHeader, string entityName, Guid recordId);

        bool ProcessOrderLine(DMLCommand command, ServiceHeader serviceHeader, Guid orderLineId);

        bool ProcessKYCRecord(DMLCommand command, ServiceHeader serviceHeader, Guid kycRecordId);
    }
}