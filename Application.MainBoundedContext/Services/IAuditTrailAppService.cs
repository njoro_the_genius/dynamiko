﻿using Application.MainBoundedContext.DTO;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public interface IAuditTrailAppService
    {
        Task<bool> AddNewAuditTrailsAsync(List<AuditTrailDTO> auditTrailDTOs, ServiceHeader serviceHeader);

        Task<AuditTrailDTO> AddNewAuditTrailAsync(AuditTrailDTO auditTrailDTO, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<AuditTrailDTO>> FindAuditTrailsByDateRangeAndFilterAsync(int pageIndex, int pageSize, DateTime startDate, DateTime endDate, string text, ServiceHeader serviceHeader);
    }
}
