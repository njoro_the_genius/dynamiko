﻿using Application.MainBoundedContext.AdministrationModule;
using Infrastructure.Crosscutting.Framework.Services.RedisClient;
using Infrastructure.Crosscutting.Framework.Services.RedisClient.Models;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public class RedisClientAppService : IRedisClientAppService
    {
        private readonly IModuleNavigationItemAppService _moduleNavigationItemAppService;

        private readonly IRedisRepository<AccessControlDTO> _accessControlRedisRepository;

        private readonly string _redisUniqueIdentifier;

        public RedisClientAppService(
            IRedisRepository<AccessControlDTO> accessControlRedisRepository,
            IModuleNavigationItemAppService moduleNavigationItemAppService)
        {
            Guard.ArgumentNotNull(moduleNavigationItemAppService, nameof(moduleNavigationItemAppService));

            Guard.ArgumentNotNull(accessControlRedisRepository, nameof(accessControlRedisRepository));

            _moduleNavigationItemAppService = moduleNavigationItemAppService;

            _accessControlRedisRepository = accessControlRedisRepository;

            _redisUniqueIdentifier = ConfigurationManager.AppSettings["RedisIdentifier"];

        }

        public async Task<bool> SeedSystemAccessRightAsync(string role, ServiceHeader serviceHeader)
        {
            if (!string.IsNullOrWhiteSpace(role))
            {
                var moduleNavigationsItemsPerRole = await _moduleNavigationItemAppService.FindModuleNavigationItemInRoleByRoleAsync(role, serviceHeader);

                var accessRightsList = new List<AccessControlList>();

                if (moduleNavigationsItemsPerRole != null && moduleNavigationsItemsPerRole.Any())
                {
                    foreach (var accessRight in moduleNavigationsItemsPerRole)
                    {
                        accessRightsList.Add(new AccessControlList
                        {
                            ControllerName = accessRight.ModuleNavigationItemControllerName,
                            ActionName = accessRight.ModuleNavigationItemActionName,
                            CreatedBy = accessRight.CreatedBy
                        });
                    }
                }

                _accessControlRedisRepository.Remove($"{_redisUniqueIdentifier}{role}");

                if (accessRightsList.Any())
                {
                    var accessControl = new AccessControlDTO()
                    {
                        Id = $"{_redisUniqueIdentifier}{role}",
                        AccessControlList = accessRightsList
                    };

                    _accessControlRedisRepository.Add(accessControl);
                }

            }

            return true;
        }

        public async Task<bool> SeedSystemAccessRightsAsync(string[] roles, ServiceHeader serviceHeader)
        {
            if (roles != null && roles.Length > 0)
            {
                Array.ForEach(roles, async role =>
                {
                    var moduleNavigationsItemsPerRole = await _moduleNavigationItemAppService.FindModuleNavigationItemInRoleByRoleAsync(role, serviceHeader);

                    var accessRightsList = new List<AccessControlList>();

                    if (moduleNavigationsItemsPerRole != null && moduleNavigationsItemsPerRole.Any())
                    {
                        foreach (var accessRight in moduleNavigationsItemsPerRole)
                        {
                            accessRightsList.Add(new AccessControlList
                            {
                                ControllerName = accessRight.ModuleNavigationItemControllerName,
                                ActionName = accessRight.ModuleNavigationItemActionName,
                                CreatedBy = accessRight.CreatedBy
                            });
                        }
                    }

                    if (accessRightsList.Any())
                    {
                        _accessControlRedisRepository.Remove($"{_redisUniqueIdentifier}{role}");

                        var accessControl = new AccessControlDTO()
                        {
                            Id = $"{_redisUniqueIdentifier}{role}",
                            AccessControlList = accessRightsList
                        };

                        _accessControlRedisRepository.Add(accessControl);
                    }
                });
            }

            return true;
        }

        public async Task<bool> ValidatePermissionAsync(string[] roles, string controllerName, string actionName, ServiceHeader serviceHeader)
        {
            if (roles != null && roles.Length > 0)
            {
                foreach (var role in roles)
                {
                    //get the access rights from the Redis server using the rolename
                    var accessRightsByRole = _accessControlRedisRepository.Get($"{_redisUniqueIdentifier}{role}");

                    if (accessRightsByRole != null)
                    {
                        if (accessRightsByRole.AccessControlList.Any())
                        {
                            var validate = accessRightsByRole.AccessControlList.Any(x => x.ControllerName.ToLower().Equals(controllerName.ToLower()) && x.ActionName.ToLower().Equals(actionName.ToLower()));

                            return validate;
                        }
                    }
                }
            }

            return false;
        }

        public async Task<bool> RemoveAllRightsAsync(string[] roles)
        {
            if (roles != null && roles.Length > 0)
            {
                foreach (var role in roles)
                {
                    _accessControlRedisRepository.Remove($"{_redisUniqueIdentifier}{role}");
                }
            }

            return true;
        }
    }
}
