﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.Services
{
    public interface IHttpRequestAppService
    {
        Task<Tuple<HttpStatusCode, string>> PostAsync(LimitedPool<HttpClient> httpClientPool, string payload,string url, string username, string password);

        Task<Tuple<HttpStatusCode, string>> GetAsync(LimitedPool<HttpClient> httpClientPool, string thirdPartyTransactionId, string url, string username, string password);
    }
}
