﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Application.MainBoundedContext.RegistryModule.Services
{
    public interface ICustomerAppService
    {
        #region CustomerDTO

        Task<Tuple<CustomerDTO, List<string>>> AddNewCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader);

        Task<bool> UpdateCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader);

        Task<List<CustomerDTO>> FindCustomersAsync(ServiceHeader serviceHeader);

        Task<CustomerDTO> FindCustomerAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAndIsEnabledAsync(string searchString, bool isEnabled, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending, ServiceHeader serviceHeader);

        Task<List<CustomerDTO>> FindCustomersByNameAsync(string name, ServiceHeader serviceHeader);

        #endregion
    }
}
