﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Application.MainBoundedContext.RegistryModule
{
    public interface IEmployeeAppService
    {
        #region EmployeeDTO

        Task<Tuple<EmployeeDTO, List<string>>> AddNewEmployeeAsync(EmployeeDTO employeeDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateEmployeeAsync(EmployeeDTO EmployeeDTO, ServiceHeader serviceHeader);

        Task<EmployeeDTO> FindEmployeeAsync(Guid id, ServiceHeader serviceHeader);

        Task<EmployeeDTO> FindEmployeeByUserIdAsync(Guid userId, Guid? customerId, ServiceHeader serviceHeader);

        Task<List<EmployeeDTO>> FindEnabledEmployeesByCustomerIdAsync(Guid customerId, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmployeeDTO>> FindCustomerEmployeesFilterInPageAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmployeeDTO>> FindEmployeeByCustomerIdAsync(Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending, ServiceHeader serviceHeader);

        #endregion
    }
}
