﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Domain.MainBoundedContext.RegistryModule;
using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.RegistryModule.Services
{
    public class CustomerAppService : ICustomerAppService
    {
        private readonly IRepository<Customer> _customerRepository;

        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public CustomerAppService(
            IRepository<Customer> customerRepository,
            IDbContextScopeFactory dbContextScopeFactory)
        {
            _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));

            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));
        }

        public async Task<Tuple<CustomerDTO, List<string>>> AddNewCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader)
        {
            var result = new List<string>();

            if (customerDto == null) return null;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                Specification<Customer> nameSpecification = CustomerSpecifications.WithNameLike(customerDto.Name);

                var match = _customerRepository.AllMatching(nameSpecification, serviceHeader);

                if (match.Any())
                {
                    result.Add($"Customer {customerDto.Name} exists already.");

                    return new Tuple<CustomerDTO, List<string>>(null, result);
                }
                //convert logo to base 64


                var customer = await AddCustomer(customerDto, serviceHeader);               

                if (customerDto.IsEnabled)
                    customer.Enable();
                else customer.Disable();

                _customerRepository.Add(customer, serviceHeader);

                if (await dbContextScope.SaveChangesAsync(serviceHeader) >= 0)
                {
                    result.Add("Customer created successfully");

                    return new Tuple<CustomerDTO, List<string>>(customer.ProjectedAs<CustomerDTO>(), result);
                }

                return new Tuple<CustomerDTO, List<string>>(null, result);
            }
        }

        public async Task<bool> UpdateCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader)
        {
            if (customerDto == null) return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = _customerRepository.Get(customerDto.Id, serviceHeader);

                if (persisted != null)
                {
                    var address = new Address(customerDto.AddressPhysical, customerDto.AddressPostal, customerDto.AddressStreet, customerDto.AddressPostal, customerDto.AddressCity, customerDto.AddressEmail, customerDto.AddressLandLine, customerDto.AddressMobileLine);

                    var current = CustomerFactory.CreateCustomer(customerDto.Name, customerDto.Type, address, (byte)RecordStatus.New, customerDto.IsEnabled, customerDto.LogoPath);

                    _customerRepository.Merge(persisted, current, serviceHeader);

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;
                }

                return false;
            }
        }

        public async Task<List<CustomerDTO>> FindCustomersAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = CustomerSpecifications.DefaultSpec();

                ISpecification<Customer> spec = filter;

                return await _customerRepository.AllMatchingAsync<CustomerDTO>(spec, serviceHeader);
            }
        }

        public async Task<CustomerDTO> FindCustomerAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return await _customerRepository.GetAsync<CustomerDTO>(id, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = CustomerSpecifications.CustomerWithFullText(searchString);

                ISpecification<Customer> spec = filter;

                var sortFields = sortedColumns != null ? sortedColumns.ToList() : new List<string> { "CreatedDate" };

                return await _customerRepository.AllMatchingPagedAsync<CustomerDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAndIsEnabledAsync(string searchString, bool isEnabled, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = CustomerSpecifications.CustomerWithFullTextAndIsEnabled(searchString, isEnabled);

                ISpecification<Customer> spec = filter;

                var sortFields = sortedColumns != null ? sortedColumns.ToList() : new List<string> { "CreatedDate" };

                return await _customerRepository.AllMatchingPagedAsync<CustomerDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public static async Task<Customer> AddCustomer(CustomerDTO customerDto, ServiceHeader serviceHeader)
        {
            var address = new Address(customerDto.AddressPhysical, customerDto.AddressPostal, customerDto.AddressStreet, customerDto.AddressPostal, customerDto.AddressCity, customerDto.AddressEmail, customerDto.AddressLandLine, customerDto.AddressMobileLine);


            var customer = CustomerFactory.CreateCustomer(customerDto.Name, customerDto.Type, address, (byte)RecordStatus.New, customerDto.IsEnabled, customerDto.LogoPath);

            return customer;
        }

        public async Task<List<CustomerDTO>> FindCustomersByNameAsync(string name, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                Specification<Customer> branchSpecification = CustomerSpecifications.WithNameLike(name);

                var match = await _customerRepository.AllMatchingAsync<CustomerDTO>(branchSpecification, serviceHeader);

                return match;

            }
        }


        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
