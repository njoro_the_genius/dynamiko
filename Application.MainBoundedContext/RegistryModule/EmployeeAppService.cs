﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.RegistryModule;
using Domain.MainBoundedContext.RegistryModule.Aggregates.EmployeeAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.RegistryModule
{
    public class EmployeeAppService : IEmployeeAppService
    {
        private readonly IRepository<Employee> _employeeRepository;

        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public EmployeeAppService(
            IRepository<Employee> employeeRepository,
            IDbContextScopeFactory dbContextScopeFactory)
        {
            _employeeRepository = employeeRepository ?? throw new ArgumentNullException(nameof(employeeRepository));

            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));
        }
        public async Task<Tuple<EmployeeDTO, List<string>>> AddNewEmployeeAsync(EmployeeDTO employeeDTO, ServiceHeader serviceHeader)
        {
            var result = new List<string>();

            if (employeeDTO == null) return null;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                Specification<Employee> nameSpecification = EmployeeSpecifications.CustomerIdAndUserIdSpec(employeeDTO.CustomerId, employeeDTO.UserId);

                var match = _employeeRepository.AllMatching(nameSpecification, serviceHeader);

                //if (match.Any())
                //{
                //    result.Add($"Employee {employeeDTO.EmployeeNames} exists already.");

                //    return new Tuple<EmployeeDTO, List<string>>(null, result);
                //}

                var employee = await AddEmployee(employeeDTO, serviceHeader);

                if (employeeDTO.IsEnabled)
                    employee.Enable();
                else employee.Disable();

                _employeeRepository.Add(employee, serviceHeader);

                if (await dbContextScope.SaveChangesAsync(serviceHeader) >= 0)
                {
                    result.Add("Employee created successfully");

                    return new Tuple<EmployeeDTO, List<string>>(employee.ProjectedAs<EmployeeDTO>(), result);
                }

                return new Tuple<EmployeeDTO, List<string>>(null, result);
            }
        } 

        public async Task<bool> UpdateEmployeeAsync(EmployeeDTO employeeDTO, ServiceHeader serviceHeader)
        {
            if (employeeDTO == null) return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = _employeeRepository.Get(employeeDTO.Id, serviceHeader);

                if (persisted != null)
                {

                    var current = EmployeeFactory.CreateEmployee(employeeDTO.CustomerId, employeeDTO.UserId, employeeDTO.UserName, employeeDTO.DepartmentId, employeeDTO.BankAccount, employeeDTO.GrossSalary, employeeDTO.IdNumber, employeeDTO.KRAPin, employeeDTO.NHIFNumber, employeeDTO.NSSFNumber, employeeDTO.RecordStatus, employeeDTO.IsEnabled, employeeDTO.DeductNSSFContribution, employeeDTO.DeductNHIFContribution, employeeDTO.BankCode, employeeDTO.BranchCode, employeeDTO.AccountName, employeeDTO.JobNumber, employeeDTO.DateOfEmployment, employeeDTO.JobTitle, employeeDTO.Gender, employeeDTO.DateOfBirth, employeeDTO.FirstName, employeeDTO.OtherNames, employeeDTO.PhoneNumber, employeeDTO.Email, employeeDTO.IsExternalUser, employeeDTO.AllocatedLeaveDays);

                    _employeeRepository.Merge(persisted, current, serviceHeader);

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;
                }

                return false;
            }
        }

        public async Task<EmployeeDTO> FindEmployeeAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return await _employeeRepository.GetAsync<EmployeeDTO>(id, serviceHeader);
            }
        }

        public static async Task<Employee> AddEmployee(EmployeeDTO employeeDTO, ServiceHeader serviceHeader)
        {

            var employee = EmployeeFactory.CreateEmployee(employeeDTO.CustomerId, employeeDTO.UserId, employeeDTO.UserName,employeeDTO.DepartmentId, employeeDTO.BankAccount, employeeDTO.GrossSalary, employeeDTO.IdNumber, employeeDTO.KRAPin, employeeDTO.NHIFNumber, employeeDTO.NSSFNumber,  employeeDTO.RecordStatus, employeeDTO.IsEnabled, employeeDTO.DeductNSSFContribution, employeeDTO.DeductNHIFContribution, employeeDTO.BankCode, employeeDTO.BranchCode, employeeDTO.AccountName, employeeDTO.JobNumber, employeeDTO.DateOfEmployment, employeeDTO.JobTitle, employeeDTO.Gender, employeeDTO.DateOfBirth, employeeDTO.FirstName,  employeeDTO.OtherNames, employeeDTO.PhoneNumber, employeeDTO.Email,employeeDTO.IsExternalUser, employeeDTO.AllocatedLeaveDays);

            return employee;
        }

        public async Task<PageCollectionInfo<EmployeeDTO>> FindCustomerEmployeesFilterInPageAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns,
           bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = EmployeeSpecifications.DefaultSpec(searchString);

                ISpecification<Employee> spec = filter;

                var sortFields = sortedColumns != null ? sortedColumns.ToList() : new List<string> { "CreatedDate" };

                return await _employeeRepository.AllMatchingPagedAsync<EmployeeDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<EmployeeDTO>> FindEmployeeByCustomerIdAsync(Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns,
            bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = EmployeeSpecifications.MatchCustomerEmployeeWithUniqueCustomerId(customerId);

                ISpecification<Employee> spec = filter;

                var sortFields = sortedColumns != null ? sortedColumns.ToList() : new List<string> { "CreatedDate" };

                return await _employeeRepository.AllMatchingPagedAsync<EmployeeDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<EmployeeDTO> FindEmployeeByUserIdAsync(Guid userId, Guid? customerId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = EmployeeSpecifications.CustomerIdAndUserIdSpec(customerId,userId);

                ISpecification<Employee> spec = filter;

                var result = await _employeeRepository.AllMatchingAsync<EmployeeDTO>(spec, serviceHeader);

                return result?.FirstOrDefault() ;
            }
        }

        public async Task<List<EmployeeDTO>> FindEnabledEmployeesByCustomerIdAsync(Guid customerId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = EmployeeSpecifications.MatchCustomerEmployeeWithUniqueCustomerIdAndAreEnabled(customerId);

                ISpecification<Employee> spec = filter;

                return await _employeeRepository.AllMatchingAsync<EmployeeDTO>(spec, serviceHeader);
            }
        }
    }
}
