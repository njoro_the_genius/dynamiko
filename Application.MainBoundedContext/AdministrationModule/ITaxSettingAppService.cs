﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.AdministrationModule
{
    public interface ITaxSettingAppService
    {
        Task<TaxOrderDTO> AddNewTaxOrder(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateTaxOrder(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader);

        TaxOrderDTO FindTaxSettingById(Guid taxOrderDTOId, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TaxOrderDTO>> FindTaxOrderFilterInPagesAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<TaxRangesDTO>> FindGraduatedScalesAsync(Guid taxOrderId, ServiceHeader serviceHeader);

        Task<TaxOrderDTO> FindActiveTaxOrderAsync(ServiceHeader serviceHeader);
    }
}
