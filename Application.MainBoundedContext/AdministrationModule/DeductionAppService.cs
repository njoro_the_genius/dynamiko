﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.Seedwork;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.DeductionsAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.NHIFAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.NSSFAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;

namespace Application.MainBoundedContext.AdministrationModule
{
    public class DeductionAppService : IDeductionAppService
    {
        private readonly IRepository<NssfOrder> _nssfOrderRepository;

        private readonly IRepository<NssfGraduatedScale> _nssfGraduatedScaleRepository;

        private readonly IRepository<NhifOrder> _nhifOrderRepository;

        private readonly IRepository<NhifGraduatedScale> _nhifGraduatedScaleRepository;

        private readonly IRepository<Deduction> _deductionRepository;

        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public DeductionAppService(IRepository<NssfOrder> nssfOrderRepository, IRepository<NhifOrder> nhifOrderRepository, IRepository<NhifGraduatedScale> nhifGraduatedScaleRepository, IRepository<NssfGraduatedScale> nssfGraduatedScaleRepository,
           IRepository<Deduction> deductionRepository, IDbContextScopeFactory dbContextScopeFactory)
        {
            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));

            _nssfOrderRepository = nssfOrderRepository ?? throw new ArgumentNullException(nameof(nssfOrderRepository));

            _nhifOrderRepository = nhifOrderRepository ?? throw new ArgumentNullException(nameof(nhifOrderRepository));

            _nssfGraduatedScaleRepository = nssfGraduatedScaleRepository ?? throw new ArgumentNullException(nameof(nssfGraduatedScaleRepository));

            _nhifGraduatedScaleRepository = nhifGraduatedScaleRepository ?? throw new ArgumentNullException(nameof(nhifGraduatedScaleRepository));

            _deductionRepository = deductionRepository ?? throw new ArgumentNullException(nameof(deductionRepository));
        }
        #region NssfOrderDTO
        public async Task<NssfOrderDTO> AddNewNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader)
        {
            if (nssfOrderDTO == null) return null;

            nssfOrderDTO.ValidateAll();

            if (nssfOrderDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, nssfOrderDTO.ErrorMessages));

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var order = CreateNssfOrder(nssfOrderDTO, serviceHeader);

                _nssfOrderRepository.Add(order, serviceHeader);

                await dbContextScope.SaveChangesAsync(serviceHeader);

                return order.ProjectedAs<NssfOrderDTO>();
            }
        }
        public async Task<NssfOrderDTO> FindNssfOrderByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var order = await _nssfOrderRepository.GetAsync(id, serviceHeader, x => x.NssfGraduatedScales);

                if (order != null) //adapt
                {
                    NssfOrderDTO nssfOrderDTO = order.ProjectedAs<NssfOrderDTO>();
                    nssfOrderDTO.NssfGraduatedScaleDTOs = order.NssfGraduatedScales.ProjectedAsCollection<NssfGraduatedScaleDTO>();
                    return nssfOrderDTO;
                }
                return null;
            }
        }

        public async Task<PageCollectionInfo<NssfOrderDTO>> FindNssfOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = NssfOrderSpecifications.DefaultSpec(searchString);

                ISpecification<NssfOrder> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _nssfOrderRepository.AllMatchingPagedAsync<NssfOrderDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<List<NssfOrderDTO>> FindNssfOrders(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = NssfOrderSpecifications.DefaultSpec(null);

                ISpecification<NssfOrder> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _nssfOrderRepository.AllMatchingAsync(specification, serviceHeader);

                if (matching != null) //adapt
                {
                    return matching.ProjectedAsCollection<NssfOrderDTO>();
                }

                return null;
            }
        }


        public async Task<bool> UpdateNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader)
        {
            if (nssfOrderDTO == null) return false;

            nssfOrderDTO.ValidateAll();

            if (nssfOrderDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, nssfOrderDTO.ErrorMessages));

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = await _nssfOrderRepository.GetAsync(nssfOrderDTO.Id, serviceHeader);

                if (persisted != null)
                {
                    var nssfOrder = CreateNssfOrder(nssfOrderDTO, serviceHeader);

                    nssfOrder.ChangeCurrentIdentity(persisted.Id, persisted.SequentialId);

                    if (persisted.NssfGraduatedScales != null)
                    {
                        var items = persisted.NssfGraduatedScales.ToArray();

                        Array.ForEach(items, (x) =>
                        {
                            _nssfGraduatedScaleRepository.Remove(x, serviceHeader);
                        });
                    }

                    if (nssfOrderDTO.NssfGraduatedScaleDTOs.Any())
                    {
                        foreach (var item in nssfOrderDTO.NssfGraduatedScaleDTOs)
                        {
                            item.NssfOrderId = nssfOrderDTO.Id;

                            var newGraduatedScale = NssfOrderFactory.GetNewNssfGraduatedScale(nssfOrder.Id, item.LowerLimit, item.UpperLimit, item.Type, item.Value, serviceHeader.ApplicationUserName);

                            _nssfGraduatedScaleRepository.Add(newGraduatedScale, serviceHeader);
                        }
                    }

                    _nssfOrderRepository.Merge(persisted, nssfOrder, serviceHeader);

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;

                }

                return false;
            }
        }

        public async Task<NssfOrderDTO> FindActiveNssfOrderAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = NssfOrderSpecifications.ActiveSpec();

                ISpecification<NssfOrder> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _nssfOrderRepository.AllMatchingAsync(specification, serviceHeader, x => x.NssfGraduatedScales);

                if (matching != null) //adapt
                {
                    NssfOrderDTO nssfOrderDTO = matching.FirstOrDefault().ProjectedAs<NssfOrderDTO>();
                    nssfOrderDTO.NssfGraduatedScaleDTOs = matching.FirstOrDefault().NssfGraduatedScales.ProjectedAsCollection<NssfGraduatedScaleDTO>();
                    return nssfOrderDTO;
                }

                return null;
            }
        }

        #endregion

        #region NhifOrder
        public async Task<NhifOrderDTO> AddNewNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader)
        {
            if (nhifOrderDTO == null) return null;

            nhifOrderDTO.ValidateAll();

            if (nhifOrderDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, nhifOrderDTO.ErrorMessages));

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var order = CreateNhifOrder(nhifOrderDTO, serviceHeader);

                _nhifOrderRepository.Add(order, serviceHeader);

                await dbContextScope.SaveChangesAsync(serviceHeader);

                return order.ProjectedAs<NhifOrderDTO>();
            }
        }

        public async Task<NhifOrderDTO> FindNhifOrderByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var order = await _nhifOrderRepository.GetAsync(id, serviceHeader, x => x.NhifGraduatedScales);

                if (order != null) //adapt
                {
                    NhifOrderDTO nhifOrderDTO = order.ProjectedAs<NhifOrderDTO>();
                    List<NhifGraduatedScaleDTO> nhifGraduatedScaleDTOs = order.NhifGraduatedScales.ProjectedAsCollection<NhifGraduatedScaleDTO>();
                    nhifOrderDTO.NhifGraduatedScaleDTOs = nhifGraduatedScaleDTOs;
                    return nhifOrderDTO;
                }
                return null;
            }
        }

        public async Task<PageCollectionInfo<NhifOrderDTO>> FindNhifOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = NhifOrderSpecification.DefaultSpec(searchString);

                ISpecification<NhifOrder> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _nhifOrderRepository.AllMatchingPagedAsync<NhifOrderDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<List<NhifOrderDTO>> FindNhifOrders(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = NhifOrderSpecification.DefaultSpec(null);

                ISpecification<NhifOrder> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _nhifOrderRepository.AllMatchingAsync(specification, serviceHeader);

                if (matching != null) //adapt
                {
                    return matching.ProjectedAsCollection<NhifOrderDTO>();
                }

                return null;
            }
        }

        public async Task<bool> UpdateNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader)
        {
            if (nhifOrderDTO == null) return false;

            nhifOrderDTO.ValidateAll();

            if (nhifOrderDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, nhifOrderDTO.ErrorMessages));

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = await _nhifOrderRepository.GetAsync(nhifOrderDTO.Id, serviceHeader);

                if (persisted != null)
                {
                    var nssfOrder = CreateNhifOrder(nhifOrderDTO, serviceHeader);

                    nssfOrder.ChangeCurrentIdentity(persisted.Id, persisted.SequentialId);

                    if (persisted.NhifGraduatedScales != null)
                    {
                        var items = persisted.NhifGraduatedScales.ToArray();

                        Array.ForEach(items, (x) =>
                        {
                            _nhifGraduatedScaleRepository.Remove(x, serviceHeader);
                        });
                    }

                    if (nhifOrderDTO.NhifGraduatedScaleDTOs.Any())
                    {
                        foreach (var item in nhifOrderDTO.NhifGraduatedScaleDTOs)
                        {
                            item.NhifOrderId = nhifOrderDTO.Id;

                            var newGraduatedScale = NhifOrderFactory.GetNewNhifGraduatedScale(nssfOrder.Id, item.LowerLimit, item.UpperLimit, item.Type, item.Value, serviceHeader.ApplicationUserName);

                            _nhifGraduatedScaleRepository.Add(newGraduatedScale, serviceHeader);
                        }
                    }

                    _nhifOrderRepository.Merge(persisted, nssfOrder, serviceHeader);

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;

                }

                return false;
            }
        }

        public async Task<NhifOrderDTO> FindActiveNhifOrderAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = NhifOrderSpecification.ActiveSpec();

                ISpecification<NhifOrder> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _nhifOrderRepository.AllMatchingAsync(specification, serviceHeader, x => x.NhifGraduatedScales);

                if (matching != null) //adapt
                {
                    NhifOrderDTO nhifOrderDTO = matching.FirstOrDefault().ProjectedAs<NhifOrderDTO>();
                    nhifOrderDTO.NhifGraduatedScaleDTOs = matching.FirstOrDefault().NhifGraduatedScales.ProjectedAsCollection<NhifGraduatedScaleDTO>();

                    return nhifOrderDTO;
                }

                return null;
            }
        }

        #endregion

        #region Deduction

        public async Task<DeductionDTO> AddNewDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader)
        {
            if (deductionDTO == null) return null;

            deductionDTO.ValidateAll();

            if (deductionDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, deductionDTO.ErrorMessages));

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var order = DeductionFactory.Create(deductionDTO.EmployeeId, deductionDTO.DeductionType, deductionDTO.Amount, deductionDTO.Frequency, deductionDTO.IsActive, deductionDTO.StartDate, deductionDTO.EndDate);

                _deductionRepository.Add(order, serviceHeader);

                await dbContextScope.SaveChangesAsync(serviceHeader);

                return order.ProjectedAs<DeductionDTO>();
            }
        }

        public async Task<bool> UpdateDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader)
        {
            if (deductionDTO == null) return false;

            deductionDTO.ValidateAll();

            if (deductionDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, deductionDTO.ErrorMessages));

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = await _deductionRepository.GetAsync(deductionDTO.Id, serviceHeader);

                if (persisted != null)
                {
                    var deduction = DeductionFactory.Create(deductionDTO.EmployeeId, deductionDTO.DeductionType, deductionDTO.Amount, deductionDTO.Frequency, deductionDTO.IsActive, deductionDTO.StartDate, deductionDTO.EndDate);

                    deduction.ChangeCurrentIdentity(persisted.Id, persisted.SequentialId);

                    _deductionRepository.Merge(persisted, deduction, serviceHeader);

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;

                }

                return false;
            }
        }

        public async Task<DeductionDTO> FindDeductionByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var deduction = await _deductionRepository.GetAsync(id, serviceHeader);

                if (deduction != null) //adapt
                {
                    return deduction.ProjectedAs<DeductionDTO>();
                }
                return null;
            }
        }

        public async Task<PageCollectionInfo<DeductionDTO>> FindDeductionFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {

            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = DeductionSpecification.DefaultSpec(searchString);

                ISpecification<Deduction> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _deductionRepository.AllMatchingPagedAsync<DeductionDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<DeductionDTO>> FindDeductionByCustomerIdFilterInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = DeductionSpecification.GetDeductionsByCustomerId(searchString, customerId);

                ISpecification<Deduction> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _deductionRepository.AllMatchingPagedAsync<DeductionDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }


        public async Task<List<DeductionDTO>> FindDeduction(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = DeductionSpecification.DefaultSpec(null);

                ISpecification<Deduction> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _deductionRepository.AllMatchingAsync(specification, serviceHeader);

                if (matching != null) //adapt
                {
                    return matching.ProjectedAsCollection<DeductionDTO>();
                }

                return null;
            }
        }

        public async Task<List<DeductionDTO>> FindDeductionByCustomerIdDue(Guid customerId, Guid employeeId, DateTime? startDate, DateTime? endDate, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = DeductionSpecification.GetDeductionsByCustomerIdInDateRange(null, customerId, employeeId, startDate, endDate);

                ISpecification<Deduction> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _deductionRepository.AllMatchingAsync(specification, serviceHeader);

                if (matching != null) //adapt
                {
                    return matching.ProjectedAsCollection<DeductionDTO>();
                }

                return null;
            }
        }

        public async Task<DeductionDTO> FindActiveDeductionAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = DeductionSpecification.ActiveSpec();

                ISpecification<Deduction> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _deductionRepository.AllMatchingAsync(specification, serviceHeader);

                if (matching != null) //adapt
                {
                    return matching.FirstOrDefault().ProjectedAs<DeductionDTO>();
                }

                return null;
            }
        }
        #endregion

        #region Helpers
        NssfOrder CreateNssfOrder(NssfOrderDTO nssfOrder, ServiceHeader serviceHeader)
        {
            var nssfOrderEntity = NssfOrderFactory.GetNssfOrder(nssfOrder.Description, nssfOrder.IsActive, (byte)RecordStatus.New, serviceHeader.ApplicationUserName);

            if (nssfOrder.NssfGraduatedScaleDTOs != null)
            {
                foreach (var item in nssfOrder.NssfGraduatedScaleDTOs)
                {
                    nssfOrderEntity.AddNewNssfGraduatedScale(nssfOrderEntity.Id, item.LowerLimit, item.UpperLimit, item.Type, item.Value, serviceHeader.ApplicationUserName);
                }
            }

            return nssfOrderEntity;
        }

        NhifOrder CreateNhifOrder(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader)
        {
            var nhifOrderEntity = NhifOrderFactory.GetNhifOrder(nhifOrderDTO.Description, nhifOrderDTO.IsActive, (byte)RecordStatus.New, serviceHeader.ApplicationUserName);

            if (nhifOrderDTO.NhifGraduatedScaleDTOs != null)
            {
                foreach (var item in nhifOrderDTO.NhifGraduatedScaleDTOs)
                {
                    nhifOrderEntity.AddNewNhifGraduatedScale(nhifOrderEntity.Id, item.LowerLimit, item.UpperLimit, item.Type, item.Value, serviceHeader.ApplicationUserName);
                }
            }

            return nhifOrderEntity;
        }





        #endregion
    }
}
