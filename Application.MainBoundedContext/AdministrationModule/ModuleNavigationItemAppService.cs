﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.MainBoundedContext.Services;
using Application.Seedwork;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemAgg;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.ModuleNavigationItemInRoleAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.AdministrationModule
{
    public class ModuleNavigationItemAppService : IModuleNavigationItemAppService
    {
        private readonly IRepository<ModuleNavigationItem> _moduleNavigationItemRepository;

        private readonly IRepository<ModuleNavigationItemInRole> _moduleNavigationItemInRoleRepository;

        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        private readonly ISqlCommandAppService _sqlCommandAppService;

        public ModuleNavigationItemAppService(IRepository<ModuleNavigationItem> moduleNavigationItemRepository,
            IDbContextScopeFactory dbContextScopeFactory,
            IRepository<ModuleNavigationItemInRole> moduleNavigationItemInRoleRepository,
            ISqlCommandAppService sqlCommandAppService)
        {
            if (moduleNavigationItemInRoleRepository == null)
                throw new ArgumentNullException(nameof(moduleNavigationItemInRoleRepository));

            if (moduleNavigationItemRepository == null)
                throw new ArgumentNullException(nameof(moduleNavigationItemRepository));

            if (dbContextScopeFactory == null)
                throw new ArgumentNullException(nameof(dbContextScopeFactory));

            if (sqlCommandAppService == null)
                throw new ArgumentNullException(nameof(sqlCommandAppService));

            _dbContextScopeFactory = dbContextScopeFactory;
            _moduleNavigationItemInRoleRepository = moduleNavigationItemInRoleRepository;
            _moduleNavigationItemRepository = moduleNavigationItemRepository;
            _sqlCommandAppService = sqlCommandAppService;
        }

        #region ModuleNavigationItem

        public async Task<bool> AddModuleNavigationItemAsync(List<ModuleNavigationItemDTO> moduleNavigationItem, ServiceHeader serviceHeader)
        {
            if (moduleNavigationItem == null)
                return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var moduleNavigationItembinding = moduleNavigationItem.ProjectedAs<List<ModuleNavigationItemBindingModel>>();

                if (moduleNavigationItembinding.Any())
                {
                    foreach (var item in moduleNavigationItembinding)
                    {
                        item.ValidateAll();

                        if (item.HasErrors)
                            throw new ArgumentException(string.Join(Environment.NewLine, item.ErrorMessages));

                        var validateItemExistence = await FindModuleNavigationItemByControllerNameAndActionNameAsync(item.ControllerName, item.ActionName, serviceHeader);

                        if (validateItemExistence == null)
                        {
                            var newModuleNavigationItem = ModuleNavigationItemFactory.AddModuleNavigationItem(item.ControllerName, item.ActionName, serviceHeader.ApplicationUserName);

                            _moduleNavigationItemRepository.Add(newModuleNavigationItem, serviceHeader);
                        }

                    }

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;
                }

                return false;
            }
        }

        public async Task<bool> BulkInsertModuleNavigationItemAsync(List<Guid> modulesNavigationIds, string roleId,
            ServiceHeader serviceHeader)
        {
            var result = default(bool);

            var existingModules = await FindModuleNavigationItemInRoleByRoleAsync(roleId, serviceHeader);

            if ((modulesNavigationIds != null) && !string.IsNullOrWhiteSpace(roleId))
            {
                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var moduleNavigationItemInRole = new List<ModuleNavigationItemInRole>();

                    foreach (var modulesNavigation in modulesNavigationIds)
                    {
                        var moduleNavigationItem = await FindModuleNavigationItemByIdAsync(modulesNavigation, serviceHeader);

                        if (moduleNavigationItem != null)
                        {
                            #region Remove existing 

                            var exists = existingModules == null ? null : existingModules.Where(x => x.ModuleNavigationItemId == modulesNavigation && x.RoleId == roleId).FirstOrDefault();

                            if (exists != null)
                                existingModules.Remove(exists);

                            #endregion

                            #region Add

                            if (exists == null)
                            {
                                var moduleNavigationItemInRoleFactory = ModuleNavigationItemInRoleFactory.AddModuleNavigationItemInRole(modulesNavigation, roleId, serviceHeader.ApplicationUserName);

                                moduleNavigationItemInRole.Add(moduleNavigationItemInRoleFactory);
                            }

                            #endregion
                        }
                    }

                    if (moduleNavigationItemInRole.Any())
                    {
                        foreach (var module in moduleNavigationItemInRole)
                        {
                            _moduleNavigationItemInRoleRepository.Add(module, serviceHeader);
                        }

                        result = dbContextScope.SaveChanges(serviceHeader) >= 0;
                    }
                }
            }

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                #region Remove existing 

                if (existingModules != null && existingModules.Any())
                {
                    foreach (var item in existingModules)
                    {
                        var module = _moduleNavigationItemInRoleRepository.Get(item.Id, serviceHeader);

                        _moduleNavigationItemInRoleRepository.Remove(module, serviceHeader);
                    }
                }

                #endregion

                result = dbContextScope.SaveChanges(serviceHeader) >= 0;
            }

            return result;
        }

        public async Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemsAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var specification = ModuleNavigationItemSpecifications.DefaultSpecification();

                var navigationItems = await _moduleNavigationItemRepository.AllMatchingAsync(specification, serviceHeader);

                var result = !navigationItems.Any() ? null : navigationItems.ProjectedAsCollection<ModuleNavigationItemDTO>();

                return result;
            }
        }

        public async Task<ModuleNavigationItemDTO> FindModuleNavigationItemByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var module = await _moduleNavigationItemRepository.GetAsync(id, serviceHeader);

                return module == null ? null : module.ProjectedAs<ModuleNavigationItemDTO>();
            }
        }

        public async Task<ModuleNavigationItemDTO> FindModuleNavigationItemInRoleByItemIdAndRoleAsync(Guid id, string roleId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var specification = ModuleNavigationItemInRoleSpecifications.ModuleNavigationItemWithItemIdAndRole(id, roleId);

                var module = await _moduleNavigationItemRepository.GetAsync(id, serviceHeader);

                return module == null ? null : module.ProjectedAs<ModuleNavigationItemDTO>();
            }
        }

        public async Task<PageCollectionInfo<ModuleNavigationItemDTO>> FindModuleNavigationItemsFilterPageCollectionInfoAsync(int startIndex, int pageSize, IList<string> sortedColumns,
            string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = ModuleNavigationItemSpecifications.DefaultSpecification();

                ISpecification<ModuleNavigationItem> spec = filter;

                var sortFields = new List<string> { "CreatedDate" };

                return await _moduleNavigationItemRepository.AllMatchingPagedAsync<ModuleNavigationItemDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemByControllerNameAndActionNameAsync(string controllerName, string actionName, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.Create())
            {
                if (!string.IsNullOrWhiteSpace(controllerName) || !string.IsNullOrWhiteSpace(actionName))
                {
                    // get the specification
                    var filter = ModuleNavigationItemSpecifications.ModuleNavigationItemWithComtrollerAndActionNames(controllerName, actionName);

                    ISpecification<ModuleNavigationItem> spec = filter;

                    //Query this criteria
                    var moduleNavigationItem = await _moduleNavigationItemRepository.AllMatchingAsync(spec, serviceHeader);

                    if (moduleNavigationItem.Any())
                    {
                        return moduleNavigationItem.ProjectedAsCollection<ModuleNavigationItemDTO>();
                    }
                }

                return null;
            }
        }

        public async Task<List<ModuleNavigationItemDTO>> FindModuleNavigationActionControllerNameAsync(string controllerName, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                if (!string.IsNullOrWhiteSpace(controllerName))
                {
                    // get the specification
                    var filter = ModuleNavigationItemSpecifications.ModuleNavigationItemWithControllerName(controllerName);

                    ISpecification<ModuleNavigationItem> spec = filter;

                    //Query this criteria
                    var moduleNavigationItem = await _moduleNavigationItemRepository.AllMatchingAsync(spec, serviceHeader);

                    if (moduleNavigationItem.Any())
                    {
                        return moduleNavigationItem.ProjectedAsCollection<ModuleNavigationItemDTO>();
                    }
                }

                return null;
            }
        }

        public async Task<bool> ValidateAccessPermissionAsync(string controllerName, string actionName, string[] roleId, ServiceHeader serviceHeader)
        {
            var navigationModule = new ModuleNavigationItemDTO();

            if (!string.IsNullOrWhiteSpace(controllerName) || !string.IsNullOrWhiteSpace(actionName) || roleId != null)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    // get the specification
                    var filter1 =
                        ModuleNavigationItemSpecifications.ModuleNavigationItemWithComtrollerAndActionNames(controllerName, actionName);

                    ISpecification<ModuleNavigationItem> spec1 = filter1;

                    //Query this criteria
                    var moduleNavigationItems = _moduleNavigationItemRepository.AllMatching(spec1, serviceHeader);

                    var moduleNavigationItem = moduleNavigationItems.FirstOrDefault();

                    if (moduleNavigationItem != null)
                    {
                        navigationModule = moduleNavigationItem.ProjectedAs<ModuleNavigationItemDTO>();
                    }

                    if (navigationModule != null)
                    {
                        // get the specification
                        var filter = ModuleNavigationItemInRoleSpecifications.ModuleNavigationItemWithRoleIdAndModuleNavigationId(navigationModule.Id);

                        ISpecification<ModuleNavigationItemInRole> spec = filter;

                        //Query this criteria
                        var moduleNavigationItemInRole = await _moduleNavigationItemInRoleRepository.AllMatchingAsync(spec, serviceHeader);

                        if (moduleNavigationItemInRole.Any())
                        {
                            return moduleNavigationItemInRole.Select(x => x.RoleId).Intersect(roleId).Any();
                        }
                    }
                    else return false;
                }
            }

            return false;
        }

        #endregion

        #region ModuleNavigationItemInRole

        public async Task<bool> BulkInsertModuleNavigationItemInRolesAsync(List<Guid> moduleNavigationItemInRole, string roleId, ServiceHeader serviceHeader)
        {
            var result = default(bool);

            if ((moduleNavigationItemInRole != null || moduleNavigationItemInRole.Any()) && !string.IsNullOrWhiteSpace(roleId))
            {
                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var moduleNavigationItemInRoleList = new List<ModuleNavigationItemInRole>();

                    foreach (var modulesNavigationId in moduleNavigationItemInRole)
                    {
                        var moduleNavigationItem = await FindModuleNavigationItemByIdAsync(modulesNavigationId, serviceHeader);

                        if (moduleNavigationItem != null)
                        {
                            var moduleNavigationItemInRoleFactory =
                                ModuleNavigationItemInRoleFactory.AddModuleNavigationItemInRole(modulesNavigationId, roleId,
                                    serviceHeader.ApplicationUserName);
                            if (!await RoleHasPermissionAsync(modulesNavigationId, roleId, serviceHeader))
                            {
                                moduleNavigationItemInRoleList.Add(moduleNavigationItemInRoleFactory);
                            }
                        }
                    }

                    if (moduleNavigationItemInRole.Any())
                    {
                        result = _sqlCommandAppService.BulkInsert(DefaultSettings.Instance.ModuleNavigationItemsInRoleTableName, moduleNavigationItemInRoleList, serviceHeader);
                    }
                }
            }

            return result;
        }

        public async Task<bool> AddModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader)
        {
            if (moduleNavigationItemInRole == null)
                return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                if (moduleNavigationItemInRole.Count > 0 && moduleNavigationItemInRole.Any())
                {
                    //This method will be used to add or update existing permissions
                    //First, check if there is any permissions set and if any, remove and add new permissions

                    var currentmoduleNavigationItemInRoles = await FindModuleNavigationItemInRoleByModuleNavigationIdAsync(moduleNavigationItemInRole[0].ModuleNavigationItemId, serviceHeader);

                    if (currentmoduleNavigationItemInRoles != null && currentmoduleNavigationItemInRoles.Any())
                    {
                        //remove first before adding if any

                        var currentmoduleNavigationItemInRolesArray = currentmoduleNavigationItemInRoles.ToArray();

                        Array.ForEach(currentmoduleNavigationItemInRolesArray, async item =>
                        {
                            var removeModuleNavigationItemInRole = await _moduleNavigationItemInRoleRepository.GetAsync(item.Id, serviceHeader);

                            _moduleNavigationItemInRoleRepository.Remove(removeModuleNavigationItemInRole, serviceHeader);
                        });
                    }

                    var navigationItemsInArray = moduleNavigationItemInRole.ToArray();

                    Array.ForEach(navigationItemsInArray, item =>
                    {
                        var itemBindingModel = item.ProjectedAs<ModuleNavigationItemInRoleBindingModel>();

                        itemBindingModel.ValidateAll();

                        if (itemBindingModel.HasErrors)
                            throw new ArgumentException(string.Join(Environment.NewLine, itemBindingModel.ErrorMessages));

                        var newModuleNavigationItemInRole =
                            ModuleNavigationItemInRoleFactory.AddModuleNavigationItemInRole(item.ModuleNavigationItemId,
                                item.RoleId, serviceHeader.ApplicationUserName);

                        _moduleNavigationItemInRoleRepository.Add(newModuleNavigationItemInRole, serviceHeader);
                    });

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;
                }

                return false;
            }
        }

        public async Task<ModuleNavigationItemInRoleDTO> FindModuleNavigationItemInRoleByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var module = await _moduleNavigationItemInRoleRepository.GetAsync(id, serviceHeader);

                return module == null ? null : module.ProjectedAs<ModuleNavigationItemInRoleDTO>();
            }
        }

        public async Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var specification = ModuleNavigationItemInRoleSpecifications.DefaultSpecification();

                var module = await _moduleNavigationItemInRoleRepository.AllMatchingAsync(specification, serviceHeader);

                return module == null ? null : module.ProjectedAsCollection<ModuleNavigationItemInRoleDTO>();
            }
        }

        public async Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByRoleAsync(string roleId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var specification = ModuleNavigationItemInRoleSpecifications.ModuleNavigationItemWithRoleId(roleId);

                var navigationItems = await _moduleNavigationItemInRoleRepository.AllMatchingAsync(specification, serviceHeader);

                return !navigationItems.Any() ? null : navigationItems.ProjectedAsCollection<ModuleNavigationItemInRoleDTO>();
            }
        }

        public async Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationIdAsync(Guid moduleNavigationId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var specification = ModuleNavigationItemInRoleSpecifications.ModuleNavigationItemWithRoleIdAndModuleNavigationId(moduleNavigationId);

                var navigationItems = await _moduleNavigationItemInRoleRepository.AllMatchingAsync(specification, serviceHeader);

                return !navigationItems.Any() ? null : navigationItems.ProjectedAsCollection<ModuleNavigationItemInRoleDTO>();
            }
        }

        public async Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationByControllerNameAndRoleIdAsync(string controllerName, string roleId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var specification = ModuleNavigationItemInRoleSpecifications.ModuleNavigationItemWithRoleIdAndControllerNameAndRoleId(controllerName, roleId);

                var navigationItems = await _moduleNavigationItemInRoleRepository.AllMatchingAsync(specification, serviceHeader);

                return !navigationItems.Any() ? null : navigationItems.ProjectedAsCollection<ModuleNavigationItemInRoleDTO>();
            }
        }

        public async Task<PageCollectionInfo<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRolePageCollectionInfoAsync(string roleId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = ModuleNavigationItemInRoleSpecifications.ModuleNavigationItemWithRoleId(roleId);

                ISpecification<ModuleNavigationItemInRole> spec = filter;

                var sortFields = new List<string> { "CreatedDate" };

                return await _moduleNavigationItemInRoleRepository.AllMatchingPagedAsync<ModuleNavigationItemInRoleDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);

            }
        }

        public async Task<bool> RemoveModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader)
        {
            if (moduleNavigationItemInRole == null)
                return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                if (moduleNavigationItemInRole != null && moduleNavigationItemInRole.Any())
                {
                    Array.ForEach(moduleNavigationItemInRole.ToArray(), async item =>
                    {
                        var removeModuleNavigationItemInRole = await _moduleNavigationItemInRoleRepository.GetAsync(item.Id, serviceHeader);

                        _moduleNavigationItemInRoleRepository.Remove(removeModuleNavigationItemInRole, serviceHeader);
                    });
                }

                return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;
            }
        }

        public async Task<bool> UpdateModuleNavigationItemInRoleListAsync(ModuleNavigationItemInRoleDTO moduleNavigationItemInRole, ServiceHeader serviceHeader)
        {
            if (moduleNavigationItemInRole == null)
                return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = await _moduleNavigationItemInRoleRepository.GetAsync(moduleNavigationItemInRole.Id, serviceHeader);

                if (persisted == null) return false;

                var current = ModuleNavigationItemInRoleFactory.AddModuleNavigationItemInRole(moduleNavigationItemInRole.ModuleNavigationItemId, moduleNavigationItemInRole.RoleId, moduleNavigationItemInRole.CreatedBy);

                current.ChangeCurrentIdentity(persisted.Id, persisted.SequentialId);

                _moduleNavigationItemInRoleRepository.Merge(persisted, current, serviceHeader);

                return await dbContextScope.SaveChangesAsync(serviceHeader) > 0;
            }
        }

        public async Task<List<RolePermissionDTO>> GetAccessPermissionsPerRolesAsync(string[] roleIds, string[] currentUserRole, ServiceHeader serviceHeader)
        {
            var rolePermissionList = new List<RolePermissionDTO>();

            if (roleIds != null && roleIds.Length > 0)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    foreach (var roleId in roleIds.ToList())
                    {
                        var permissionsList = await FindModuleNavigationItemInRoleByRoleAsync(roleId, serviceHeader);

                        if (permissionsList != null && permissionsList.Any())
                        {
                            foreach (var item in permissionsList)
                            {
                                var controllerActionsList = new List<ControllerAction>
                                {
                                    new ControllerAction
                                    {
                                        ActionName = item.ModuleNavigationItemActionName.ToLower(),
                                        ControllerName = item.ModuleNavigationItemControllerName.ToLower()
                                    }
                                };

                                rolePermissionList.Add(new RolePermissionDTO
                                {
                                    RoleId = item.RoleId,
                                    CurrentUserRoles = currentUserRole,
                                    ControllerActionList = controllerActionsList

                                });
                            }
                        }
                    }

                    return rolePermissionList;
                }
            }

            return rolePermissionList;
        }

        public async Task<bool> RoleHasPermissionAsync(Guid moduleNavigationItemId, string roleId, ServiceHeader serviceHeader)
        {
            if (!string.IsNullOrWhiteSpace(roleId) && moduleNavigationItemId != Guid.Empty)
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var specification = ModuleNavigationItemInRoleSpecifications.ModuleNavigationItemWithRoleIdAndModuleNavigationId(moduleNavigationItemId);

                    var navigationItems = await _moduleNavigationItemInRoleRepository.AllMatchingAsync(specification, serviceHeader);

                    return navigationItems.Any(x => x.RoleId == roleId);
                }
            }
            return false;
        }

        #endregion
    }
}
