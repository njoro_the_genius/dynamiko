﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.Seedwork;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.LeaveAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;

namespace Application.MainBoundedContext.AdministrationModule
{
    public class LeaveAppService : ILeaveAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        private readonly IRepository<Leave> _leaveRepository;

        public LeaveAppService(IDbContextScopeFactory dbContextScopeFactory, IRepository<Leave> leaveRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));

            _leaveRepository = leaveRepository ?? throw new ArgumentNullException(nameof(leaveRepository));
        }

        public async Task<LeaveDTO> AddNewLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader)
        {
            if (leaveDTO == null) return null;
            leaveDTO.ValidateAll();
            if (leaveDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, leaveDTO.ErrorMessages));

            using (var dbContext = _dbContextScopeFactory.Create())
            {
                var leaveEntity = LeaveFactory.Create(leaveDTO.EmployeeId, leaveDTO.Period, leaveDTO.DateApplied, leaveDTO.DateApproved, leaveDTO.StartDate, leaveDTO.EndDate, leaveDTO.ApprovedBy, leaveDTO.Reason, serviceHeader.ApplicationUserName,leaveDTO.RecordStatus);
                _leaveRepository.Add(leaveEntity, serviceHeader);
                await dbContext.SaveChangesAsync(serviceHeader);
                return leaveEntity.ProjectedAs<LeaveDTO>();
            }
        }

        public async Task<LeaveDTO> FindLeaveByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var leave = await _leaveRepository.GetAsync(id, serviceHeader);

                if (leave != null) //adapt
                {
                    return leave.ProjectedAs<LeaveDTO>();
                }
                return null;
            }
        }

        public async Task<LeaveDTO> FindLeaveByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = LeaveSpecification.GetLeaveByEmployeeId(employeeId);

                ISpecification<Leave> spec = filter;

                var result = await _leaveRepository.AllMatchingAsync<LeaveDTO>(spec, serviceHeader);

                return result?.FirstOrDefault();
            }
        }
        public async Task<List<LeaveDTO>> FindLeavesByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnlyWithTransaction(IsolationLevel.ReadUncommitted))
            {
                var filter = LeaveSpecification.GetLeaveByEmployeeId(employeeId);

                ISpecification<Leave> spec = filter;

                var result = await _leaveRepository.AllMatchingAsync<LeaveDTO>(spec, serviceHeader);

                return result?.ToList();
            }
        }

        public async Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByCustomerInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = LeaveSpecification.GetLeaveByCustomerId(searchString,customerId);

                ISpecification<Leave> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _leaveRepository.AllMatchingPagedAsync<LeaveDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByEmployeeInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = LeaveSpecification.GetLeaveByEmployeeId(searchString, employeeId);

                ISpecification<Leave> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var leave =  await _leaveRepository.AllMatchingPagedAsync<LeaveDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);

                return leave;
            }
        }

        public async Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = LeaveSpecification.DefaultSpec(searchString);

                ISpecification<Leave> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _leaveRepository.AllMatchingPagedAsync<LeaveDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<List<LeaveDTO>> FindLeaves(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = LeaveSpecification.DefaultSpec(null);

                ISpecification<Leave> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _leaveRepository.AllMatchingAsync(specification, serviceHeader);

                if (matching != null) //adapt
                {
                    return matching.ProjectedAsCollection<LeaveDTO>();
                }

                return null;
            }
        }

        public async Task<bool> UpdateLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader)
        {
            if (leaveDTO == null)
                return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = _leaveRepository.Get(leaveDTO.Id, serviceHeader);

                if (persisted == null) return false;

                var current = LeaveFactory.Create(leaveDTO.EmployeeId, leaveDTO.Period, leaveDTO.DateApplied, leaveDTO.DateApproved, leaveDTO.StartDate, leaveDTO.EndDate, leaveDTO.ApprovedBy, leaveDTO.Reason, serviceHeader.ApplicationUserName, leaveDTO.RecordStatus);

                current.ChangeCurrentIdentity(persisted.Id, persisted.ClusteredId, persisted.CreatedBy, persisted.CreatedDate);

                _leaveRepository.Merge(persisted, current, serviceHeader);

                return await dbContextScope.SaveChangesAsync(serviceHeader) > 0;
            }
        }
    }
}
