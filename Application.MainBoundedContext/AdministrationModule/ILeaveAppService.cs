﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.AdministrationModule
{
    public interface ILeaveAppService
    {
        Task<LeaveDTO> AddNewLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader);

        Task<LeaveDTO> FindLeaveByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<LeaveDTO> FindLeaveByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader);

        Task<List<LeaveDTO>> FindLeavesByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByCustomerInPageAsync(Guid customerId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByEmployeeInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<LeaveDTO>> FindLeaves(ServiceHeader serviceHeader);
    }
}
