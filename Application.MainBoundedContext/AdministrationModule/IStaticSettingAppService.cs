﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;

namespace Application.MainBoundedContext.AdministrationModule
{
    public interface IStaticSettingAppService
    {
        StaticSettingDTO AddNewStaticSetting(StaticSettingDTO staticSettingDTO, ServiceHeader serviceHeader);

        bool UpdateStaticSetting(StaticSettingDTO staticSettingDTO, ServiceHeader serviceHeader);

        List<StaticSettingDTO> FindStaticSettings(string text, ServiceHeader serviceHeader);

        StaticSettingDTO FindstaticSettingById(Guid staticSettingId, ServiceHeader serviceHeader);

        StaticSettingDTO FindSettingByKey(string keyName, ServiceHeader serviceHeader);

        PageCollectionInfo<StaticSettingDTO> FindStaticSettingsFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        bool RemoveStaticSetting(Guid staticSettingId, ServiceHeader serviceHeader);
        
    }
}
