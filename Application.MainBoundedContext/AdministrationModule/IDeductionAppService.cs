﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.AdministrationModule
{
    public interface IDeductionAppService
    {
        #region NssfOrderDTO
        Task<NssfOrderDTO> AddNewNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader);

        Task<NssfOrderDTO> FindNssfOrderByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<NssfOrderDTO> FindActiveNssfOrderAsync(ServiceHeader serviceHeader);

        Task<PageCollectionInfo<NssfOrderDTO>> FindNssfOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<NssfOrderDTO>> FindNssfOrders(ServiceHeader serviceHeader);
        #endregion

        #region NhifOrderDTO
        Task<NhifOrderDTO> AddNewNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader);

        Task<NhifOrderDTO> FindNhifOrderByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<NhifOrderDTO>> FindNhifOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<NhifOrderDTO>> FindNhifOrders(ServiceHeader serviceHeader);

        Task<NhifOrderDTO> FindActiveNhifOrderAsync(ServiceHeader serviceHeader);
        #endregion

        #region DeductionDTO
        Task<DeductionDTO> AddNewDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader);

        Task<DeductionDTO> FindDeductionByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<DeductionDTO>> FindDeductionFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<DeductionDTO>> FindDeductionByCustomerIdFilterInPageAsync(Guid customerId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<DeductionDTO>> FindDeduction(ServiceHeader serviceHeader);

        Task<List<DeductionDTO>> FindDeductionByCustomerIdDue(Guid customerId,Guid employeeId, DateTime? startDate, DateTime? endDate,ServiceHeader serviceHeader);

        Task<DeductionDTO> FindActiveDeductionAsync(ServiceHeader serviceHeader);
        #endregion
    }
}
