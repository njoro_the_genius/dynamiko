﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.Seedwork;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.TaxAgg;
using Domain.MainBoundedContext.ValueObjects;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.AdministrationModule
{
    public class TaxSettingAppService : ITaxSettingAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        private readonly IRepository<TaxOrder> _taxOrderRepository;

        private readonly IRepository<TaxGraduatedScale> _taxRangeRepository;

        public TaxSettingAppService(
            IDbContextScopeFactory dbContextScopeFactory,
            IRepository<TaxOrder> taxOrderRepository,
            IRepository<TaxGraduatedScale> graduatedScaleRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));

            _taxOrderRepository = taxOrderRepository ?? throw new ArgumentNullException(nameof(taxOrderRepository));

            _taxRangeRepository = graduatedScaleRepository ?? throw new ArgumentNullException(nameof(graduatedScaleRepository));

        }

        #region TaxOrderDTO

        public async Task<TaxOrderDTO> AddNewTaxOrder(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader)
        {
            if (taxOrderDTO != null)
            {

                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    Specification<TaxOrder> specification = TaxSpecifications.TaxOrderWithFullText(taxOrderDTO.Description);

                    var chargeWithExactName = await _taxOrderRepository.AllMatchingAsync<TaxOrderDTO>(specification, serviceHeader);

                    if (chargeWithExactName.Any())
                        throw new Exception($"Tax Order  with name '{taxOrderDTO.Description}' exists already!");

                    taxOrderDTO.IsActive = true;

                    var taxOrder = CreateNewTaxOrder(taxOrderDTO);

                    _taxOrderRepository.Add(taxOrder, serviceHeader);

                    await dbContextScope.SaveChangesAsync(serviceHeader);

                    return taxOrder.ProjectedAs<TaxOrderDTO>();
                }
            }

            return null;
        }

        public async Task<bool> UpdateTaxOrder(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = await _taxOrderRepository.GetAsync(taxOrderDTO.Id, serviceHeader);

                if (persisted != null)
                {
                    var current = TaxOrderFactory.CreateTaxOrder(taxOrderDTO.Description, taxOrderDTO.IsActive, taxOrderDTO.CreatedBy);

                    current.ChangeCurrentIdentity(persisted.Id, persisted.SequentialId);

                    if (persisted.GraduatedScales != null)
                    {
                        var items = persisted.GraduatedScales.ToArray();

                        Array.ForEach(items, (x) =>
                        {
                            _taxRangeRepository.Remove(x, serviceHeader);
                        });
                    }

                    if (taxOrderDTO.TaxRangesItemList.Any())
                    {
                        foreach (var item in taxOrderDTO.TaxRangesItemList)
                        {
                            item.TaxOrderId = taxOrderDTO.Id;

                            var range = new Range(item.RangeLowerLimit, item.RangeUpperLimit);

                            var newGraduatedScale = TaxOrderFactory.AddNewGraduatedScale(persisted.Id, item.Type, item.TaxValue, range, item.CreatedBy);

                            _taxRangeRepository.Add(newGraduatedScale, serviceHeader);
                        }
                    }

                    _taxOrderRepository.Merge(persisted, current, serviceHeader);

                    return await dbContextScope.SaveChangesAsync(serviceHeader) >= 0;
                }
                return false;
            }
        }

        public TaxOrderDTO FindTaxSettingById(Guid taxOrderDTOId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var setting = _taxOrderRepository.Get(taxOrderDTOId, serviceHeader);

                return setting != null ? setting.ProjectedAs<TaxOrderDTO>() : null;
            }
        }

        public async Task<PageCollectionInfo<TaxOrderDTO>> FindTaxOrderFilterInPagesAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending,
           ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = TaxSpecifications.TaxOrderWithFullText(searchString);

                ISpecification<TaxOrder> spec = filter;

                var sortFields = new List<string> { "CreatedDate" };

                return await _taxOrderRepository.AllMatchingPagedAsync<TaxOrderDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }


        #region tax ranges

        public async Task<List<TaxRangesDTO>> FindGraduatedScalesAsync(Guid taxOrderId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = TaxSpecifications.GraduatedScaleWithTaxId(taxOrderId);

                ISpecification<TaxGraduatedScale> spec = filter;

                return await _taxRangeRepository.AllMatchingAsync<TaxRangesDTO>(spec, serviceHeader);
            }
        }

        public async Task<TaxOrderDTO> FindActiveTaxOrderAsync(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = TaxSpecifications.ActiveSpec();

                ISpecification<TaxOrder> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _taxOrderRepository.AllMatchingAsync(specification, serviceHeader, x => x.GraduatedScales);

                if (matching != null) //adapt
                {
                    TaxOrderDTO taxOrderDTO = matching.FirstOrDefault().ProjectedAs<TaxOrderDTO>();
                    taxOrderDTO.TaxRangeItems = matching.FirstOrDefault().GraduatedScales.ProjectedAsCollection<TaxRangesDTO>();
                    return taxOrderDTO;

                }

                return null;
            }
        }

        #endregion

        #region Helpers

        public static TaxOrder CreateNewTaxOrder(TaxOrderDTO taxOrderDTO)
        {
            TaxOrder taxOrder = TaxOrderFactory.CreateTaxOrder(taxOrderDTO.Description, taxOrderDTO.IsActive, taxOrderDTO.CreatedBy);

            //If graduated scale(s) available.. add them
            if (taxOrderDTO.TaxRangesItemList != null)
            {
                foreach (var graduatedScale in taxOrderDTO.TaxRangesItemList)
                {
                    var range = new Range(graduatedScale.RangeLowerLimit, graduatedScale.RangeUpperLimit);

                    taxOrder.AddNewGraduatedScale(taxOrderDTO.Id, (byte)graduatedScale.Type, graduatedScale.TaxValue, range, taxOrderDTO.CreatedBy);
                }
            }

            return taxOrder;
        }



        #endregion

        #endregion
    }
}
