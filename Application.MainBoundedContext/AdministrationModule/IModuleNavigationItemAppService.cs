﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Infrastructure.Crosscutting.Framework.Utils;

namespace Application.MainBoundedContext.AdministrationModule
{
    public interface IModuleNavigationItemAppService
    {
        #region ModuleNavigationItem

        Task<bool> AddModuleNavigationItemAsync(List<ModuleNavigationItemDTO> moduleNavigationItem, ServiceHeader serviceHeader);

        Task<bool> BulkInsertModuleNavigationItemAsync(List<Guid> modulesNavigationIds, string roleId, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemByControllerNameAndActionNameAsync(string controllerName, string actionName, ServiceHeader serviceHeader);

        Task<ModuleNavigationItemDTO> FindModuleNavigationItemByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<ModuleNavigationItemDTO> FindModuleNavigationItemInRoleByItemIdAndRoleAsync(Guid id, string role, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemDTO>> FindModuleNavigationActionControllerNameAsync(string controllerName, ServiceHeader serviceHeader);

        Task<bool> ValidateAccessPermissionAsync(string controllerName, string actionName, string[] roleId, ServiceHeader serviceHeader);

        #endregion

        #region ModuleNavigationItemInRole

        Task<ModuleNavigationItemInRoleDTO> FindModuleNavigationItemInRoleByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<bool> AddModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader);

        Task<bool> UpdateModuleNavigationItemInRoleListAsync(ModuleNavigationItemInRoleDTO moduleNavigationItemInRole, ServiceHeader serviceHeader);

        Task<bool> RemoveModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader);

        Task<bool> BulkInsertModuleNavigationItemInRolesAsync(List<Guid> moduleNavigationItemInRole, string roleId, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemsAsync(ServiceHeader serviceHeader);

        Task<PageCollectionInfo<ModuleNavigationItemDTO>> FindModuleNavigationItemsFilterPageCollectionInfoAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByRoleAsync(string roleId, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationIdAsync(Guid moduleNavigationId, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationByControllerNameAndRoleIdAsync(string controllerName, string roleIds, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleAsync(ServiceHeader serviceHeader);

        Task<PageCollectionInfo<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRolePageCollectionInfoAsync(string roleIds, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<RolePermissionDTO>> GetAccessPermissionsPerRolesAsync(string[] roleIds, string[] currentUserRole, ServiceHeader serviceHeader);

        Task<bool> RoleHasPermissionAsync(Guid moduleNavigationItemId, string roleIds, ServiceHeader serviceHeader);

        #endregion
    }
}
