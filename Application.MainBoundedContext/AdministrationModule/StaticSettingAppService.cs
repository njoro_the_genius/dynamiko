﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.Seedwork;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.SettingAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Adapter;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.MainBoundedContext.AdministrationModule
{
    public class StaticSettingAppService : IStaticSettingAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        private readonly IRepository<StaticSetting> _staticSettingRepository;

        public StaticSettingAppService(IDbContextScopeFactory dbContextScopeFactory, IRepository<StaticSetting> staticSettingRepository)
        {
            if (dbContextScopeFactory == null)
                throw new ArgumentNullException(nameof(dbContextScopeFactory));

            if (staticSettingRepository == null)
                throw new ArgumentNullException(nameof(staticSettingRepository));

            _staticSettingRepository = staticSettingRepository ?? throw new ArgumentNullException(nameof(staticSettingRepository));

            _dbContextScopeFactory = dbContextScopeFactory;
        }

        #region StaticSetting

        public StaticSettingDTO AddNewStaticSetting(StaticSettingDTO staticSettingDTO, ServiceHeader serviceHeader)
        {
            if (staticSettingDTO != null)
            {
                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    Specification<StaticSetting> key = StaticSettingSpecification.SettingByKey(staticSettingDTO.Key);

                    var result = _staticSettingRepository.AllMatching(key, serviceHeader).FirstOrDefault();

                    if (result != null)
                    {
                        throw new Exception($"Setting with same key naming already exists");
                    }

                    var staticSetting = StaticSettingFactory.CreateSetting(staticSettingDTO.Key, staticSettingDTO.Value, staticSettingDTO.CreatedBy);

                    _staticSettingRepository.Add(staticSetting, serviceHeader);

                    return dbContextScope.SaveChanges(serviceHeader) >= 0 ? staticSetting.ProjectedAs<StaticSettingDTO>() : null;
                }
            }


            return null;
        }

        public StaticSettingDTO FindSettingByKey(string keyName, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = StaticSettingSpecification.SettingByKey(keyName);

                ISpecification<StaticSetting> spec = filter;

                var allMatching = _staticSettingRepository.AllMatching(spec, serviceHeader);

                return allMatching?.FirstOrDefault().ProjectedAs<StaticSettingDTO>();

            }
        }

        public StaticSettingDTO FindstaticSettingById(Guid staticSettingId, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var setting = _staticSettingRepository.Get(staticSettingId, serviceHeader);

                return setting != null ? setting.ProjectedAs<StaticSettingDTO>() : null;
            }
        }

        public List<StaticSettingDTO> FindStaticSettings(string text, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = StaticSettingSpecification.DefaultSpecification();

                ISpecification<StaticSetting> spec = filter;

                var staticSettings = _staticSettingRepository.AllMatching(spec, serviceHeader);

                if (staticSettings != null)
                {
                    return staticSettings.ProjectedAsCollection<StaticSettingDTO>();
                }

                return null;
            }
        }

        public PageCollectionInfo<StaticSettingDTO> FindStaticSettingsFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = StaticSettingSpecification.SettingByKey(searchString);

                ISpecification<StaticSetting> spec = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.ClusteredId };

                return _staticSettingRepository.AllMatchingPaged<StaticSettingDTO>(spec, startIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public bool RemoveStaticSetting(Guid staticSettingId, ServiceHeader serviceHeader)
        {
            if (staticSettingId == Guid.Empty)
            {
                return false;
            }
            else
            {
                using (var dbContextScope = _dbContextScopeFactory.Create())
                {
                    var staticSetting = _staticSettingRepository.Get(staticSettingId, serviceHeader);

                    if (staticSetting != null)
                    {
                        _staticSettingRepository.Remove(staticSetting, serviceHeader);
                        return dbContextScope.SaveChanges(serviceHeader) >= 0;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

        }

        public bool UpdateStaticSetting(StaticSettingDTO staticSettingDTO, ServiceHeader serviceHeader)
        {
            if (staticSettingDTO == null)
            {
                return false;
            }
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = _staticSettingRepository.Get((Guid)staticSettingDTO.Id, serviceHeader);

                if (persisted != null)
                {
                    var current = StaticSettingFactory.CreateSetting(persisted.Key, staticSettingDTO.Value, persisted.CreatedBy);

                    current.ChangeCurrentIdentity(persisted.Id, persisted.ClusteredId, persisted.CreatedBy, persisted.CreatedDate);

                    _staticSettingRepository.Merge(persisted, current, serviceHeader);

                    return dbContextScope.SaveChanges(serviceHeader) >= 0;
                }
            }
            return false;
        }

        #endregion

    }
}
