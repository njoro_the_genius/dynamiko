﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainBoundedContext.AdministrationModule
{
    public interface IPayRollAppService
    {
        Task<PayRollDTO> AddNewPayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader);

        Task<bool> UpdatePayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader);

        Task<PayRollDTO> FindPayRollByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollDTO>> FindPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollDTO>> FindPayRollByCustomerIdFilterInPageAsync(Guid customerId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollDTO>> FindUnProcessedPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<PayRollDTO>> FindPayRolls(ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemDTOFilterInPageAsync(Guid payRollId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemByEmployeeIdDTOFilterInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);
    }
}
