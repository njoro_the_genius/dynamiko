﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.Seedwork;
using Domain.MainBoundedContext.AdministrationModule.Aggregates.PayRollAgg;
using Domain.Seedwork;
using Domain.Seedwork.Specification;
using Infrastructure.Crosscutting.Framework.Utils;
using Numero3.EntityFramework.Interfaces;

namespace Application.MainBoundedContext.AdministrationModule
{
    public class PayRollAppService : IPayRollAppService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        private readonly IRepository<PayRoll> _payRollRepository;

        private readonly IRepository<PayRollItem> _payRollItemRepository;

        public PayRollAppService(IDbContextScopeFactory dbContextScopeFactory, IRepository<PayRoll> payRollRepository, IRepository<PayRollItem> payRollItemRepository)
        {
            _dbContextScopeFactory = dbContextScopeFactory ?? throw new ArgumentNullException(nameof(dbContextScopeFactory));

            _payRollRepository = payRollRepository ?? throw new ArgumentNullException(nameof(payRollRepository));

            _payRollItemRepository = payRollItemRepository ?? throw new ArgumentNullException(nameof(payRollItemRepository));
        }

        public async Task<PayRollDTO> AddNewPayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader)
        {
            if (payRollDTO == null) return null;
            payRollDTO.ValidateAll();
            if (payRollDTO.HasErrors) throw new InvalidOperationException(string.Join(Environment.NewLine, payRollDTO.ErrorMessages));

            using (var dbContext = _dbContextScopeFactory.Create())
            {
                var payRollEntity = PayRollFactory.Create(payRollDTO.CustomerId, payRollDTO.TotalGrosspay, payRollDTO.TotalNonCashBenefits, payRollDTO.TotalPension, payRollDTO.TotalPayee, payRollDTO.TotalAllowableDeductions, payRollDTO.TotalPayee,
                    payRollDTO.TotalHealthInsurance, payRollDTO.StartDate, payRollDTO.EndDate, (int)PayRollStatus.UnProcessed, payRollDTO.IsQueued, serviceHeader.ApplicationUserName, payRollDTO.RecordStatus);
                _payRollRepository.Add(payRollEntity, serviceHeader);
                await dbContext.SaveChangesAsync(serviceHeader);
                return payRollEntity.ProjectedAs<PayRollDTO>();
            }

        }

        public async Task<PageCollectionInfo<PayRollDTO>> FindPayRollByCustomerIdFilterInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = PayrollSpecification.PayRollByCustomerId(customerId);

                ISpecification<PayRoll> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _payRollRepository.AllMatchingPagedAsync<PayRollDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<PayRollDTO> FindPayRollByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var payRoll = await _payRollRepository.GetAsync(id, serviceHeader);

                if (payRoll != null) //adapt
                {
                    return payRoll.ProjectedAs<PayRollDTO>();
                }
                return null;
            }
        }

        public async Task<PageCollectionInfo<PayRollDTO>> FindPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = PayrollSpecification.DefaultSpec();

                ISpecification<PayRoll> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _payRollRepository.AllMatchingPagedAsync<PayRollDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemByEmployeeIdDTOFilterInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = PayrollSpecification.PayRollItemByEmployeeIdSpec(employeeId);

                ISpecification<PayRollItem> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _payRollItemRepository.AllMatchingPagedAsync<PayRollItemDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemDTOFilterInPageAsync(Guid payRollId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = PayrollSpecification.PayRollItemByPayrollIdSpec(payRollId);

                ISpecification<PayRollItem> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _payRollItemRepository.AllMatchingPagedAsync<PayRollItemDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<List<PayRollDTO>> FindPayRolls(ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = PayrollSpecification.DefaultSpec();

                ISpecification<PayRoll> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                var matching = await _payRollRepository.AllMatchingAsync(specification, serviceHeader);

                if (matching != null) //adapt
                {
                    return matching.ProjectedAsCollection<PayRollDTO>();
                }

                return null;
            }
        }

        public async Task<PageCollectionInfo<PayRollDTO>> FindUnProcessedPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var filter = PayrollSpecification.UnProcessedPayrollSpec();

                ISpecification<PayRoll> specification = filter;

                var sortFields = new List<string> { DefaultSettings.Instance.CreatedDate };

                return await _payRollRepository.AllMatchingPagedAsync<PayRollDTO>(specification, pageIndex, pageSize, sortFields, sortAscending, serviceHeader);
            }
        }

        public async Task<bool> UpdatePayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader)
        {
            if (payRollDTO == null)
                return false;

            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var persisted = _payRollRepository.Get(payRollDTO.Id, serviceHeader);

                if (persisted == null) return false;

                var current = CreatePayroll(payRollDTO, serviceHeader);

                current.ChangeCurrentIdentity(persisted.Id, persisted.ClusteredId, persisted.CreatedBy, persisted.CreatedDate);

                foreach (var payrollItem in current.PayRollItems)
                {
                    payrollItem.PayRollId = persisted.Id;
                    _payRollItemRepository.Add(payrollItem, serviceHeader);
                }

                _payRollRepository.Merge(persisted, current, serviceHeader);

                return await dbContextScope.SaveChangesAsync(serviceHeader) > 0;
            }
        }

        #region Helpers
        PayRoll CreatePayroll(PayRollDTO payRollDTO, ServiceHeader serviceHeader)
        {
            var payrollEntity = PayRollFactory.Create(payRollDTO.CustomerId, payRollDTO.TotalGrosspay, payRollDTO.TotalNonCashBenefits, payRollDTO.TotalPension, payRollDTO.TotalPayee, payRollDTO.TotalAllowableDeductions, payRollDTO.TotalPayee, payRollDTO.TotalHealthInsurance, payRollDTO.StartDate, payRollDTO.EndDate, payRollDTO.Status, payRollDTO.IsQueued, serviceHeader.ApplicationUserName, payRollDTO.RecordStatus);

            if (payRollDTO.PayRollItemDTOs != null)
            {
                foreach (var item in payRollDTO.PayRollItemDTOs)
                {
                    payrollEntity.Create(item.EmployeeId, item.Grosspay, item.NonCashBenefits, item.Pension, item.Payee, item.AllowableDeductions, item.NetPay, item.HealthInsurance, payrollEntity.CreatedBy);
                }
            }

            return payrollEntity;
        }
        #endregion
    }
}
