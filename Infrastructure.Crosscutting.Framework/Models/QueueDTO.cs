﻿using System;

namespace Infrastructure.Crosscutting.Framework.Models
{
    [Serializable]
    public class QueueDTO
    {
        public Guid RecordId { get; set; }

        public int DeliveryStatus { get; set; }

        public string AppDomainName { get; set; }

        public string CredentialsPayload { get; set; }

        public Guid CustomerId { get; set; }

        public int SMSServiceProvider { get; set; }

        public string SMSBody { get; set; }

        public string SMSRecipient { get; set; }

        public string SMSSenderId { get; set; }

        public string Payload { get; set; }

        public string CommissionDescription { get; set; }

        public string CommissionReference { get; set; }

        public Guid CommissionSettlementGLId { get; set; }

        public Guid CommissionCustomerAccountId { get; set; }

        public decimal CommissionAmount { get; set; }

        public int ServiceProvider { get; set; }

        public string BulkTextUrl { get; set; }

        public string BulkTextUsername { get; set; }

        public string BulkTextPassword { get; set; }

        public string BulkTextSenderId { get; set; }

        public string EntityName { get; set; }

        public string ApplicationUserName { get; set; }

        public string Narration { get; set; }

        public int DebitAdviser { get; set; }

        public int Gateway { get; set; }

        public int EntityState { get; set; }

        public int TransactionType { get; set; }

        public byte RecordStatus { get; set; }

        public string Body { get; set; }
    }
}