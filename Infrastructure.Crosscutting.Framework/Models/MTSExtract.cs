﻿using FileHelpers;
using Infrastructure.Crosscutting.Framework.Extensions;
using System;

namespace Infrastructure.Crosscutting.Framework.Models
{
    [DelimitedRecord(","), IgnoreFirst(1)]
    [Serializable]
    public class MTSExtract
    {
        [FieldOrder(1), FieldTitle("Amount")]
        string _amount;

        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        [FieldOrder(2), FieldTitle("CreditIdentityString")]
        string _creditIdentityString;

        public string CreditIdentityString
        {
            get { return _creditIdentityString; }
            set { _creditIdentityString = value; }
        }

        [FieldOrder(3), FieldTitle("CreditIdentityStringType")]
        string _creditIdentityStringType;

        public string CreditIdentityStringType
        {
            get { return _creditIdentityStringType; }
            set { _creditIdentityStringType = value; }
        }

        [FieldOrder(4), FieldTitle("ValidationIdentityString")]
        string _validationIdentityString;

        public string ValidationIdentityString
        {
            get { return _validationIdentityString; }
            set { _validationIdentityString = value; }
        }

        [FieldOrder(5), FieldTitle("ValidationIdentityStringType")]
        string _validationIdentityStringType;

        public string ValidationIdentityStringType
        {
            get { return _validationIdentityStringType; }
            set { _validationIdentityStringType = value; }
        }

        [FieldOrder(6), FieldTitle("RecordIdentifier")]
        Guid _recordIdentifier;

        public Guid RecordIdentifier
        {
            get { return _recordIdentifier; }
            set { _recordIdentifier = value; }
        }

        [FieldOrder(7), FieldTitle("RecordIdentifier")]
        DateTime _recordUtcTime;

        public DateTime RecordUtcTime
        {
            get { return _recordUtcTime; }
            set { _recordUtcTime = value; }
        }

        [FieldOrder(8), FieldTitle("MPESASpId")]
        string _mpesaSpId;

        public string MPESASpId
        {
            get { return _mpesaSpId; }
            set { _mpesaSpId = value; }
        }

        [FieldOrder(9), FieldTitle("MPESASpPassword")]
        string _mpesaSpPassword;

        public string MPESASpPassword
        {
            get { return _mpesaSpPassword; }
            set { _mpesaSpPassword = value; }
        }

        [FieldOrder(10), FieldTitle("MPESAServiceId")]
        string _mpesaServiceId;

        public string MPESAServiceId
        {
            get { return _mpesaServiceId; }
            set { _mpesaServiceId = value; }
        }

        [FieldOrder(11), FieldTitle("MPESAInitiatorIdentifier")]
        string _mpesaInitiatorIdentifier;

        public string MPESAInitiatorIdentifier
        {
            get { return _mpesaInitiatorIdentifier; }
            set { _mpesaInitiatorIdentifier = value; }
        }

        [FieldOrder(12), FieldTitle("MPESAInitiatorPassword")]
        string _mpesaInitiatorPassword;

        public string MPESAInitiatorPassword
        {
            get { return _mpesaInitiatorPassword; }
            set { _mpesaInitiatorPassword = value; }
        }

        [FieldOrder(13), FieldTitle("MPESAInitiatorPassword")]
        string _mpesaOrganizationShortCode;

        public string MPESAOrganizationShortCode
        {
            get { return _mpesaOrganizationShortCode; }
            set { _mpesaOrganizationShortCode = value; }
        }

        [FieldOrder(14), FieldTitle("CommandId")]
        string _commandId;

        public string CommandId
        {
            get { return _commandId; }
            set { _commandId = value; }
        }

        [FieldOrder(15), FieldTitle("Remarks")]
        string _remarks;

        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }

        [FieldOrder(16), FieldTitle("AccountReference")]
        string _accountReference;

        public string AccountReference
        {
            get { return _accountReference; }
            set { _accountReference = value; }
        }

        [FieldOrder(17), FieldTitle("GenericApiResponse")]
        string _genericApiResponse;

        public string GenericApiResponse
        {
            get { return _genericApiResponse; }
            set { _genericApiResponse = value; }
        }

        [FieldOrder(18), FieldTitle("AirtelUsername")]
        string _airtelUsername;

        public string AirtelUsername
        {
            get { return _airtelUsername; }
            set { _airtelUsername = value; }
        }


        [FieldOrder(19), FieldTitle("AirtelPassword")]
        string _airtelPassword;

        public string AirtelPassword
        {
            get { return _airtelPassword; }
            set { _airtelPassword = value; }
        }


        [FieldOrder(20), FieldTitle("AirtelNickname")]
        string _airtelNickname;

        public string AirtelNickname
        {
            get { return _airtelNickname; }
            set { _airtelNickname = value; }
        }

        [FieldOrder(21), FieldTitle("AirtelMerchantQueryUsername")]
        string _airtelMerchantQueryUsername;

        public string AirtelMerchantQueryUsername
        {
            get { return _airtelMerchantQueryUsername; }
            set { _airtelMerchantQueryUsername = value; }
        }

        [FieldOrder(22), FieldTitle("AirtelMerchantQueryPassword")]
        string _airtelMrchantQueryPassword;

        public string AirtelMerchantQueryPassword
        {
            get { return _airtelMrchantQueryPassword; }
            set { _airtelMrchantQueryPassword = value; }
        }

        [FieldOrder(23), FieldTitle("CustomerIdentifier")]
        Guid _customerIdentifier;

        public Guid CustomerIdentifier
        {
            get { return _customerIdentifier; }
            set { _customerIdentifier = value; }
        }

        [FieldOrder(24), FieldTitle("CustomerType")]
        int _customerType;

        public int CustomerType
        {
            get { return _customerType; }
            set { _customerType = value; }
        }

        [FieldOrder(25), FieldTitle("MCCMNC")]
        int _mccmnc;

        public int MCCMNC
        {
            get { return _mccmnc; }
            set { _mccmnc = value; }
        }

        [FieldOrder(26), FieldTitle("RemitterName")]
        string _remitterName;

        public string RemitterName
        {
            get { return _remitterName; }
            set { _remitterName = value; }
        }

        [FieldOrder(27), FieldTitle("RemitterPhoneNumber")]
        string _remitterPhoneNumber;

        public string RemitterPhoneNumber
        {
            get { return _remitterPhoneNumber; }
            set { _remitterPhoneNumber = value; }
        }

        [FieldOrder(28), FieldTitle("RecipientName")]
        string _recipientName;

        public string RecipientName
        {
            get { return _recipientName; }
            set { _recipientName = value; }
        }

        [FieldOrder(29), FieldTitle("RecipientPhoneNumber")]
        string _recipientPhoneNumber;

        public string RecipientPhoneNumber
        {
            get { return _recipientPhoneNumber; }
            set { _recipientPhoneNumber = value; }
        }

        [FieldOrder(30), FieldTitle("TransactionNumber")]
        string _transactionNumber;
        public string TransactionNumber
        {
            get { return _transactionNumber; }
            set { _transactionNumber = value; }
        }

        [FieldOrder(31), FieldTitle("HttpStatusCode")]
        string _httpStatusCode;
        public string HttpStatusCode
        {
            get { return _httpStatusCode; }
            set { _httpStatusCode = value; }
        }

        [FieldOrder(32), FieldTitle("MobileEFTCode")]
        private int _mobileEFTCode;
        public int MobileEFTCode
        {
            get { return _mobileEFTCode; }
            set { _mobileEFTCode = value; }
        }

        [FieldOrder(33), FieldTitle("SystemTraceAuditNumber")]
        private string _systemTraceAuditNumber;
        public string SystemTraceAuditNumber
        {
            get { return _systemTraceAuditNumber; }
            set { _systemTraceAuditNumber = value; }
        }
    }
}
