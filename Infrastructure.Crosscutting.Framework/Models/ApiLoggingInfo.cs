﻿using Infrastructure.Crosscutting.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Infrastructure.Crosscutting.Framework.Models
{
    public class ApiLoggingInfo
    {
        public string HttpMethod { get; set; }
        public string UriAccessed { get; set; }
        public string BodyContent { get; set; }
        public HttpStatusCode ResponseStatusCode { get; set; }
        public string ResponseStatusMessage { get; set; }
        public string IpAddress { get; set; }
        public HttpMessageType MessageType { get; set; }

        public List<string> Headers { get; } = new List<string>();

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"MessageType: {MessageType}");
            if (MessageType == HttpMessageType.Response)
                stringBuilder.AppendLine($"ResponseStatusCode: {(int) ResponseStatusCode} {ResponseStatusMessage}");
            stringBuilder.AppendLine($"UriAccessed: {UriAccessed}");
            stringBuilder.AppendLine($"HttpMethod: {HttpMethod}");
            stringBuilder.AppendLine($"IpAddress: {IpAddress}");

            stringBuilder.AppendLine(string.Join(Environment.NewLine, Headers));

            if (!string.IsNullOrWhiteSpace(BodyContent))
                stringBuilder.AppendLine(BodyContent);

            return $"{stringBuilder}";
        }
    }
}
