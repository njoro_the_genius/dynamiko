﻿using Infrastructure.Crosscutting.Framework.Logging;
using System.Web.Script.Serialization;

namespace Infrastructure.Crosscutting.Framework.Extensions
{
    public static class TransactionRequestsExtension
    {
        private static readonly ILogger _logger = new SerilogLogger();

        private static JavaScriptSerializer _serializer = new JavaScriptSerializer();
    }
}
