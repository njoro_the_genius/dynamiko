﻿using System;
using System.ComponentModel;

namespace Infrastructure.Crosscutting.Framework.List
{
    public class SelectionItem<T> : INotifyPropertyChanged
    {
        #region Fields

        private bool isSelected;

        private T item;

        #endregion

        #region Properties

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value == isSelected)
                {
                    OnPropertyChanged("IsSelected");
                }
                else
                {
                    isSelected = value;
                    OnPropertyChanged("IsSelected");
                    OnSelectionChanged();
                }
            }
        }

        public T Item
        {
            get { return item; }
            set
            {
                if (value.Equals(item)) return;
                item = value;
                OnPropertyChanged("Item");
            }
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler SelectionChanged;

        #endregion

        #region ctor

        public SelectionItem(T item)
            : this(false, item)
        { }

        public SelectionItem(bool selected, T item)
        {
            this.isSelected = selected;
            this.item = item;
        }

        #endregion

        #region Event invokers

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnSelectionChanged()
        {
            SelectionChanged?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
