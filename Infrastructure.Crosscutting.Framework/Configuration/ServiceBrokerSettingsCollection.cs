﻿using System.Configuration;

namespace Infrastructure.Crosscutting.Framework.Configuration
{
    public class ServiceBrokerSettingsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ServiceBrokerSettingsElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ServiceBrokerSettingsElement)element).UniqueId;
        }

        public ServiceBrokerSettingsElement this[int index] => (ServiceBrokerSettingsElement)BaseGet(index);

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name => (string)base["name"];

        [ConfigurationProperty("auditLogDispatcherQueuePath", IsRequired = true)]
        public string AuditLogDispatcherQueuePath => (string)base["auditLogDispatcherQueuePath"];

        [ConfigurationProperty("timeToBeReceived", IsRequired = true)]
        public int TimeToBeReceived => (int)base["timeToBeReceived"];

        [ConfigurationProperty("logEnabled", IsRequired = true)]
        public int LogEnabled => (int)base["logEnabled"];

        [ConfigurationProperty("textDispatcherQueuePath", IsRequired = true)]
        public string TextDispatcherQueuePath => (string)base["textDispatcherQueuePath"];

        [ConfigurationProperty("emailDispatcherQueuePath", IsRequired = true)]
        public string EmailDispatcherQueuePath => (string)base["emailDispatcherQueuePath"];

        [ConfigurationProperty("approvalRequestDispatcherQueuePath", IsRequired = true)]
        public string ApprovalRequestDispatcherQueuePath => (string)base["approvalRequestDispatcherQueuePath"];

        [ConfigurationProperty("approvalDispatcherQueuePath", IsRequired = true)]
        public string ApprovalDispatcherQueuePath => (string)base["approvalDispatcherQueuePath"];

        [ConfigurationProperty("debitRequestDispatcherQueuePath", IsRequired = true)]
        public string DebitRequestDispatcherQueuePath => (string)base["debitRequestDispatcherQueuePath"];

        [ConfigurationProperty("reversalDispatcherQueuePath", IsRequired = true)]
        public string ReversalDispatcherQueuePath => (string)base["reversalDispatcherQueuePath"];

        [ConfigurationProperty("gatewayDispatcherQueuePath", IsRequired = true)]
        public string GatewayDispatcherQueuePath => (string)base["gatewayDispatcherQueuePath"];

        [ConfigurationProperty("transUnionDispatcherQueuePath", IsRequired = true)]
        public string TransUnionDispatcherQueuePath => (string)base["transUnionDispatcherQueuePath"];

        [ConfigurationProperty("debitAdviser", IsRequired = true)]
        public int DebitAdviser => (int)base["debitAdviser"];

        [ConfigurationProperty("orderProcessor", IsRequired = true)]
        public int OrderGateway => (int)base["orderProcessor"];

        [ConfigurationProperty("clientCode", IsRequired = true)]
        public int ClientCode => (int)base["clientCode"];

    }
}