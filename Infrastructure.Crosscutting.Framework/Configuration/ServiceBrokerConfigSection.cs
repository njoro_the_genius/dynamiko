﻿using System.Configuration;

namespace Infrastructure.Crosscutting.Framework.Configuration
{
    public class ServiceBrokerConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("serviceBrokerSettings")]
        public ServiceBrokerSettingsCollection ServiceBrokerSettingsItems => (ServiceBrokerSettingsCollection)base["serviceBrokerSettings"];
    }
}
