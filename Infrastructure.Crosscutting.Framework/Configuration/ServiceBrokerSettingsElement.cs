﻿using System.Configuration;

namespace Infrastructure.Crosscutting.Framework.Configuration
{
    public class ServiceBrokerSettingsElement : ConfigurationElement
    {
        [ConfigurationProperty("uniqueId", IsKey = true, IsRequired = true)]
        public string UniqueId
        {
            get => ((string)(base["uniqueId"]));
            set => base["uniqueId"] = value;
        }
    }
}
