﻿using Newtonsoft.Json.Linq;
using System;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public static class JsonValidator
    {
        public static bool IsValidJson(this string strInput)
        {
            strInput = strInput.Trim();

            if ((!strInput.StartsWith("{") || !strInput.EndsWith("}")) &&
                (!strInput.StartsWith("[") || !strInput.EndsWith("]"))) return false;
            try
            {
                JToken.Parse(strInput);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
