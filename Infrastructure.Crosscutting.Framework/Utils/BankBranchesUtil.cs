﻿using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public static class BankBranchesUtil
    {
        public static async Task<List<Tuple<string, string, string>>> GetBankBranchesTupleAsync(string existingFile, ServiceHeader serviceHeader)
        {
            var bankCodeBranchCodePayBillNumberTuples = new List<Tuple<string, string, string>>();

            var task = await Task.Run(() =>
            {
                if (!File.Exists(existingFile))
                    return bankCodeBranchCodePayBillNumberTuples;

                using (var file = new FileStream(existingFile, FileMode.Open, FileAccess.Read))
                {
                    var hssfWorkbook = new HSSFWorkbook(file);

                    var sheet = hssfWorkbook.GetSheetAt(0);

                    var rows = sheet?.GetRowEnumerator();

                    if (rows == null)
                        return bankCodeBranchCodePayBillNumberTuples;

                    rows.MoveNext(); // skip header row

                    var counter = default(int);

                    while (rows.MoveNext())
                    {
                        counter += 1;

                        var row = (HSSFRow)rows.Current;

                        if (row == null)
                            continue;

                        var cell1Value = row.GetCell(0);  // BankCode
                        var cell2Value = row.GetCell(1);  // BranchCode
                        var cell3Value = row.GetCell(2);  // BankName
                        var cell4Value = row.GetCell(3);  // BranchName
                        var cell5Value = row.GetCell(4);  // Status

                        object col1Value = cell1Value?.ToString();
                        object col2Value = cell2Value?.ToString();
                        object col3Value = cell3Value?.ToString();
                        object col4Value = cell4Value?.ToString();
                        object col5Value = cell5Value?.ToString();

                        var tuple = new Tuple<string, string, string>(
                            $"{col1Value} ".Trim().PadLeft(2, '0'), $"{col2Value} ".Trim().PadLeft(3, '0'), col4Value.ToString());

                        bankCodeBranchCodePayBillNumberTuples.Add(tuple);
                    }
                }

                return bankCodeBranchCodePayBillNumberTuples;
            });

            return task;
        }
    }
}
