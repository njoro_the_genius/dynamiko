﻿using System;
using System.ComponentModel;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public enum ChartOfAccountType
    {
        [Description("Asset")]
        Asset = 0x3E8,/*1000*/
        [Description("Liability")]
        Liability = 0x7D0,/*2000*/
        [Description("Equity/Capital")]
        Equity = 0xBB8,/*3000*/
        [Description("Income/Revenue")]
        Income = 0xFA0,/*4000*/
        [Description("Expense")]
        Expense = 0x1388,/*5000*/
    }

    public enum AuditLogEventType
    {
        [Description("Entity_Added")]
        Entity_Added = 0XBEEF/*48879*/,
        [Description("Entity_Modified")]
        Entity_Modified = 0XBEEF + 1,
        [Description("Entity_Deleted")]
        Entity_Deleted = 0XBEEF + 2,
        [Description("Sys_Login")]
        Sys_Login = 0XBEEF + 3,
        [Description("Sys_Logout")]
        Sys_Logout = 0XBEEF + 4,
        [Description("Sys_Other")]
        Sys_Other = 0XBEEF + 5
    }

    public enum WalletGateway
    {
        [Description("Fikiwa")]
        Fikiwa
    }

    public enum ApprovableEntity
    {
        [Description("AspNetUsers")]
        AspNetUsers,
        [Description("Orders")]
        Orders
    }

    public enum FikiwaEntryNature
    {
        [Description("Debit")]
        Debit,
        [Description("Credit")]
        Credit
    }

    public enum TemplateType
    {
        [Description("LoginCredentials")]
        LoginCredentials,
        [Description("ResetPassword")]
        ResetPassword
    }

    public enum MessageCategory
    {
        [Description("Audit Log")]
        AuditLog,
        [Description("Audit Trail")]
        AuditTrail,
        [Description("Transaction Request")]
        TransactionRequest,
        [Description("Transaction Request")]
        TransactionRequestIPN,
        [Description("SMS")]
        SMS,
        [Description("USSD")]
        USSD,
        [Description("E-mail Alert")]
        EmailAlert,
        [Description("SMS Charge")]
        SMSCharge,
        [Description("Mobile To Wallet")]
        MobileToWallet,
        [Description("SMS Delivery Receipt")]
        SMSDeliveryReceipt,
        [Description("SMS IPN")]
        SMSIPN,
        [Description("SMS Charge Reversal")]
        SMSChargeReversal,
        [Description("Balance Alert")]
        BalanceAlertSubscription,
        [Description("Topup Acknowledgement Alert")]
        TopupAcknowledgementAlert,
        [Description("Billing")]
        Billing,
        [Description("Journal Reversal Batch Entry")]
        JournalReversalBatchEntry,
        [Description("Wallet Status Watchdog")]
        WalletStatusWatchdog,
        [Description("Payment Request IPN")]
        PaymentRequestIPN,
        [Description("SMS Commission Settlement")]
        SMSCommissionSettlement,
        [Description("Text Alert")]
        TextAlert,
        [Description("Approval Request")]
        ApprovalRequest,
        [Description("Approvable Entity")]
        ApprovableEntity,
        [Description("Approvals")]
        Approvals,
        [Description("Debit Request")]
        DebitRequest,
        [Description("Debit Request Result Sync")]
        DebitRequestResultSync,
        [Description("Debit Query Status")]
        DebitQueryStatus,
        [Description("Order Process")]
        OrderProcess,
        [Description("Airtime Purchase Request")]
        AirtimePurchaseRequest,
        [Description("Airtime Purchase Response")]
        AirtimePurchaseResponse,
        [Description("Airtime Puchase Result")]
        AirtimePurchaseStatus,
        [Description("Reversal Post Request")]
        ReversalPostRequest,
        [Description("Reversal Query Status")]
        ReversalQueryStatus,
        [Description("Reversal Result Async")]
        ReversalResultAsync,
        [Description("Mpesa Api Response")]
        MpesaApiResponse,
        [Description("Mpesa Api Result")]
        MpesaApiResult,
        [Description("Mpesa Account Balance Request")]
        MpesaAccountBalanceRequest,
        [Description("Mpesa Float Management Request")]
        MpesaFloatManagementRequest,
        [Description("Mpesa C2B Register URL")]
        MpesaC2BRegisterURL,
        [Description("Xpress-Money Request")]
        XpressMoneyRequest,
        [Description("Mpesa C2B Payment Confirmation Request")]
        MpesaC2BPaymentConfirmationRequest,
        [Description("Mpesa Api Queue Timeout")]
        MpesaApiQueueTimeout,
        [Description("Mpesa IMT ReceiveValidationRequest Api Result")]
        MpesaIMTReceiveValidationRequestApiResult,
        [Description("Mpesa TransactionStatusQuery Api Result")]
        MpesaTransactionStatusQueryApiResult,
        [Description("Mpesa B2C Request")]
        MpesaB2CRequest,
        [Description("Mpesa B2B Request")]
        MpesaB2BRequest,
        [Description("Mpesa Org Float Management Request")]
        MpesaOrgFloatManagementRequest,
        [Description("Mpesa Org Revenue Settlement Request")]
        MpesaOrgRevenueSettlementRequest,
        [Description("STP")]
        STP,
        [Description("Plugin Alert")]
        PluginAlert,
        [Description("Bill Payment")]
        BillPayment,
        [Description("Bill Payment Call Back")]
        BillPaymentCallBack,
        [Description("PayRollQueue")]
        PayRollQueue,
        [Description("CRB Posting")]
        CRBPosting
    }

    public enum RecordStatus
    {
        [Description("New")]
        New = 0,
        [Description("Edited")]
        Edited = 1,
        [Description("Approved")]
        Approved = 2,
        [Description("Rejected")]
        Rejected = 3,
    }

    public enum MobileNetworkCode
    {
        [Description("Kenya - Safaricom")]
        KE_Safaricom = 63902,
        [Description("Kenya - Airtel")]
        KE_Airtel = 63903,
        [Description("Kenya - Yu")]
        KE_Yu = 63905,
        [Description("Kenya - Orange")]
        KE_Orange = 63907,
        [Description("Rwanda - MTN")]
        RW_MTN = 63510,
        [Description("Uganda - MTN")]
        UG_MTN = 64110,
        [Description("Uganda - Airtel")]
        UG_Airtel = 64101,
        [Description("Ghana - MTN")]
        GH_MTN = 62001,
        [Description("Ghana - Vodafone")]
        GH_Vodafone = 62002,
        [Description("Ghana - Airtel Tigo")]
        GH_AirtelTigo = 62003
    }

    public enum MobileEFTCode
    {
        [Description("Business Payment")]
        BusinessPayment = 1,
        [Description("Business Payment With Withdrawal Charge Paid")]
        BusinessPaymentWithWithdrawalChargePaid = 2,
        [Description("Salary Payment With Withdrawal Charge Paid")]
        SalaryPaymentWithWithdrawalChargePaid = 3,
        [Description("Salary Payment")]
        SalaryPayment = 4,
        [Description("Promotion Payment")]
        PromotionPayment = 5,
        [Description("Business Buy Goods")]
        BusinessBuyGoods = 6,
        [Description("Business Pay Bill")]
        BusinessPayBill = 7,
        [Description("Disburse Funds To Business")]
        DisburseFundsToBusiness = 8,
        [Description("InitTrans Intra-Account")]
        InitTrans_IntraAccount = 9,
        [Description("InitTrans Inter-Account")]
        InitTrans_InterAccount = 10,
        [Description("Account Balance")]
        AccountBalance = 11,
        [Description("Business To Business Transfer")]
        BusinessToBusinessTransfer = 12,
        [Description("Agency Redistribution Of Float Funds")]
        AgencyRedistributionOfFloatFunds = 13,
        [Description("Agency Organization Move Funds")]
        AgencyOrganizationMoveFunds = 14,
        [Description("Register URL")]
        RegisterURL = 15,
        [Description("Business Deposit")]
        BusinessDeposit = 16,
        [Description("Org Bank Account Withdrawal")]
        OrgBankAccountWithdrawal = 17,
        [Description("Business Transfer From MMF To Utility")]
        BusinessTransferFromMMFToUtility = 18,
        [Description("Business Transfer From Utility To MMF")]
        BusinessTransferFromUtilityToMMF = 19,
        [Description("Org Revenue Settlement")]
        OrgRevenueSettlement = 20,
        [Description("Business Transfer From MMF To Utility (Intra-Org)")]
        OrgIntraBusinessTransferFromMMFToUtility = 21,
        [Description("Business To Business Transfer (Intra-Org)")]
        OrgIntraBusinessToBusinessTransfer = 22,
        [Description("Business To Business Transfer (Inter-Org)")]
        OrgInterBusinessToBusinessTransfer = 23,
        [Description("FSI to Pay Bill")]
        FSItoPayBill = 24,
        [Description("FSI to FSI Payment")]
        FSItoFSIPayment = 25,
        [Description("FSI to FSI Merchant")]
        FSItoMerchant = 26,
        [Description("Agency Float Advance")]
        AgencyFloatAdvance = 27,
        [Description("IMT Receive Validation Request")]
        IMTReceiveValidationRequest = 28,
        [Description("IMT Receive Money Transfer Request")]
        IMTReceiveMoneyTransferRequest = 29,
        [Description("Transaction Status Query")]
        TransactionStatusQuery = 30,
        [Description("Uniwallet EFT Result")]
        ITCAccountTransfer = 31,
        [Description("Pay Business Payment")]
        PayBusinessPayment = 32,
        [Description("Pay Business Payment With Withdrawal Charge Paid")]
        PayBusinessPaymentWithWithdrawalChargePaid = 33,
        [Description("Pay Promotion")]
        PayPromotion = 34,
        [Description("Pay Salary")]
        PaySalary = 35,
        [Description("Pay Salary Payment With Withdrawal Charge Paid")]
        PaySalaryPaymentWithWithdrawalChargePaid = 36,
    }

    public enum SystemChargeType
    {
        [Description("Principal")]
        Principal,
        [Description("Commission")]
        Commission
    }

    public enum HttpMessageType
    {
        [Description("Request")]
        Request = 0,
        [Description("Response")]
        Response
    }

    public enum TwoFactorProvidersCodes
    {
        [Description("Phone Code")]
        PhoneCode,
        [Description("Email Code")]
        EmailCode
    }

    public enum DLRStatus
    {
        [Description("UnKnown")]
        UnKnown,
        [Description("Failed")]
        Failed,
        [Description("Pending")]
        Pending,
        [Description("Delivered")]
        Delivered,
        [Description("Delivered To Network")]
        DeliveredToNetwork,
        [Description("Delivered To Terminal")]
        DeliveredToTerminal,
        [Description("Not Applicable")]
        NotApplicable,
        [Description("Submitted")]
        Submitted,
        [Description("Message Waiting")]
        MessageWaiting,
        [Description("Delivery Impossible")]
        DeliveryImpossible,
        [Description("Delivery Uncertain")]
        DeliveryUncertain,
        [Description("Delivery Notification Not Supported")]
        DeliveryNotificationNotSupported,
    }

    [Flags]
    public enum MessageOrigin
    {
        [Description("Internal")]
        Within = 1,
        [Description("External")]
        Without = 2,
        [Description("Other")]
        Other = 4
    }

    public enum QueuePriority
    {
        [Description("Lowest")]
        Lowest = 0,
        [Description("Very Low")]
        VeryLow = 1,
        [Description("Low")]
        Low = 2,
        [Description("Normal")]
        Normal = 3,
        [Description("Above Normal")]
        AboveNormal = 4,
        [Description("High")]
        High = 5,
        [Description("Very High")]
        VeryHigh = 6,
        [Description("Highest")]
        Highest = 7,
    }

    [Flags]
    public enum DMLCommand
    {
        [Description("None")]
        None = 0,
        [Description("Insert")]
        Insert = 1 << 1,
        [Description("Update")]
        Update = 1 << 2,
        [Description("Delete")]
        Delete = 1 << 3
    }

    public enum TwoFactorProviders
    {
        [Description("Phone Code")]
        PhoneCode,
        [Description("Email Code")]
        EmailCode
    }

    public enum InstantPaymentNotificationStatus
    {
        [Description("Pending")]
        Pending = 0,
        [Description("Submitted")]
        Submitted = 1,
        [Description("Acknowledged")]
        Acknowledged = 2,
        [Description("Verified")]
        Verified = 3
    }

    public enum Salutation
    {
        [Description("")]
        Unknown = 0x000,
        [Description("Mr")]
        Mr = 0x000 + 1,
        [Description("Mrs")]
        Mrs = 0x000 + 2,
        [Description("Miss")]
        Miss = 0x000 + 3,
        [Description("Dr")]
        Dr = 0x000 + 4,
        [Description("Prof")]
        Prof = 0x000 + 5,
        [Description("Rev")]
        Rev = 0x000 + 6,
        [Description("Eng")]
        Eng = 0x000 + 7,
        [Description("Hon")]
        Hon = 0x000 + 8,
        [Description("Cllr")]
        Cllr = 0x000 + 9,
        [Description("Sir")]
        Sir = 0x000 + 10,
        [Description("Ms")]
        Ms = 0x000 + 11,
        [Description("Fr")]
        Fr = 0x000 + 12,
        [Description("Sr")]
        Sr = 0x000 + 13,
    }

    public enum Gender
    {
        [Description("Female")]
        Male = 0,
        [Description("Male")]
        Female = 1,       
        [Description("Other")]
        Other = 2
    }

    public enum Nationality
    {
        [Description("")]
        Unknown,
        [Description("Kenya")]
        Kenya,
        [Description("Uganda")]
        Uganda,
        [Description("Tanzania")]
        Tanzania,
        [Description("Rwanda")]
        Rwanda,
        [Description("Burundi")]
        Burundi,
        [Description("Sudan")]
        Sudan,
        [Description("South Sudan")]
        SouthSudan,
        [Description("Malawi")]
        Malawi,
        [Description("Zimbabwe")]
        Zimbabwe,
        [Description("Zambia")]
        Zambia,
        [Description("Somalia")]
        Somalia,
        [Description("Djibouti")]
        Djibouti,
        [Description("Ethiopia")]
        Ethiopia,
    }

    public enum MaritalStatus
    {
        [Description("")]
        Unknown = 0x000,
        [Description("Married")]
        Married = 0x000 + 1,
        [Description("Single")]
        Single = 0x000 + 2,
        [Description("Divorced")]
        Divorced = 0x000 + 3,
        [Description("Separated")]
        Separated = 0x000 + 4,
    }

    public enum ChartOfAccountCategory
    {
        [Description("Header Account (Non-Postable)")]
        HeaderAccount = 0x1000,/*4096*/
        [Description("Detail Account (Postable)")]
        DetailAccount = 0x1000 + 1/*4097*/
    }

    [Flags]
    public enum AuthType
    {
        [Description("Approve")]
        Approve = 1,
        [Description("Reject")]
        Reject = 2,
    }

    public enum TransactionRequestStatus
    {
        [Description("Pending")]
        Pending = 0,
        [Description("Processed")]
        Processed = 1
    }

    public enum TextMessageAction
    {
        [Description("None")]
        None = 0,
        [Description("Charge")]
        Charge = 1,
        [Description("IPN")]
        IPN = 2
    }

    public enum WellKnownUserRoles
    {
        [Description("Super Administrator")]
        SuperAdministrator = 1,
        [Description("Administrator")]
        Administrator = 2,
        [Description("Accountant")]
        APIAccount = 3,
        [Description("Human Resource")]
        HumanResource = 4,
        [Description("Standard User")]
        StandardUser = 5,
    }

    public enum RoleType
    {
        [Description("Internal Role")]
        InternalRole,
        [Description("External Role")]
        ExternalRole
    }

    public enum StrippedUserRoles
    {
        [Description("Administrator")]
        Administrator = 2,
        [Description("Api Account")]
        APIAccount = 3,
        [Description("Relationship Manager")]
        RelationshipManager = 4,
        [Description("Standard User")]
        StandardUser = 5,
    }

    public enum CurrencyISOCode
    {
        [Description("KES")]
        KES = 404,
        [Description("USD")]
        USD = 840,
        [Description("GBP")]
        GBP = 826,
        [Description("EUR")]
        EUR = 978,
        [Description("AUD")]
        AUD = 036,
        [Description("JPY")]
        JPY = 392,
        [Description("CHF")]
        CHF = 756,
        [Description("CAD")]
        CAD = 124,
        [Description("ZAR")]
        ZAR = 710,
        [Description("UGX")]
        UGX = 800,
        [Description("TZS")]
        TZS = 834
    }

    public enum SMSServiceProvider
    {
        [Description("Africa's Talking")]
        AfricasTalking = 1,
        [Description("RoamTech")]
        RoamTech = 2,
        [Description("Info Bip")]
        InfoBip = 3,
        [Description("Fikiwa")]
        Fikiwa = 4,
        [Description("Oracom")]
        Oracom = 5
    }

    public enum TransactionType
    {
        [Description("M-Pesa")]
        Mpesa = 1,
        [Description("Pesalink")]
        Pesalink = 2,
        [Description("EFT")]
        EFT = 3,
        [Description("RTGS")]
        RTGS = 4,
        [Description("MTN Ghana")]
        MTNGhana = 5,
        [Description("B2B")]
        B2B = 6,
        [Description("AirtimePurchase-AfricasTalking")]
        AirtimePurchaseAfricasTalking = 7,
        [Description("Bill Payments")]
        BillPayment = 8,
        [Description("Wechat")]
        Wechat = 9,
        [Description("KYC-TransUnion")]
        TransUnion
    }

    public enum TransactionCategory
    {
        [Description("Mobile Wallet Transfer")]
        MobileWalletTransfer,
        [Description("Bank Transfer")]
        BankTransfer,
        [Description("Airtime Purchase")]
        AirtimePurchase,
        [Description("Online Money Transfers")]
        OnlineMoneyTransfers,
        [Description("Bills")]
        Bills,
        [Description("KYC")]
        KYC
    }

    public enum CustomerType
    {
        [Description("Corporate")]
        Corporate = 1,
        [Description("Retail")]
        Retail
    }

    public enum ServiceOrigin
    {
        [Description("Web MVC")]
        WebMVC = 1,
        [Description("Web API")]
        WebAPI = 2,
        [Description("Windows Service")]
        WindowsService = 3
    }

    public enum ClientCode
    {
        [Description("Kenya Commercial Bank Limited")]
        KCB = 1,
        [Description("Standard Chartered Bank Kenya Limited")]
        StanChart = 2,
        [Description("Barclays Bank of Kenya Limited")]
        BBK = 3,
        [Description("Bank of India")]
        BOI = 5,
        [Description("Bank of Baroda (Kenya Limited)")]
        Baroda = 6,
        [Description("Commercial Bank of Africa Limited")]
        CBA = 7,
        [Description("Co-operative Bank of Kenya Limited")]
        COOP = 11,
        [Description("SBM Bank Kenya Limited")]
        SBM = 60,
        [Description("NIC Bank Limited")]
        NIC = 41,
        [Description("Jamii Bora Bank")]
        JamiiBoraBank = 51,
        [Description("Sidian Bank")]
        SidianBank = 66,
        [Description("Sidian Bank")]
        UBAKenyaBank = 76,
        [Description("UBA Kenya Bank Ltd")]
        RafikiDTM = 100,
        [Description("Empire")]
        Empire = 101,
        [Description("EADH")]
        EADH = 102,
        [Description("Interswitch")]
        Interswitch = 103,
        [Description("PyCs")]
        PyCs = 999,
        [Description("Fikiwa")]
        Fikiwa = 4
    }

    public enum BankToBankFundsTransferMode
    {
        [Description("LegacyEFT")]
        LegacyEFT,
        [Description("B2B")]
        B2B,
        [Description("RTGS")]
        RTGS,
        [Description("PesaLink")]
        PesaLink
    }

    public enum ChargeType
    {
        [Description("Fixed Amount")]
        FixedAmount = 0,
        [Description("Percentage")]
        Percentage = 1
    }

    public enum DebitMode
    {
        [Description("Single (Multiple Debits)")]
        Single,
        [Description("Bulk (Single Debit)")]
        Bulk
    }

    public enum TransactionProcedure
    {
        [Description("To Bank Account")]
        AccountToAccount = 0,
        [Description("To Mobile Account")]
        AccountToMobile = 1,
        [Description("Business to Business")]
        BusinessToBusiness = 2,
        [Description("Cheque")]
        Cheque = 3
    }

    public enum CommissionChargeMode
    {
        [Description("Payee To Pay")]
        PayeeToPay = 0,
        [Description("Customer To Pay (Part Commission)")]
        CustomerToPayPart,
        [Description("Customer To Pay (Full Commission)")]
        CustomerToPayFull
    }

    public enum DebitAdviser
    {
        [Description("Fake")]
        Fake,
        [Description("Fikiwa")]
        Fikiwa
    }

    public enum OrderGateway
    {
        [Description("Fake")]
        Fake,
        [Description("SPS")]
        SPS,
        [Description("AfricasTalking")]
        AfricasTalking
    }

    public enum DebitAdviceStatus
    {
        [Description("Success")]
        Success = 1,
        [Description("Failed")]
        Failed = 2,
        [Description("Insufficient Balance")]
        InsufficientBalance = 3,
        [Description("Pending")]
        Pending = 4,
        [Description("No Charge")]
        NoCharge = 5
    }

    public enum TransactionStatus
    {
        [Description("Pending")]
        Pending = 0,
        [Description("Queued")]
        Queued = 1,
        [Description("SubmitAcknowledged")]
        SubmitAcknowledged = 2,
        [Description("Completed")]
        Completed = 3,
        [Description("Cached")]
        Cached = 4,
        [Description("Failed")]
        Failed = 5,
        [Description("Submitted")]
        Submitted = 6,
        [Description("Rejected")]
        Rejected = 7,
    }

    public enum FikiwaStatus
    {
        [Description("Success")]
        success
    }

    public enum FikiwaTransactionTypes
    {
        [Description("Resubmit Callback")]
        ResubmitCallback = 17,
        [Description("Third Party")]
        ThirdParty = 19
    }


    public enum SPSTransactionType
    {
        [Description("EFT")]
        EFT = 0,
        [Description("B2C")]
        B2C,
        [Description("B2B")]
        B2B
    }

    public enum SPSStatus
    {
        [Description("Completed")]
        Completed = 3,
    }

    public enum IPNDataFormat
    {
        [Description("XML")]
        XML,
        [Description("JSON")]
        JSON
    }

    public enum CRBStatus
    {
        [Description("Pending")]
        Pending,
        [Description("Queued")]
        Queued,
        [Description("Submitted")]
        Submitted,
        [Description("Failed")]
        Failed,
        [Description("Rejected")]
        Rejected,
        [Description("Completed")]
        Completed,
        [Description("Under Required Age")]
        UnderAge,
        [Description("Over Required Age")]
        OverAge,
        [Description("Pending Retry")]
        PendingRetry,
        [Description("Phone Number Series Blacklisted")]
        PhoneNumberSeriesBlackListed
    }

    public enum CRBProduct
    {
        [Description("ID Decision")]
        Product121,
        [Description("Mobile Credit Consumer Report with Default History")]
        Product124,
        [Description("Mobile Credit Consumer Report with Mobile Score & Mobile Loan Principals")]
        Product125,
        [Description("Mobile Credit Consumer Report with Default History and Generic Score")]
        Product126,
        [Description("Mobile Credit Consumer Report with Mobile Score & Mobile Loan Principals")]
        Product131,
        [Description("Mobile Credit Corporate Report")]
        Product159
    }

    public enum ReportReason
    {
        [Description("Account Opening")]
        AccountOpening = 1,
        [Description("Loan Application")]
        LoanApplication,
        [Description("Monitoring Credit")]
        MonitoringCredit,
        [Description("Person Verification")]
        Verification,
        [Description("Overdraft")]
        Overdraft,
        [Description("Credit Cards")]
        CreditCards,
        [Description("Working Capital Loans")]
        WorkingCapitalLoans,
        [Description("Business Expansion Loans")]
        BusinessExpansionLoans,
        [Description("Mortgage Loans")]
        MortgageLoans,
        [Description("Asset Finance Loans")]
        AssetFinanceLoans,
        [Description("Trade Finance Facilities")]
        TradeFinanceFacilities,
        [Description("Mobile Banking Loan")]
        MobileBankingLoan
    }

    public enum DateFilter
    {
        [Description("CreatedDate")]
        CreatedDate,
        [Description("DueDate")]
        DueDate
    }

    public enum PluginMessageType
    {
        [Description("ERROR")]
        Error = 1,
        [Description("INFORMATION")]
        Information = 2,
        [Description("WARNING")]
        Warning = 3
    }

    public enum AirtimePurchaseStatus
    {
        [Description("Pending")]
        Pending = 0,
        [Description("Queued")]
        Queued = 1,
        [Description("Posted")]
        Posted = 2,
        [Description("Success")]
        Success = 3,
        [Description("Failed")]
        Failed = 4,
    }

    public enum UtilityBillServiceId
    {
        [Description("DStv")]
        DStv = 1,
        [Description("GOtv")]
        GOtv = 2,
        [Description("Jambo Jet")]
        JamboJet = 8,
        [Description("Safaricom Airtime")]
        SafaricomAirtime = 9,
        [Description("Airtel Airtime")]
        AirtelAirtime = 10,
        [Description("Orange Airtime")]
        OrangeAirtime = 11,
        [Description("Nairobi Water")]
        NairobiWater = 12,
        [Description("Zuku")]
        Zuku = 13,
        [Description("KPLC Prepaid")]
        KPLCPrepaid = 39,
        [Description("KPLC Postpaid")]
        KPLCPostpaid = 44,
        [Description("Startimes")]
        Startimes = 54,
    }

    public enum UtilityBillTransactionType
    {
        [Description("Query Status")]
        QueryStatus = 1,
        [Description("Post Payment")]
        PostPayment = 2,
        [Description("Validate Account")]
        ValidateAccount = 3
    }

    public enum WeChatTransactionType
    {
        [Description("Wallet Top Up")]
        WeChatWalletTopUp = 1,
        [Description("WeChatPay")]
        WeChatPay = 2
    }

    public enum TransUnionProductResponseCodes
    {
        [Description("General Authentication Error")]
        GeneralAuthenticationError = 101,
        [Description("Invalid Infinity Code")]
        InvalidInfinityCode = 102,
        [Description("Invalid Authentication Credentials")]
        InvalidAuthenticationCredentials = 103,
        [Description("Password expired")]
        Passwordexpired = 104,
        [Description("Access Denied")]
        AccessDenied = 106,
        [Description("Account locked")]
        AccountLocked = 109,
        [Description("Product request processed successfully")]
        ProductRequestProcessedSuccessfully = 200,
        [Description("Credit Reference Number not found")]
        CreditReferenceNumberNotFound = 203,
        [Description("Multiple Credit Reference Number Found")]
        InvalidReportReason = 204,
        [Description("Invalid Sector ID")]
        InvalidSectorID = 209,
        [Description("Insufficient Credit")]
        InsufficientCredit = 301,
        [Description("Required input missing")]
        RequiredInputMissing = 402,
        [Description("General Application Error")]
        GeneralApplicationError = 403,
        [Description("Service temporarily unavailable")]
        ServiceTemporarilyUnavailable = 404,
        [Description("Unable to verify National ID")]
        UnableToVerifyNationalID = 408,
        [Description("Credit Reference Number not found")]
        NotFound = 202
    }

    public enum CurrencyConvertion
    {

        [Description("KES/USD")]
        KESUSD,
        [Description("KES/GBP")]
        KESGBP,
        [Description("KES/CNY")]
        KESCNY
    }

    public enum Toppings
    {
        [Description("Cheese")]
        Cheese,
        [Description("Pepperoni")]
        Pepperoni,
        [Description("Ham")]
        Ham,
        [Description("Pineapple")]
        Pineapple,
        [Description("Sausage")]
        Sausage,
        [Description("FetaCheese")]
        FetaCheese,
        [Description("Tomatoes")]
        Tomatoes,
        [Description("Olives")]
        Olives
    }

    public enum Department
    {
        [Description("ICT")]
        New = 0,
        [Description("FINANCE")]
        Edited = 1,
        [Description("Human Resource")]
        Approved = 2,
        [Description("Administration")]
        Rejected = 3,
    }

    public enum LeaveType
    {
        [Description("Sick")]
        Sick = 0,
        [Description("Normal")]
        Normal = 1,
        [Description("Matternity")]
        Matternity = 2,
        [Description("Patternity")]
        Patternity = 3,
    }

    public enum PayRollStatus
    {
        [Description("UnProcessed")]
        UnProcessed = 0,
        [Description("Processed")]
        Processed = 1,
    }

    public enum DeductionType
    {
        [Description("Loan")]
        Loan = 0,
        [Description("Salary Advance")]
        SalaryAdvance = 1,
        [Description("Retirement Fund")]
        RetirementFund = 2,
    }

    public enum Frequency
    {
        [Description("Once")]
        Once = 0,
        [Description("Monthly")]
        Monthly = 1,
    }
}