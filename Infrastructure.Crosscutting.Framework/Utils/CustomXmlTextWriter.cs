﻿using System.IO;
using System.Text;
using System.Xml;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public class CustomXmlTextWriter : XmlTextWriter
    {
        public CustomXmlTextWriter(Stream stream)
            : base(stream, new UTF8Encoding())
        { }

        public override void WriteEndElement()
        {
            base.WriteFullEndElement();
        }
    }
}
