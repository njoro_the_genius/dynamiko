﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public class RulesException : Exception
    {
        public readonly IList<RuleViolation> Errors = new List<RuleViolation>();

        private readonly static Expression<Func<object, object>> thisObject = x => x;

        public void ErrorForModel(string message)
        {
            Errors.Add(new RuleViolation { PropertyExpression = thisObject, Message = message });
        }

        public void ErrorsForModel(List<string> messageList)
        {
            foreach (var item in messageList)
                Errors.Add(new RuleViolation { PropertyExpression = thisObject, Message = item });
        }

        public void ErrorsForModel(IEnumerable<RuleViolation> errors)
        {
            foreach (var item in errors)
                Errors.Add(item);
        }
    }

    public class RulesException<TModel> : RulesException
    {
        public void ErrorFor<TProperty>(Expression<Func<TModel, TProperty>> propertyExpression, string message)
        {
            Errors.Add(new RuleViolation { PropertyExpression = propertyExpression, Message = message });
        }
    }

    public class RuleViolation
    {
        public LambdaExpression PropertyExpression { get; set; }

        public string Message { get; set; }
    }
}
