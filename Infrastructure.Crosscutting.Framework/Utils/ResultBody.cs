﻿namespace Infrastructure.Crosscutting.Framework.Utils
{
    public class ResultBody
    {
        public string TransactionId { get; set; }

        public string Status { get; set; }

        public string Narration { get; set; }

        public double AvailableBalance { get; set; }

        public string InitiatorReference { get; set; }
    }
}
