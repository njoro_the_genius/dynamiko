﻿namespace Infrastructure.Crosscutting.Framework.Utils
{
    public class RequestBodyLineItem
    {
        public string Payee { get; set; }

        public string PrimaryAccountNumber { get; set; }

        public decimal Amount { get; set; }

        public string BankCode { get; set; }

        public string Reference { get; set; }

        public string SystemTraceAuditNumber { get; set; }

        public string MCCMNC { get; set; }

        public string type { get; set; }

        public string typeDesc { get; set; }

        public int MCCMNCDescription { get; set; }

        public int Status { get; set; }

        public string StatusDesc { get; set; }

        public string B2MResponseCode { get; set; }

        public string B2MResponseDesc { get; set; }

        public string B2MResultCode { get; set; }

        public string B2MResultDesc { get; set; }

        public string B2MTransactionID { get; set; }

        public string TransactionDateTime { get; set; }

        public decimal WorkingAccountAvailableFunds { get; set; }

        public decimal UtilityAccountAvailableFunds { get; set; }

        public decimal ChargePaidAccountAvailableFunds { get; set; }

        public decimal WalletAccountAvailableFunds { get; set; }

        public string TransactionCreditParty { get; set; }

        public int IPNStatus { get; set; }

        public string IPNStatusDesc { get; set; }

        public string IPNResponse { get; set; }
    }
}
