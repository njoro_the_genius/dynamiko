﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public static class StockKeepingUnitConfiguration
    {
        public static StockKeepingUnit DeSerialize(string path)
        {
            var serializer = new XmlSerializer(typeof(StockKeepingUnit));

            var reader = new StreamReader(path);

            var kYC = (StockKeepingUnit)serializer.Deserialize(reader);

            reader.Close();

            return kYC;
        }
    }

    [Serializable]
    public class GraduateScale
    {
        [XmlAttribute]
        public string sku { get; set; }

        [XmlAttribute]
        public int pocketmoney { get; set; }

        [XmlAttribute]
        public decimal price { get; set; }      
    }

    [Serializable]
    public class StockKeepingUnit
    {
        public List<GraduateScale> GraduateScales { get; set; }
    }
}
