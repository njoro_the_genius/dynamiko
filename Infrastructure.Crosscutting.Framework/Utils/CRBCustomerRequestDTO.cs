﻿using System;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    [Serializable]
    public class CRBCustomerRequestDTO
    {
        public Guid Id { get; set; }

        public string IdentificationNumber { get; set; }

        public int IdentificationType { get; set; }

        public string ApplicationDomainName { get; set; }

        public Guid CustomerId { get; set; }

        public decimal TransUnionUnitsBalance { get; set; }
    }
}
