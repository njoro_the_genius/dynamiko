﻿/****************************************************
Client Server Helper class, developed by Gary McAllister
(c) Gary McAllister 2004.
*****************************************************
I would appreciate it if my name was left within this
comment segment and assembly info.

If you plan to use this code within a profit making
project or if you have any comments or suggestions 
please email me : garymcallister@hotmail.com
*****************************************************/
using System;
using System.Net.Sockets;
using System.Net;
using System.Collections;
using System.Threading;
using System.Text;
using System.IO;

namespace Kings.KCHSocket
{
    #region KCHSocketEventArgs public class, used for event parameters.
    /// <summary>
    /// KCHSocketEventArgs, this is used for events.
    /// Returns necessary information to the application in a managable format. 
    /// </summary>
    public class KCHSocketEventArgs : EventArgs
    {

        #region Constructor
        /// <summary>
        /// Constructor, accepts no parameters.
        /// </summary>
        public KCHSocketEventArgs() { }
        /// <summary>
        /// Constructor, accepts connection parameter.
        /// </summary>
        /// <param name="Connections">int: Current server connections.</param>
        public KCHSocketEventArgs(int Connections)
        {
            this._Connections = Connections;
        }
        /// <summary>
        /// Constructor, accepts Connection, Socket parameters.
        /// </summary>
        /// <param name="Connections">int: Current server connections.</param>
        /// <param name="_Socket">Socket: Socket causing event.</param>
        public KCHSocketEventArgs(int Connections, Socket _Socket)
        {
            this.KCHSocket = _Socket;
            this._Connections = Connections;
        }
        /// <summary>
        /// Constructor, accepts Connection, Data, Socket parameters.
        /// </summary>
        /// <param name="Connections">int: Current server connections.</param>
        /// <param name="sDataIn">string: DataIn</param>
        /// <param name="_Socket">Socket: Socket causing event.</param>
        public KCHSocketEventArgs(int Connections, byte[] bytesIn, Socket _Socket)
        {
            this.KCHSocket = _Socket;
            this._DataIn = bytesIn;
            this._Connections = Connections;
        }
        #endregion

        #region Properties
        private Socket _Socket;
        /// <summary>
        /// Socket causing event.
        /// </summary>
        public Socket KCHSocket
        {
            get { return _Socket; }
            set { _Socket = value; }
        }

        /// <summary>
        /// Returns the Remote IP Address.
        /// </summary>
        public string RemoteIPAddress
        {
            get
            {
                if (this.KCHSocket != null)
                {
                    if (this.KCHSocket.RemoteEndPoint != null)
                        return this.KCHSocket.RemoteEndPoint.ToString().Split(':')[0];
                    else
                        return "";
                }
                else
                    return "";
            }
        }

        /// <summary>
        /// Returns the Remote HostName.
        /// </summary>
        public string RemoteHost
        {
            get
            {
                try { return Dns.GetHostByAddress(this.RemoteIPAddress).HostName; }
                catch { return this.RemoteIPAddress; }
            }
        }

        /// <summary>
        /// Returns the Local IP Address.
        /// </summary>
        public string LocalIPAddress
        {
            get
            {
                if (this.KCHSocket.LocalEndPoint != null)
                    return this.KCHSocket.LocalEndPoint.ToString();
                else
                    return "";
            }
        }

        /// <summary>
        /// Returns the LocalHost Name.
        /// </summary>
        public string LocalHost
        {
            get { return Dns.GetHostName(); }
        }

        private byte[] _DataIn;
        /// <summary>
        /// DataArrived with event.
        /// </summary>
        public string DataArrived
        {
            get { return Encoding.GetEncoding(1252).GetString(_DataIn, 0, _DataIn.Length); }
        }

        /// <summary>
        /// Bytes arrived with event.
        /// </summary>
        public byte[] BytesArrived
        {
            get { return _DataIn; }
        }

        private int _Connections;
        /// <summary>
        /// Total server connections.
        /// </summary>
        public int Connections
        {
            get { return _Connections; }
            set { _Connections = value; }
        }
        #endregion
    }
    #endregion

    #region Delegates, used for event pointing.
    /// <summary>
    /// Delegate pointers, these are used for calling events, and async methods.
    /// </summary>

    /// <summary>
    /// Delegate for Event Handling
    /// </summary>
    public delegate void KCHSocketEventHandler(object sender, KCHSocketEventArgs e);
    /// <summary>
    /// Delegate for Error Handling
    /// </summary>
    public delegate void KCHSocketError(object sender, Exception e);

    #endregion

    #region KCHTcpClient internal class, used for exposing socket (derives from TcpClient)
    /// <summary>
    /// KCHTcpClient, this is used to expose the underlying socket within the TcpClient Class.
    /// (Derives from TcpClient)
    /// </summary>
    public class KCHTcpClient : TcpClient
    {
        /// <summary>
        /// Initializes a new instance of the System.Net.Sockets.TcpClient class and
        /// binds it to the specified local endpoint.
        /// </summary>
        /// <param name="localEP">The System.Net.IPEndPoint to which you bind the TCP System.Net.Sockets.Socket.</param>
        public KCHTcpClient(IPEndPoint localEP)
            : base(localEP)
        { }

        /// <summary>
        /// Exposes the Underlying socket.
        /// </summary>
        public Socket ClientSocket
        {
            get { return base.Client; }
        }

        public bool IsActive
        {
            get { return base.Active; }
        }
    }
    #endregion

    #region KchTcpListener internal clss, used for exposed socket (derives from TcpListner)
    /// <summary>
    /// KchTcpListener internal clss, used for exposed socket (derives from TcpListner)
    /// </summary>
    internal class KCHTcpListener : TcpListener
    {
        //Constructors
        //public KCHTcpListener(int Port):base(Port){} //Obsolete
        public KCHTcpListener(IPAddress localAddr, int Port) : base(localAddr, Port) { }
        public KCHTcpListener(IPEndPoint localEP) : base(localEP) { }

        /// <summary>
        /// Exposes the underlying server socket.
        /// </summary>
        public Socket ServerSocket
        {
            get { return base.Server; }
        }

        /// <summary>
        /// Exposes the underlying Active boolean
        /// </summary>
        public new bool Active
        {
            get { return base.Active; }
        }
    }
    #endregion

    #region KCHClient Helper public class, used to connect to remote hosts.
    /// <summary>
    /// KCHClient Class, Helper class used to communicate with remote hosts.
    /// </summary>
    public class KCHClient
    {
        public KCHClient(string uniqueId, string remoteHost, int remotePort, string localHost, int localPort)
        {
            _uniqueId = uniqueId;

            this.RemoteHostIP = Dns.GetHostByName(remoteHost).AddressList[0];
            this.RemoteHost = remoteHost;
            this.RemotePort = remotePort;

            this.LocalHostIP = Dns.GetHostByName(localHost).AddressList[0];
            this.LocalHost = localHost;
            this.LocalPort = localPort;

            this.LocalEP = new IPEndPoint(this.LocalHostIP, this.LocalPort);

            KCHtc = new KCHTcpClient(this.LocalEP);
        }

        #region Events
        /// <summary>
        /// KCHClient_DataArrival event, this is fired when data arrives from a connection.
        /// </summary>
        public event KCHSocketEventHandler KCHClient_DataArrival;
        /// <summary>
        /// KCHClient_Connection event, this is fired when a new connection is established.
        /// </summary>
        public event KCHSocketEventHandler KCHClient_Connection;
        /// <summary>
        /// KCHClient_Disconnection event, this is fired when a connection is closed.
        /// </summary>
        public event KCHSocketEventHandler KCHClient_Disconnection;
        /// <summary>
        /// KCHClient_Error event, this is fired when an error occurs.
        /// </summary>
        public event KCHSocketError KCHClient_Error;
        #endregion

        private KCHTcpClient KCHtc = null;
        private Thread trdConnect;
        private bool hasConnected = false;
        private ArrayList _ArrayDataArrival = new ArrayList();
        private Thread trdCheckForData;

        public KCHTcpClient KCHClientSocket
        {
            get { return KCHtc; }
        }

        private int _ThreadSleep = 100;
        /// <summary>
        /// The amount of time to wait when literating through threaded loops
        /// </summary>
        public int ThreadSleep
        {
            get { return _ThreadSleep; }
            set { _ThreadSleep = value; }
        }

        private bool _AcceptData = true;
        /// <summary>
        /// gets or sets whether to listen for Data_Arrival.
        /// </summary>
        public bool AcceptData
        {
            get
            {
                return _AcceptData;
            }
            set
            {
                _AcceptData = value;
            }
        }

        /// <summary>
        /// get whether the client is connected.
        /// </summary>
        public bool Connected
        {
            get
            {
                if (this.KCHtc != null && this.KCHtc.ClientSocket != null)
                {
                    if (!this.hasConnected)
                        return this.KCHtc.ClientSocket.Connected;
                    else
                    {
                        //Added try catch to check for disposal of object whilst checking if connected.
                        try
                        {
                            if (this.KCHtc.ClientSocket.Available > 0) return true;

                            if (this.KCHtc.ClientSocket.Poll(1, SelectMode.SelectRead) == false || (this.KCHtc.ClientSocket.Available != 0))
                                return true;
                            else
                                return false;
                        }
                        catch (ObjectDisposedException)
                        {
                            return false;
                        }
                    }
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// Send a message to the server.
        /// </summary>
        /// <param name="Message">string: Message to send.</param>
        public bool Send(string Message)
        {
            if (this.Connected)
            {
                try
                {
                    byte[] bMessage = Encoding.GetEncoding(1252).GetBytes(Message);
                    IAsyncResult ar = this.KCHtc.ClientSocket.BeginSend(bMessage, 0, bMessage.Length, SocketFlags.None, null, null);

                    while (!ar.IsCompleted)
                    {
                        if (!this.Connected) return false; ;
                        Thread.Sleep(this.ThreadSleep);
                    }

                    //If connection is closed by remote host this method will fail.
                    this.KCHtc.ClientSocket.EndSend(ar);

                    return true;
                }
                catch { return false; } //Catch the send message error.
            }
            else { return false; } //Can't send if not connected.
        }

        /// <summary>
        /// Send a message to the server.
        /// </summary>
        /// <param name="Message">string: Message to send.</param>
        public bool Send(byte[] Message)
        {
            if (this.Connected)
            {
                try
                {
                    IAsyncResult ar = this.KCHtc.ClientSocket.BeginSend(Message, 0, Message.Length, SocketFlags.None, null, null);

                    while (!ar.IsCompleted)
                    {
                        if (!this.Connected) return false;
                        Thread.Sleep(this.ThreadSleep);
                    }

                    //If connection is closed by remote host this method will fail.
                    this.KCHtc.ClientSocket.EndSend(ar);

                    return true;
                }
                catch { return false; } //Catch the send message error.
            }
            else { return false; } //Can't send if not connected.
        }

        /// <summary>
        /// Disconnect the socket.
        /// </summary>
        public void Disconnect()
        {
            try
            {
                if (KCHtc != null)
                {
                    if (KCHtc.ClientSocket != null)
                    {
                        if (this.KCHClient_Disconnection != null)
                            this.KCHClient_Disconnection(this, new KCHSocketEventArgs(0, this.KCHtc.ClientSocket));

                        if (KCHtc.Connected)
                            KCHtc.GetStream().Close();
                        KCHtc.ClientSocket.Close();
                    }
                }

                this.Dispose();
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                if (this.KCHClient_Error != null)
                    this.KCHClient_Error(this, ex);
                else
                    throw (ex);
            }
        }

        private string _RemoteHost;
        /// <summary>
        /// The remote host to connect to.
        /// </summary>
        private string RemoteHost
        {
            get { return _RemoteHost; }
            set { _RemoteHost = value; }
        }

        private string _LocalHost;
        /// <summary>
        /// The local host to connect to.
        /// </summary>
        public string LocalHost
        {
            get { return _LocalHost; }
            set { _LocalHost = value; }
        }

        private IPAddress _RemoteHostIP;
        /// <summary>
        /// The remote host to connect to.
        /// </summary>
        private IPAddress RemoteHostIP
        {
            get { return _RemoteHostIP; }
            set { _RemoteHostIP = value; }
        }

        private IPAddress _LocalHostIP;
        /// <summary>
        /// The local host to connect to.
        /// </summary>
        public IPAddress LocalHostIP
        {
            get { return _LocalHostIP; }
            set { _LocalHostIP = value; }
        }

        private int _RemotePort;
        /// <summary>
        /// The remote port to connect on,
        /// </summary>
        private int RemotePort
        {
            get { return _RemotePort; }
            set { _RemotePort = value; }
        }

        private int _LocalPort;
        /// <summary>
        /// The local port to connect on,
        /// </summary>
        public int LocalPort
        {
            get { return _LocalPort; }
            set { _LocalPort = value; }
        }

        private IPEndPoint _LocalEP;
        /// <summary>
        /// gets or sets local endpoint.
        /// </summary>
        public IPEndPoint LocalEP
        {
            get { return _LocalEP; }
            set { _LocalEP = value; }
        }

        private readonly string _uniqueId;
        /// <summary>
        /// gets the unique id
        /// </summary>
        public string UniqueId
        {
            get { return _uniqueId; }
        }

        private Tuple<int, object> _payload;
        /// <summary>
        /// gets the payload
        /// </summary>
        public Tuple<int, object> Payload
        {
            get { return _payload; }
            set { _payload = value; }
        }


        /// <summary>
        /// Connect to a remote host.
        /// </summary>
        public void Connect()
        {
            try
            {
                InitConnection();
            }
            catch (Exception ex)
            {
                if (this.KCHClient_Error != null)
                    this.KCHClient_Error(this, ex);
                else
                    throw (ex);
            }
        }

        private void StartCheckForData()
        {
            try
            {
                trdCheckForData = new Thread(new ThreadStart(CheckForData));
                trdCheckForData.Name = "Thread Check for Data";
                trdCheckForData.IsBackground = true;
                trdCheckForData.Start();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Connect to a remote host on designated thread.
        /// </summary>
        private void ConnectThread()
        {
            try
            {
                KCHtc.Connect(this.RemoteHostIP, this.RemotePort);

                if (this.KCHClient_Connection != null)
                    this.KCHClient_Connection(this, new KCHSocketEventArgs(1, KCHtc.ClientSocket));

                this.StartCheckForData();

                while (this.Connected)
                {
                    try
                    {
                        this.hasConnected = true;

                        while (this.KCHtc.ClientSocket.Available == 0 && this.Connected)
                            Thread.Sleep(10);

                        Byte[] bytes = new Byte[this.KCHtc.ClientSocket.Available];//this.KCHtc.ClientSocket.Available];

                        this.KCHtc.ClientSocket.Receive(bytes);

                        if (bytes.Length > 0)
                            _ArrayDataArrival.Add(new KCHSocketEventArgs(1, bytes, this.KCHtc.ClientSocket));
                    }
                    catch (SocketException) { this.Disconnect(); }
                }

                while (this._ArrayDataArrival.Count > 0)
                    Thread.Sleep(this.ThreadSleep);

                this.hasConnected = false;
                this.Disconnect();
                this.KCHtc = new KCHTcpClient(this.LocalEP);
            }
            catch (ObjectDisposedException) { }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                if (this.KCHClient_Error != null)
                    this.KCHClient_Error(this, ex);
                else
                    throw (ex);
            }
        }

        private void ThrowData()
        {
            try
            {
                while (this._ArrayDataArrival.Count >= 1)
                {
                    try
                    {
                        if (this.KCHClient_DataArrival != null)
                            this.KCHClient_DataArrival(this, (KCHSocketEventArgs)this._ArrayDataArrival[0]);
                    }
                    catch { }

                    this._ArrayDataArrival.RemoveAt(0);
                }
            }
            catch
            { }
        }

        /// <summary>
        /// Check For Data Thread, checks for incoming data.
        /// </summary>
        private void CheckForData()
        {
            try
            {
                while (this.AcceptData)
                {
                    while (this._ArrayDataArrival.Count == 0)
                        Thread.Sleep(this.ThreadSleep);

                    try { this.ThrowData(); }
                    catch { }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                if (this.KCHClient_Error != null)
                    this.KCHClient_Error(this, ex);
                else
                    throw (ex);
            }
        }

        /// <summary>
        /// Initializes the CheckConnection void onto a new thread.
        /// </summary>
        private void InitConnection()
        {
            ThreadStart tsConnection = new ThreadStart(this.ConnectThread);
            trdConnect = new Thread(tsConnection);
            trdConnect.Name = "Thread Connection.";
            trdConnect.IsBackground = true;
            trdConnect.Start();
        }

        /// <summary>
        /// Clean up socket and Kill Threads.
        /// </summary>
        public void Dispose()
        {
            try
            {
                KillConnection();

                //Kill the check for data thread.
                if (trdCheckForData.ThreadState != ThreadState.Aborted && trdCheckForData.ThreadState != ThreadState.AbortRequested)
                {
                    trdCheckForData.Abort();
                    trdCheckForData.Join(10);
                }
            }
            catch { }
        }

        /// <summary>
        /// Kill the check for data thread.
        /// </summary>
        private void KillConnection()
        {
            if (trdConnect != null)
            {
                if (trdConnect.ThreadState != ThreadState.Aborted && trdConnect.ThreadState != ThreadState.AbortRequested)
                {
                    trdConnect.Abort();
                    trdConnect.Join(10);
                }
            }
        }
    }
    #endregion

    #region KCHServer Helper public class, used to produce a server application.
    /// <summary>
    /// KCHServer Helper class, this is used for producing Tcp/Ip Server applications.
    /// </summary>
    public class KCHServer
    {
        private readonly string _uniqueId;

        public string UniqueId
        {
            get { return _uniqueId; }
        }

        /// <summary>
        /// Constructor, takes 0 args.
        /// </summary>
        public KCHServer(string uniqueId)
        {
            _uniqueId = uniqueId;
        }

        #region Events
        /// <summary>
        /// Data_Arrival event, this is fired when data arrives from a connection.
        /// </summary>
        public event KCHSocketEventHandler KCHServer_DataArrival;
        /// <summary>
        /// Server_Connection event, this is fired when a new connection is established.
        /// </summary>
        public event KCHSocketEventHandler KCHServer_Connection;
        /// <summary>
        /// Server_Disconnection event, this is fired when a connection is closed.
        /// </summary>
        public event KCHSocketEventHandler KCHServer_Disconnection;
        /// <summary>
        /// Server_Error event, this is fired whenever a server exception occurs.
        /// </summary>
        public event KCHSocketError KCHServer_Error;
        #endregion

        /// <summary>
        /// Arrays, used to manage threads and KCHTcpHandlers.
        /// </summary>
        private ArrayList _ArrayListTcpClients = new ArrayList();
        private ArrayList _ArrayThreadPool = new ArrayList();
        private ArrayList _ArrayDataArrival = new ArrayList();

        private Thread _trdCheckForDataThread;
        private Thread _trdCheckStatusThread;
        private Thread _trdAddListenThread;

        private KCHTcpListener _TcpListener;

        private bool _AcceptConnections = true;
        /// <summary>
        /// Whether or not to accept connections, this will not interrupt current connections.
        /// New connections will not be established.
        /// </summary>
        public bool AcceptConnections
        {
            get { return _AcceptConnections; }
            set { _AcceptConnections = value; }
        }

        private bool _AcceptData = true;
        /// <summary>
        /// Whether or not to currently process data. (Data will remain on Tcp queue.)
        /// True = Start the CheckForData Thread.
        /// False = Abort the CheckForData Thread, and ignore inbound data.B
        /// (No data will be lost from changing this value but data will stop being processed.)
        /// </summary>
        public bool AcceptData
        {
            get { return _AcceptData; }
            set
            {
                if (value == true)
                    this.AddCheckForDataThread();
                else
                {
                    if (_trdCheckForDataThread != null)
                        if (_trdCheckForDataThread.ThreadState != ThreadState.Aborted && _trdCheckForDataThread.ThreadState != ThreadState.AbortRequested)
                            _trdCheckForDataThread.Abort();
                }

                _AcceptData = value;
            }
        }

        private int _ConnectionLimit = -1;
        /// <summary>
        /// Sets a connection limit, set this to -1 for no limit.
        /// </summary>
        public int ConnectionLimit
        {
            get { return _ConnectionLimit; }
            set { _ConnectionLimit = value; }
        }

        private int _ThreadSleep = 100;
        /// <summary>
        /// The amount of time to wait when literating through threaded loops
        /// </summary>
        public int ThreadSleep
        {
            get { return _ThreadSleep; }
            set { _ThreadSleep = value; }
        }

        private int _ThreadAbortWait = 10;
        /// <summary>
        /// The amount of time to wait when aborting threads.
        /// </summary>
        public int ThreadAbortWait
        {
            get { return _ThreadAbortWait; }
            set { _ThreadAbortWait = value; }
        }

        private bool _IgnoreData = false;
        /// <summary>
        /// Whether or not to Ignore all inbound data. (Data will be collected and removed.)
        /// True = Ignore all data, the Data_Arrival Event is not fired.
        /// False = Collect all data, the Data_Arrival Event is fired.
        /// </summary>
        private bool IgnoreData
        {
            get { return _IgnoreData; }
            set { _IgnoreData = value; }
        }

        private int _Connections = 0;
        /// <summary>
        /// Total amount of current connections to the server.
        /// </summary>
        private int Connections
        {
            get { return _Connections; }
        }

        private int _Port;
        /// <summary>
        /// The port to listen on.
        /// </summary>
        public int Port
        {
            get { return _Port; }
            set { _Port = value; }
        }

        private string _ipString;
        /// <summary>
        /// The IP addr to listen on.
        /// </summary>
        public string IpString
        {
            get { return _ipString; }
            set { _ipString = value; }
        }

        /// <summary>
        /// Stops, listening and Kills Threads, 
        /// (Same as Dispose.)
        /// </summary>
        public void Ignore()
        {
            this.Dispose();
        }

        /// <summary>
        /// Starts the server listening for connections.
        /// </summary>
        public void Listen()
        {
            try
            {
                this.CleanUp();
                AddCheckForDataThread();
                AddCheckStatusThread();
                AddListenThread();
            }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }

        /// <summary>
        /// Starts the server listening for connections.
        /// </summary>
        public void Listen(string IpString, int Port)
        {
            try
            {
                this.IpString = IpString;
                this.Port = Port;
                this.Listen();
            }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }

        /// <summary>
        /// Exposed the underlying Active property.
        /// (Sometimes used to determine whether the port is currently in use.)
        /// </summary>
        public bool Active
        {
            get
            {
                if (_TcpListener != null)
                    return _TcpListener.Active;
                else
                    return false;
            }
        }

        /// <summary>
        /// Listen method, This is used to start the TcpServer.
        /// </summary>
        private void ListenThread()
        {
            try
            {

                IPAddress ipAddress = IPAddress.Parse(this.IpString);

                _TcpListener = new KCHTcpListener(ipAddress, this.Port);
                _TcpListener.Start();

                while (this.AcceptConnections)
                {
                    Thread.Sleep(this.ThreadSleep);
                    try
                    {
                        Socket _Socket = _TcpListener.AcceptSocket();

                        if (this.AcceptConnections && (this.ConnectionLimit != this.Connections))
                        {
                            this._Connections += 1;

                            //Add a if to the event caller
                            //(this is just in case the client app does not use this event.)
                            if (this.KCHServer_Connection != null)
                                this.KCHServer_Connection(this, new KCHSocketEventArgs(this.Connections, _Socket));

                            this._ArrayListTcpClients.Add(new KCHClientHandler());
                            ((KCHClientHandler)this._ArrayListTcpClients[this._ArrayListTcpClients.Count - 1]).SocketToHandle = _Socket;
                            ((KCHClientHandler)this._ArrayListTcpClients[this._ArrayListTcpClients.Count - 1]).KCHClientHandler_DataArrival += new KCHSocketEventHandler(DataArrival);

                            ThreadStart delThrd = new ThreadStart(((KCHClientHandler)this._ArrayListTcpClients[this._ArrayListTcpClients.Count - 1]).HandleClient);
                            this.LaunchThread(delThrd);
                        }
                    }
                    catch
                    { }
                }

                _TcpListener.Stop();
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }


        /// <summary>
        /// This is for the KCHClientHandler to notfiy of data Arrival.
        /// </summary>
        /// <param name="sender">KCHClientHandler.</param>
        /// <param name="e">Event Args.</param>
        private void DataArrival(object sender, KCHSocketEventArgs e)
        {
            if (this._ArrayDataArrival != null && !this.IgnoreData)
            {
                e.Connections = this.Connections;
                this._ArrayDataArrival.Add(e);
            }
        }

        #region Sending textual data commands.
        /// <summary>
        /// Sends a message to designated socket.
        /// </summary>
        /// <param name="Message">string: Message to send.</param>
        /// <param name="SocketHandle">string: Socket to send to.</param>
        public void Send(string Message, IntPtr SocketHandle)
        {
            try
            {
                int i;

                for (i = 0; i < this._ArrayListTcpClients.Count; i++)
                {
                    KCHClientHandler KCHch = (KCHClientHandler)this._ArrayListTcpClients[i];

                    if (SocketHandle == KCHch.SocketToHandle.Handle)
                    {
                        KCHch.Send(System.Text.Encoding.GetEncoding(1252).GetBytes(Message));
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }

        /// <summary>
        /// Sends a message to designated socket.
        /// </summary>
        /// <param name="Message">string: Message to send.</param>
        /// <param name="SocketHandle">string: Socket to send to.</param>
        public void Send(byte[] Message, IntPtr SocketHandle)
        {
            try
            {
                int i;
                for (i = 0; i < this._ArrayListTcpClients.Count; i++)
                {
                    KCHClientHandler KCHch = (KCHClientHandler)this._ArrayListTcpClients[i];
                    if (SocketHandle == KCHch.SocketToHandle.Handle)
                    {
                        KCHch.Send(Message);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }

        #region BroadCast
        /// <summary>
        /// Broadcast a message to clients.
        /// </summary>
        /// <param name="Message">string: Message to send.</param>
        /// <param name="SocketToIgnore">IntPtr: Socket Handle to ingore.</param>
        public void BroadCast(string Message, IntPtr SocketToIgnore)
        {
            try
            {
                int i;
                for (i = 0; i < this._ArrayListTcpClients.Count; i++)
                {
                    KCHClientHandler KCHch = (KCHClientHandler)this._ArrayListTcpClients[i];
                    if (SocketToIgnore != KCHch.SocketToHandle.Handle)
                    {
                        KCHch.Send(System.Text.Encoding.GetEncoding(1252).GetBytes(Message));
                    }
                }
            }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }

        /// <summary>
        /// Broadcast a message to clients.
        /// </summary>
        /// <param name="Message">string: Message to send.</param>
        public void BroadCast(string Message)
        {
            try
            {
                int i;
                for (i = 0; i < this._ArrayListTcpClients.Count; i++)
                {
                    KCHClientHandler KCHch = (KCHClientHandler)this._ArrayListTcpClients[i];
                    KCHch.Send(System.Text.Encoding.GetEncoding(1252).GetBytes(Message));
                }
            }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }
        #endregion
        #endregion

        /// <summary>
        /// Check the TcpArray to see if the clients are still connected.
        /// </summary>
        private void CheckClientStatus()
        {
            try
            {
                while (this.AcceptData)
                {
                    Thread.Sleep(this.ThreadSleep);

                    int i;
                    for (i = (this._ArrayListTcpClients.Count - 1); i >= 0; i--)
                    {
                        if (!((KCHClientHandler)this._ArrayListTcpClients[i]).Connected)
                        {
                            this._Connections -= 1;
                            if (this.KCHServer_Disconnection != null)
                                this.KCHServer_Disconnection(this, new KCHSocketEventArgs(this.Connections, ((KCHClientHandler)this._ArrayListTcpClients[i]).SocketToHandle));

                            this._ArrayListTcpClients.RemoveAt(i);
                        }
                    }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }


        /// <summary>
        /// CheckForData, run in a seperate thread, literates through the _ArrayDataArrival Array.
        /// This is the routine that detects whether Data has arrived and calls the Data_Arrived Event.
        /// </summary>
        private void CheckForData()
        {
            try
            {
                while (this.AcceptData)
                {
                    while (this._ArrayDataArrival.Count == 0 && this.AcceptData)
                        Thread.Sleep(this.ThreadSleep);

                    while (this._ArrayDataArrival.Count >= 1)
                    {
                        try
                        {
                            if (this.KCHServer_DataArrival != null)
                                this.KCHServer_DataArrival(this, (KCHSocketEventArgs)this._ArrayDataArrival[0]);
                        }
                        catch (ObjectDisposedException) { }

                        this._ArrayDataArrival.RemoveAt(0);
                    }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                if (this.KCHServer_Error != null)
                    this.KCHServer_Error(this, ex);
                else
                    throw (ex);
            }
        }

        /// <summary>
        /// Close server and abort all threads.
        /// </summary>
        public void Dispose()
        {
            try
            {
                if (_TcpListener != null) _TcpListener.Stop();
                this.AcceptData = false;
                this.AcceptConnections = false;
                CleanUp();
                if (this._ArrayListTcpClients != null) this._ArrayListTcpClients.Clear();
                if (this._ArrayThreadPool != null) this._ArrayThreadPool.Clear();
                if (this._ArrayDataArrival != null) this._ArrayDataArrival.Clear();
            }
            catch { }
        }

        /// <summary>
        /// Kills all threads and closes open connections.
        /// </summary>
        private void CleanUp()
        {
            this.KillThreads();
            this.KillClientHandlers();
        }

        /// <summary>
        /// Closes all server connections.
        /// </summary>
        public void CloseConnections()
        {
            _TcpListener.Stop();
            this.CleanUp();

            this._Connections = 0;

            if (this.KCHServer_Disconnection != null)
                this.KCHServer_Disconnection(this, new KCHSocketEventArgs(0));
        }

        /// <summary>
        /// Adds the CheckForData Thread to the application.
        /// </summary>
        private void AddListenThread()
        {
            _trdAddListenThread = new Thread(new ThreadStart(this.ListenThread));
            _trdAddListenThread.IsBackground = true;
            _trdAddListenThread.Start();
        }
        /// <summary>
        /// Adds the CheckForData Thread to the application.
        /// </summary>
        private void AddCheckForDataThread()
        {
            _trdCheckForDataThread = new Thread(new ThreadStart(this.CheckForData));
            _trdCheckForDataThread.IsBackground = true;
            _trdCheckForDataThread.Start();
        }
        /// <summary>
        /// Adds the CheckStatus Thread to the application.
        /// </summary>
        private void AddCheckStatusThread()
        {
            _trdCheckStatusThread = new Thread(new ThreadStart(this.CheckClientStatus));
            _trdCheckStatusThread.IsBackground = true;
            _trdCheckStatusThread.Start();
        }

        /// <summary>
        /// Launch a new thread and add it to the thread Array.
        /// </summary>
        /// <param name="thrdStart">The thread start pointer.</param>
        private void LaunchThread(ThreadStart thrdStart)
        {
            Thread oThread = new Thread(thrdStart);
            oThread.IsBackground = true;

            this._ArrayThreadPool.Add(oThread);
            ((Thread)this._ArrayThreadPool[this._ArrayThreadPool.Count - 1]).Start();
        }

        /// <summary>
        /// Close all client handlers.
        /// </summary>
        private void KillClientHandlers()
        {
            try
            {
                if (this._ArrayListTcpClients != null)
                {
                    int i;
                    for (i = 0; i < this._ArrayListTcpClients.Count; i++)
                    {
                        if (this._ArrayListTcpClients[i] != null)
                            ((KCHClientHandler)this._ArrayListTcpClients[i]).Close();
                    }
                    this._ArrayListTcpClients.Clear();
                }
            }
            catch { }
        }

        /// <summary>
        /// Kill a specified thread., Returns whether Thread was Active.
        /// </summary>
        /// <param name="ThreadIndex"></param>
        private bool KillThread(int ThreadIndex)
        {
            try
            {
                if (this._ArrayThreadPool != null)
                {
                    if (this._ArrayThreadPool[ThreadIndex] != null)
                    {
                        Thread _Thread = (Thread)this._ArrayThreadPool[ThreadIndex];
                        if (_Thread.ThreadState != ThreadState.Aborted && _Thread.ThreadState != ThreadState.AbortRequested)
                        {
                            _Thread.Abort();
                            _Thread.Join(this.ThreadAbortWait);
                            return true;
                        }
                        else
                            return false;
                    }
                    return false;
                }
                return false;
            }
            catch { }
            return false;
        }

        /// <summary>
        /// Abort all threads.
        /// </summary>
        private void KillThreads()
        {
            try
            {
                try { _trdAddListenThread.Abort(); }
                catch { }
                try { _trdCheckForDataThread.Abort(); }
                catch { }
                try { _trdCheckStatusThread.Abort(); }
                catch { }

                if (this._ArrayThreadPool != null)
                {
                    int i;
                    for (i = 0; i < this._ArrayThreadPool.Count; i++)
                        KillThread(i);

                    this._ArrayThreadPool.Clear();
                }
            }
            catch { }
        }
    }
    #endregion

    #region KCHClientHandler internal class, used to Handle TcpClient Sockets.
    /// <summary>
    /// internal KCHClientHandler class, this is used for Handling sockets.
    /// </summary>
    internal class KCHClientHandler
    {
        public KCHClientHandler()
        { }

        public event KCHSocketEventHandler KCHClientHandler_Disconnected;
        public event KCHSocketEventHandler KCHClientHandler_DataArrival;

        private Socket _Socket;
        /// <summary>
        /// gets or sets the socket this class will handle.
        /// </summary>
        public Socket SocketToHandle
        {
            get { return _Socket; }
            set
            {

                trdSendBuffer = new Thread(new ThreadStart(SendBuffer));
                trdSendBuffer.IsBackground = true;
                trdSendBuffer.Start();

                _Socket = value;
            }
        }

        private bool _AcceptData = true;
        /// <summary>
        /// gets or sets whether to check for data arrival.
        /// </summary>
        public bool AcceptData
        {
            get { return _AcceptData; }
            set { _AcceptData = value; }
        }

        /// <summary>
        /// Whether the socket is available to send data.
        /// </summary>
        public bool CanSendData
        {
            get { return this.SocketToHandle.Poll(1, SelectMode.SelectWrite); }
        }

        public bool HasError
        {
            get
            {
                return this.SocketToHandle.Poll(1, SelectMode.SelectError);
            }
        }

        /// <summary>
        /// gets whether the client is currently connected.
        /// </summary>
        public bool Connected
        {
            get
            {
                if (this.SocketToHandle.Available > 0) return true;

                if (this.SocketToHandle != null)
                {
                    if (this.SocketToHandle.Poll(1, SelectMode.SelectRead) == false || (this.SocketToHandle.Available != 0))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        private int _ThreadSleep = 100;
        /// <summary>
        /// gets or sets the sleep time for the current thread.
        /// </summary>
        public int ThreadSleep
        {
            get { return _ThreadSleep; }
            set { _ThreadSleep = value; }
        }

        /// <summary>
        /// Closes the sockets connection.
        /// </summary>
        public void Close()
        {
            try
            {
                this.AcceptData = false;
                if (this.SocketToHandle != null)
                {
                    this.SocketToHandle.Shutdown(SocketShutdown.Both);
                    this.SocketToHandle.Close();
                }
            }
            catch { }
        }

        /// <summary>
        /// Sends bytes to the remote host.
        /// </summary>
        /// <param name="Message">byte[]: Message to be sent.</param>
        public void Send(byte[] Message)
        {
            if (this.Connected)
            {
                _ArrSendBuffer.Add(Message);
            }
        }

        private Thread trdSendBuffer;
        private ArrayList _ArrSendBuffer = new ArrayList();
        public void SendBuffer()
        {
            try
            {
                bool bThreadActive = true;
                while (bThreadActive)
                {
                    while (this._ArrSendBuffer.Count == 0 && this.Connected)
                    {
                        Thread.Sleep(this.ThreadSleep);
                    }

                    while (this._ArrSendBuffer.Count >= 1)
                    {

                        if (this.CanSendData)
                        {
                            int i = this.SocketToHandle.Send((byte[])_ArrSendBuffer[0], 0, ((byte[])_ArrSendBuffer[0]).Length, SocketFlags.None);
                            this._ArrSendBuffer.RemoveAt(0);
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Detects whether the there is data waiting to be collected on the socket.
        /// This void should be called async or from a new thread.
        /// </summary>
        public void HandleClient()
        {
            while (this.AcceptData && this.Connected)
            {
                while (this.SocketToHandle.Available == 0 && this.Connected)
                    Thread.Sleep(this.ThreadSleep);

                try
                {
                    Byte[] bytes = new Byte[this.SocketToHandle.Available];
                    this.SocketToHandle.Receive(bytes, 0, bytes.Length, SocketFlags.None);

                    if (bytes.Length != 0)
                    {
                        if (this.KCHClientHandler_DataArrival != null)
                            this.KCHClientHandler_DataArrival(this, new KCHSocketEventArgs(1, bytes, this.SocketToHandle));
                    }
                }
                catch { }
            }

            if (this.KCHClientHandler_Disconnected != null)
                this.KCHClientHandler_Disconnected(this, new KCHSocketEventArgs(1, this.SocketToHandle));

            if (trdSendBuffer != null)
            {
                try { trdSendBuffer.Abort(); }
                catch { }
            }
        }

    }
    #endregion
}

