﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public sealed class Utility
    {
        /// <summary>
        /// Returns a site relative HTTP path from a partial path starting out with a ~.
        /// Same syntax that ASP.Net internally supports but this method can be used
        /// outside of the Page framework.
        /// 
        /// Works like Control.ResolveUrl including support for ~ syntax
        /// but returns an absolute URL.
        /// </summary>
        /// <param name="originalUrl">Any Url including those starting with ~</param>
        /// <returns>relative url</returns>
        public static string ResolveUrl(string originalUrl)
        {
            if (string.IsNullOrEmpty(originalUrl))
                return originalUrl;

            // *** Absolute path - just return
            if (originalUrl.IndexOf("://") != -1)
                return originalUrl;

            // *** Fix up path for ~ root app dir directory
            if (originalUrl.StartsWith("~"))
            {
                // VirtualPathUtility blows up if there is a query string, so we
                // have to account for this.
                int queryStringStartIndex = originalUrl.IndexOf('?');
                if (queryStringStartIndex != -1)
                {
                    string queryString = originalUrl.Substring(queryStringStartIndex);
                    string baseUrl = originalUrl.Substring(0, queryStringStartIndex);

                    return VirtualPathUtility.ToAbsolute(baseUrl) + queryString;
                }
                else
                {
                    return VirtualPathUtility.ToAbsolute(originalUrl);
                }
            }

            return originalUrl;
        }

        /// <summary>
        /// This method returns a fully qualified absolute server Url which includes
        /// the protocol, server, port in addition to the server relative Url.
        /// 
        /// Works like Control.ResolveUrl including support for ~ syntax
        /// but returns an absolute URL.
        /// </summary>
        /// <param name="ServerUrl">Any Url, either App relative or fully qualified</param>
        /// <param name="forceHttps">if true forces the url to use https</param>
        /// <returns></returns>
        public static string ResolveServerUrl(string serverUrl, bool forceHttps)
        {
            if (string.IsNullOrEmpty(serverUrl))
                return serverUrl;

            // *** Is it already an absolute Url?
            if (serverUrl.IndexOf("://") > -1)
                return serverUrl;

            string newServerUrl = ResolveUrl(serverUrl);
            Uri result = new Uri(HttpContext.Current.Request.Url, newServerUrl);

            if (forceHttps)
            {
                UriBuilder builder = new UriBuilder(result);
                builder.Scheme = Uri.UriSchemeHttps;
                builder.Port = 443;

                result = builder.Uri;
            }

            return result.ToString();
        }

        /// <summary>
        /// This method returns a fully qualified absolute server Url which includes
        /// the protocol, server, port in addition to the server relative Url.
        /// 
        /// It work like Page.ResolveUrl, but adds these to the beginning.
        /// This method is useful for generating Urls for AJAX methods
        /// </summary>
        /// <param name="ServerUrl">Any Url, either App relative or fully qualified</param>
        /// <returns></returns>
        public static string ResolveServerUrl(string serverUrl)
        {
            try
            {
                if (Uri.IsWellFormedUriString(serverUrl, UriKind.RelativeOrAbsolute))
                {
                    return ResolveServerUrl(serverUrl, false);
                }
                else return serverUrl;
            }
            catch
            {
                return serverUrl;
            }
        }

        /// <summary>
        /// Generates a key to be used for encryption/decryption
        /// http://msdn.microsoft.com/en-us/library/zb9zth5a.aspx
        /// http://blogs.msdn.com/b/shawnfa/archive/2004/04/14/113514.aspx
        /// </summary>
        public static byte[] GenerateKey(string password, string salt, string hashType, int iterations, int keyLength)
        {
            byte[] result;
            Encoding enc = ASCIIEncoding.ASCII;

            byte[] pwdBytes = enc.GetBytes(password);
            byte[] saltBytes = enc.GetBytes(salt);
            string hashAlgType = hashType;

            try
            {
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(pwdBytes, saltBytes, hashType, iterations);

                // For other encryption algorithms, pdb.CryptDeriveKey() is preferred instead of GetBytes(length), 
                // but I could not get this to work with Rijndael

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                result = pdb.GetBytes(keyLength / 8);
            }
            finally
            {
                ClearBytes(pwdBytes);
                ClearBytes(saltBytes);
            }

            return result;
        }

        static void ClearBytes(byte[] buffer)
        {
            // Check arguments.
            if (buffer == null)
            {
                throw new ArgumentException("buffer");
            }

            // Set each byte in the buffer to 0.
            for (int x = 0; x < buffer.Length; x++)
            {
                buffer[x] = 0;
            }
        }

        public static byte[] RijndaelEncrypt(string plainText, byte[] key, int keySize, byte[] iv)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("IV");

            MemoryStream msEncrypt = null;

            RijndaelManaged aesAlg = null;

            try
            {
                aesAlg = new RijndaelManaged();
                aesAlg.Padding = PaddingMode.PKCS7;  // PKCS7 is default, but I hardcode it here for clarity
                // AES has a fixed block size of 128 bits and a key size of 128, 192, or 256 bits, 
                // whereas Rijndael can be specified with block and key sizes in any multiple of 32 bits, 
                // with a minimum of 128 bits. The blocksize has a maximum of 256 bits, 
                // but the keysize has theoretically no maximum. (from Wikipedia on AES)
                aesAlg.BlockSize = 128;         // 128 (for AES), 192 and 256 (Rijndael only), default = 128
                aesAlg.KeySize = keySize;       // 128, 192, or 256
                aesAlg.Key = key;               // Secret Key

                // In cryptography, an initialization vector (IV) is a block of bits that is required 
                // to allow a stream cipher or a block cipher to be executed... 
                // to produce a unique stream independent from other streams produced by the 
                // same encryption key, without having to go through a (usually lengthy) re-keying process.
                aesAlg.IV = iv;                 // Initialization Vector - bit length must match BlockSize

                // create an encryptor
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(key, iv);

                // create streams used for encryption
                msEncrypt = new MemoryStream();
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(plainText);
                    }
                }
            }
            finally
            {
                if (aesAlg != null)
                    aesAlg.Dispose();
            }

            return msEncrypt.ToArray();
        }

        public static byte[] RijndaelDecrypt(byte[] cipherText, byte[] key, int keySize, byte[] iv)
        {
            byte[] result;

            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("encryptedString");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("IV");

            RijndaelManaged aesAlg = null;
            try
            {
                aesAlg = new RijndaelManaged();
                aesAlg.BlockSize = 128;         // 128 (for AES), 192 and 256 (Rijndael only), default = 128
                aesAlg.Padding = PaddingMode.PKCS7;
                aesAlg.KeySize = keySize;       // 128, 192, or 256; Key size must be set BEFORE actual Key
                aesAlg.Key = key;               // Secret Key
                aesAlg.IV = iv;                 // Initialization Vector (bit length must match BlockSize)

                // create a decryptor
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(key, iv);

                // create streams used for encryption
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (BinaryReader rdr = new BinaryReader(csDecrypt))
                        {
                            result = rdr.ReadBytes(cipherText.Length);
                        }
                    }
                }
            }
            finally
            {
                if (aesAlg != null)
                    aesAlg.Dispose();
            }

            return result;
        }

        public static void OpenNetworkConnection(string savePath, string username, string password)
        {
            var directory = Path.GetDirectoryName(savePath).Trim();
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments =
                        "NET USE " + directory + " /user:" +
                        username +
                        " " + password
                }
            };

            process.Start();
            process.Close();
        }

        public static void CloseNetworkConnection(string savePath)
        {
            var directory = Path.GetDirectoryName(savePath).Trim();
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments = "NET USE " +
                                directory +
                                " /delete"
                }
            };
            process.Start();
            process.Close();
        }

        /// <summary>
        /// Return the previous or next business day of the date specified.
        /// </summary>
        /// <param name="today"></param>
        /// <param name="addValue"></param>
        /// <returns></returns>
        public static DateTime GetBusinessDay(DateTime today, int addValue)
        {
            #region Sanity Checks
            if ((addValue != -1) && (addValue != 1))
                throw new ArgumentOutOfRangeException("addValue must be -1 or 1");
            #endregion

            if (addValue > 0)
                return NextBusinessDay(today);
            else
                return PreviousBusinessDay(today);
        }

        /// <summary>
        /// Return the previous or next business day of the date specified.
        /// </summary>
        /// <param name="today"></param>
        /// <param name="addValue"></param>
        /// <returns></returns>
        public static DateTime GetBusinessDay(DateTime today, int addValue, ICollection<DateTime> holidays)
        {
            #region Sanity Checks
            if ((addValue != -1) && (addValue != 1))
                throw new ArgumentOutOfRangeException("addValue must be -1 or 1");
            #endregion

            if (addValue > 0)
                return NextBusinessDay(today, holidays);
            else
                return PreviousBusinessDay(today, holidays);
        }

        /// <summary>
        /// return the previous business date of the date specified.
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        public static DateTime PreviousBusinessDay(DateTime today)
        {
            DateTime result;
            switch (today.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    result = today.AddDays(-2);
                    break;

                case DayOfWeek.Monday:
                    result = today.AddDays(-3);
                    break;

                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Friday:
                    result = today.AddDays(-1);
                    break;

                case DayOfWeek.Saturday:
                    result = today.AddDays(-1);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("DayOfWeek=" + today.DayOfWeek);
            }
            return ScreenHolidays(result, -1);
        }

        /// <summary>
        /// return the previous business date of the date specified.
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        public static DateTime PreviousBusinessDay(DateTime today, ICollection<DateTime> holidays)
        {
            DateTime result;
            switch (today.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    result = today.AddDays(-2);
                    break;

                case DayOfWeek.Monday:
                    result = today.AddDays(-3);
                    break;

                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Friday:
                    result = today.AddDays(-1);
                    break;

                case DayOfWeek.Saturday:
                    result = today.AddDays(-1);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("DayOfWeek=" + today.DayOfWeek);
            }

            return ScreenHolidays(result, -1, holidays);
        }

        /// <summary>
        /// return the next business date of the date specified.
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        public static DateTime NextBusinessDay(DateTime today)
        {
            DateTime result;
            switch (today.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    result = today.AddDays(1);
                    break;

                case DayOfWeek.Friday:
                    result = today.AddDays(3);
                    break;

                case DayOfWeek.Saturday:
                    result = today.AddDays(2);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("DayOfWeek=" + today.DayOfWeek);
            }
            return ScreenHolidays(result, 1);
        }

        /// <summary>
        /// return the next business date of the date specified.
        /// </summary>
        /// <param name="today"></param>
        /// <returns></returns>
        public static DateTime NextBusinessDay(DateTime today, ICollection<DateTime> holidays)
        {
            DateTime result;
            switch (today.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    result = today.AddDays(1);
                    break;

                case DayOfWeek.Friday:
                    result = today.AddDays(3);
                    break;

                case DayOfWeek.Saturday:
                    result = today.AddDays(2);
                    break;

                default:
                    throw new ArgumentOutOfRangeException("DayOfWeek=" + today.DayOfWeek);
            }
            return ScreenHolidays(result, 1, holidays);
        }

        /// <summary>
        /// return the mm/dd string of the date specified.
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string MonthDay(DateTime time)
        {
            return String.Format("{0:00}/{1:00}", time.Month, time.Day);
        }

        /// <summary>
        /// screen for holidays 
        /// (simple mode)
        /// </summary>
        /// <param name="result"></param>
        /// <param name="addValue"></param>
        /// <returns></returns>
        public static DateTime ScreenHolidays(DateTime result, int addValue)
        {
            #region Sanity Checks
            if ((addValue != -1) && (addValue != 1))
                throw new ArgumentOutOfRangeException("addValue must be -1 or 1");
            #endregion

            // holidays on fixed date
            switch (MonthDay(result))
            {
                case "01/01":  // Happy New Year
                case "07/04":  // Independent Day
                case "12/25":  // Christmas
                    return GetBusinessDay(result, addValue);
                default:
                    return result;
            }
        }

        public static DateTime ScreenHolidays(DateTime result, int addValue, ICollection<DateTime> holidays)
        {
            #region Sanity Checks
            if ((addValue != -1) && (addValue != 1))
                throw new ArgumentOutOfRangeException("addValue must be -1 or 1");
            #endregion

            // holidays on fixed date
            var temp = from dt in holidays
                       select MonthDay(dt);

            if (temp.Contains(MonthDay(result)))
                return GetBusinessDay(result, addValue, holidays);
            else return result;
        }

        static DateTime CalculateFutureDate(DateTime fromDate, int numberofWorkDays, ICollection<DateTime> holidays)
        {
            var futureDate = fromDate;

            for (var i = 0; i < numberofWorkDays; i++)
            {
                if (futureDate.DayOfWeek == DayOfWeek.Saturday || futureDate.DayOfWeek == DayOfWeek.Sunday ||
                    (holidays != null && holidays.Contains(futureDate)))
                    futureDate = futureDate.AddDays(1); // Increase FutureDate by one because of condition

                futureDate = futureDate.AddDays(1); // Add a working day
            }
            return futureDate;
        }

        /// <SUMMARY>Computes the Levenshtein Edit Distance between two enumerables.</SUMMARY>
        /// <TYPEPARAM name="T">The type of the items in the enumerables.</TYPEPARAM>
        /// <PARAM name="x">The first enumerable.</PARAM>
        /// <PARAM name="y">The second enumerable.</PARAM>
        /// <RETURNS>The edit distance.</RETURNS>
        public static int EditDistance<T>(IEnumerable<T> x, IEnumerable<T> y)
            where T : IEquatable<T>
        {
            // Validate parameters
            if (x == null) throw new ArgumentNullException("x");
            if (y == null) throw new ArgumentNullException("y");

            // Convert the parameters into IList instances
            // in order to obtain indexing capabilities
            IList<T> first = x as IList<T> ?? new List<T>(x);
            IList<T> second = y as IList<T> ?? new List<T>(y);

            // Get the length of both.  If either is 0, return
            // the length of the other, since that number of insertions
            // would be required.
            int n = first.Count, m = second.Count;
            if (n == 0) return m;
            if (m == 0) return n;

            // Rather than maintain an entire matrix (which would require O(n*m) space),
            // just store the current row and the next row, each of which has a length m+1,
            // so just O(m) space. Initialize the current row.
            int curRow = 0, nextRow = 1;
            int[][] rows = new int[][] { new int[m + 1], new int[m + 1] };
            for (int j = 0; j <= m; ++j) rows[curRow][j] = j;

            // For each virtual row (since we only have physical storage for two)
            for (int i = 1; i <= n; ++i)
            {
                // Fill in the values in the row
                rows[nextRow][0] = i;
                for (int j = 1; j <= m; ++j)
                {
                    int dist1 = rows[curRow][j] + 1;
                    int dist2 = rows[nextRow][j - 1] + 1;
                    int dist3 = rows[curRow][j - 1] +
                        (first[i - 1].Equals(second[j - 1]) ? 0 : 1);

                    rows[nextRow][j] = Math.Min(dist1, Math.Min(dist2, dist3));
                }

                // Swap the current and next rows
                if (curRow == 0)
                {
                    curRow = 1;
                    nextRow = 0;
                }
                else
                {
                    curRow = 0;
                    nextRow = 1;
                }
            }

            // Return the computed edit distance
            return rows[curRow][m];
        }

        public static string GenerateOrderLineTransactionNumber(string requestId)
        {
            if (!string.IsNullOrWhiteSpace(requestId))
            {
                Random random = new Random(requestId.GetHashCode());

                int recordId = random.Next();

                Random rnd = new Random();

                int next = rnd.Next(99);

                return $"{recordId}{next}".PadRight(12, '0'); ;
            }
            return requestId;
        }

        /// <summary>
        /// log-sigmoid function 
        /// </summary>
        /// <param name="x">power</param>
        /// <returns>a value between 0 and 1</returns>
        public static double LogSigmoid(double x)
        {
            return 1.0 / (1.0 + Math.Exp(-x));
        }

        /// <summary>
        /// hyperbolic tangent function
        /// </summary>
        /// <param name="x">angle in radians</param>
        /// <returns>a value between -1 and +1</returns>
        public static double HyperbolicTangent(double x)
        {
            return Math.Tanh(x);
        }

        public static string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
        
        public static string GenerateSHA512String(string inputString)
        {
            SHA512 sha512 = SHA512Managed.Create();

            byte[] bytes = Encoding.UTF8.GetBytes(inputString);

            byte[] hash = sha512.ComputeHash(bytes);

            return GetStringFromHash(hash);
        }

        public static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("x2"));
            }

            var xyz = result.ToString();

            return Base64Encode(xyz);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            return Convert.ToBase64String(plainTextBytes);
        }

        public static long ToUnixTime(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return Convert.ToInt64((date.ToUniversalTime() - epoch).TotalSeconds);
        }

        public static string CalculateSignature(string accessToken, string timeStamp, string username, string password)
        {
            var rawString = $"{timeStamp}&{username}&{accessToken}&{password}";

            return GenerateSHA512String(rawString);
        }

        public static DateTime AdjustTimeSpan(DateTime value)
        {
            if (value.TimeOfDay == TimeSpan.Zero)
            {
                if (value.Date == DateTime.Today)
                    return DateTime.Now;
                else return value.Add(new TimeSpan(23, 59, 59));
            }
            else return value;
        }
    }
}
