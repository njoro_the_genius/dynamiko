﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public class RequestBody
    {
        public int Type { get; set; }

        public string TypeDesc { get; set; }

        public string CompanyId { get; set; }

        public string CompanyDesc { get; set; }

        public string Remarks { get; set; }

        public bool IPNEnabled { get; set; }

        public int IPNDataFormat { get; set; }

        public string ApplicationDomainName { get; set; }

        public string CallbackURL { get; set; }

        public bool IsDelivered { get; set; }

        public List<RequestBodyLineItem> OrderLines { get; set; }

        public string TransactionId { get; set; }

        public string Status { get; set; }

        public string Narration { get; set; }

        public decimal AvailableBalance { get; set; }

        public string InitiatorReference { get; set; }
    }
}
