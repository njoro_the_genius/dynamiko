﻿using Infrastructure.Crosscutting.Framework.Models;
using System;
using System.Messaging;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public static class ApprovalRequestMessageFactory
    {
        public static QueueDTO CreateApprovalRequestMessage(Guid recordId, string applicationDomainName, string entityName, string applicationUserName, string description)
        {
            var queueDTO = new QueueDTO
            {
                RecordId = recordId,
                AppDomainName = applicationDomainName,
                EntityName = entityName,
                ApplicationUserName = applicationUserName,
                Narration = description,
            };

            return queueDTO;
        }

        public static QueueDTO CreateApprovalRequestMessage(Guid recordId, string applicationDomainName, string entityName, string applicationUserName, string description, int entityState)
        {
            var queueDTO = new QueueDTO
            {
                RecordId = recordId,
                AppDomainName = applicationDomainName,
                EntityName = entityName,
                ApplicationUserName = applicationUserName,
                Narration = description,
                EntityState=entityState
            };

            return queueDTO;
        }

        public static void SendMessage(QueueDTO data, MessageCategory messageCategory)
        {
            var serviceBrokerConfigSection = ConfigurationHelper.GetServiceBrokerConfiguration();

            if (!MessageQueue.Exists(serviceBrokerConfigSection.ServiceBrokerSettingsItems.ApprovalRequestDispatcherQueuePath))
                MessageQueue.Create(serviceBrokerConfigSection.ServiceBrokerSettingsItems.ApprovalRequestDispatcherQueuePath, true);

            using (MessageQueue messageQueue = new MessageQueue(serviceBrokerConfigSection.ServiceBrokerSettingsItems.ApprovalRequestDispatcherQueuePath, QueueAccessMode.Send))
            {
                messageQueue.Formatter = new BinaryMessageFormatter();

                messageQueue.MessageReadPropertyFilter.SetAll();

                using (MessageQueueTransaction mqt = new MessageQueueTransaction())
                {
                    mqt.Begin();

                    using (Message message = new Message(data, new BinaryMessageFormatter()))
                    {
                        message.Label = string.Format("{0}|{1}", EnumHelper.GetDescription(messageCategory), EnumHelper.GetDescription(MessagePriority.High));

                        message.AppSpecific = (int)messageCategory;

                        message.Priority = MessagePriority.High;

                        message.TimeToBeReceived = TimeSpan.FromMinutes(serviceBrokerConfigSection.ServiceBrokerSettingsItems.TimeToBeReceived);

                        messageQueue.Send(message, mqt);
                    }

                    mqt.Commit();
                }
            }
        }
    }
}
