﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public class DebitAdviceModel
    {
        public string TransactionId { get; set; }

        public string Status { get; set; }

        public string Narration { get; set; }

        public decimal AvailableBalance { get; set; }

        public string InitiatorReference { get; set; }
    }
}
