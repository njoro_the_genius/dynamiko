﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Crosscutting.Framework.Utils
{
    public sealed class DefaultSettings
    {
        private static readonly object SyncRoot = new object();

        private DefaultSettings() { }

        private static DefaultSettings instance;
        public static DefaultSettings Instance
        {
            get
            {
                lock (SyncRoot)
                {
                    if (instance == null)
                        instance = new DefaultSettings();
                    instance.RootUser = "su";
                    instance.RootAuthoriser = "authoriser";
                    instance.RootPassword = "Hard2Cr@K!";
                    instance.RootAuthoriserPassword = "";
                    instance.AuditUser = "auditor";
                    instance.RootEmail = "system@dynamiko.co.ke";
                    instance.TablePrefix = "Dynamiko_";
                    instance.PageSizes = new List<int> { 15, 25, 50, 100, 200, 300, 400 };
                    instance.ServerDate = DateTime.Now;
                    instance.ApplicationDisplayName = "Dynamiko ™";
                    instance.ApplicationCopyright = $"Copyright © 2020-{DateTime.Now.Year}. All rights reserved.";
                    instance.EndDateTimeSpan = new TimeSpan(23, 59, 59);
                    instance.ModuleNavigationItemsInRoleTableName = $"{instance.TablePrefix}ModuleNavigationItemInRoles";
                    instance.CreatedDate = "CreatedDate";
                    instance.MaximumLineItemAmount = 1000000m;
                    instance.ClientCode = "ClientCode";
                    instance.AirtimePurchaseLimit = "AIRTIMEPURCHASELIMIT";
                    instance.MobileTransferLimit = "MOBILETRANSFERLIMIT";
                    instance.BankTransferLimit = "BANKTRANSFERLIMIT";
                    instance.MPESASpId = "MPESASpId";
                    instance.MPESAServiceId = "MPESAServiceId";
                    instance.MPESASpPassword = "MPESASpPassword";
                    instance.TransUnionMinimumUnits = "TRANSUNION_MINIMUM_UNITS";
                    instance.CrbAgeLowerLimit = "CRB_AGE_LOWER_LIMIT";
                    instance.CrbAgeUpperLimit = "CRB_AGE_UPPER_LIMIT";
                    instance.ClusteredId = "ClusteredId";
                }

                return instance;
            }
        }

        public string RootUser { get; private set; }

        public string RootAuthoriser { get; private set; }

        public string RootPassword { get; private set; }

        public string RootAuthoriserPassword { get; private set; }

        public string AuditUser { get; private set; }

        public string RootEmail { get; private set; }

        public string TablePrefix { get; private set; }

        public List<int> PageSizes { get; private set; }

        public DateTime ServerDate { get; set; }

        public string ApplicationDisplayName { get; private set; }

        public decimal MaximumLineItemAmount { get; private set; }

        public string ApplicationCopyright { get; private set; }

        public TimeSpan EndDateTimeSpan { get; private set; }

        public string ModuleNavigationItemsInRoleTableName { get; private set; }

        public string CreatedDate { get; private set; }

        public string ClientCode { get; private set; }

        public string ClusteredId { get; private set; }

        public string AirtimePurchaseLimit { get; private set; }

        public string MobileTransferLimit { get; private set; }

        public string BankTransferLimit { get; private set; }

        public string MPESASpId { get; private set; }

        public string MPESASpPassword { get; private set; }

        public string MPESAServiceId { get; private set; }

        public string TransUnionMinimumUnits { get; set; }

        public string CrbAgeLowerLimit { get; set; }

        public string CrbAgeUpperLimit { get; set; }
    }
}