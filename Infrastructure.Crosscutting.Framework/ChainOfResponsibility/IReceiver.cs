﻿using System.Threading.Tasks;

namespace Infrastructure.Crosscutting.Framework.ChainOfResponsibility
{
    public interface IReceiver
    {
        Task HandleRequestAsync(object @object, int @int);
    }
}