﻿namespace Infrastructure.Crosscutting.Framework.Logging
{
    public static class LoggerFactory
    {
        static ILoggerFactory _currentLogFactory = null;

        public static void SetCurrent(ILoggerFactory logFactory)
        {
            _currentLogFactory = logFactory;
        }

        public static ILogger CreateLog()
        {
            return _currentLogFactory?.Create();
        }
    }
}
