﻿using ServiceStack.Redis;
using System;
using System.Collections.Generic;

namespace Infrastructure.Crosscutting.Framework.Services.RedisClient
{
    public class RedisRepository<TEntity> : IRedisRepository<TEntity> where TEntity : class
    {
        private readonly IRedisClient _client = new ServiceStack.Redis.RedisClient();

        public void Add(TEntity entity)
        {
            var entityClient = _client.As<TEntity>();

            entityClient.Store(entity);
        }

        public TEntity Get(object entityId)
        {
            var entityClient = _client.As<TEntity>();

            return entityClient.GetById(entityId);
        }

        public IEnumerable<TEntity> GetAll()
        {
            throw new NotImplementedException();
        }

        public void Merge(TEntity original)
        {
            var entityClient = _client.As<TEntity>();

            entityClient.Store(original);
        }

        public void Remove(object entityId)
        {
            var entityClient = _client.As<TEntity>();

            entityClient.DeleteById(entityId);
        }
    }
}
