﻿using System.Collections.Generic;

namespace Infrastructure.Crosscutting.Framework.Services.RedisClient.Models
{
    public class AccessControlDTO
    {
        /// <summary>
        /// This will be the domainname concatenated with the role name. This is to help in multitenant setup
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// This will hold the rights per role
        /// </summary>
        public List<AccessControlList> AccessControlList { get; set; }
    }

    public class AccessControlList
    {
        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public string CreatedBy { get; set; }
    }
}
