﻿using System.Collections.Generic;

namespace Infrastructure.Crosscutting.Framework.Services.RedisClient
{
    public interface IRedisRepository<TEntity> where TEntity : class
    {
        TEntity Get(object entityId);

        void Add(TEntity entity);

        void Merge(TEntity original);

        void Remove(object entityId);

        IEnumerable<TEntity> GetAll();
    }
}
