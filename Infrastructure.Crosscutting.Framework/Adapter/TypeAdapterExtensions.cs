﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Crosscutting.Framework.Adapter
{
    public static class TypeAdapterExtensions
    {
        public static TProjection ProjectedAs<TProjection>(this object item)
            where TProjection : class, new()
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var adapter = TypeAdapterFactory.CreateAdapter();

            return adapter.Adapt<TProjection>(item);
        }

        public static List<TProjection> ProjectedAsCollection<TProjection>(this IEnumerable<object> items)
           where TProjection : class, new()
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));

            var adapter = TypeAdapterFactory.CreateAdapter();

            return adapter.Adapt<List<TProjection>>(items);
        }
    }
}