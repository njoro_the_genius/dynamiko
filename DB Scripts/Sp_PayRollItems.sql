-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE Sp_PayRollItems(
@Claim int,
@StartDate datetime, 
@EndDate datetime,
@EmployeeId uniqueIdentifier,
@StartIndex int,
@pageSize int,
@SearchString varchar(200)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	  SELECT CONCAT(E.FirstName, ' ', E.OtherNames) NAMES, ISNULL(P.Grosspay,0.00) Grosspay,ISNULL(PY.NetPay,0.00) NetPay,
	ISNULL(PY.AllowableDeductions,0.00) AllowableDeductions,
	ISNULL(PY.HealthInsurance,0.00) HealthInsurance,ISNULL(PY.NonCashBenefits,0.00) NonCashBenefits,ISNULL(PY.Pension,0.00) Pension
	FROM Dynamiko_PayRollItems P
	INNER JOIN Dynamiko_PayRollItems PY ON P.Id = PY.PayRollId
	INNER JOIN Dynamiko_Employees E ON E.Id = PY.EmployeeId and E.CustomerId = @EmployeeId;
END
GO
