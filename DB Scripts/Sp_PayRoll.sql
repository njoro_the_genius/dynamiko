USE [Dynamiko_Mainstore]
GO
/****** Object:  StoredProcedure [dbo].[Sp_PayRoll]    Script Date: 12/20/2020 8:49:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Sp_PayRoll](
@Claim int,
@StartDate datetime, 
@EndDate datetime,
@CustomerId uniqueIdentifier,
@StartIndex int,
@pageSize int,
@SearchString varchar(200)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ItemsCount int;

    -- Insert statements for procedure here
	If(@Claim=1)
	Begin
          
			SELECT CONCAT(E.FirstName, ' ', E.OtherNames) NAMES, ISNULL(P.Grosspay,0.00) Grosspay,ISNULL(PY.NetPay,0.00) NetPay,
			ISNULL(PY.AllowableDeductions,0.00) AllowableDeductions,
			ISNULL(PY.HealthInsurance,0.00) HealthInsurance,ISNULL(PY.NonCashBenefits,0.00) NonCashBenefits,
			ISNULL(PY.Pension,0.00) Pension,P.CreatedDate
			INTO #TEMP
			FROM Dynamiko_PayRollItems P
			INNER JOIN Dynamiko_PayRollItems PY ON P.Id = PY.PayRollId
			INNER JOIN Dynamiko_Employees E ON E.Id = PY.EmployeeId 
			--E.CustomerId = @customerId;

			SET @ItemsCount = (SELECT  COUNT(*) from #TEMP);

		   SELECT *, @ItemsCount AS ItemsCount FROM  #TEMP ORDER BY CreatedDate DESC
		   OFFSET @StartIndex ROWS 
		   FETCH NEXT @PageSize ROWS ONLY;
		   
		   DROP TABLE #TEMP;

		   RETURN;
	END
	ELse
	Begin
           SELECT CONCAT(E.FirstName, ' ', E.OtherNames) NAMES, ISNULL(P.Grosspay,0.00) Grosspay,ISNULL(PY.NetPay,0.00) NetPay,
			ISNULL(PY.AllowableDeductions,0.00) AllowableDeductions,
			ISNULL(PY.HealthInsurance,0.00) HealthInsurance,ISNULL(PY.NonCashBenefits,0.00) NonCashBenefits,
			ISNULL(PY.Pension,0.00) Pension,
			P.CreatedDate
			INTO #TEMP2
			FROM Dynamiko_PayRollItems P
			INNER JOIN Dynamiko_PayRollItems PY ON P.Id = PY.PayRollId
			INNER JOIN Dynamiko_Employees E ON E.Id = PY.EmployeeId 
			WHERE E.CustomerId = @customerId;

			SET @ItemsCount = (SELECT  COUNT(*) from #TEMP2);

		   SELECT *, @ItemsCount AS ItemsCount FROM  #TEMP2 ORDER BY CreatedDate DESC
		   OFFSET @StartIndex ROWS 
		   FETCH NEXT @PageSize ROWS ONLY;

		   DROP TABLE #TEMP2;
		   
		   RETURN;
	End
END
