USE [Dynamiko_Mainstore]
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetSalaryStatistics_GraphData]    Script Date: 11/28/2020 11:51:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Sp_GetSalaryStatistics_GraphData](
@Claim int, -- 1 super admin(dynamiko id=00000000-0000-0000-0000-000000000000) , 2 -- other customer
@CustomerId uniqueidentifier
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if(@Claim=1) -- get su data stats
	Begin
	--check if no data exists in table
	IF NOT EXISTS(SELECT PR.Id FROM Dynamiko_PayRolls PR WITH(NOLOCK) INNER JOIN Dynamiko_PayRollItems PRI ON PR.Id = PRI.PayRollId)
	BEGIN
	     select  c.Name Customer, DATEPART(YEAR,GETDATE()) YearNo, 
		 0 Quarter1, 0 Quarter2, 0 Quarter3,
		 0 Quarter4 from Dynamiko_Customers c group by c.Name; 

		 return;
	END
	      --Get salary data based on Q1,Q2,Q3,Q4
		  ;with cte1 as
    (
     SELECT Name AS 'Customer', YEAR(p.CreatedDate) AS 'Year' ,
        Quarter1 = CASE(DATEPART(QQ, p.CreatedDate))
        WHEN 1 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END,
        Quarter2 = CASE(DATEPART(QQ, P.CreatedDate))
        WHEN 2 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END,
        Quarter3 = CASE(DATEPART(QQ, p.CreatedDate))
        WHEN 3 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END,
        Quarter4 = CASE(DATEPART(QQ, p.CreatedDate))
        WHEN 4 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END
     FROM Dynamiko_PayRolls P LEFT JOIN Dynamiko_PayRollItems PI ON P.Id = PI.PayRollId
     LEFT JOIN Dynamiko_Customers c ON c.Id = P.CustomerId
     GROUP BY C.Name, YEAR(P.CreatedDate), DATEPART(QQ, P.CreatedDate)
    )
   select ISNULL(Customer,null) Customer,ISNULL(SUM(Year), DATEPART(YEAR,GETDATE())) YearNo, ISNULL(max(Quarter1),0) Quarter1, ISNULL(max(Quarter2),0) Quarter2, ISNULL(max(Quarter3),0) Quarter3,ISNULL(max(Quarter4),0) Quarter4 from cte1 group by Customer;

		 return;
	End

	if(@Claim=2) -- get su data stats
	Begin
	      --get total customers

		  ;with cte1 as
    (
     SELECT Name AS 'Customer', YEAR(p.CreatedDate) AS 'Year' ,
        Quarter1 = CASE(DATEPART(q, p.CreatedDate))
        WHEN 1 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END,
        Quarter2 = CASE(DATEPART(q, P.CreatedDate))
        WHEN 2 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END,
        Quarter3 = CASE(DATEPART(q, p.CreatedDate))
        WHEN 3 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END,
        Quarter4 = CASE(DATEPART(q, p.CreatedDate))
        WHEN 4 THEN ISNULL(SUM((p.TotalGrosspay)),0)
        ELSE 0
        END
     FROM Dynamiko_PayRolls P LEFT JOIN Dynamiko_PayRollItems PI ON P.Id = PI.PayRollId
     LEFT JOIN Dynamiko_Customers c ON c.Id = P.CustomerId WHERE P.CustomerId = @CustomerId
     GROUP BY C.Name, YEAR(P.CreatedDate), DATEPART(q, P.CreatedDate)
    )
    select ISNULL(Customer,null) Customer,ISNULL(max(Year), DATEPART(YEAR,GETDATE())) YearNo, ISNULL(max(Quarter1),0) Quarter1, ISNULL(max(Quarter2),0) Quarter2, ISNULL(max(Quarter3),0) Quarter3,ISNULL(max(Quarter4),0) Quarter4 from cte1 group by Customer;	     	 

		 return;
	End
END
