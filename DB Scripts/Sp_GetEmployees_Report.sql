USE [Dynamiko_Mainstore]
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetEmployees_Report]    Script Date: 12/20/2020 8:54:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Sp_GetEmployees_Report](
@Claim int,
@StartDate datetime, 
@EndDate datetime,
@CustomerId uniqueIdentifier,
@StartIndex int,
@pageSize int,
@SearchString varchar(200)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ItemsCount int;

	If(@Claim=1)
	Begin
          Select  * INTO #TEMP from Dynamiko_Employees E  WHERE
		  CAST(E.CreatedDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)

		 -- SELECT  * from #TEMP

		  SET @ItemsCount = (SELECT  COUNT(*) from #TEMP);

		   SELECT *, @ItemsCount AS ItemsCount FROM  #TEMP ORDER BY CreatedDate DESC
		   OFFSET @StartIndex ROWS 
		   FETCH NEXT @PageSize ROWS ONLY;
		   
		   DROP TABLE #TEMP;

		   RETURN;
	End
	ELse
	Begin
          Select  *  INTO #TEMP2 from Dynamiko_Employees E where 
		  CAST(E.CreatedDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE) AND
		  E.CustomerId = @CustomerId;

		  SET @ItemsCount = (SELECT  COUNT(*) from #TEMP2);

		   SELECT *, @ItemsCount AS ItemsCount FROM  #TEMP2 ORDER BY CreatedDate DESC
		   OFFSET @StartIndex ROWS 
		   FETCH NEXT @PageSize ROWS ONLY;

		   DROP TABLE #TEMP2;
		   
		   RETURN;
	End	
END
