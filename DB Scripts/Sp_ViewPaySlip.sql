USE [Dynamiko_Mainstore]
GO
/****** Object:  StoredProcedure [dbo].[Sp_ViewPaySlip]    Script Date: 12/20/2020 9:47:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,SAM>
-- Create date: <Create Date,,14-12-2020>
-- Description:	<Description,,View Pay Slip>
-- =============================================
ALTER PROCEDURE [dbo].[Sp_ViewPaySlip](
@period int,
@employeeId uniqueidentifier
) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--CHECK IF PAYROLL RECORD EXIST
    -- Insert statements for procedure here
	SELECT CONCAT(E.FirstName, ' ', E.OtherNames) Names,
	Case  WHEN E.DepartmentId=0 THEN 'ICT'
	      WHEN E.DepartmentId=1 THEN 'FINANCE'
		  WHEN E.DepartmentId=2 THEN 'Human Resource'
		  WHEN E.DepartmentId=3 THEN 'Administration'
		  END as UNIT,
		  E.JobTitle AS Job,
		  'HEAD OFFICE' AS Station,
		  CONCAT(DATEPART(MM,GETDATE()),'-',DATEPART(YYYY,GETDATE())) AS EmployeePaySlip,
		  C.Name as Company,
		  ISNULL(E.GrossSalary,0.00) as Basicpay,
		  --leave pay if month equals dec then get leave allownace else 0.00
		  0.00 as LeaveAllowance,
	ISNULL(P.Grosspay,0.00) Grosspay,ISNULL(PY.NetPay,0.00) NetPay,
	ISNULL(PY.AllowableDeductions,0.00) AllowableDeductions,
	ISNULL(PY.HealthInsurance,0.00) HealthInsurance,
	ISNULL(PY.NonCashBenefits,0.00) NonCashBenefits,
	ISNULL(PY.Pension,0.00) Pension,
	0.00 as SaccoBenevoltFund,
	0.00 SaccoSavings,
	0.00 StaffWelfare,
	0.00 PenisonWelfare,
	0.00 TaxablePay,
	0.00 TaxCharged,
	0.00 TaxRelief,
	0.00 TaxDeducted,
	0.00 TotalDeductions,
	0.00 PersonalRelief,
	0.00 TaxableYPay
	FROM Dynamiko_PayRollItems P
	INNER JOIN Dynamiko_PayRollItems PY ON P.Id = PY.PayRollId
	INNER JOIN Dynamiko_Employees E ON E.Id = PY.EmployeeId and P.EmployeeId = @employeeId
	INNER JOIN Dynamiko_Customers C ON C.Id = E.CustomerId
	WHERE E.Id = @employeeId
	AND DATEPART(MM,E.CreatedDate)=@period;
END
