USE [Dynamiko_Mainstore]
GO
/****** Object:  StoredProcedure [dbo].[Sp_DashBoardStatistics]    Script Date: 11/28/2020 11:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,SAM>
-- Create date: <Create Date,,22-11-2020>
-- Description:	<Description,,GET DASHBAORD STATS>
-- =============================================
ALTER PROCEDURE [dbo].[Sp_DashBoardStatistics](
@Claim int, -- 1 super admin(dynamiko id=00000000-0000-0000-0000-000000000000) , 2 -- other customer
@CustomerId uniqueidentifier 
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @TotalCustomer int=0,
	        @CustomerName varchar(300),
			@TotalEMployees int=0,
			@TotalSalary int=0,
			@TotalAverageSalary decimal(18,2),
			@TotalEmployeesOnLeave int,
			@TotalLeaveDays int,
			@TotalUsedLeaveDays INT;

			Create table #Temp(
			 TotalCustomer int,
	         CustomerName varchar(300),
			 TotalEMployees int,
			 TotalSalary int,
			 TotalAverageSalary decimal(18,2),
			 TotalEmployeesOnLeave int,
			 TotalLeaveDays int
			);

	-- check the type of user
	if(@Claim=1) -- get su data stats
	Begin
	      --get total customers
	     SELECT @TotalCustomer=ISNULL(COUNT(Id),0) FROM Dynamiko_Customers;

		 --employees per customer
		 
		 SELECT @CustomerName=C.Name,@TotalEMployees=ISNULL(COUNT(E.Id),0),@TotalSalary=SUM(E.GrossSalary),
		 @TotalAverageSalary=AvG(GrossSalary) FROM Dynamiko_Customers C WITH(NOLOCK)
		 INNER JOIN Dynamiko_Employees E WITH(NOLOCK) on C.Id = E.CustomerId Group by C.Name;

		 SELECT @TotalUsedLeaveDays=ISNULL(SUM(L.Period),0) FROM Dynamiko_Employees E WITH(NOLOCK) INNER JOIN Dynamiko_Leaves L 
		 ON E.Id = L.EmployeeId WHERE E.CustomerId = @CustomerId AND L.RecordStatus=3;

		 ---remaining leave days
		 SET @TotalLeaveDays = (SELECT 21-@TotalUsedLeaveDays);

		 INSERT INTO #Temp(CustomerName,TotalEMployees,TotalSalary,TotalAverageSalary,TotalEmployeesOnLeave,TotalLeaveDays)
		 SELECT @CustomerName,@TotalEMployees,@TotalSalary,@TotalAverageSalary,0,@TotalLeaveDays;

		 UPDATE #Temp SET TotalCustomer=@TotalCustomer;

		 SELECT * from #Temp;

		 return;
	End

	if(@Claim=2) -- get su data stats
	Begin
	      --get total customers
	     SELECT @TotalCustomer=ISNULL(COUNT(Id),0) FROM Dynamiko_Customers C WHERE C.Id=@CustomerId;

		 --employees per customer
		 
		 SELECT @CustomerName=C.Name,@TotalEMployees=ISNULL(COUNT(E.Id),0),@TotalSalary=SUM(E.GrossSalary), @TotalAverageSalary=AvG(GrossSalary) FROM Dynamiko_Customers C
		 INNER JOIN Dynamiko_Employees E on C.Id = E.CustomerId Group by C.Name;

		 SELECT @TotalEmployeesOnLeave=Count(L.Id) FROM Dynamiko_Leaves L
		 INNER JOIN Dynamiko_Employees E ON L.EmployeeId = E.Id
		 WHERE L.RecordStatus=3 AND E.CustomerId=@CustomerId;

		 SELECT @TotalUsedLeaveDays=ISNULL(SUM(L.Period),0) FROM Dynamiko_Employees E WITH(NOLOCK) INNER JOIN Dynamiko_Leaves L 
		 ON E.Id = L.EmployeeId WHERE E.CustomerId = @CustomerId AND L.RecordStatus=3;

		 ---remaining leave days
		 SET @TotalLeaveDays = (SELECT 21-@TotalUsedLeaveDays);
		 

		 INSERT INTO #Temp(CustomerName,TotalEMployees,TotalSalary,TotalAverageSalary,TotalEmployeesOnLeave,TotalLeaveDays)
		 SELECT @CustomerName,@TotalEMployees,@TotalSalary,@TotalAverageSalary,@TotalEmployeesOnLeave,@TotalLeaveDays;

		 UPDATE #Temp SET TotalCustomer=@TotalCustomer;

		 SELECT * from #Temp;

		 

		 return;
	End

	drop table #Temp;
END
