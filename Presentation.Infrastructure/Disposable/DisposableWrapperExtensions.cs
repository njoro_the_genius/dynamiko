﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Infrastructure.Disposable
{
    public static class DisposableWrapperExtensions
    {
        public static IDisposableWrapper<T> Wrap<T>(this T baseObject)
            where T : class, IDisposable
        {
            return baseObject as IDisposableWrapper<T> ?? new DisposableWrapper<T>(baseObject);
        }

        public static IDisposableWrapper<TProxy> Wrap<TProxy, TService>(this TProxy proxy, double timeoutMinutes)
            where TProxy : ClientBase<TService>
            where TService : class
        {
            return new ClientWrapper<TProxy, TService>(proxy, null, timeoutMinutes);
        }

        public static IDisposableWrapper<TProxy> Wrap<TProxy, TService>(this TProxy proxy, IEndpointBehavior behavior, double timeoutMinutes)
            where TProxy : ClientBase<TService>
            where TService : class
        {
            return new ClientWrapper<TProxy, TService>(proxy, behavior, timeoutMinutes);
        }
    }
}
