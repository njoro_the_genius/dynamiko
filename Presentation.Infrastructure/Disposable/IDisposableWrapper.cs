﻿using System;

namespace Presentation.Infrastructure.Disposable
{
    public interface IDisposableWrapper<T> : IDisposable
    {
        T BaseObject { get; }
    }
}
