﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.MainBoundedContext.DTO.MessagingModule;
using Application.MainBoundedContext.DTO.RegistryModule;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Infrastructure.Disposable;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading.Tasks;

namespace Presentation.Infrastructure.Services
{
    public interface IChannelService
    {

        #region ModuleNavigationItem

        Task<bool> AddModuleNavigationItemAsync(List<ModuleNavigationItemDTO> moduleNavigationItem, ServiceHeader serviceHeader);

        Task<ModuleNavigationItemDTO> FindModuleNavigationItemByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemsAsync(ServiceHeader serviceHeader);

        Task<PageCollectionInfo<ModuleNavigationItemDTO>> FindModuleNavigationItemsFilterPageCollectionInfoAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemDTO>> FindModuleNavigationActionControllerNameAsync(string controllerName, ServiceHeader serviceHeader);

        Task<bool> ValidateAccessPermissionAsync(string controllerName, string actionName, string username, ServiceHeader serviceHeader);

        Task<bool> BulkInsertModuleNavigationItemAsync(List<Guid> modulesNavigationIds, string roleId, ServiceHeader serviceHeader);

        #endregion

        #region ModuleNavigationItemInRole

        Task<bool> AddModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRoleAsync(string role, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRolePageCollectionInfoAsync(string roleId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationIdAsync(Guid moduleNavigationId, ServiceHeader serviceHeader);

        Task<bool> RemoveModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader);

        Task<List<RolePermissionDTO>> CacheRoleAccessPermissionsAsync(string[] currentUserRole, ServiceHeader serviceHeader);

        Task<bool> BulkInsertModuleNavigationItemInRolesAsync(List<Guid> moduleNavigationItemInRole, string roleId, ServiceHeader serviceHeader);

        Task<ModuleNavigationItemInRoleDTO> FindModuleNavigationItemInRoleByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<bool> UpdateModuleNavigationItemInRoleListAsync(ModuleNavigationItemInRoleDTO moduleNavigationItemInRole, ServiceHeader serviceHeader);

        #endregion

        #region Redis Client

        Task<bool> SeedSystemAccessRightAsync(string role, ServiceHeader serviceHeader);

        Task<bool> SeedSystemAccessRightsAsync(ServiceHeader serviceHeader);

        Task<bool> ValidatePermissionAsync(string username, string controllerName, string actionName, ServiceHeader serviceHeader);

        Task<bool> RemoveAllRightsAsync(string[] roles, ServiceHeader serviceHeader);

        #endregion

        #region AspnetIdentityManager


        Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);


        Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByCustomerIdAsync(Guid customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);


        Task<ApplicationUserDTO> FindApplicationUserAsync(string userName, ServiceHeader serviceHeader);


        Task<ApplicationUserDTO> FindApplicationUserByIdAsync(string id, ServiceHeader serviceHeader);


        Task<ApplicationRoleDTO> FindApplicationRoleByIdAsync(string id, ServiceHeader serviceHeader);


        Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUsersInRolesAsync(string roleName, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);


        Task<List<ApplicationUserDTO>> FindApplicationUsersInRolesAsync(string roleName, ServiceHeader serviceHeader);


        Task<ApplicationUserDTO> CreateUserAsync(ApplicationUserDTO applicationUserDto, ServiceHeader serviceHeader);


        Task<bool> UpdateAspnetIdentityUserAsync(ApplicationUserDTO applicationUserDto, ServiceHeader serviceHeader);

        Task<bool> UpdateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto, ServiceHeader serviceHeader);


        Task<string[]> GetUserRolesAsync(string username, ServiceHeader serviceHeader);

        Task<List<ApplicationUserDTO>> GetApplicationUsers(ServiceHeader serviceHeader);


        Task<ApplicationUserDTO> FindApplicationUserByUsernameAndPasswordAsync(string username, string password, ServiceHeader serviceHeader);


        Task<PageCollectionInfo<ApplicationRoleDTO>> GetApplicationRoleCollectionByFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);


        Task<bool> CreateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto, ServiceHeader serviceHeader);


        Task<bool> ResetPassword(PasswordResetModel passwordResetModel, ServiceHeader serviceHeader);

        Task<bool> ResetPasswordLinkAsync(string userName, string callBackUrl, ServiceHeader serviceHeader);

        Task<List<string>> GetRolesAsync(ServiceHeader serviceHeader);
        #endregion

        #region CustomerDTO

        Task<Tuple<CustomerDTO, List<string>>> AddNewCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader);

        Task<bool> UpdateCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader);

        Task<List<CustomerDTO>> FindCustomersAsync(ServiceHeader serviceHeader);

        Task<CustomerDTO> FindCustomerAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAndIsEnabledAsync(string searchString, bool isEnabled, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<CustomerDTO>> FindCustomersByNameAsync(string name, ServiceHeader serviceHeader);

        #endregion

        #region EmployeeDTO

        Task<Tuple<EmployeeDTO, List<string>>> AddNewEmployeeAsync(EmployeeDTO employeeDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateEmployeeAsync(EmployeeDTO employeeDto, ServiceHeader serviceHeader);

        Task<EmployeeDTO> FindEmployeeAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmployeeDTO>> FindCustomerEmployeesFilterInPageAsync(string searchString, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmployeeDTO>> FindEmployeeByCustomerIdAsync(Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader);

        Task<EmployeeDTO> FindEmployeeByUserIdAsync(Guid userId, Guid? customerId, ServiceHeader serviceHeader);
        #endregion

        #region utility

        Task<bool> ConfigureApplicationDatabaseAsync(ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<bool> ConfigureAspNetIdentityDatabaseAsync(ServiceHeader serviceHeader, double timeoutMinutes = 10d);

        Task<List<Bank>> GetBanksListAsync(ServiceHeader serviceHeader);

        Task<List<Branch>> GetBranchesListAsync(string bankCode, ServiceHeader serviceHeader);

        Task<Bank> GetBankByBankCode(string bankCode, ServiceHeader serviceHeader);

        Task<Branch> GetBranchByBranchCodeAsync(string bankCode, string branchCode, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<Branch>> GetBranchesListFilterInPageAsync(string bankCode, string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<Bank>> GetBanksListFilterInPageAsync(string text, int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        #endregion

        #region PayRollDTO

        Task<PayRollDTO> AddNewPayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader);

        Task<bool> UpdatePayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader);

        Task<PayRollDTO> FindPayRollByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollDTO>> FindPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemDTOFilterInPageAsync(Guid payRollId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<PayRollDTO>> FindPayRolls(ServiceHeader serviceHeader);

        Task<PayRollDTO> ProcessPayRoll(Guid payRollId, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollDTO>> FindUnProcessedPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemByEmployeeIdDTOFilterInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PayRollDTO>> FindPayRollByCustomerIdFilterInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);
        #endregion

        #region LeaveDTO

        Task<Tuple<LeaveDTO, string>> AddNewLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader);

        Task<LeaveDTO> FindLeaveByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<LeaveDTO> FindLeaveByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader);

        Task<List<LeaveDTO>> FindLeavesByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByEmployeeInPageAsync(Guid employeeId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByCustomerInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<LeaveDTO>> FindLeaves(ServiceHeader serviceHeader);

        #endregion

        #region StaticSettingDTO

        Task<StaticSettingDTO> AddNewSettingAsync(StaticSettingDTO settingDTO, ServiceHeader serviceHeader,
      double timeOutMinutes = 1.5d);

        Task<bool> UpdateSettingAsync(StaticSettingDTO settingDTO, ServiceHeader serviceHeader,
         double timeOutMinutes = 1.5d);

        Task<PageCollectionInfo<StaticSettingDTO>> FindSettingFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader,
        double timeOutMinutes = 1.5d);

        Task<StaticSettingDTO> FindSettingByKeyAsync(string keyName, ServiceHeader serviceHeader,
            double timeOutMinutes = 1.5d);

        Task<StaticSettingDTO> FindSettingByIdAsync(Guid id, ServiceHeader serviceHeader,
         double timeOutMinutes = 1.5d);

        Task<bool> RemoveStaticSettingAsync(Guid id, ServiceHeader serviceHeader,
         double timeOutMinutes = 1.5d);

        #endregion

        #region TAX

        Task<TaxOrderDTO> AddNewTaxOrderAsync(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateTaxOrderAsync(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader);

        Task<TaxOrderDTO> FindTaxSettingByIdAsync(Guid id, ServiceHeader serviceHeader,
         double timeOutMinutes = 1.5d);

        Task<PageCollectionInfo<TaxOrderDTO>> FindTaxOrderFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<TaxRangesDTO>> FindGraduatedScalesAsync(Guid taxOrderId, ServiceHeader serviceHeader);

        #endregion

        #region DeductionApp

        #region NssfOrderDTO
        Task<NssfOrderDTO> AddNewNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader);

        Task<NssfOrderDTO> FindNssfOrderByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<NssfOrderDTO>> FindNssfOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<NssfOrderDTO>> FindNssfOrders(ServiceHeader serviceHeader);
        #endregion

        #region NhifOrderDTO
        Task<NhifOrderDTO> AddNewNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader);

        Task<NhifOrderDTO> FindNhifOrderByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<NhifOrderDTO>> FindNhifOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<NhifOrderDTO>> FindNhifOrders(ServiceHeader serviceHeader);
        #endregion

        #region DeductionDTO
        Task<DeductionDTO> AddNewDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader);

        Task<bool> UpdateDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader);

        Task<DeductionDTO> FindDeductionByIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<DeductionDTO>> FindDeductionFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<DeductionDTO>> FindDeductionByCustomerIdFilterInPageAsync(Guid customerId,int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<List<DeductionDTO>> FindDeduction(ServiceHeader serviceHeader);

        Task<DeductionDTO> FindActiveDeductionAsync(ServiceHeader serviceHeader);
        #endregion
        #endregion

        #region EmailAlertDTO

        Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsAsync(Guid? customerId, int pageIndex, int pageSize, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(Guid? customerId, string searchText, int pageIndex, int pageSize, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithDLRStatusAsync(Guid? customerId, int dlrStatus, int pageIndex, int pageSize, ServiceHeader serviceHeader);

        Task<EmailAlertDTO> FindEmailAlertWithIdAsync(Guid id, ServiceHeader serviceHeader);

        Task<bool> UpdateEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader);

        Task<bool> GenerateEmailAsync(String templatePath, IDictionary<string, object> expandoObject, string subject, string messageTo, bool mailMessageIsBodyHtml, bool mailMessageSecurityCritical, ServiceHeader serviceHeader);

        #endregion

        #region TextAlertDTO

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByFilterInPageAsync(int dlrStatus, string text, int pageIndex,
            int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByDateRangeAndFilterInPageAsync(int dlrStatus, DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields,
            bool ascending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsInPageAsync(int pageIndex, int pageSize, List<string> sortFields,
            bool ascending, ServiceHeader serviceHeader);

        Task<bool> AddTextAlertsAsync(ObservableCollection<TextAlertDTO> textAlertDTOs, ServiceHeader serviceHeader);

        Task<TextAlertDTO> AddTextAlertAsync(TextAlertDTO textAlertDto, ServiceHeader serviceHeader);

        Task<bool> UpdateTextAlertAsync(TextAlertDTO textAlertDTO, ServiceHeader serviceHeader);

        Task<TextAlertDTO> FindTextAlertAsync(Guid textAlertId, ServiceHeader serviceHeader);

        Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAsync(int dlrStatus, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsFilterInPageAsync(DateTime startDate, DateTime endDate, Guid? customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByRecipientMobileNumberFilterInPageAsync(string mobileNumber, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader);

        #endregion

        #region Dashboard

        Task<DashboardWidgetDTO> FindDashboardWidgetsAsync(int claim, Guid? customerId, ServiceHeader serviceHeader);

        Task<List<DashboardGraphDTO>> GetDashboardGraphDataSetAsync(int claim, Guid? customerId, ServiceHeader serviceHeader);

        //Task<List<DashboardGraphDTO>> GetDashboardGraphDataSetAsync(int claim, Guid customerId, ServiceHeader serviceHeader);

        //Task<List<DashboardGraphSideBarDTO>> GetDashboardGraphSideBarAsync(int claim, Guid customerId, ServiceHeader serviceHeader);

        #endregion

        #region Reports

        Task<PageCollectionInfo<EmployeeDTO>> GetEmployeesReportAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PaySlipDTO>> GetPaySlipAsync(Guid employeeId, int period, ServiceHeader serviceHeader);

        Task<PageCollectionInfo<PaySlipDTO>> GetPayRollAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader);

        #endregion 

        void SetupServiceProxy<TProxy, TService>(Action<IDisposableWrapper<TProxy>> callback, double timeoutMinutes = 10d)
          where TProxy : ClientBase<TService>, new()
          where TService : class;

        IDisposableWrapper<TProxy> SetupServiceProxy<TProxy, TService>(double timeoutMinutes = 10d)
            where TProxy : ClientBase<TService>, new()
            where TService : class;

        IDisposableWrapper<TProxy> SetupServiceProxy<TProxy, TService>(IEndpointBehavior behavior, double timeoutMinutes = 10d)
            where TProxy : System.ServiceModel.ClientBase<TService>, new()
            where TService : class;

        IDisposableWrapper<TProxy> SetupServiceProxy<TProxy, TService>(string endpointConfigurationName, IEndpointBehavior behavior, double timeoutMinutes = 10d)
            where TProxy : System.ServiceModel.ClientBase<TService>, new()
            where TService : class;

    }
}
