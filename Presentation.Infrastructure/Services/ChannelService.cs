﻿using Application.MainBoundedContext.DTO;
using Application.MainBoundedContext.DTO.AdministrationModule;
using Application.MainBoundedContext.DTO.MessagingModule;
using Application.MainBoundedContext.DTO.RegistryModule;
using DistributedServices.Seedwork.EndpointBehaviors;
using DistributedServices.Seedwork.ErrorHandlers;
using Infrastructure.Crosscutting.Framework.Extensions;
using Infrastructure.Crosscutting.Framework.Logging;
using Infrastructure.Crosscutting.Framework.Utils;
using Presentation.Contracts;
using Presentation.Contracts.AdministrationModule;
using Presentation.Contracts.MessagingModule;
using Presentation.Contracts.RegistryModule;
using Presentation.Infrastructure.Disposable;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Infrastructure.Services
{
    public class ChannelService : IChannelService
    {
        private static readonly object SyncRoot = new object();

        private readonly Dictionary<string, object> _channelFactoryHashtable;

        private readonly ILogger _logger;

        [ImportingConstructor]
        public ChannelService(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _channelFactoryHashtable = new Dictionary<string, object>();
        }



        #region AspnetIdentityManagerService
        public Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByFilterInPageAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<ApplicationUserDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<ApplicationUserDTO>>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndGetApplicationUserCollectionByFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetApplicationUserCollectionByFilterInPage(startIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUserCollectionByCustomerIdAsync(Guid customerId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<ApplicationUserDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<ApplicationUserDTO>>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndGetApplicationUserCollectionByCustomerId(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetApplicationUserCollectionByCustomerId(customerId, startIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<ApplicationUserDTO> FindApplicationUserAsync(string userName, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<ApplicationUserDTO> tcs = new TaskCompletionSource<ApplicationUserDTO>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndFindApplicationUser(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindApplicationUser(userName, asyncCallback, service);

            return tcs.Task;
        }

        public Task<ApplicationUserDTO> FindApplicationUserByIdAsync(string id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<ApplicationUserDTO> tcs = new TaskCompletionSource<ApplicationUserDTO>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndFindApplicationUserById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindApplicationUserById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<ApplicationRoleDTO> FindApplicationRoleByIdAsync(string id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<ApplicationRoleDTO> tcs = new TaskCompletionSource<ApplicationRoleDTO>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndFindApplicationRoleById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindApplicationRoleById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<ApplicationUserDTO>> GetApplicationUsersInRolesAsync(string roleName, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<ApplicationUserDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<ApplicationUserDTO>>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndGetApplicationUsersInRoles(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetApplicationUsersInRoles(roleName, startIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<ApplicationUserDTO>> FindApplicationUsersInRolesAsync(string roleName, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<ApplicationUserDTO>> tcs = new TaskCompletionSource<List<ApplicationUserDTO>>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndFindApplicationUsersInRoles(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindApplicationUsersInRoles(roleName, asyncCallback, service);

            return tcs.Task;
        }

        public Task<ApplicationUserDTO> CreateUserAsync(ApplicationUserDTO applicationUserDto, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<ApplicationUserDTO> tcs = new TaskCompletionSource<ApplicationUserDTO>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndCreateUser(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginCreateUser(applicationUserDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateAspnetIdentityUserAsync(ApplicationUserDTO applicationUserDto, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndUpdateAspnetIdentityUser(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateAspnetIdentityUser(applicationUserDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndUpdateApplicationRole(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateApplicationRole(applicationRoleDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<string[]> GetUserRolesAsync(string username, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<string[]> tcs = new TaskCompletionSource<string[]>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndGetUserRoles(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetUserRoles(username, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<ApplicationUserDTO>> GetApplicationUsers(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<ApplicationUserDTO>> tcs = new TaskCompletionSource<List<ApplicationUserDTO>>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndGetApplicationUsers(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetApplicationUsers(asyncCallback, service);

            return tcs.Task;
        }

        public Task<ApplicationUserDTO> FindApplicationUserByUsernameAndPasswordAsync(string username, string password, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<ApplicationUserDTO> tcs = new TaskCompletionSource<ApplicationUserDTO>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndFindApplicationUserByUsernameAndPassword(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindApplicationUserByUsernameAndPassword(username, password, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<ApplicationRoleDTO>> GetApplicationRoleCollectionByFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<ApplicationRoleDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<ApplicationRoleDTO>>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndGetApplicationRoleCollectionByFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetApplicationRoleCollectionByFilterInPage(startIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> CreateApplicationRoleAsync(ApplicationRoleDTO applicationRoleDto, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndCreateApplicationRole(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginCreateApplicationRole(applicationRoleDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> ResetPassword(PasswordResetModel passwordResetModel, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndResetPassword(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginResetPassword(passwordResetModel, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> ResetPasswordLinkAsync(string userName, string callBackUrl, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IAspnetIdentityManagerService service = GetService<IAspnetIdentityManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspnetIdentityManagerService)result.AsyncState).EndResetPasswordLink(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginResetPasswordLink(userName, callBackUrl, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<string>> GetRolesAsync(ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<string>>();

            IAspNetRoleManagerService service = GetService<IAspNetRoleManagerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IAspNetRoleManagerService)result.AsyncState).EndGetRoles(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null); else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetRoles(asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region CustomerDTO

        public Task<Tuple<CustomerDTO, List<string>>> AddNewCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<Tuple<CustomerDTO, List<string>>> tcs = new TaskCompletionSource<Tuple<CustomerDTO, List<string>>>();

            ICustomerService service = GetService<ICustomerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    Tuple<CustomerDTO, List<string>> response = ((ICustomerService)result.AsyncState).EndAddNewCustomer(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewCustomer(customerDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateCustomerAsync(CustomerDTO customerDto, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            ICustomerService service = GetService<ICustomerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ICustomerService)result.AsyncState).EndUpdateCustomer(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(false);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateCustomer(customerDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<CustomerDTO>> FindCustomersAsync(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<CustomerDTO>> tcs = new TaskCompletionSource<List<CustomerDTO>>();

            ICustomerService service = GetService<ICustomerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ICustomerService)result.AsyncState).EndFindCustomers(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindCustomers(asyncCallback, service);

            return tcs.Task;
        }

        public Task<CustomerDTO> FindCustomerAsync(Guid id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<CustomerDTO> tcs = new TaskCompletionSource<CustomerDTO>();

            ICustomerService service = GetService<ICustomerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ICustomerService)result.AsyncState).EndFindCustomerById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindCustomerById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAsync(string searchString, int startIndex,
              int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<CustomerDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<CustomerDTO>>();

            ICustomerService service = GetService<ICustomerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ICustomerService)result.AsyncState).EndFindCustomersByFullText(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindCustomersByFullText(searchString, startIndex, pageSize, sortedColumns, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<CustomerDTO>> FindCustomersByFullTextAndIsEnabledAsync(string searchString, bool isEnabled, int startIndex,
              int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<CustomerDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<CustomerDTO>>();

            ICustomerService service = GetService<ICustomerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ICustomerService)result.AsyncState).EndFindCustomersByFullTextAndIsEnabled(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindCustomersByFullTextAndIsEnabled(searchString, isEnabled, startIndex, pageSize, sortedColumns, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<CustomerDTO>> FindCustomersByNameAsync(string name, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<CustomerDTO>> tcs = new TaskCompletionSource<List<CustomerDTO>>();

            ICustomerService service = GetService<ICustomerService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ICustomerService)result.AsyncState).EndFindCustomersByName(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindCustomersByName(name, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region ModuleNavigationItem

        public Task<List<ModuleNavigationItemDTO>> FindModuleNavigationItemsAsync(ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<ModuleNavigationItemDTO>>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndFindModuleNavigationItems(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationItems(asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> AddModuleNavigationItemAsync(List<ModuleNavigationItemDTO> moduleNavigationItem, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndAddModuleNavigationItem(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddModuleNavigationItem(moduleNavigationItem,
                asyncCallback, service);

            return tcs.Task;
        }

        public Task<ModuleNavigationItemDTO> FindModuleNavigationItemByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<ModuleNavigationItemDTO>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndFindModuleNavigationItemById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationItemById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<ModuleNavigationItemDTO>> FindModuleNavigationItemsFilterPageCollectionInfoAsync(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<ModuleNavigationItemDTO>>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndFindModuleNavigationItemsFilterPageCollectionInfo(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationItemsFilterPageCollectionInfo(startIndex, pageSize, sortedColumns,
                searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<ModuleNavigationItemDTO>> FindModuleNavigationActionControllerNameAsync(string controllerName, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<ModuleNavigationItemDTO>>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndFindModuleNavigationActionControllerName(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationActionControllerName(controllerName, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> ValidateAccessPermissionAsync(string controllerName, string actionName, string username, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndValidateAccessPermission(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginValidateAccessPermission(controllerName, actionName, username, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> BulkInsertModuleNavigationItemAsync(List<Guid> modulesNavigationIds, string roleId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader, 3.0);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndBulkInsertModuleNavigationItem(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginBulkInsertModuleNavigationItem(modulesNavigationIds, roleId, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region ModuleNavigationItemRole

        public Task<bool> AddModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndAddModuleNavigationItemInRole(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddModuleNavigationItemInRole(moduleNavigationItemInRole, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRoleAsync(string roleId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<ModuleNavigationItemInRoleDTO>>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndFindModuleNavigationItemByRole(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationItemByRole(roleId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemByRolePageCollectionInfoAsync(string roleId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<ModuleNavigationItemInRoleDTO>>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndFindModuleNavigationItemByRolePageCollectionInfo(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationItemByRolePageCollectionInfo(roleId, startIndex, pageSize, sortedColumns,
                searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<ModuleNavigationItemInRoleDTO>> FindModuleNavigationItemInRoleByModuleNavigationIdAsync(Guid moduleNavigationId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<ModuleNavigationItemInRoleDTO>>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndFindModuleNavigationItemInRoleByModuleNavigationId(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationItemInRoleByModuleNavigationId(moduleNavigationId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> RemoveModuleNavigationItemInRoleAsync(List<ModuleNavigationItemInRoleDTO> moduleNavigationItemInRole, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IModuleNavigationItemService service = GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndRemoveModuleNavigationItemInRole(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginRemoveModuleNavigationItemInRole(moduleNavigationItemInRole, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<RolePermissionDTO>> CacheRoleAccessPermissionsAsync(string[] currentUserRole, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<RolePermissionDTO>>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response =
                        ((IModuleNavigationItemService)result.AsyncState).EndCacheRoleAccessPermissions(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginCacheRoleAccessPermissions(currentUserRole, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> BulkInsertModuleNavigationItemInRolesAsync(List<Guid> moduleNavigationItemInRole, string roleId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndBulkInsertModuleNavigationItemInRoles(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginBulkInsertModuleNavigationItemInRoles(moduleNavigationItemInRole, roleId, asyncCallback,
                service);

            return tcs.Task;
        }

        public Task<ModuleNavigationItemInRoleDTO> FindModuleNavigationItemInRoleByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<ModuleNavigationItemInRoleDTO>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndFindModuleNavigationItemInRoleById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindModuleNavigationItemInRoleById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateModuleNavigationItemInRoleListAsync(ModuleNavigationItemInRoleDTO moduleNavigationItemInRole, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IModuleNavigationItemService service =
                GetService<IModuleNavigationItemService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IModuleNavigationItemService)result.AsyncState)
                        .EndUpdateModuleNavigationItemInRoleList(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateModuleNavigationItemInRoleList(moduleNavigationItemInRole, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region RedisClient

        public Task<bool> SeedSystemAccessRightAsync(string role, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IRedisClientService service = GetService<IRedisClientService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IRedisClientService)result.AsyncState).EndSeedSystemAccessRight(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginSeedSystemAccessRight(role, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> SeedSystemAccessRightsAsync(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IRedisClientService service = GetService<IRedisClientService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IRedisClientService)result.AsyncState).EndSeedSystemAccessRights(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginSeedSystemAccessRights(asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> ValidatePermissionAsync(string username, string controllerName, string actionName, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IRedisClientService service = GetService<IRedisClientService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IRedisClientService)result.AsyncState).EndValidatePermission(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginValidatePermission(username, controllerName, actionName, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> RemoveAllRightsAsync(string[] roles, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IRedisClientService service = GetService<IRedisClientService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IRedisClientService)result.AsyncState).EndRemoveAllRights(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginRemoveAllRights(roles, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region EmployeeDTO

        public Task<Tuple<EmployeeDTO, List<string>>> AddNewEmployeeAsync(EmployeeDTO employeeDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<Tuple<EmployeeDTO, List<string>>> tcs = new TaskCompletionSource<Tuple<EmployeeDTO, List<string>>>();

            IEmployeeService service = GetService<IEmployeeService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    Tuple<EmployeeDTO, List<string>> response = ((IEmployeeService)result.AsyncState).EndAddNewEmployee(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex,
                       (msgcb) => { tcs.SetResult(new Tuple<EmployeeDTO, List<string>>(null, new List<string> { msgcb})); },
                       true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewEmployee(employeeDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateEmployeeAsync(EmployeeDTO employeeDto, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IEmployeeService service = GetService<IEmployeeService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmployeeService)result.AsyncState).EndUpdateEmployee(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(false);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateEmployee(employeeDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<EmployeeDTO> FindEmployeeAsync(Guid id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<EmployeeDTO> tcs = new TaskCompletionSource<EmployeeDTO>();

            IEmployeeService service = GetService<IEmployeeService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmployeeService)result.AsyncState).EndFindEmployeeById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindEmployeeById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<EmployeeDTO>> FindEmployeeByCustomerIdAsync(Guid? customerId, int startIndex,
              int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<EmployeeDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<EmployeeDTO>>();

            IEmployeeService service = GetService<IEmployeeService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmployeeService)result.AsyncState).EndFindEmployeeByCustomerId(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindEmployeeByCustomerId(customerId, startIndex, pageSize, sortedColumns, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<EmployeeDTO>> FindCustomerEmployeesFilterInPageAsync(string searchString, int startIndex,
              int pageSize, IList<string> sortedColumns, bool sortAscending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmployeeDTO>>();

            IEmployeeService service = GetService<IEmployeeService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<EmployeeDTO> response =
                        ((IEmployeeService)result.AsyncState).EndFindCustomerEmployeesFilterInPage(result);

                    tcs.TrySetResult(response);

                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindCustomerEmployeesFilterInPage(searchString, startIndex, pageSize, sortedColumns, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<EmployeeDTO> FindEmployeeByUserIdAsync(Guid userId, Guid? customerId, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<EmployeeDTO> tcs = new TaskCompletionSource<EmployeeDTO>();

            IEmployeeService service = GetService<IEmployeeService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmployeeService)result.AsyncState).EndFindEmployeeByUserId(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindEmployeeByUserId(userId,customerId, asyncCallback, service);

            return tcs.Task;
        }
        #endregion

        #region Utility
        public Task<bool> ConfigureApplicationDatabaseAsync(ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IUtilityService)result.AsyncState).EndConfigureApplicationDatabase(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginConfigureApplicationDatabase(asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> ConfigureAspNetIdentityDatabaseAsync(ServiceHeader serviceHeader, double timeoutMinutes = 10)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IUtilityService)result.AsyncState).EndConfigureAspNetIdentityDatabase(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginConfigureAspNetIdentityDatabase(asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<Bank>> GetBanksListAsync(ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<Bank>>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<Bank> response = ((IUtilityService)result.AsyncState).EndGetBanksList(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetBanksList(asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<Branch>> GetBranchesListAsync(string bankCode, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<Branch>>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<Branch> response = ((IUtilityService)result.AsyncState).EndGetBranchesList(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetBranchesList(bankCode, asyncCallback, service);

            return tcs.Task;
        }

        public Task<Bank> GetBankByBankCode(string bankCode,  ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<Bank>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    Bank response = ((IUtilityService)result.AsyncState).EndGetBankByBankCode(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetBankByBankCode(bankCode, asyncCallback, service);

            return tcs.Task;
        }

        public Task<Branch> GetBranchByBranchCodeAsync(string bankCode, string branchCode, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<Branch>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    Branch response = ((IUtilityService)result.AsyncState).EndGetBrancheByBranchCode(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetBrancheByBranchCode(bankCode, branchCode, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<Branch>> GetBranchesListFilterInPageAsync(string bankCode, string text,
            int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<Branch>>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<Branch> response = ((IUtilityService)result.AsyncState).EndGetBranchesListFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetBranchesListFilterInPage(bankCode, text, pageIndex, pageSize, sortFields, ascending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<Bank>> GetBanksListFilterInPageAsync(string text,
            int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<Bank>>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<Bank> response = ((IUtilityService)result.AsyncState).EndGetBanksListFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetBanksListFilterInPage(text, pageIndex, pageSize, sortFields, ascending, asyncCallback, service);

            return tcs.Task;
        }

        #region Dashboard

        public Task<DashboardWidgetDTO> FindDashboardWidgetsAsync(int claim, Guid? customerId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<DashboardWidgetDTO>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    DashboardWidgetDTO response = ((IUtilityService)result.AsyncState)
                        .EndFindDashboardWidgets(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindDashboardWidgets(claim, customerId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<DashboardGraphDTO>> GetDashboardGraphDataSetAsync(int claim, Guid? customerId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<DashboardGraphDTO>>();

            IUtilityService service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<DashboardGraphDTO> response = ((IUtilityService)result.AsyncState)
                        .EndGetDashboardGraphDataSet(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGetDashboardGraphDataSet(claim, customerId, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #endregion

        #region PayRollDTO

        public Task<PayRollDTO> AddNewPayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PayRollDTO> tcs = new TaskCompletionSource<PayRollDTO>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndAddNewPayRoll(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewPayRoll(payRollDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdatePayRollAsync(PayRollDTO payRollDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndUpdatePayRoll(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdatePayRoll(payRollDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PayRollDTO> FindPayRollByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PayRollDTO> tcs = new TaskCompletionSource<PayRollDTO>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndFindPayRollById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindPayRollById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<PayRollDTO>> FindPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<PayRollDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<PayRollDTO>>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndFindPayRollFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindPayRollFilterInPage(pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<PayRollDTO>> FindPayRolls(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<PayRollDTO>> tcs = new TaskCompletionSource<List<PayRollDTO>>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndFindPayRolls(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindPayRolls(asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemDTOFilterInPageAsync(Guid payRollId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<PayRollItemDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<PayRollItemDTO>>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndFindPayRollItemsFilterInPageAsync(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindPayRollItemsFilterInPageAsync(payRollId, pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PayRollDTO> ProcessPayRoll(Guid payRollId, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PayRollDTO> tcs = new TaskCompletionSource<PayRollDTO>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndProcessPayRoll(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginProcessPayRoll(payRollId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<PayRollDTO>> FindUnProcessedPayRollFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<PayRollDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<PayRollDTO>>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndFindUnProcessedPayRollFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindUnProcessedPayRollFilterInPage(pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<PayRollItemDTO>> FindPayRollItemByEmployeeIdDTOFilterInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<PayRollItemDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<PayRollItemDTO>>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndFindPayRollItemByEmployeeIdDTOFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindPayRollItemByEmployeeIdDTOFilterInPage(employeeId, pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<PayRollDTO>> FindPayRollByCustomerIdFilterInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<PayRollDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<PayRollDTO>>();

            IPayRollService service = GetService<IPayRollService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IPayRollService)result.AsyncState).EndFindPayRollByCustomerIdFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindPayRollByCustomerIdFilterInPage(customerId,pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region LeaveDTO

        public Task<Tuple<LeaveDTO, string>> AddNewLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<Tuple<LeaveDTO, string>> tcs = new TaskCompletionSource<Tuple<LeaveDTO, string>>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndAddNewLeave(result);

                    tcs.TrySetResult(new Tuple<LeaveDTO, string>(response, null));
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(new Tuple<LeaveDTO, string>(null, ex.Message)); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewLeave(leaveDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateLeaveAsync(LeaveDTO leaveDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndUpdateLeave(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateLeave(leaveDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<LeaveDTO> FindLeaveByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<LeaveDTO> tcs = new TaskCompletionSource<LeaveDTO>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndFindLeaveById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindLeaveById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<LeaveDTO> FindLeaveByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<LeaveDTO> tcs = new TaskCompletionSource<LeaveDTO>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndFindLeaveByEmployeeId(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindLeaveByEmployeeId(employeeId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<LeaveDTO>> FindLeavesByEmployeeIdAsync(Guid employeeId, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<LeaveDTO>> tcs = new TaskCompletionSource<List<LeaveDTO>>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndFindLeavesByEmployeeId(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindLeavesByEmployeeId(employeeId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<LeaveDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<LeaveDTO>>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndFindLeaveFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindLeaveFilterInPage(pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByEmployeeInPageAsync(Guid employeeId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<LeaveDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<LeaveDTO>>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndFindLeaveFilterByEmployeeInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindLeaveFilterByEmployeeInPage(employeeId,pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<LeaveDTO>> FindLeaveFilterByCustomerInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<LeaveDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<LeaveDTO>>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndFindLeaveFilterByCustomerInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindLeaveFilterByCustomerInPage(customerId, pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }
        public Task<List<LeaveDTO>> FindLeaves(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<LeaveDTO>> tcs = new TaskCompletionSource<List<LeaveDTO>>();

            ILeaveService service = GetService<ILeaveService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ILeaveService)result.AsyncState).EndFindLeaves(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindLeaves(asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region StaticSettingDTO

        public Task<StaticSettingDTO> AddNewSettingAsync(StaticSettingDTO settingDTO, ServiceHeader serviceHeader,
            double timeOutMinutes = 1.5)
        {
            TaskCompletionSource<StaticSettingDTO> tcs = new TaskCompletionSource<StaticSettingDTO>();

            IStaticSettingService service = GetService<IStaticSettingService>(serviceHeader, timeOutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    StaticSettingDTO response = ((IStaticSettingService)result.AsyncState).EndAddNewSetting(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewSetting(settingDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateSettingAsync(StaticSettingDTO settingDTO, ServiceHeader serviceHeader,
            double timeOutMinutes = 1.5)
        {
            var tcs = new TaskCompletionSource<bool>();

            IStaticSettingService service = GetService<IStaticSettingService>(serviceHeader, timeOutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IStaticSettingService)result.AsyncState).EndUpdateSetting(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateSetting(settingDTO, asyncCallback, service);

            return tcs.Task;

        }

        public Task<PageCollectionInfo<StaticSettingDTO>> FindSettingFilterInPageAsync(int startIndex, int pageSize,
            IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader,
            double timeOutMinutes = 1.5)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<StaticSettingDTO>>();

            IStaticSettingService service = GetService<IStaticSettingService>(serviceHeader, timeOutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<StaticSettingDTO> response =
                        ((IStaticSettingService)result.AsyncState).EndFindSettingFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindSettingFilterInPage(startIndex, pageSize, sortedColumns, searchString, sortAscending,
                asyncCallback, service);

            return tcs.Task;
        }

        public Task<StaticSettingDTO> FindSettingByKeyAsync(string keyName, ServiceHeader serviceHeader,
            double timeOutMinutes = 1.5)
        {
            var tcs = new TaskCompletionSource<StaticSettingDTO>();

            IStaticSettingService service = GetService<IStaticSettingService>(serviceHeader, timeOutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    StaticSettingDTO response = ((IStaticSettingService)result.AsyncState).EndFindSettingByKey(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindSettingByKey(keyName, asyncCallback, service);

            return tcs.Task;
        }

        public Task<StaticSettingDTO> FindSettingByIdAsync(Guid id, ServiceHeader serviceHeader,
            double timeOutMinutes = 1.5)
        {
            TaskCompletionSource<StaticSettingDTO> tcs = new TaskCompletionSource<StaticSettingDTO>();

            IStaticSettingService service = GetService<IStaticSettingService>(serviceHeader, timeOutMinutes);

            AsyncCallback callback = result =>
            {
                try
                {
                    var myresult = ((IStaticSettingService)result.AsyncState).EndFindSettingById(result);

                    tcs.TrySetResult(myresult);
                }
                catch (Exception ex)
                {

                    HandleFault(ex, (msgcb) => { tcs.SetResult(null); });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            };

            service.BeginFindSettingById(id, callback, service);

            return tcs.Task;
        }

        public Task<bool> RemoveStaticSettingAsync(Guid id, ServiceHeader serviceHeader, double timeOutMinutes = 1.5)
        {
            var tcs = new TaskCompletionSource<bool>();

            IStaticSettingService service = GetService<IStaticSettingService>(serviceHeader, timeOutMinutes);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((IStaticSettingService)result.AsyncState).EndRemoveStaticSetting(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginRemoveStaticSetting(id, asyncCallback, service);

            return tcs.Task;
        }

        #endregion


        #region TAX

        public Task<TaxOrderDTO> AddNewTaxOrderAsync(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<TaxOrderDTO> tcs = new TaskCompletionSource<TaxOrderDTO>();

            ITaxSettingService service = GetService<ITaxSettingService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    TaxOrderDTO response = ((ITaxSettingService)result.AsyncState).EndAddNewTaxOrder(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(null);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewTaxOrder(taxOrderDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateTaxOrderAsync(TaxOrderDTO taxOrderDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            ITaxSettingService service = GetService<ITaxSettingService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ITaxSettingService)result.AsyncState).EndUpdateTaxOrder(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb))
                        {
                            tcs.TrySetResult(false);
                        }
                        else
                        {
                            tcs.TrySetException(ex);
                        }
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateTaxOrder(taxOrderDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<TaxOrderDTO> FindTaxSettingByIdAsync(Guid id, ServiceHeader serviceHeader,
           double timeOutMinutes = 1.5)
        {
            TaskCompletionSource<TaxOrderDTO> tcs = new TaskCompletionSource<TaxOrderDTO>();

            ITaxSettingService service = GetService<ITaxSettingService>(serviceHeader, timeOutMinutes);

            AsyncCallback callback = result =>
            {
                try
                {
                    var myresult = ((ITaxSettingService)result.AsyncState).EndFindTaxSettingById(result);

                    tcs.TrySetResult(myresult);
                }
                catch (Exception ex)
                {

                    HandleFault(ex, (msgcb) => { tcs.SetResult(null); });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            };

            service.BeginFindTaxSettingById(id, callback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<TaxOrderDTO>> FindTaxOrderFilterInPageAsync(int startIndex,
              int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<TaxOrderDTO>>();

            ITaxSettingService service = GetService<ITaxSettingService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<TaxOrderDTO> response =
                        ((ITaxSettingService)result.AsyncState).EndFindTaxOrderFilterInPage(result);

                    tcs.TrySetResult(response);

                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTaxOrderFilterInPage(startIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<TaxRangesDTO>> FindGraduatedScalesAsync(Guid taxOrderId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<TaxRangesDTO>>();

            ITaxSettingService service = GetService<ITaxSettingService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((ITaxSettingService)result.AsyncState).EndFindGraduatedScales(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindGraduatedScales(taxOrderId, asyncCallback, service);

            return tcs.Task;
        }

        #endregion 

        #region DeductionAppService

        public Task<NssfOrderDTO> AddNewNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<NssfOrderDTO> tcs = new TaskCompletionSource<NssfOrderDTO>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndAddNewNssfOrder(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewNssfOrder(nssfOrderDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateNssfOrderAsync(NssfOrderDTO nssfOrderDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndUpdateNssfOrder(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateNssfOrder(nssfOrderDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<NssfOrderDTO> FindNssfOrderByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<NssfOrderDTO> tcs = new TaskCompletionSource<NssfOrderDTO>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindNssfOrderById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindNssfOrderById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<NssfOrderDTO>> FindNssfOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<NssfOrderDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<NssfOrderDTO>>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindNssfOrderFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindNssfOrderFilterInPage(pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<NssfOrderDTO>> FindNssfOrders(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<NssfOrderDTO>> tcs = new TaskCompletionSource<List<NssfOrderDTO>>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindNssfOrders(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindNssfOrders(asyncCallback, service);

            return tcs.Task;
        }

        public Task<NhifOrderDTO> AddNewNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<NhifOrderDTO> tcs = new TaskCompletionSource<NhifOrderDTO>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndAddNewNhifOrder(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewNhifOrder(nhifOrderDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateNhifOrderAsync(NhifOrderDTO nhifOrderDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndUpdateNhifOrder(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateNhifOrder(nhifOrderDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<NhifOrderDTO> FindNhifOrderByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<NhifOrderDTO> tcs = new TaskCompletionSource<NhifOrderDTO>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindNhifOrderById(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindNhifOrderById(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<NhifOrderDTO>> FindNhifOrderFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<NhifOrderDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<NhifOrderDTO>>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindNhifOrderFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindNhifOrderFilterInPage(pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<NhifOrderDTO>> FindNhifOrders(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<NhifOrderDTO>> tcs = new TaskCompletionSource<List<NhifOrderDTO>>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindNhifOrders(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindNhifOrders(asyncCallback, service);

            return tcs.Task;
        }
        #endregion

        #region DeductionDTO

        public Task<DeductionDTO> AddNewDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<DeductionDTO> tcs = new TaskCompletionSource<DeductionDTO>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndAddNewDeductionAsync(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewDeductionAsync(deductionDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateDeductionAsync(DeductionDTO deductionDTO, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndUpdateDeductionAsync(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(false); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateDeductionAsync(deductionDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<DeductionDTO> FindDeductionByIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<DeductionDTO> tcs = new TaskCompletionSource<DeductionDTO>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindDeductionByIdAsync(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindDeductionByIdAsync(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<DeductionDTO>> FindDeductionFilterInPageAsync(int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<DeductionDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<DeductionDTO>>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindDeductionFilterInPageAsync(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindDeductionFilterInPageAsync(pageIndex,pageSize,sortedColumns,searchString,sortAscending,asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<DeductionDTO>> FindDeductionByCustomerIdFilterInPageAsync(Guid customerId, int pageIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            TaskCompletionSource<PageCollectionInfo<DeductionDTO>> tcs = new TaskCompletionSource<PageCollectionInfo<DeductionDTO>>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindDeductionByCustomerIdFilterInPageAsync(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindDeductionByCustomerIdFilterInPageAsync(customerId,pageIndex, pageSize, sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<DeductionDTO>> FindDeduction(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<List<DeductionDTO>> tcs = new TaskCompletionSource<List<DeductionDTO>>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindDeduction(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindDeduction(asyncCallback, service);

            return tcs.Task;
        }

        public Task<DeductionDTO> FindActiveDeductionAsync(ServiceHeader serviceHeader)
        {
            TaskCompletionSource<DeductionDTO> tcs = new TaskCompletionSource<DeductionDTO>();

            IDeductionService service = GetService<IDeductionService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IDeductionService)result.AsyncState).EndFindActiveDeductionAsync(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); }, true);
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindActiveDeductionAsync(asyncCallback, service);

            return tcs.Task;
        }
        #endregion

        #region EmailAlertDTO
        public Task<EmailAlertDTO> AddEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<EmailAlertDTO>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmailAlertService)result.AsyncState).EndAddEmailAlert(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null); else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddEmailAlert(emailAlertDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsAsync(Guid? customerId, int pageIndex, int pageSize, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmailAlertDTO>>();

            var service = GetService<IEmailAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlerts(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null); else tcs.TrySetException(ex);
                    });
                }
            });

            service.BeginFindEmailAlerts(customerId, pageIndex, pageSize, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithRecipientOrSubjectOrBodyAsync(Guid? customerId, string searchText, int pageIndex, int pageSize, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmailAlertDTO>>();

            var service = GetService<IEmailAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlertsWithRecipientOrSubjectOrBody(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null); else tcs.TrySetException(ex);
                    });
                }
            });

            service.BeginFindEmailAlertsWithRecipientOrSubjectOrBody(customerId, searchText, pageIndex, pageSize, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<EmailAlertDTO>> FindEmailAlertsWithDLRStatusAsync(Guid? customerId, int dlrStatus, int pageIndex, int pageSize, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmailAlertDTO>>();

            var service = GetService<IEmailAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlertsWithDLRStatus(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null); else tcs.TrySetException(ex);
                    });
                }
            });

            service.BeginFindEmailAlertsWithDLRStatus(customerId, dlrStatus, pageIndex, pageSize, asyncCallback, service);

            return tcs.Task;
        }

        public Task<EmailAlertDTO> FindEmailAlertWithIdAsync(Guid id, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<EmailAlertDTO>();

            var service = GetService<IEmailAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmailAlertService)result.AsyncState).EndFindEmailAlertWithId(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null); else tcs.TrySetException(ex);
                    });
                }
            });

            service.BeginFindEmailAlertWithId(id, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateEmailAlertAsync(EmailAlertDTO emailAlertDTO, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmailAlertService)result.AsyncState).EndUpdateEmailAlert(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false); else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateEmailAlert(emailAlertDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> GenerateEmailAsync(string templatePath, IDictionary<string, object> expandoObject, string subject, string messageTo, bool mailMessageIsBodyHtml, bool mailMessageSecurityCritical, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            IEmailAlertService service = GetService<IEmailAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IEmailAlertService)result.AsyncState).EndGenerateEmail(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (!string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false); else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginGenerateEmail(templatePath, expandoObject, subject, messageTo, mailMessageIsBodyHtml, mailMessageSecurityCritical, asyncCallback, service);

            return tcs.Task;
        }
        #endregion

        #region TextAlertDTO

        public Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByFilterInPageAsync(int dlrStatus, string text,
            int pageIndex, int pageSize, List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<TextAlertDTO>>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<TextAlertDTO> response =
                        ((ITextAlertService)result.AsyncState).EndFindTextAlertsByFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTextAlertsByFilterInPage(dlrStatus, text, pageIndex, pageSize, sortFields, ascending,
                asyncCallback,
                service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByDateRangeAndFilterInPageAsync(int dlrStatus,
            DateTime startDate, DateTime endDate, string text, int pageIndex, int pageSize, List<string> sortFields,
            bool ascending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<TextAlertDTO>>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<TextAlertDTO> response =
                        ((ITextAlertService)result.AsyncState).EndFindTextAlertsByDateRangeAndFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTextAlertsByDateRangeAndFilterInPage(dlrStatus, startDate, endDate, text, pageIndex,
                pageSize,
                sortFields, ascending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsByRecipientMobileNumberFilterInPageAsync(
            string mobileNumber,
            int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending,
            ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<TextAlertDTO>>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<TextAlertDTO> response =
                        ((ITextAlertService)result.AsyncState).EndFindTextAlertsByRecipientMobileNumberFilterInPage(
                            result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTextAlertsByRecipientMobileNumberFilterInPage(mobileNumber, startIndex, pageSize,
                sortedColumns, searchString, sortAscending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsInPageAsync(int pageIndex, int pageSize,
            List<string> sortFields, bool ascending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<TextAlertDTO>>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<TextAlertDTO> response =
                        ((ITextAlertService)result.AsyncState).EndFindTextAlertsInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTextAlertsInPage(pageIndex, pageSize, sortFields, ascending, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> AddTextAlertsAsync(ObservableCollection<TextAlertDTO> textAlertDTO,
            ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((ITextAlertService)result.AsyncState).EndAddTextAlerts(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddTextAlerts(textAlertDTO.ExtendedToList(), asyncCallback, service);

            return tcs.Task;
        }

        public Task<TextAlertDTO> AddTextAlertAsync(TextAlertDTO textAlertDto, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<TextAlertDTO>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);


            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    TextAlertDTO response = ((ITextAlertService)result.AsyncState).EndAddNewText(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginAddNewText(textAlertDto, asyncCallback, service);

            return tcs.Task;
        }

        public Task<bool> UpdateTextAlertAsync(TextAlertDTO textAlertDTO, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<bool>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    bool response = ((ITextAlertService)result.AsyncState).EndUpdateTextAlert(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(false);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginUpdateTextAlert(textAlertDTO, asyncCallback, service);

            return tcs.Task;
        }

        public Task<TextAlertDTO> FindTextAlertAsync(Guid emailAlertId, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<TextAlertDTO>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    TextAlertDTO response = ((ITextAlertService)result.AsyncState).EndFindTextAlert(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTextAlert(emailAlertId, asyncCallback, service);

            return tcs.Task;
        }

        public Task<List<TextAlertDTO>> FindTextAlertsByDLRStatusAsync(int dlrStatus, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<List<TextAlertDTO>>();

            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    List<TextAlertDTO> response =
                        ((ITextAlertService)result.AsyncState).EndFindTextAlertsByDLRStatus(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTextAlertsByDLRStatus(dlrStatus, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<TextAlertDTO>> FindTextAlertsFilterInPageAsync(DateTime startDate, DateTime endDate, Guid? partnerId, int startIndex, int pageSize,
            IList<string> sortedColumns, string searchString, bool sortAscending, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<TextAlertDTO>>();
            ITextAlertService service = GetService<ITextAlertService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    PageCollectionInfo<TextAlertDTO> response =
                        ((ITextAlertService)result.AsyncState).EndFindTextAlertsFilterInPage(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) =>
                    {
                        if (string.IsNullOrWhiteSpace(msgcb)) tcs.TrySetResult(null);
                        else tcs.TrySetException(ex);
                    });
                }
                finally
                {
                    DisposeService(service as IClientChannel);
                }
            });

            service.BeginFindTextAlertsFilterInPage(startDate, endDate, partnerId, startIndex, pageSize, sortedColumns, searchString, sortAscending,
                asyncCallback, service);

            return tcs.Task;
        }

        #endregion

        #region reports

        public Task<PageCollectionInfo<EmployeeDTO>> GetEmployeesReportAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<EmployeeDTO>>();

            var service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IUtilityService)result.AsyncState).EndGetEmployeesReport(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); });
                }
            });

            service.BeginGetEmployeesReport(claim, customerId, startDate, endDate, pageIndex, pageSize, text, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<PaySlipDTO>> GetPaySlipAsync(Guid employeeId, int period, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<PaySlipDTO>>();

            var service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IUtilityService)result.AsyncState).EndGetPayRoll(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); });
                }
            });

            service.BeginGetPaySlip(employeeId, period, asyncCallback, service);

            return tcs.Task;
        }

        public Task<PageCollectionInfo<PaySlipDTO>> GetPayRollAsync(int claim, Guid customerId, DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string text, ServiceHeader serviceHeader)
        {
            var tcs = new TaskCompletionSource<PageCollectionInfo<PaySlipDTO>>();

            var service = GetService<IUtilityService>(serviceHeader);

            AsyncCallback asyncCallback = (result =>
            {
                try
                {
                    var response = ((IUtilityService)result.AsyncState).EndGetPayRoll(result);

                    tcs.TrySetResult(response);
                }
                catch (Exception ex)
                {
                    HandleFault(ex, (msgcb) => { tcs.TrySetResult(null); });
                }
            });

            service.BeginGetPayRoll(claim, customerId, startDate, endDate, pageIndex, pageSize, text, asyncCallback, service);

            return tcs.Task;
        }


        #endregion


        public void SetupServiceProxy<TProxy, TService>(Action<IDisposableWrapper<TProxy>> callback, double timeoutMinutes = 10)
            where TProxy : ClientBase<TService>, new()
            where TService : class
        {
            IDisposableWrapper<TProxy> disposableProxy =
               new TProxy()
               .Wrap<TProxy, TService>(timeoutMinutes);

            callback(disposableProxy);
        }

        public IDisposableWrapper<TProxy> SetupServiceProxy<TProxy, TService>(double timeoutMinutes = 10)
            where TProxy : ClientBase<TService>, new()
            where TService : class
        {
            IDisposableWrapper<TProxy> disposableProxy =
               new TProxy()
               .Wrap<TProxy, TService>(timeoutMinutes);

            return disposableProxy;
        }

        public IDisposableWrapper<TProxy> SetupServiceProxy<TProxy, TService>(IEndpointBehavior behavior, double timeoutMinutes = 10)
            where TProxy : ClientBase<TService>, new()
            where TService : class
        {
            IDisposableWrapper<TProxy> disposableProxy =
                new TProxy()
                .Wrap<TProxy, TService>(behavior, timeoutMinutes);

            return disposableProxy;
        }

        public IDisposableWrapper<TProxy> SetupServiceProxy<TProxy, TService>(string endpointConfigurationName, IEndpointBehavior behavior, double timeoutMinutes = 10)
            where TProxy : ClientBase<TService>, new()
            where TService : class
        {
            IDisposableWrapper<TProxy> disposableProxy =
                ((TProxy)Activator.CreateInstance(typeof(TProxy), endpointConfigurationName))
                .Wrap<TProxy, TService>(behavior, timeoutMinutes);

            return disposableProxy;
        }

        private TChannel GetService<TChannel>(ServiceHeader serviceHeader, double timeoutMinutes = 10d)
        {
            lock (SyncRoot)
            {
                string endpointConfigurationName = $"{typeof(TChannel)}";

                ChannelFactory<TChannel> factory = null;

                if (_channelFactoryHashtable.ContainsKey(endpointConfigurationName))
                {
                    factory = (ChannelFactory<TChannel>)_channelFactoryHashtable[endpointConfigurationName];
                }
                else
                {
                    string configurationName = endpointConfigurationName.Substring(endpointConfigurationName.LastIndexOf('.') + 1);

                    factory = new ChannelFactory<TChannel>(configurationName);

                    _channelFactoryHashtable.Add(endpointConfigurationName, factory);
                }

                if (factory.Endpoint.Behaviors.Any())
                {
                    factory.Endpoint.Behaviors.Clear();
                }

                factory.Endpoint.Behaviors.Add(new CustomBehavior(serviceHeader));

                TChannel channel = factory.CreateChannel();

                ((IContextChannel)channel).OperationTimeout = TimeSpan.FromMinutes(timeoutMinutes);

                return channel;
            }
        }

        private void DisposeService(IClientChannel channel)
        {
            try
            {
                if (channel.State != CommunicationState.Faulted)
                {
                    channel.Close();
                }
            }
            finally
            {
                if (channel.State != CommunicationState.Closed)
                {
                    channel.Abort();
                }
            }
        }

        private ChannelFactory<TChannel> GetChannelFactory<TChannel>()
        {
            lock (SyncRoot)
            {
                string endpointConfigurationName = $"{typeof(TChannel)}";

                ChannelFactory<TChannel> factory = null;

                if (_channelFactoryHashtable.ContainsKey(endpointConfigurationName))
                {
                    factory = (ChannelFactory<TChannel>)_channelFactoryHashtable[endpointConfigurationName];
                }
                else
                {
                    string configurationName = endpointConfigurationName.Substring(endpointConfigurationName.LastIndexOf('.') + 1);

                    factory = new ChannelFactory<TChannel>(configurationName);

                    _channelFactoryHashtable.Add(endpointConfigurationName, factory);
                }

                return factory;
            }
        }

        private void HandleFault(Exception exception, Action<string> callback, bool callerRequiresErrorMessage = false)
        {
            if (exception is FaultException<ApplicationServiceError>)
            {
                FaultException<ApplicationServiceError> fault = exception as FaultException<ApplicationServiceError>;

                _logger.LogError("HandleFault -> {0}", exception, fault.Message);

                callback(callerRequiresErrorMessage ? $"{fault.Detail.ErrorMessage}" : null);
            }
            else if (exception is ProtocolException)
            {
                if ((exception.InnerException as WebException).Response.GetResponseStream() is MemoryStream responseStream)
                {
                    byte[] responseBytes = responseStream.ToArray();

                    string responseString = Encoding.UTF8.GetString(responseBytes);

                    _logger.LogError("HandleFault -> {0}", exception, responseString);

                    callback(callerRequiresErrorMessage ? $"{responseString}" : null);
                }
                else
                {
                    _logger.LogError("HandleFault", exception);

                    callback(callerRequiresErrorMessage ? $"{exception.Message}" : null);
                }
            }
            else
            {
                _logger.LogError("HandleFault", exception);

                callback(callerRequiresErrorMessage ? $"{exception.Message}" : null);
            }
        }

        
    }
}
