﻿using Quartz;
using System;

namespace Presentation.Infrastructure.Services
{
    public interface IPlugin
    {
        Guid Id { get; }

        string Description { get; }

        void DoWork(IScheduler scheduler, params string[] args);

        void Exit();
    }
}
